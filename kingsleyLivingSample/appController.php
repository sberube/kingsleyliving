<?php
/* This is the controller for the site */
$siteHeader = file_get_contents('main/header.htm');
$slideout_left = file_get_contents('main/notifications_slideout.htm');
$slideout_right = file_get_contents('main/links_slideout.htm');
$footerLinks = file_get_contents('main/footerLinks.htm');
$latestNews = '';
$footerContact = '';
if (!isset($_GET['location'])) {
	/* Set defaults all here, then just change them as the $_GET is changed... */
	// include 'main/landingPage/landingController.php';
	$stylesheetLinks = '<link rel="stylesheet" type="text/css" href="webroot/css/landing.css">' .
			'<link rel="stylesheet" type="text/css" href="webroot/engine1/style.css" media="screen" />';
	$jsLinks = '<script type="text/javascript" src="webroot/engine1/jquery.js"></script>' .
			'<script src="webroot/js/raphael.js"></script>' .
			'<script src="webroot/js/us-map-1.0.1/jquery.usmap.js"></script>' .
			'<script src="webroot/js/kmc_map.js"></script>';
	$beforeBody_content = file_get_contents('main/landingPage/landing_beforeBody.htm');
	$content = file_get_contents('main/landingPage/landing_content.htm');
} else {
	// Do stuff here to get the right files in
}
?>