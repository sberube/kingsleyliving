<?php
/* This is the controller for the landing page */
$defaultSlideShowImages = '<li><img src="../../webroot/images/banner1.jpg" alt="Come Live With Us!" title="Come Live With Us!" id="wows1_0"/>Click the map to see where a community is near you.</li>
	<li><img src="../../webroot/images/banner2.jpg" alt="Small Private Communities!" title="Small Private Communities!" id="wows1_1"/>We\'re all one big family here, and we\'d love to have you.</li>
	<li><img src="../../webroot/images/banner3.jpg" alt="From Coast to Coast!" title="From Coast to Coast!" id="wows1_2"/>We\'ve got communities in more than 9 states</li>
	<li><img src="../../webroot/images/banner4.jpg" alt="Great Management!" title="Great Management!" id="wows1_3"/>At Kingsley, we pride ourselves in proper management of parks</li>
	<li><img src="../../webroot/images/banner5.jpg" alt="Clean Properties!" title="Clean Properties!" id="wows1_4"/>Our properties are well kept and cared for, stop by any time to see what we have to offer you.</li>
	<li><img src="../../webroot/images/banner6.jpg" alt="All Seasons All Around the Country!" title="All Seasons All Around the Country" id="wows1_5"/>We have communities all over the country, so if you are looking for just a summer home, we have that too.</li>
	<li><img src="../../webroot/images/banner7.jpg" alt="Garages, Car Covers, And More!" title="Garages, Car Covers, And More!" id="wows1_6"/>Most of our homes have a canopy or garage to park your car in or under, so your car stays cleaner too.</li>
	<li><img src="../../webroot/images/banner8.jpg" alt="Amenities And So Much More!" title="Amenities And So Much More!" id="wows1_7"/>From pools to recreational houses, our communities have much to offer</li>';

$defaultSlideShowBullets = '<a href="#" title="Come Live With Us!">1</a>
	<a href="#" title="Small Private Communities!">2</a>
	<a href="#" title="From Coast to Coast!">3</a>
	<a href="#" title="Great Management!">4</a>
	<a href="#" title="Clean Properties!">5</a>
	<a href="#" title="All Seasons All Around the Country">6</a>
	<a href="#" title="Garages, Car Covers, And More!">7</a>
	<a href="#" title="Amenities And So Much More!">8</a>';

if (!isset($slideShowImages)) {
	// $slideShowImages = file_get_contents('default_slides.htm');
	$slideShowImages = $defaultSlideShowImages;
}

if (!isset($slideShowBullets)) {
	// $slideShowBullets = file_get_contents('default_bullets_slides.htm');
	$slideShowBullets = $defaultSlideShowBullets;
}
?>