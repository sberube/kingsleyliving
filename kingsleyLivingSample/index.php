<?php
include 'appController.php';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Kingsleyliving.com Landing Page</title>
		<link rel="stylesheet" type="text/css" href="webroot/css/main.css">
		<link rel="stylesheet" type="text/css" href="webroot/css/tooltips.css">
		<link rel="stylesheet" type="text/css" href="webroot/css/slideout.css">
		<?php echo $stylesheetLinks; ?>
		<script src="js/jquery-2.1.1.js"></script>
		<?php echo $jsLinks; ?>
		</style>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<?php echo $siteHeader; ?>
			</div>
			<div class="slideout-left">
				<?php echo $slideout_left; ?>
			</div>
			<div class="slideout-right">
				<?php echo $slideout_right; ?>
			</div>
			<div class="body-content">
				<?php echo $beforeBody_content; ?>
				<div class="main-page-content">
					<?php echo $content; ?>
				</div>
			</div>
			<div class="footer">
				<div class="footer-main-section">
					<div class="footer-links">
						<div>Explore</div>
						<?php echo $footerLinks; ?>
					</div>
					<div class="footer-news">
						<div>Latest News</div>
						<?php echo $latestNews; ?>
					</div>
					<div class="footer-contact">
						<div>Contact Kingsley Management Corporation</div>
						<?php echo $footerContact; ?>
					</div>
				</div>
				<div class="footer-sliver">
					<div><a href="index.php?location=terms_and_conditions">Terms and Conditions</a></div>
					<div><a href="index.php?location=privacy_policy">Privacy Policy</a></div>
					<div><a href="index.php?location=sitemap">Sitemap</a></div>
				</div>
			</div>
		</div>
	</body>
</html>