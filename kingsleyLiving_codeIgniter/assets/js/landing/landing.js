$(window).resize(function() {
	var windowWidth = $(window).width();
	if (windowWidth >= 1200){
		$('div.kingsley_map').removeClass('kingsley_map-md').addClass('kingsley_map-lg');
	} else if (windowWidth >= 992 && windowWidth < 1200){
		$('div.kingsley_map').removeClass('kingsley_map-lg');
		$('div.kingsley_map').removeClass('kingsley_map-sm');
		$('div.kingsley_map').addClass('kingsley_map-md');
	} else if (windowWidth >= 768 && windowWidth < 992){
		$('div.kingsley_map').removeClass('kingsley_map-md');
		$('div.kingsley_map').removeClass('kingsley_map-xs');
		$('div.kingsley_map').addClass('kingsley_map-sm');
	} else {
		$('div.kingsley_map').removeClass('kingsley_map-sm').addClass('kingsley_map-xs');
	}
});
$(document).ready(function() {
	$('.map-legend').hide();
	$('.map-reset').hide();
	var json_communities = $('.json_communities').text();
	var communities = $.parseJSON(json_communities);
	var windowWidth = $(window).width();
	if (windowWidth >= 1200){
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-lg');
	} else if (windowWidth >= 992 && windowWidth < 1200){
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-md');
	} else if (windowWidth >= 768 && windowWidth < 992){
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-sm');
	} else {
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-xs');
	}
	var highlighted_states = {
		az : '#A366E0',
		ca : '#A366E0',
		co : '#A366E0',
		id : '#A366E0',
		mi : '#A366E0',
		nv : '#A366E0',
		nm : '#A366E0',
		ny : '#A366E0',
		tx : '#A366E0',
		ut : '#A366E0'
	};

	$('.map-reset').on('click', function() { map_reset(); });
	var operationStates = ['az', 'ca', 'co', 'id', 'mi', 'nv', 'nm', 'ny', 'tx', 'ut'];
	var class_map = {
		AZ : 'arizona',
		CA : 'california',
		CO : 'colorado',
		ID : 'idaho',
		MI : 'michigan',
		NV : 'nevada',
		NM : 'new_mexico',
		NY : 'new_york',
		TX : 'texas',
		UT : 'utah'
	};
	$('.kingsley_map').vectorMap({
		map: 'usa_en',
		backgroundColor: null,
		color: '#531690',
		colors: highlighted_states,
		hoverColor: '#F3B6FF', // '#F38EFF', // '#DFA2FF',// '#CB8EFF',
		selectedColor: '#FFFF00',
		enableZoom: false,
		showTooltip: false,
		normalizeFunction: 'linear',
		onLabelShow: function(element, label, code)
		{
			if (operationStates.indexOf(code) <= -1)
			{
				element.preventDefault();
			}
		},
		onRegionOver: function (element, code, region)
		{
			if (operationStates.indexOf(code) <= -1)
			{
				element.preventDefault();
			}
		},
		onRegionClick: function(element, code, region)
		{
			if (operationStates.indexOf(code) <= -1)
			{
				element.preventDefault();
			} else {
				// Up here we'll take the json string and process it to get the communities within the state that was clicked
				$('.map-header').fadeOut();
				$('.map-footer').fadeOut();
				$('.open-search').fadeOut();
				$('.map-legend').fadeOut(200);
				// $('.map-reset').fadeIn();
				var ISO_code = code.toUpperCase();
				if ($('.json_last_selected').text().length > 0) {
					var last_selected = $('.json_last_selected').text();
					$('.map-legend').removeClass(class_map[last_selected]);
				}
				$('.json_last_selected').html(ISO_code);
				setTimeout(function() {
					build_us_map_legend(communities, region);
					$('.map-legend').addClass(class_map[ISO_code]).fadeIn();
				}, 200);
			}
		}
	});
});

function map_reset()
{
	$('.map-legend').fadeOut();
	$('.map-header').fadeIn();
	$('.map-footer').fadeIn();
	$('.open-search').fadeIn();
	// $('.map-reset').fadeOut();
	var selected_state = $('.json_last_selected').html().toLowerCase();
	$('.kingsley_map').vectorMap('deselect', selected_state);
}

function build_us_map_legend(communities, region)
{
	var default_title = $('.legend-title-default').html();
	var close = '<div class="legend-close" onclick="map_reset()"><span class="icon ion-close-round"></span></div>';// '<button type="button" class="close" data-dismiss="map-legend"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';// '<a class="map-close"><span class="icon ion-close-round"></span></a>';
	var community_href = CI_ROOT + '/community/';
	$('.legend-labels').empty();
	$.each(communities[region], function(key, community) {
		var list_item = '<li><a href="' + (community_href + community['id']) + '">' + community['name'] + '</a></li>';
		var current_contents = $('.legend-labels').html();
		$('.legend-labels').html(current_contents + list_item);
	});
	$('.legend-title').html(region + ' ' + default_title + ' ' + close);
	return;
}