$(window).resize(function() {
	var windowWidth = $(window).width();
	if (windowWidth >= 1247){
		$('div.main-body').removeClass('main-body-md').addClass('main-body-lg');
		$('div.site-errors').removeClass('site-errors-md').addClass('site-errors-lg');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-md').addClass('slideout-left-lg');
		$('div.slideout_title-left').removeClass('landing-target-left-md').addClass('landing-target-left-lg');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-md').addClass('slideout_inner-left-lg');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-md').addClass('slideout-right-lg');
		$('div.slideout_title-right').removeClass('landing-target-right-md').addClass('landing-target-right-lg');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-md').addClass('slideout_inner-right-lg');
	} else if (windowWidth >= 992 && windowWidth < 1247){
		$('div.main-body').removeClass('main-body-lg');
		$('div.site-errors').removeClass('site-errors-lg');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-lg');
		$('div.slideout_title-left').removeClass('landing-target-left-lg');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-lg');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-lg');
		$('div.slideout_title-right').removeClass('landing-target-right-lg');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-lg');

		$('div.main-body').removeClass('main-body-sm');
		$('div.site-errors').removeClass('site-errors-sm');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-sm');
		$('div.slideout_title-left').removeClass('landing-target-left-sm');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-sm');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-sm');
		$('div.slideout_title-right').removeClass('landing-target-right-sm');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-sm');

		$('div.main-body').addClass('main-body-md');
		$('div.site-errors').addClass('site-errors-md');
		// slide out left
		$('div.slideout-left').addClass('slideout-left-md');
		$('div.slideout_title-left').addClass('landing-target-left-md');
		$('div.slideout_inner-left').addClass('slideout_inner-left-md');
		// slide out right
		$('div.slideout-right').addClass('slideout-right-md');
		$('div.slideout_title-right').addClass('landing-target-right-md');
		$('div.slideout_inner-right').addClass('slideout_inner-right-md');
	} else if (windowWidth >= 768 && windowWidth < 992){
		$('div.main-body').removeClass('main-body-md');
		$('div.site-errors').removeClass('site-errors-md');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-md');
		$('div.slideout_title-left').removeClass('landing-target-left-md');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-md');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-md');
		$('div.slideout_title-right').removeClass('landing-target-right-md');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-md');

		$('div.main-body').removeClass('main-body-xs');
		$('div.site-errors').removeClass('site-errors-xs');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-xs');
		$('div.slideout_title-left').removeClass('landing-target-left-xs');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-xs');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-xs');
		$('div.slideout_title-right').removeClass('landing-target-right-xs');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-xs');

		$('div.main-body').addClass('main-body-sm');
		$('div.site-errors').addClass('site-errors-sm');
		// slide out left
		$('div.slideout-left').addClass('slideout-left-sm');
		$('div.slideout_title-left').addClass('landing-target-left-sm');
		$('div.slideout_inner-left').addClass('slideout_inner-left-sm');
		// slide out right
		$('div.slideout-right').addClass('slideout-right-sm');
		$('div.slideout_title-right').addClass('landing-target-right-sm');
		$('div.slideout_inner-right').addClass('slideout_inner-right-sm');
	} else {
		$('div.main-body').removeClass('main-body-sm').addClass('main-body-xs');
		$('div.site-errors').removeClass('site-errors-sm').addClass('site-errors-xs');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-sm').addClass('slideout-left-xs');
		$('div.slideout_title-left').removeClass('landing-target-left-sm').addClass('landing-target-left-xs');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-sm').addClass('slideout_inner-left-xs');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-sm').addClass('slideout-right-xs');
		$('div.slideout_title-right').removeClass('landing-target-right-sm').addClass('landing-target-right-xs');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-sm').addClass('slideout_inner-right-xs');
	}
});
$(document).ready(function() {
	var windowWidth = $(window).width();
	if (windowWidth >= 1247){
		$('div.main-body').removeClass('main-body-default').addClass('main-body-lg');
		$('div.site-errors').removeClass('site-errors-default').addClass('site-errors-lg');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-default').addClass('slideout-left-lg');
		$('div.slideout_title-left').removeClass('landing-target-left-default').addClass('landing-target-left-lg');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-default').addClass('slideout_inner-left-lg');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-default').addClass('slideout-right-lg');
		$('div.slideout_title-right').removeClass('landing-target-right-default').addClass('landing-target-right-lg');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-default').addClass('slideout_inner-right-lg');
	} else if (windowWidth >= 992 && windowWidth < 1247){
		$('div.main-body').removeClass('main-body-default').addClass('main-body-md');
		$('div.site-errors').removeClass('site-errors-default').addClass('site-errors-md');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-default').addClass('slideout-left-md');
		$('div.slideout_title-left').removeClass('landing-target-left-default').addClass('landing-target-left-md');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-default').addClass('slideout_inner-left-md');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-default').addClass('slideout-right-md');
		$('div.slideout_title-right').removeClass('landing-target-right-default').addClass('landing-target-right-md');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-default').addClass('slideout_inner-right-md');
	} else if (windowWidth >= 768 && windowWidth < 992){
		$('div.main-body').removeClass('main-body-default').addClass('main-body-sm');
		$('div.site-errors').removeClass('site-errors-default').addClass('site-errors-sm');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-default').addClass('slideout-left-sm');
		$('div.slideout_title-left').removeClass('landing-target-left-default').addClass('landing-target-left-sm');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-default').addClass('slideout_inner-left-sm');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-default').addClass('slideout-right-sm');
		$('div.slideout_title-right').removeClass('landing-target-right-default').addClass('landing-target-right-sm');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-default').addClass('slideout_inner-right-sm');
	} else {
		$('div.main-body').removeClass('main-body-default').addClass('main-body-xs');
		$('div.site-errors').removeClass('site-errors-default').addClass('site-errors-xs');
		// slide out left
		$('div.slideout-left').removeClass('slideout-left-default').addClass('slideout-left-xs');
		$('div.slideout_title-left').removeClass('landing-target-left-default').addClass('landing-target-left-xs');
		$('div.slideout_inner-left').removeClass('slideout_inner-left-default').addClass('slideout_inner-left-xs');
		// slide out right
		$('div.slideout-right').removeClass('slideout-right-default').addClass('slideout-right-xs');
		$('div.slideout_title-right').removeClass('landing-target-right-default').addClass('landing-target-right-xs');
		$('div.slideout_inner-right').removeClass('slideout_inner-right-default').addClass('slideout_inner-right-xs');
	}

	if ($('div.site-errors').html().trim().length > 0) {
		$('.under-header-errors').removeClass('hidden');
	} else {
		$('.under-header-errors').addClass('hidden');
	}
	// $('.chosen-select').chosen({ width: "100%", disable_search_threshold: 10 });
	$('#phone').change(function() {
		var text = this.value;
		var newContents = text.replace(/(\d)*(\d\d\d)-*(\d\d\d)-*(\d\d\d\d)/, '$1 ($2) $3-$4');
		this.value = newContents;
	});

	var google = 'http://maps.google.com/maps/api/geocode/json?address=';
	var full_address = '';
	$('#address_1, #locality, #administrative_area_level_1').change(function() {
		var closestrow = $(this).closest('div').parent('div.row');
		var bors = $(closestrow).siblings('#fieldset_identifier').val();
		var fieldset = $(closestrow).parent('fieldset');
		var address = '';
		var city = '';
		var state = '';
		if ($(fieldset).find('#address_1').val().length > 0) {
		    address = $(fieldset).find('#address_1').val();
		}
		if ($(fieldset).find('#locality').val().length > 0) {
		    city = $(fieldset).find('#locality').val();
		}
		if ($(fieldset).find('#administrative_area_level_1').val().length > 0) {
		    state = $(fieldset).find('#administrative_area_level_1').val();
		}
		if ( address != "" && city != "" && state != "" )
		{
			full_address = address + ', ' + city + ', ' + state;
			var prepared_address = full_address.replace(' ','+');
			var googleURL = google + prepared_address;
			$.post(googleURL, function(data) {
				if ('partial_match' in data['results'][0])
				{
					address_invalid(bors);
				} else {
					if (data['status'] == "OK") {
						address_valid(bors);
						$.each(data['results'][0]['address_components'], function(id, addr_comp) {
							var long_name = '';
							var short_name = '';
							$.each(addr_comp, function(key, value) {
								if (key == 'long_name')
								{
									long_name = value;
								} else if (key == 'short_name') {
									short_name = value;
								} else {
									if (key == 'types') {
										if (value == 'postal_code') {
											$(fieldset).find('#postal_code').val(long_name);
										}
									}
								}
							});
						});
					} else {
						address_invalid(bors);
					}
				}
				full_address = '';
			});
		}
	});
	// initialize autocomplete function, but pass it the element it is going to use.
});

// function headerformlogin() {
// 	var loginvalues = {
// 		login_identity : $('#login_identity').val(),
// 		login_password : $('#login_password').val()
// 	};
// 	// console.log(loginvalues);
// 	// throw "Loginvalues submitted: " + loginvalues['login_identity'];
// 	$.ajax({
// 		type : "POST",
// 		url : CI_ROOT + "/user/login_via_ajax",
// 		data : {
// 			'login_identity' : loginvalues['login_identity'],
// 			'login_password' : loginvalues['login_password']
// 		},
// 		success : function(data) {
// 			console.log('Log in successful!');
// 			$('.sign-in-register').html(data);
// 		},
// 		error : function(data) {
// 			console.log('Log in error');
// 		}
// 	});
// }
var map;
function map_initialize(myCenter) {
	var map_canvas = document.getElementById('map_canvas');
	var map_options = {
		center: myCenter,
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(map_canvas, map_options);
	var marker = new google.maps.Marker({
		position: myCenter
	});
	marker.setMap(map);

	var request_transit = {
		location: myCenter,
		rankBy: google.maps.places.RankBy.DISTANCE,
		types: ['bus_station']
	};

	var request_restaurant = {
		location: myCenter,
		radius: 800,
		types: ['restaurant']
	};

	var request_grocery = {
		location: myCenter,
		radius: 1000,
		types: ['grocery_or_supermarket']
	};

	var request_school = {
		location: myCenter,
		radius: 1500,
		types: ['school']
	};

	service = new google.maps.places.PlacesService(map);
	service.nearbySearch(request_transit, map_callback);
	// service.nearbySearch(request_restaurant, map_callback);
	// service.nearbySearch(request_grocery, map_callback);
	// service.nearbySearch(request_school, map_callback);
}

function map_callback(results, status) {
	if (status == google.maps.places.PlacesServiceStatus.OK) {
		for (var i = 0; i < results.length; i++) {
			createMarker(results[i]);
		}
	}
}

function createMarker(place) {
	var placeLoc = place.geometry.location;
	// Right here do some logic to change the markers based on type
	// var marker_icon;
	// if ($.inArray('grocery_or_supermarket', place.types) > -1){
	// 	marker_icon = '../images/icons/grocery_icon_50pix.png';
	// } 
	// else if ($.inArray('restaurant', place.types) > -1) {
	// 	marker_icon = '../images/icons/restaurant_icon_50pix.png';
	// } 
	// else if ($.inArray('school', place.types) > -1) {
	// 	marker_icon = '../images/icons/schools_icon_50pix.png';
	// } 
	// else if ($.inArray('bus_station', place.types) > -1) {
	// 	marker_icon = '../images/icons/transit_icon_50pix.png';
	// } 
	// else {
	// 	marker_icon = '';
	// }
	// if (marker_icon == '' || marker_icon == null) {
		var marker = new google.maps.Marker({
			map: map,
			position: placeLoc
		});
	// } else {
	// 	var marker = new google.maps.Marker({
	// 		map: map,
	// 		icon: marker_icon,
	// 		position: placeLoc
	// 	});
	// }

	// google.maps.event.addListener(marker, 'click', function() {
	// 	infowindow.setContent(place.name);
	// 	infowindow.open(map, this);
	// });
}

/******************************************************************************************************************************************************/
/**** The following is explicitly for addresses, these jquery / javascript functions validate and autocomplete addresses using Google's places API ****/
/******************************************************************************************************************************************************/
// bors actually stands for B or S --> billing or shipping
function address_valid(bors) {
	var span = '<span class="icon ion-ios7-checkmark-outline"></span>';
	var bsuccessclass = 'valid_billing_success';
	var ssuccessclass = 'valid_shipping_success';
	var successclass = 'valid_success';
	var berrorclass = 'valid_billing_error';
	var serrorclass = 'valid_shipping_error';
	var errorclass = 'valid_error';
	var successtext = '&nbsp;&nbsp;valid address';
	if (bors == 'billing') {
		$('.google_valid_billing_address').removeClass(berrorclass);
		$('.google_valid_billing_address').html(span + successtext).addClass(bsuccessclass);
	} else if (bors == 'shipping') {
		$('.google_valid_shipping_address').removeClass(serrorclass);
		$('.google_valid_shipping_address').html(span + successtext).addClass(ssuccessclass);
	} else {
		$('.google_valid_address').removeClass(errorclass);
		$('.google_valid_address').html(span + successtext).addClass(successclass);
	}
}

function address_invalid(bors) {
	var span = '<span class="icon ion-alert-circled"></span>';
	var bsuccessclass = 'valid_billing_success';
	var ssuccessclass = 'valid_shipping_success';
	var successclass = 'valid_success';
	var berrorclass = 'valid_billing_error';
	var serrorclass = 'valid_shipping_error';
	var errorclass = 'valid_error';
	var errortext = 'invalid address&nbsp;&nbsp;';
	if (bors == 'billing') {
		$('.google_valid_billing_address').removeClass(bsuccessclass);
		$('.google_valid_billing_address').html(errortext + span).addClass(berrorclass);
	} else if (bors == 'shipping') {
		$('.google_valid_shipping_address').removeClass(ssuccessclass);
		$('.google_valid_shipping_address').html(errortext + span).addClass(serrorclass);
	} else {
		$('.google_valid_address').removeClass(successclass);
		$('.google_valid_address').html(errortext + span).addClass(errorclass);
	}
}
/**********************************************/
/*** 		Below is not working yet 		***/
/**********************************************/
// var placeSearch, autocomplete;
// var componentForm = {
// 	street_number: 'short_name',
// 	route: 'long_name',
// 	locality: 'long_name',
// 	administrative_area_level_1: 'short_name',
// 	// country: 'long_name',
// 	postal_code: 'short_name'
// };

// function autocomplete_initialize(parent_element) {
// 	console.log('Hit autocomplete function');
// 	console.log(parent_element);
// 	autocomplete = new google.maps.places.Autocomplete(
// 		($(parent_element).find('.autocomplete')), // class of element
// 		{ types: ['geocode'] }
// 	);
// 	console.log('autocomplete variable has been filled');
// 	console.log(autocomplete);
// 	google.maps.event.addListener(autocomplete, 'place_changed', function() {
// 		fillInAddress(parent_element);
// 	});
// 	console.log('listener is working');
// }

// function fillInAddress(parent_element) {
// 	console.log('fillInAddress fired');
// 	var place = autocomplete.getPlace();
// 	console.log('place variable filled');
// 	for (var component in componentForm) {
// 		$(parent_element).find(component).val('');
// 		// $(parent_element).find(component).prop('disabled', false);
// 	}

// 	for (var i = 0; i < place.address_components.length; i++) {
// 		var addressType = place.address_components[i].types[0];
// 		if (componentForm[addressType]) {
// 			var value = place.address_components[i][componentForm[addressType]];
// 			$(parent_element).find(addressType).val(value);
// 		}
// 	}
// }

// function geolocate() {
// 	console.log('Hit geolocate function');
// 	if (navigator.geolocation) {
// 		navigator.geolocation.getCurrentPosition(function(position) {
// 			var geolocation = new google.maps.LatLng(
// 				position.coords.latitude,
// 				position.coords.longitude
// 			);
// 			autocomplete.setBounds(
// 				new google.maps.LatLngBounds(
// 					geolocation,
// 					geolocation
// 				)
// 			);
// 		});
// 	}
// }