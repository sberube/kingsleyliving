<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Space extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('space_model');
		$this->template->addCSS( base_url('assets/css/space/space.css') );
		$this->template->addCSS( base_url('assets/js/jquery-ui-1.11.1.dark_hive/jquery-ui.css') );
		$this->template->addJS( 'https://maps.googleapis.com/maps/api/js?key=AIzaSyClw9UO0i-yS0bXOPgGdblmOVxsmVaHUQE&libraries=places' );
		$this->template->addJS( base_url('assets/js/space/space.js') );
		$this->load->helper(array('html'));
	}

	// New spaces etc...
	public function get_listings()
	{
		// Parse query string and display relevant search results
		// Sample query string: ?state=TX&city=Houston&radius=15&page=1&pageSize=10&sortcolumn=Distance&sortdirection=Ascending&partials=False&beds=1&baths=1&paymenttype=PurchasePrice&pricemin=0&pricemax=200000&amenities=
	}

	public function create_space()
	{
		// if user id is greater than 4 redirect to communities page or page that says they don't have access to this page
		$this->load->library('form_validation');
		$data['title'] = 'Create new space';
		$data['amenities'] = $this->space_model->read_space_static(1);
		$data['space_statuses'] = $this->space_model->read_space_static(2);
		$data['communities'] = $this->space_model->read_space_static(3);
		$data['communities_json'] = json_encode($data['communities']);
		$data['countries'] = $this->space_model->read_space_static(4);
		$validConfig = array(
			array(
				'field' => 'space_communities',
				'label' => 'Space Number',
				'rules' => 'required|greater_than[0]'
			),
			array(
				'field' => 'space_number',
				'label' => 'Space Number',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_manufacturer',
				'label' => 'Manufacturer',
				'rules' => 'required'
			),
			array(
				'field' => 'space_vin',
				'label' => 'Space VIN',
				'rules' => 'required'
			),
			array(
				'field' => 'space_square_feet',
				'label' => 'Space Square Feet',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_width',
				'label' => 'Space Width',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_length',
				'label' => 'Space Length',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_year',
				'label' => 'Space Year',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_beds',
				'label' => 'Beds',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_baths',
				'label' => 'Baths',
				'rules' => 'required|numeric'
			),
			array(
				'field' => 'space_address_1',
				'label' => 'Address 1',
				'rules' => 'required'
			),
			array(
				'field' => 'space_city',
				'label' => 'City',
				'rules' => 'required'
			),
			array(
				'field' => 'space_state',
				'label' => 'State',
				'rules' => 'required'
			),
			array(
				'field' => 'space_postal_code',
				'label' => 'Zip Code',
				'rules' => 'required'
			),
			array(
				'field' => 'space_country',
				'label' => 'Country',
				'rules' => 'required'
			),
			array(
				'field' => 'space_rent_price',
				'label' => 'Rent Price',
				'rules' => 'required'
			),
			array(
				'field' => 'space_sale_price',
				'label' => 'Sale Price',
				'rules' => 'required'
			),
			array(
				'field' => 'space_cash_sale_price',
				'label' => 'Cash Sale Price',
				'rules' => 'required'
			),
			array(
				'field' => 'space_interest_rate',
				'label' => 'Interest Rate',
				'rules' => 'required'
			),
			array(
				'field' => 'space_lease_rate',
				'label' => 'Lease Rate',
				'rules' => 'required'
			),
			array(
				'field' => 'space_loc_rate',
				'label' => 'Line of Credit Rate',
				'rules' => 'required'
			),
			array(
				'field' => 'space_yearly_tax_savings',
				'label' => 'Yearly Tax Savings',
				'rules' => 'required'
			),
			array(
				'field' => 'space_budgeted_repairs',
				'label' => 'Budgeted Repairs',
				'rules' => 'required'
			),
			array(
				'field' => 'space_deposit',
				'label' => 'Deposit',
				'rules' => 'required'
			),
			array(
				'field' => 'space_closed_date',
				'label' => 'Closed Date',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($validConfig);

		if ($this->form_validation->run() === FALSE)
		{
			// $this->template->addBeforeBody('modals/community_new_images_modal.php');
			$this->template->show('space', 'new_space', $data);//, false, true, true, true);
		} else {
			// First we should check the residentmap db to see if there is already information for this space there, if yes then the user can only modify existing information
			$this->space_model->insert_space();
		}
	}

	public function view($comm_id, $space_id)
	{
		$this->load->library('table');
		$data['title'] = 'Listing Information for Unit: ' . $space_id;
		$data['space'] = $this->space_model->read_space($space_id);
		$data['space_pricing'] = $this->space_model->read_space_pricing($space_id);
		$data['space_details'] = $this->space_model->read_space_details($space_id);
		$data['community'] = $this->space_model->read_space_community($comm_id);
		$space_amenities = $this->space_model->read_space_amenities($space_id);
		$community_amenities = $this->space_model->read_space_community_amenities($comm_id);
		$this->load->model('community_model');
		$data['space_amenities'] = $this->community_model->read_amenities($space_amenities);
		$data['community_amenities'] = $this->community_model->read_amenities($community_amenities);

		$address = $data['community']['address_1'] . ', ' . $data['community']['city'] . ', ' . $data['community']['state'];
		$prepAddr = str_replace(' ', '+', $address);
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
		$output = json_decode($geocode);
		$data['latitude'] = $output->results[0]->geometry->location->lat;
		$data['longitude'] = $output->results[0]->geometry->location->lng;

		$this->template->show('space', 'view', $data);
	}

	public function load_space_from_residentmap($return=FALSE, $comm_id=FALSE)
	{
		// Auto load spaces from residentmap
		// residentmap.com/space/all/community/propid --> post authentication steps so that we can login to the system to get the information we're needing
		// if $return is true then return decoded JSON object to parent function
	}

	public function update_space_from_residentmap($comm_id)
	{
		// Auto update space from residentmap
	}
}