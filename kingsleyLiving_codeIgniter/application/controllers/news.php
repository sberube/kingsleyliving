<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
	{
		$data['news'] = $this->news_model->read_news();
		$data['title'] = 'News archive';

		$this->template->show('news', 'index', $data);
	}

	public function view($slug)
	{
		$data['news_item'] = $this->news_model->read_news($slug);

		if (empty($data['news_item']))
		{
			show_404();
		}

		$data['title'] = $data['news_item']['title'];

		$this->template->show('news', 'view', $data);
	}

	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Create a news item';

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'text', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->template->show('news', 'create', $data);

		}
		else
		{
			$this->news_model->insert_news();
			$this->template->show('news', 'success', $data);
		}
	}

}