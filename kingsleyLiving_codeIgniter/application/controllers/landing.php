<?php

class Landing extends CI_Controller {

	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'/views/landing/'.$page.'.php'))
		{
			show_404();
		}
		$this->template->addCSS('assets/css/landing/landing.css');
		$this->template->addCSS('assets/wow_slider/engine1/style.css');
		$this->template->addJS('assets/wow_slider/engine1/jquery.js');
		$this->template->addJS('assets/js/raphael.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/jquery.vmap.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/maps/jquery.vmap.usa.js');
		$this->template->addJS('assets/js/landing/landing.js');
		$data['wow_slider'] = $this->load->file('assets/wow_slider/kingsley_slider.html', true);
		$data_array = array(
			'legend_style' => 'legend-vertical',
			'legend_title' => 'Communities'
		);
		$map_data = array();
		$map_data['map_data'] = $data_array;
		$data['map_legend'] = $this->load->view('components/map_legend.php', $map_data, true);
		// search form here
		$this->load->model('community_model');
		$communities = $this->community_model->read_community_order_state();
		$data['communities'] = json_encode($communities);
		$this->template->show('landing', $page, $data);
	}
}