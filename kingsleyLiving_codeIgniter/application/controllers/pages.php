<?php

class Pages extends CI_Controller {

	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
		{
			show_404();
		}
		$this->template->addCSS( base_url('assets/css/pages/pages.css') );
		$this->template->show('pages', $page);
	}
}