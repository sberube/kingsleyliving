<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Application extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('application_model');
		$this->template->addCSS( base_url('assets/css/application/application.css') );
		$this->template->addJS( base_url('assets/js/application/application.js'));
		$this->load->helper(array('html'));
	}

	// Figure out if it is a job application or a resident application
	public function create_application()
	{
		// if user id is greater than 4 redirect to communities page or page that says they don't have access to this page
		$this->load->helper(array('html'));
		$this->load->library('form_validation');
		$data['title'] = 'Create new application';
		$data['account_managers'] = $this->application_model->read_application_static(1);
		$validConfig = array(
			array(
				'field' => 'community_name',
				'label' => 'Community Title',
				'rules' => 'required'
			),
			array(
				'field' => 'community_phone',
				'label' => 'Community Phone',
				'rules' => 'required'
			),
			array(
				'field' => 'community_property_manager',
				'label' => 'Community Manager',
				'rules' => 'required'
			),
			array(
				'field' => 'community_assistant_manager',
				'label' => 'Community Assistant Manager',
				'rules' => 'required'
			),
			array(
				'field' => 'community_account_manager',
				'label' => 'Account Manager',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_address_1',
				'label' => 'Billing Address',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_city',
				'label' => 'Billing City',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_state',
				'label' => 'Billing State',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_postal_code',
				'label' => 'Billing Zip',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_country',
				'label' => 'Billing Country',
				'rules' => 'required'
			),
			array(
				'field' => 'community_short',
				'label' => 'Short Description',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($validConfig);

		if ($this->form_validation->run() === FALSE)
		{
			// $this->template->addBeforeBody('modals/community_new_images_modal.php');
			$this->template->show('application', 'create_application', $data);
		} else {
			// First we should check the residentmap db to see if there is already information for this park there, if yes then the user can only modify existing information
			// $view_data = $this->application_model->insert_community();
			// $this->form_validation->clear_form_validation();
			// $this->community_spaces($view_data);
		}
	}
}