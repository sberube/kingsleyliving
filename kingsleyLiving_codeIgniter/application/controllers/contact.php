<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('contact_model');
		$this->template->addCSS( base_url('assets/css/communications/communications.css') );
		$this->template->addJS( 'https://maps.googleapis.com/maps/api/js?libraries=places' );
	}

	public function create_contact()
	{
		$this->load->helper(array('html', 'form', 'captcha'));
		$this->load->library('form_validation');

		$data['title'] = 'Contact Kingsley Management Corporate Office';
		$data['topics'] = $this->contact_model->read_contact_static(1);
		$data['means'] = $this->contact_model->read_contact_static(2);
		// $data['type'] = $this->contact_model->read_contact_static(4);
		// $vals = array(
		//     'img_path' => 'assets/images/capcha/',
		//     'img_url' => base_url('assets/images/captcha/')
		// );

		// $data['cap'] = create_captcha($vals);
		// $capData = array(
		// 	'captcha_time' => $cap['time'],
		// 	'ip_address' => $this->input->ip_address(),
		// 	'word' => $cap['word']
		// );

		// $query = $this->db->insert_string('contact_captcha', $capData);
		// $this->db->query($query);

		$validConfig = array(
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email|is_unique[contact.email]'
			),
			array(
				'field' => 'contact_interest',
				'label' => 'Topic of Interest',
				'rules' => 'required'
			),
			array(
				'field' => 'contact_means',
				'label' => 'Means of Contact',
				'rules' => 'required'
			),
			array(
				'field' => 'first_name',
				'label' => 'First Name',
				'rules' => 'required'
			),
			array(
				'field' => 'last_name',
				'label' => 'Last Name',
				'rules' => 'required'
			),
			array(
				'field' => 'comments',
				'label' => 'Comments',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($validConfig);

		if ($this->form_validation->run() === FALSE)
		{
			$this->template->show('contact', 'contact_us', $data);
		}
		else
		{
			$this->contact_model->insert_contact();
		}
	}

}