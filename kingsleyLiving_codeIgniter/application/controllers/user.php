<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->template->addCSS( base_url('assets/css/user/user.css') );
		$this->template->addJS( 'https://maps.googleapis.com/maps/api/js?libraries=places' );
		$this->template->addJS( base_url('assets/js/user/user.js') );
	}

	public function login()
    {
		$this->load->helper(array('html', 'form', 'captcha'));
		if ($this->input->post('login_user'))
		{
			$this->user_model->login(); // model
		}

		if ($this->flexi_auth->ip_login_attempts_exceeded())
		{
			$this->data['captcha'] = $this->flexi_auth->recaptcha(FALSE);
		}

		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		$this->template->show('user', 'login', $this->data);
    }

    public function login_via_ajax()
    {
		if ($this->input->is_ajax_request())
		{
			$this->user_model->login_via_ajax();

			die($this->flexi_auth->is_logged_in());
		}
		else
		{
			$this->template->show('user', 'login', $this->data);
		}
    }

    public function register_account()
	{
		// Perhaps check residentmap to see if user email is in there, then maybe we create a user in this DB based on that user
		$this->data['gender'] = $this->user_model->read_user_static(1);
		$this->data['marital_status'] = $this->user_model->read_user_static(2);
		$billing_data = array(
			'prefixes' => array(
				'fieldset' => 'billing_',
				'legend' => 'Billing',
				'name' => 'register_billing_'
			),
			'fieldset_class' => 'col-xs-12 col-md-6',
			'field_classes' => array(
				'address_1' => 'col-xs-12',
				'address_2' => 'col-xs-12',
				'city' => 'col-xs-12',
				'state' => 'col-xs-12',
				'postal_code' => 'col-xs-12',
				'country' => 'col-xs-12',
			),
			'countries' => $this->user_model->read_user_static(3)
		);
		$data = array();
		$data['data'] = $billing_data;
		$this->data['billing_address_form'] = $this->load->view('components/address_form.php', $data, true);
		$shipping_data = array(
			'prefixes' => array(
				'fieldset' => 'shipping_',
				'legend' => 'Shipping',
				'name' => 'register_shipping_'
			),
			'fieldset_class' => 'col-xs-12 col-md-6',
			'field_classes' => array(
				'address_1' => 'col-xs-12',
				'address_2' => 'col-xs-12',
				'city' => 'col-xs-12',
				'state' => 'col-xs-12',
				'postal_code' => 'col-xs-12',
				'country' => 'col-xs-12',
			),
			'countries' => $this->user_model->read_user_static(3)
		);
		$data = array();
		$data['data'] = $shipping_data;
		$this->data['shipping_address_form'] = $this->load->view('components/address_form.php', $data, true);
		$this->load->helper(array('html', 'form', 'captcha'));
		if ($this->flexi_auth->is_logged_in()) 
		{
			if ($this->flexi_auth->is_admin())
			{
				// Go ahead and allow admin to create a new user... Admin gets access to the full user creation including status and group
				$this->user_model->load_landingpage();
			} else {
				// Regular user doesn't get to create another user.
				$this->user_model->load_landingpage();
			}
		}
		else if ($this->input->post('register_user'))
		{
			$this->user_model->register_account();
		}
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		

		$this->template->show('user', 'register_user', $this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	// Account Activation
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function activate_account($user_id, $token = FALSE)
	{
		$this->user_model->activate_account($user_id, $token, TRUE);
		// $this->session->set_flashdata('message', $this->flexi_auth->get_messages());

		// $this->user_model->load_landingpage();
	}
	
	public function resend_activation_token()
	{
		if ($this->input->post('send_activation_token')) 
		{
			$this->user_model->resend_activation_token();
		}
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		

		// $this->load->view('demo/public_examples/resend_activation_token_view', $this->data);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	// Forgotten Password
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function forgotten_password()
	{
		if ($this->input->post('send_forgotten_password')) 
		{
			$this->user_model->forgotten_password();
		}
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		

		// $this->load->view('demo/public_examples/forgot_password_view', $this->data);		
	}

	public function manual_reset_forgotten_password($user_id = FALSE, $token = FALSE)
	{
		if ($this->input->post('change_forgotten_password')) 
		{
			$this->user_model->manual_reset_forgotten_password($user_id, $token);
		}
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		

		// $this->load->view('demo/public_examples/forgot_password_update_view', $this->data);
	}

	public function auto_reset_forgotten_password($user_id = FALSE, $token = FALSE)
	{
		$this->flexi_auth->forgotten_password_complete($user_id, $token, FALSE, TRUE);
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		
		// redirect('auth');
	}
		
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	// Logout
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function logout() 
	{
		$this->flexi_auth->logout(TRUE);
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());		
 
		$this->user_model->load_landingpage();
	}

	public function logout_session() 
	{
		$this->flexi_auth->logout(FALSE);
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());		
        
		$this->user_model->load_landingpage();
	}

	public function user_exists_residentmap()
	{
		// Check rmap for user data
	}

	public function load_user_from_residentmap()
	{
		// load specific user data from GL or ResidentMap (account managers, park managers, admins, etc...)
	}

	public function update_user_from_residentmap()
	{
		// update specific user data from GL or ResidentMap (account managers, park managers, admins, etc...)
	}
}