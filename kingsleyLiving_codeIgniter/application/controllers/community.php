<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('community_model');
		$this->template->addCSS( base_url('assets/css/community/community.css') );
		$this->template->addJS( 'https://maps.googleapis.com/maps/api/js?key=AIzaSyClw9UO0i-yS0bXOPgGdblmOVxsmVaHUQE&libraries=places' );
		$this->template->addJS( base_url('assets/js/community/community.js'));
		$this->load->helper(array('html'));
	}

	public function index()
	{
		$data['title'] = 'Kingsley Communities';
		$data['communities'] = $this->community_model->read_community_order_state();
		// find out if any of these parks are 55+ parks and display an icon accordingly.
		// $data['seniors'] = $this->check_community_seniors($data['communities']); // Should return a key value pair array: ('community_name' => 'True/False')

		$this->template->show('community', 'index', $data);
	}

	public function create_community()
	{
		// if user id is greater than 4 redirect to communities page or page that says they don't have access to this page
		$this->load->helper(array('html'));
		$this->load->library('form_validation');
		// $response = $this->load_community_from_residentmap(true);
		// print_r($response);
		// exit();
		$data['title'] = 'Create new community';
		$data['account_managers'] = $this->community_model->read_community_static(1);
		$data['property_managers'] = $this->community_model->read_community_static(2);
		$data['assistant_managers'] = $this->community_model->read_community_static(3);
		$data['amenities'] = $this->community_model->read_community_static(4);
		$data['countries'] = $this->community_model->read_community_static(6);
		$validConfig = array(
			array(
				'field' => 'community_name',
				'label' => 'Community Title',
				'rules' => 'required'
			),
			array(
				'field' => 'community_phone',
				'label' => 'Community Phone',
				'rules' => 'required'
			),
			array(
				'field' => 'community_property_manager',
				'label' => 'Community Manager',
				'rules' => 'required'
			),
			array(
				'field' => 'community_assistant_manager',
				'label' => 'Community Assistant Manager',
				'rules' => 'required'
			),
			array(
				'field' => 'community_account_manager',
				'label' => 'Account Manager',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_address_1',
				'label' => 'Billing Address',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_city',
				'label' => 'Billing City',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_state',
				'label' => 'Billing State',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_postal_code',
				'label' => 'Billing Zip',
				'rules' => 'required'
			),
			array(
				'field' => 'community_billing_country',
				'label' => 'Billing Country',
				'rules' => 'required'
			),
			array(
				'field' => 'community_short',
				'label' => 'Short Description',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($validConfig);

		if ($this->form_validation->run() === FALSE)
		{
			$this->template->addBeforeBody('modals/community_new_images_modal.php');
			$this->template->show('community', 'new_community', $data, false, true, true, true);
		} else {
			// First we should check the residentmap db to see if there is already information for this park there, if yes then the user can only modify existing information
			$view_data = $this->community_model->insert_community();
			$this->form_validation->clear_form_validation();
			$this->create_community_spaces($view_data);
		}
	}

	public function create_community_images()
	{
		//code...
	}

	public function create_community_spaces($viewData)
	{
		$this->load->helper(array('html'));
		$this->load->library('form_validation');
		$viewData['title'] = 'Create community spaces';
		$data['account_managers'] = $this->community_model->read_community_static(1);
		$data['property_managers'] = $this->community_model->read_community_static(2);
		$data['assistant_managers'] = $this->community_model->read_community_static(3);
		$viewData['amenities'] = $this->community_model->read_community_static(5);
		foreach ($data['account_managers'] as $id => $acc_man) {
			if ($acc_man['u_id'] == $viewData['community_details']['account_manager_id'])
			{
				$viewData['community_details']['account_manager'] = $acc_man['first_name'] . ' ' . $acc_man['last_name'];
			} else {
				continue;
			}
		}
		foreach ($data['property_managers'] as $id => $prop_man) {
			if ($prop_man['u_id'] == $viewData['community_details']['property_manager_id'])
			{
				$viewData['community_details']['property_manager'] = $prop_man['first_name'] . ' ' . $prop_man['last_name'];
			} else {
				continue;
			}
		}
		foreach ($data['assistant_managers'] as $id => $assist_man) {
			if ($assist_man['u_id'] == $viewData['community_details']['assistant_property_manager_id'])
			{
				$viewData['community_details']['assistant_property_manager'] = $assist_man['first_name'] . ' ' . $assist_man['last_name'];
			} else {
				continue;
			}
		}

		$validConfig = array(
			array(
				'field' => 'community_name',
				'label' => 'Community Title',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($validConfig);
		if ($this->form_validation->run() === FALSE)
		{
			$this->template->show('community', 'community_spaces', $viewData);
		} else {
			// First check resident map to see if space data already exists.
			$this->community_model->insert_community_spaces();
		}
	}

	private function get_community_seniors($comm_array)
	{
		// check communities for the 'senior community' amenity
	}

	public function view($comm_id)
	{
		// echo 'Comm_id: ' . $comm_id;
		$community = $this->community_model->read_community($comm_id);
		// print_r($community);
		// exit();
		if (empty($community))
		{
			show_404();
		}
		$amenities = $this->community_model->read_community_amenities($comm_id);
		$community['amenities'] = $this->community_model->read_amenities($amenities);
		$community['account_manager'] = $this->community_model->read_community_user($community['account_manager_id']);
		$community['property_manager'] = $this->community_model->read_community_user($community['property_manager_id']);
		$community['assistant_manager'] = $this->community_model->read_community_user($community['assistant_property_manager_id']);
		$data['community'] = $community;
		$data['title'] = $community['name'];

		// Move this getting Latitude and Longitude into any function that gets / stores address, store this information in the database too, there's no need to run this every time we want the coordinates.
		$address = $data['community']['b_addr_1'] . ', ' . $data['community']['b_addr_city'] . ', ' . $data['community']['b_addr_state'];
		$prepAddr = str_replace(' ', '+', $address);
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
		$output = json_decode($geocode);
		$data['latitude'] = $output->results[0]->geometry->location->lat;
		$data['longitude'] = $output->results[0]->geometry->location->lng;

		$this->template->show('community', 'view', $data);
	}

	public function update_community($comm_id)
	{
		// Update a community manually
	}

	public function load_community_from_residentmap($return=FALSE, $comm_id=FALSE, $offset=1) // $return is whether or not this function returns any data
	{
		// Auto load communities from residentmap
		// kingsley.residentmap.com/kingsleyliving?lookup=community&request=allrecords&limit=25&offset=1 --> post authentication steps so that we can login to the system to get the information we're needing
		// if $comm_id has a value, look up it's propid and then use: residentmap.com/community/(propid) to get information on that particular community
		// if $return is true then return decoded JSON object to parent function
		$data = array();
		$curl = curl_init();
		if ($comm_id != FALSE) {
			$communityData = $this->community_model->read_community($comm_id);
			$data['propid'] = $communityData['propid'];
			$data['lookup'] = 'community'; // lookup or push (push to send information)
			$data['request'] = 'singlerecord';
		} else {
			$data['limit'] = 25;
			$data['offset'] = $offset;
			$data['lookup'] = 'community';
			$data['request'] = 'allrecords';
		}
		$curl_options = array(
			CURLOPT_URL => 'https://kingsley.residentmap.com/kingsleyliving',
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_HTTPHEADER => array('Content-type: application/json'),
			CURLOPT_ENCODING => ''
			// CURLOPT_SSL_VERIFYHOST => 2,
			// CURLOPT_SSL_VERIFYPEER => FALSE,
			// CURLOPT_USERPWD => '$username:$password'
		);
		curl_setopt_array($curl, $curl_options);
		$response = curl_exec($curl);
		curl_close($curl);
		// now parse rmap results
		return $response;
	}

	public function update_community_from_residentmap($comm_id)
	{
		// Auto update community from residentmap
	}
}