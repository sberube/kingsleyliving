<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="contact_success">
			<div class="panel-heading"><?php echo heading('Successful Submission', 3); ?></div>
			<div class="panel-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12">Thank you, <?php echo $page_var['first_name'] . ' ' . $page_var['last_name'];  ?>, the contact form has been submitted.&nbsp;&nbsp;Please allow up to two business days for us to get to your message and respond.</div>
					</div>
					<div class="row">
						<div class="col-xs-12">Below is a summary of the contact request that you submitted:</div>
					</div>
					<div class="container-fluid success-details">
						<div class="row">
							<div class="col-xs-12 col-sm-6">Contact Name: <?php echo $page_var['first_name'] . ' ' . $page_var['last_name'];  ?></div>
							<div class="col-xs-12 col-sm-6">Contact Email: <?php echo $page_var['email']; ?></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">Topic of Interest: <?php echo $page_var['contact_interest']; ?></div>
							<div class="col-xs-12 col-sm-6">Best Means of Contact: <?php echo $page_var['contact_means']; ?></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">Address: <?php echo $page_var['address_1']; ?></div>
							<div class="col-xs-12 col-sm-6">Phone: <?php echo $page_var['phone']; ?></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">Alt Address: <?php echo (!empty($page_var['address_2'])?$page_var['address_2']:'N/A'); ?></div>
							<div class="col-xs-12 col-sm-6">City: <?php echo $page_var['city']; ?></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">Zip: <?php echo $page_var['postal_code']; ?></div>
							<div class="col-xs-12 col-sm-6">State: <?php echo $page_var['state']; ?></div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">Created: <?php echo $page_var['created']; ?></div>
						</div>
						<div class="row">
							<div class="col-xs-12">Contact Details: <?php echo $page_var['comments']; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>