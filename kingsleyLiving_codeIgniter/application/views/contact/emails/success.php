<!DOCTYPE html>
<html lang="en">
	<head>
		<title>KingsleyLiving Contact</title>
		<style type="text/css">
			/* Styling rules for email message (Keep simple) */
		</style>
	</head>
	<body>
		<?php echo heading('Successful Submission', 3); ?>
		Thank you, <?php echo $data['first_name'] . ' ' . $data['last_name'];  ?>, the contact form has been submitted.&nbsp;&nbsp;Please allow up to two business days for us to get to your message and respond.
		<br /><br />
		Below is a summary of the contact request that you submitted:
		<br /><br />
		Contact Name: <?php echo $data['first_name'] . ' ' . $data['last_name'];  ?><br />
		Contact Email: <?php echo $data['email']; ?>
		<br /><br />
		Topic of Interest: <?php echo $data['contact_interest']; ?><br />
		Best Means of Contact: <?php echo $data['contact_means']; ?>
		<br /><br />
		Address: <?php echo $data['address_1']; ?><br />
		Phone: <?php echo $data['phone']; ?>
		<br /><br />
		Alt Address: <?php echo (!empty($data['address_2'])?$data['address_2']:'N/A'); ?><br />
		City: <?php echo $data['city']; ?>
		<br /><br />
		Zip: <?php echo $data['postal_code']; ?><br />
		State: <?php echo $data['state']; ?>
		<br /><br />
		Created: <?php echo $data['created']; ?>
		<br /><br />
		Contact Details: <?php echo $data['comments']; ?>
	</body>
</html>