<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="contact_us-form">
			<div class="panel-heading"><?php echo heading($page_var['title'], 3); ?></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">If you are attempting to contact a specific community then please go to that community's contact page.</p>
						<p class="help-block">This form is only for contacting our corporate office.  Individual communities will not get your correspondence.</p>
					</div>
				</div>
				<?php
					$formAttr = array('role' => 'form');//, 'class' => 'form-horizontal'); // For Bootstrap
					echo form_open('contact/contact_us', $formAttr);

					// print_r($page_var);
					// echo br();
					echo '<div class="container-fluid">';
					echo '<div class="row"><div class="col-xs-12 col-md-7 col-lg-6 form-group">'; // For Bootstrap
					// $emailLabelData = array(
					// 	'class' => 'col-xs-12 col-sm-3 control-label' // For Bootstrap
					// );
					echo form_label('Email:', 'email');//, $emailLabelData);
					$emailData = array(
						'name' => 'email',
						'class' => 'form-control' // For Bootstrap
						// 'value' => set_value()
					);
					// echo '<div class="col-xs-12 col-sm-10 col-md-5">'; // For Bootstrap
					echo form_input($emailData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-7 col-lg-6 form-group">';
					// $interestLabelData = array(
					// 	'class' => 'col-xs-12 col-sm-3 control-label' // For Bootstrap
					// );
					echo form_label('Topic of Interest:', 'contact_interest');//, $interestLabelData);
					$topicData = 'class="form-control"';
					$topicDDL = array();
					foreach ($page_var['topics'] as $id => $topic) {
						foreach ($topic as $key => $interest) {
							if ($key == 'id'){
								continue;
							}
							$topicDDL[$topic['id']] = $interest;
						}
					}
					// echo '<div class="col-xs-12 col-sm-10 col-md-5">'; // For Bootstrap
					echo form_dropdown('contact_interest', $topicDDL, '', $topicData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-7 col-lg-6 form-group">';
					// $meansLabelData = array(
					// 	'class' => 'col-xs-12 col-sm-3 control-label' // For Bootstrap
					// );
					echo form_label('Best Means of Contact:', 'contact_means');//, $meansLabelData);
					$meansData = 'class="form-control"';
					$meansDDL = array();
					foreach ($page_var['means'] as $arry) {
						foreach ($arry as $key => $means) {
							if ($key == 'id'){
								continue;
							}
							$meansDDL[$arry['id']] = $means;
						}
					}
					// echo '<div class="col-xs-12 col-sm-10 col-md-5">'; // For Bootstrap
					echo form_dropdown('contact_means', $meansDDL, '', $meansData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					// $firstNameLabelData = array(
					// 	'class' => 'col-xs-12 col-sm-3 control-label' // For Bootstrap
					// );
					echo form_label('First Name:', 'first_name');//, $firstNameLabelData);
					$firstNameData = array(
						'name' => 'first_name',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_input($firstNameData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					// $lastNameLabelData = array(
					// 	'class' => 'col-xs-12 col-sm-3 control-label' // For Bootstrap
					// );
					echo form_label('Last Name:', 'last_name');//, $lastNameLabelData);
					$lastNameData = array(
						'name' => 'last_name',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_input($lastNameData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					echo form_label('Address 1:', 'address_1');
					$address_1Data = array(
						'name' => 'address_1',
						'class' => 'form-control', // For Bootstrap
						'placeholder' => 'Please input your address'
					);
					echo form_input($address_1Data);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					echo form_label('City:', 'city');
					$cityData = array(
						'name' => 'city',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_input($cityData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					echo form_label('Address 2:', 'address_2');
					$address_2Data = array(
						'name' => 'address_2',
						'class' => 'form-control', // For Bootstrap
						'placeholder' => 'Input alternate address'
					);
					echo form_input($address_2Data);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					echo form_label('State:', 'state');
					$stateData = array(
						'name' => 'state',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_input($stateData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					echo form_label('Phone:', 'phone');
					$phoneData = array(
						'name' => 'phone',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_input($phoneData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 col-lg-4 form-group">';
					echo form_label('Zip:', 'postal_code');
					$postalCodeData = array(
						'name' => 'postal_code',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_input($postalCodeData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-lg-8 form-group">';
					echo form_label('Comments:', 'comments');
					$commentsData = array(
						'name' => 'comments',
						'rows' => '3',
						'class' => 'form-control' // For Bootstrap
					);
					echo form_textarea($commentsData);
					echo '</div></div>';

					// echo 'Submit the word you see below:';
					// echo $page_var['cap']['image'];
					// echo form_input('captcha');
					echo '<div class="row"><div class="col-xs-12 form-group">';
					$buttonSubmitData = 'class="btn btn-primary contact-submit-btn"';
					$buttonResetData = 'class="btn btn-primary contact-reset-btn"';
					echo form_submit('submit', 'Submit Contact Form', $buttonSubmitData);
					echo form_reset('reset', 'Clear Form', $buttonResetData);
					echo '</div></div></div>';
					echo form_close();
				?>
			</div>
		</div>
	</div>
</div>