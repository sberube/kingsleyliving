<div class='map-legend'>
	<div class="<?php echo $map_data['legend_style']; ?>">
		<div class='legend-title'><?php echo (isset($map_data['legend_title']))?$map_data['legend_title']:''; ?></div>
		<div class="legend-title-default hidden"><?php echo (isset($map_data['legend_title']))?$map_data['legend_title']:''; ?></div>
		<div class='legend-scale'>
			<ul class='legend-labels'>
				<?php
				if (isset($map_data['legend-labels'])) {
					foreach ($map_data['legend_labels'] as $elem_array) {
						$li_attr = '';
						foreach ($elem_array['list_attributes'] as $id => $attr) {
							$li_attr .= ' ' . $id . '=' . '"' . $attr . '"';
						}
						echo '<li'.$li_attr.'>';
						if (isset($elem_array['span_after'])){
							if (isset($elem_array['list_contents']))
							{
								echo $elem_array['list_contents'];
							}
							if (isset($elem_array['span_attributes'])){
								$span_attr = '';
								foreach ($elem_array['span_attributes'] as $id => $attr) {
									$span_attr .= ' ' . $id . '=' . '"' . $attr . '"';
								}
								echo '<span'.$span_attr.'>';
								if (isset($elem_array['span_contents']))
								{
									echo $elem_array['span_contents'];
								}
								echo '</span>';
							}
						} else {
							if (isset($elem_array['span_attributes'])){
								$span_attr = '';
								foreach ($elem_array['span_attributes'] as $id => $attr) {
									$span_attr .= ' ' . $id . '=' . '"' . $attr . '"';
								}
								echo '<span'.$span_attr.'>';
								if (isset($elem_array['span_contents']))
								{
									echo $elem_array['span_contents'];
								}
								echo '</span>';
							}
							if (isset($elem_array['list_contents']))
							{
								echo $elem_array['list_contents'];
							}
						}
						echo '</li>';
					}
				}
				?>
			</ul>
		</div>
		<?php echo (isset($map_data['legend_source']))?'<div class="legend-source">' . $map_data['legend_source'] . '</div>':''; ?>
	</div>
</div>