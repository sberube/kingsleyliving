<?php
$fieldset_dash_prefix = str_replace('_', '-', $data['prefixes']['fieldset']); // This should be underscored so change underscore to dashes
$fieldset_underscore_prefix = $data['prefixes']['fieldset'];
$legend_prefix = $data['prefixes']['legend'];
$name_prefix = $data['prefixes']['name'];

$fieldsetclass = $data['fieldset_class'];
$address_1_class = $data['field_classes']['address_1'];
$address_2_class = $data['field_classes']['address_2'];
$city_class = $data['field_classes']['city'];
$state_class = $data['field_classes']['state'];
$postal_code_class = $data['field_classes']['postal_code'];
$country_class = $data['field_classes']['country'];

$countryData = 'class="form-control chosen-select" data-placeholder="Please choose a country..." id="country"';
$countryDDL = array(0 => '');
foreach ($data['countries'] as $id => $country) {
	foreach ($country as $key => $value) {
		if ($key == 'id' || $key == 'ISO_code')
		{
			continue;
		}
		$countryDDL[$country['id']] = humanize($value);
	}
}
?>

<fieldset class="<?php echo $fieldsetclass; ?> <?php echo $fieldset_dash_prefix; ?>address-info" id="<?php echo $fieldset_underscore_prefix; ?>address_info">
	<legend><?php echo $legend_prefix; ?>&nbsp;Address</legend>
	<input type="hidden" value="<?php echo strtolower($legend_prefix); ?>" id="fieldset_identifier">
	<div class="row">
		<div class="<?php echo $address_1_class; ?> form-group">
			<label for="<?php echo $name_prefix; ?>address_1">Address:</label>
			<input type="text" class="form-control autocomplete" value="" name="<?php echo $name_prefix; ?>address_1" id="address_1" onFocus="geolocate()">
			<input type="hidden" value="" id="street_number">
			<input type="hidden" value="" id="route">
		</div>
	</div>
	<div class="row">
		<div class="<?php echo $address_2_class; ?> form-group">
			<label for="<?php echo $name_prefix; ?>address_2">Alt Address:</label>
			<input type="text" class="form-control" value="" name="<?php echo $name_prefix; ?>address_2" id="address_2">
		</div>
	</div>
	<div class="row">
		<div class="<?php echo $city_class; ?> form-group">
			<label for="<?php echo $name_prefix; ?>city">City:</label>
			<input type="text" class="form-control" value="" name="<?php echo $name_prefix; ?>city" id="locality">
		</div>
	</div>
	<div class="row">
		<div class="<?php echo $state_class; ?> form-group">
			<label for="<?php echo $name_prefix; ?>state">State:</label>
			<input type="text" class="form-control" value="" name="<?php echo $name_prefix; ?>state" id="administrative_area_level_1">
		</div>
	</div>
	<div class="row">
		<div class="<?php echo $postal_code_class; ?> form-group">
			<label for="<?php echo $name_prefix; ?>postal_code">Zip:</label>
			<input type="text" class="form-control" value="" name="<?php echo $name_prefix; ?>postal_code" id="postal_code">
		</div>
	</div>
	<div class="row">
		<div class="<?php echo $country_class; ?> form-group">
			<label for="<?php echo $name_prefix; ?>country">Country:</label>
			<?php echo form_dropdown($name_prefix.'country', $countryDDL, '', $countryData); ?>
		</div>
	</div>
</fieldset>