<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="about-page">
			<div class="panel-heading">About Kingsley Management Corporation</div>
			<div class="panel-body">
				<p>From Texas to Idaho and New York to California, and everywhere in between, you&#39;ll find more than 45 of our communities that enhance the cities of which they are a part.</p>
				<p>Home is more than just a place to hang your hat.  It&#39;s a place to gather loved ones near.  A place where you can connect with your neighbors and friends.  It&#39;s a place where you belong.  Home ownership is a significant accomplishment.  Our goal is to help everyone seeking that dream to find it.  We offer financing and flexible payment terms to help good, hard-working people have a place to call their own.</p>
				<p>Though we&#39;ve experienced tremendous growth and success during the last three decades, we haven&#39;t forgotten the values that got us to this point. Integrity, fairness, hard work and doing the right thing still shape the choices we make every day.</p>
			</div>
		</div>
	</div>
</div>