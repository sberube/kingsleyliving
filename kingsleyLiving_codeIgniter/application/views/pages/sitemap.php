<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="sitemap">
			<div class="panel-heading">Site Map</div>
			<div class="panel-body">
				<ul><!-- class="list-unstyled" -->
					<li><a href="<?php echo base_url("index.php/communities/meet_new_communities"); ?>">Meet Our New Communities</a></li>
					<li><a href="retirement_a_breeze.php">Make Retirement a Breeze</a></li>
					<li><a href="our_communities.php">Our Communities</a></li>
					<li><a href="kingsley_living_careers/join_our_team.php">Join Our Team</a>
						<ul>
							<li><a href="kingsley_living_careers/available_positions.php">Available Positions</a></li>
						</ul>
					</li>
					<li><a href="acquisitions.php">Acquisitions</a></li>
					<li><a href="kingsley_living_investors/investors.php">Investors</a></li>
					<li><a href="contact_kingsley.php">Contact Us</a></li>
					<li><a href="buy_a_home.php">Buy a Home</a></li>
					<li><a href="rent_a_home.php">Rent a Home</a></li>
					<li><a href="lease_options.php">Lease Options</a></li>
					<li><a href="find_a_home_site.php">Find a Home Site</a></li>
					<li><a href="privacy_policy.php">Privacy Policy</a></li>
					<li><a href="terms_of_use.php">Terms of Use</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>