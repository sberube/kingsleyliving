<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="contact_success">
			<div class="panel-heading">
				<!-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> -->
				<?php echo heading('User Log In', 3); ?>
			</div>
			<div class="panel-body">
				<?php echo form_open(current_url()); ?>
					<label for="username">Username or Email:</label>
					<input type="text" id="identity" name="login_identity"/>
					<br/>
					<label for="password">Password:</label>
					<input type="password" id="password" name="login_password"/>
					<input type="submit" name="login_user" id="submit" value="Log In" class="btn btn-primary" />
				</form>
			</div>
		</div>
	</div>
</div>