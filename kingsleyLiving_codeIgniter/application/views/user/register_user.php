<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="register_user-form">
			<div class="panel-heading"><?php echo heading('Create New User', 3); ?></div>
			<div class="panel-body">
				<div class="google_valid_billing_address">Address Valid?</div>
				<div class="google_valid_shipping_address">Address Valid?</div>
				<?php if (! empty($message)): ?>
				<div class="form-group">
					<div id="message" class="col-xs-12">
						<?php echo $message; ?>
					</div>
				</div>
				<?php endif; ?>
				<?php
					$formAttr = array('role' => 'form');
					echo form_open('user/register_account', $formAttr);
/**********************************************************************************************************************************/
/***														Log In Details														***/
/**********************************************************************************************************************************/
					echo '<div class="container-fluid">';
					echo '<div class="row">';
					echo heading('Login Details', 4);
					echo '</div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Username (Optional):', 'register_username');
					$usernameData = array(
						'name' => 'register_username',
						'class' => 'form-control',
						'value' => set_value('register_username')
					);
					echo form_input($usernameData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Email:', 'register_email');
					$emailData = array(
						'name' => 'register_email',
						'class' => 'form-control',
						'value' => set_value('register_email')
					);
					echo form_input($emailData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('First Name:', 'register_first_name');
					$firstNameData = array(
						'name' => 'register_first_name',
						'class' => 'form-control',
						'value' => set_value('register_first_name')
					);
					echo form_input($firstNameData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Last Name:', 'register_last_name');
					$lastNameData = array(
						'name' => 'register_last_name',
						'class' => 'form-control',
						'value' => set_value('register_last_name')
					);
					echo form_input($lastNameData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Gender:', 'register_gender');
					$genderData = 'class="form-control chosen-select" data-placeholder="Please choose a gender..."';
					$genderDDL = array(0 => '');
					foreach ($page_var['gender'] as $id => $gen) {
						foreach ($gen as $key => $gender) {
							if ($key == 'ugen_id')
							{
								continue;
							}
							$genderDDL[$gen['ugen_id']] = $gender;
						}
					}
					echo form_dropdown('register_gender', $genderDDL, '', $genderData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Group:', 'register_group');
					$groupData = 'class="form-control chosen-select" data-placeholder="Please choose a group..."';
					$groupDDL = array(
						'0' => '',
						'8' => 'Current Resident',
						'9' => 'Future Resident'
					);
					echo form_dropdown('register_group', $groupDDL, '', $groupData);
					echo '<p class="help-block">If you are an employee of Kingsley Management Corporation then you need to contact the main office or your community / account manager to get a user, this portal is specifically for customers</p>';
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Password:', 'register_password');
					$passwordData = array(
						'name' => 'register_password',
						'class' => 'form-control',
						'value' => set_value('register_password')
					);
					echo form_password($passwordData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Confirm Password:', 'register_confirm_password');
					$passwordData = array(
						'name' => 'register_confirm_password',
						'class' => 'form-control',
						'value' => set_value('register_confirm_password')
					);
					echo form_password($passwordData);
					echo '</div></div>';
/**********************************************************************************************************************************/
/***													Address																	***/
/**********************************************************************************************************************************/
					echo '<div class="row">';
					echo heading('Address Details', 4);
					echo '</div>';

					echo '<div class="row"><div class="col-xs-12 checkbox">';
					echo '<label for="register_billing_same_shipping">';
					$billingSameShippingData = array(
						'name' => 'register_billing_same_shipping',
						'value' => 'register_billing_same_shipping'
					);
					echo form_checkbox($billingSameShippingData);
					echo 'Check here if billing and shipping address are the same.</label></div></div>';

					echo '<div class="row">';
					echo $page_var['billing_address_form']; // Now an address component in /view/components/
					echo $page_var['shipping_address_form']; // Now an address component in /view/components/
					echo '</div>';
/**********************************************************************************************************************************/
/***													Profile Details															***/
/**********************************************************************************************************************************/
					echo '<div class="row">';
					echo heading('Profile Details', 4);
					echo '</div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Profile Image:', 'register_profile_image');
					// $profileImageData = array(
					// 	'name' => 'register_profile_image',
					// 	// 'class' => 'form-control',
					// 	'value' => set_value('register_profile_image')
					// );
					// // echo '<div class="col-xs-12 col-sm-10 col-md-5">';
					// echo form_upload($profileImageData); // Allow user to change certain details about the image like the description.
					echo '<span class="help-block">Coming Soon</span>';
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Phone:', 'register_phone');
					$phoneData = array(
						'name' => 'register_phone',
						'id' => 'phone',
						'class' => 'form-control',
						'value' => set_value('register_phone')
					);
					echo form_input($phoneData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Marital Status:', 'register_marital_status');
					$maritalStatusData = 'class="form-control chosen-select" data-placeholder="Please choose a marital status..."';
					$maritalStatusDDL = array(0 => '');
					foreach ($page_var['marital_status'] as $id => $maritalstatus) {
						foreach ($maritalstatus as $key => $status) {
							if ($key == 'umari_stat_id')
							{
								continue;
							}
							$maritalStatusDDL[$maritalstatus['umari_stat_id']] = $status;
						}
					}
					echo form_dropdown('register_marital_status', $maritalStatusDDL, '', $maritalStatusData);
					echo '</div></div>';

					// Insert Captcha stuff here?
					echo '<div class="row"><div class="col-xs-12 form-group">';
					$buttonSubmitData = 'class="btn btn-primary register-submit-btn"';
					$buttonResetData = 'class="btn btn-primary register-reset-btn"';
					echo form_submit('register_user', 'Create User', $buttonSubmitData);
					echo form_reset('reset', 'Clear Form', $buttonResetData);
					echo '</div></div></div>';
					?>
				<?php echo form_close();?>
			</div>
		</div>
		<script type="text/javascript">
			var billingfieldset = document.getElementById('billing_address_info');
			// autocomplete_initialize(billingfieldset);
			var shippingfieldset = document.getElementById('shipping_address_info');
			// autocomplete_initialize(shippingfieldset);
		</script>
	</div>
</div>