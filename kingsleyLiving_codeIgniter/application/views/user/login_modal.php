<div class="modal fade" id="login-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">User Log In</h4>
			</div>
			<div class="modal-body">
				<?php echo form_open(current_url()); ?>
					<label for="username">Username or Email:</label>
					<input type="text" id="username" name="login_identity"/>
					<br/>
					<label for="password">Password:</label>
					<input type="password" id="passowrd" name="login_password"/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" name="login_user">Log In</button>
			</div>
		</div>
	</div>
</div>