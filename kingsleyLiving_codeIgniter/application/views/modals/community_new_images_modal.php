<div class="modal fade" id="newCommunityImages" tabindex="-1" role="dialog" aria-labelledby="community_new_images_modal" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Add Community Images
				</h4>
			</div>
			<div class="modal-body">
				<?php
					echo form_open_multipart('community/add_community_images');
					echo form_label('Upload main community image:', 'community_main_image');
					$communityMainImageData = array(
						'name' => 'community_main_image',
						'class' => 'form-control',
						'value' => set_value('community_main_image')
					);
					echo form_upload($communityMainImageData);
					echo form_label('Upload other community images:', 'community_images');
					$communityImagesData = array(
						'name' => 'community_images',
						'class' => 'form-control',
						'value' => set_value('community_images')
					);
					echo form_upload($communityImagesData);
					echo form_close();
				?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">
					Close
				</button>
				<button type="button" class="btn btn-success">
					<?php echo anchor('community/add_community_images', 'Save Images', 'id="new_images_modal_submit"'); ?>
				</button>
			</div>
		</div>
	</div>
</div>