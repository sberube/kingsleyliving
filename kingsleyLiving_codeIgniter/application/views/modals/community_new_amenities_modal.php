<div class="modal fade" id="newCommunityAmenities" tabindex="-1" role="dialog" aria-labelledby="community_new_amenities_modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Add Community Amenities
				</h4>
			</div>
			<div class="modal-body">
				<?php
					echo form_open_multipart('community/add_community_amenities');
					$communityMainAmenitiesData = array(
						'name' => 'community_main_image',
						'class' => 'form-control',
						'value' => set_value('community_main_image')
					);
					echo form_upload($communityMainAmenitiesData);
					echo form_close();
				?>
			<script type="text/javascript">
				// This is to provide in modal adding / previewing before we actually close the modal form and submit information to the database.
			</script>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">
					Close
				</button>
				<button type="button" class="btn btn-success">
					<?php echo anchor('community/add_community_amenities', 'Save Images', 'id="new_amenities_modal_submit"'); ?>
				</button>
			</div>
		</div>
	</div>
</div>