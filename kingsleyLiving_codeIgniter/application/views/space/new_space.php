<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_space-form">
			<div class="panel-heading"><?php echo heading($page_var['title'], 3); ?></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">This form is for admin use only.&nbsp;&nbsp;Access by any other user is forbidden.</p>
						<p class="help-block">The purpose of this creation form is that it simply another way to input space / unit information into the system.</p>
					</div>
				</div>
				<?php
					$formAttr = array('role' => 'form');
					echo form_open('space/create', $formAttr);

					echo '<div class="container-fluid">';
/**********************************************************************************************************************************/
/***													Space Details															***/
/**********************************************************************************************************************************/
					echo '<div class="row"><div class="col-xs-12 col-md-4 communities_json hidden">';
					$communityIDData = array(
						'communities_json' => $page_var['communities_json']
					);
					echo form_hidden($communityIDData);
					echo '</div>';
					echo '<div class="col-xs-12 col-md-4 community_id hidden">';
					$communityIDData = array(
						'community_id' => ''
					);
					echo form_hidden($communityIDData);
					echo '</div></div>';
					echo '<div class="row"><div class="col-xs-12 col-md-4 community_name">';
					// echo heading('Shadow Ridge', 3);
					echo '</div>';
					echo '<div class="col-xs-12 col-md-4 community_manager">';
					// echo 'Community Manager: Gail Chavez';
					echo '</div>';
					echo '<div class="col-xs-12 col-md-4 community_phone">';
					// echo 'Phone: (801) 888-2314';
					echo '</div></div>';
					echo '<div class="row"><div class="col-xs-12 col-md-6 communities">';
					echo form_label('Communities:', 'space_communities');
					$spaceCommunitiesData = 'class="form-control space_communities chosen-select" data-placeholder="Please choose a community..."';
					$spaceCommunitiesDDL = array(0 => '');
					foreach ($page_var['communities'] as $id => $community) {
						foreach ($community as $key => $info) {
							if ($key != 'name' )
							{
								continue;
							}
							$spaceCommunitiesDDL[$community['id']] = $info;
						}
					}
					echo form_dropdown('space_communities', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Unit Number:', 'space_number');
					$spaceNumberData = array(
						'name' => 'space_number',
						'class' => 'form-control',
						'value' => set_value('space_number')
					);
					echo form_input($spaceNumberData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Space Status:', 'space_status');
					$spaceStatusData = 'class="form-control chosen-select" data-placeholder="Please choose a status..."';
					$spaceStatusDDL = array(0 => '');
					foreach ($page_var['space_statuses'] as $id => $stat) {
						foreach ($stat as $key => $status) {
							if ($key == 'sp_stat_id')
							{
								continue;
							}
							$spaceStatusDDL[$stat['sp_stat_id']] = humanize($status);
						}
					}
					echo form_dropdown('space_status', $spaceStatusDDL, '', $spaceStatusData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Manufacturer:', 'space_manufacturer');
					$spaceManufacturerData = array(
						'name' => 'space_manufacturer',
						'class' => 'form-control',
						'value' => set_value('space_manufacturer')
					);
					echo form_input($spaceManufacturerData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('VIN:', 'space_vin');
					$spaceVINData = array(
						'name' => 'space_vin',
						'class' => 'form-control',
						'value' => set_value('space_vin')
					);
					echo form_input($spaceVINData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Square Feet:', 'space_square_feet');
					$spaceSquareFeetData = array(
						'name' => 'space_square_feet',
						'class' => 'form-control',
						'value' => set_value('space_square_feet')
					);
					echo '<div class="input-group">';
					echo form_input($spaceSquareFeetData);
					echo '<span class="input-group-addon">ft</span>';
					echo '</div></div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Space Width:', 'space_width');
					$spaceWidthData = array(
						'name' => 'space_width',
						'class' => 'form-control',
						'value' => set_value('space_width')
					);
					echo '<div class="input-group">';
					echo form_input($spaceWidthData);
					echo '<span class="input-group-addon">ft</span>';
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Year:', 'space_year');
					$spaceYearData = array(
						'name' => 'space_year',
						'class' => 'form-control',
						'value' => set_value('space_year')
					);
					echo form_input($spaceYearData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Space Length:', 'space_length');
					$spaceLengthData = array(
						'name' => 'space_length',
						'class' => 'form-control',
						'value' => set_value('space_length')
					);
					echo '<div class="input-group">';
					echo form_input($spaceLengthData);
					echo '<span class="input-group-addon">ft</span>';
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Beds:', 'space_beds');
					$spaceBedsData = array(
						'name' => 'space_beds',
						'class' => 'form-control',
						'value' => set_value('space_beds')
					);
					echo form_input($spaceBedsData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Baths:', 'space_baths');
					$spaceBathsData = array(
						'name' => 'space_baths',
						'class' => 'form-control',
						'value' => set_value('space_baths')
					);
					echo form_input($spaceBathsData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Notes:', 'space_notes');
					$spaceNotesData = array(
						'name' => 'space_notes',
						'rows' => '3',
						'class' => 'form-control',
						'value' => set_value('space_notes')
					);
					echo form_textarea($spaceNotesData);
					echo '</div></div>';

/**********************************************************************************************************************************/
/***													Address																	***/
/**********************************************************************************************************************************/
					$addressAttr = array('id' => 'address_info', 'class' => 'col-xs-12 address-info');
					echo '<div class="row">';
					echo form_fieldset('Address Details', $addressAttr);

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Address:', 'space_address_1');
					$address1Data = array(
						'name' => 'space_address_1',
						'class' => 'form-control',
						'value' => set_value('space_address_1')
					);
					echo form_input($address1Data);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Alt Address:', 'space_address_2');
					$address2Data = array(
						'name' => 'space_address_2',
						'class' => 'form-control',
						'value' => set_value('space_address_2')
					);
					echo form_input($address2Data);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-5 form-group">';
					echo form_label('City:', 'space_city');
					$cityData = array(
						'name' => 'space_city',
						'class' => 'form-control',
						'value' => set_value('space_city')
					);
					echo form_input($cityData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-5 form-group">';
					echo form_label('State:', 'space_state');
					$stateData = array(
						'name' => 'space_state',
						'class' => 'form-control',
						'value' => set_value('space_state')
					);
					echo form_input($stateData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-2 form-group">';
					echo form_label('Zip:', 'space_postal_code');
					$postalCodeData = array(
						'name' => 'space_postal_code',
						'class' => 'form-control',
						'value' => set_value('space_postal_code')
					);
					echo form_input($postalCodeData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Country:', 'space_country');
					$countryData = 'class="form-control chosen-select" data-placeholder="Please choose a country..."';
					$countryDDL = array(0 => '');
					foreach ($page_var['countries'] as $id => $country) {
						foreach ($country as $key => $value) {
							if ($key == 'id' || $key == 'ISO_code')
							{
								continue;
							}
							$countryDDL[$country['id']] = humanize($value);
						}
					}
					echo form_dropdown('space_country', $countryDDL, '', $countryData);
					echo '</div></div>';
					echo form_fieldset_close();
					echo '</div>';
/**********************************************************************************************************************************/
/***												Appliances																	***/
/**********************************************************************************************************************************/
					$applianceAttr = array('id' => 'appliance_info', 'class' => 'col-xs-12 appliance-info');
					echo '<div class="row">';
					echo form_fieldset('Appliance Details', $applianceAttr);
					echo '<div class="row"><div class="col-xs-12">';
					echo '<p class="help-block">Coming Soon</p>';
					echo '</div></div>';
					echo form_fieldset_close();
					echo '</div>';
/**********************************************************************************************************************************/
/***												Unit Pricing																***/
/**********************************************************************************************************************************/
					$pricingAttr = array('id' => 'pricing_info', 'class' => 'col-xs-12 pricing-info');
					echo '<div class="row">';
					echo form_fieldset('Space Pricing Details', $pricingAttr);

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Rent Price:', 'space_rent_price');
					$rentPriceData = array(
						'name' => 'space_rent_price',
						'id' => 'space_rent_price',
						'class' => 'form-control',
						'value' => set_value('space_rent_price')
					);
					echo '<div class="input-group">';
					echo '<span class="input-group-addon">&#36;</span>';
					echo form_input($rentPriceData);
					echo '</div></div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Lease Rate:', 'space_lease_rate');
					$leaseRateData = array(
						'name' => 'space_lease_rate',
						'id' => 'space_lease_rate',
						'class' => 'form-control',
						'value' => set_value('space_lease_rate')
					);
					echo '<div class="input-group">';
					echo form_input($leaseRateData);
					echo '<span class="input-group-addon">&#37;</span>';
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Sale Price:', 'space_sale_price');
					$salePriceData = array(
						'name' => 'space_sale_price',
						'id' => 'space_sale_price',
						'class' => 'form-control',
						'value' => set_value('space_sale_price')
					);
					echo '<div class="input-group">';
					echo '<span class="input-group-addon">&#36;</span>';
					echo form_input($salePriceData);
					echo '</div></div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Interest Rate:', 'space_interest_rate');
					$interestRateData = array(
						'name' => 'space_interest_rate',
						'id' => 'space_interest_rate',
						'class' => 'form-control',
						'value' => set_value('space_interest_rate')
					);
					echo '<div class="input-group">';
					echo form_input($interestRateData);
					echo '<span class="input-group-addon">&#37;</span>';
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Cash Sale Price:', 'space_cash_sale_price');
					$cashSalePriceData = array(
						'name' => 'space_cash_sale_price',
						'id' => 'space_cash_sale_price',
						'class' => 'form-control',
						'value' => set_value('space_cash_sale_price')
					);
					echo '<div class="input-group">';
					echo '<span class="input-group-addon">&#36;</span>';
					echo form_input($cashSalePriceData);
					echo '</div></div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Line of Credit Rate:', 'space_loc_rate');
					$locRateData = array(
						'name' => 'space_loc_rate',
						'id' => 'space_loc_rate',
						'class' => 'form-control',
						'value' => set_value('space_loc_rate')
					);
					echo '<div class="input-group">';
					echo form_input($locRateData);
					echo '<span class="input-group-addon">&#37;</span>';
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Yearly Tax Savings:', 'space_yearly_tax_savings');
					$yearlyTaxSavingsData = array(
						'name' => 'space_yearly_tax_savings',
						'id' => 'space_yearly_tax_savings',
						'class' => 'form-control',
						'value' => set_value('space_yearly_tax_savings')
					);
					echo '<div class="input-group">';
					echo '<span class="input-group-addon">&#36;</span>';
					echo form_input($yearlyTaxSavingsData);
					echo '</div></div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Budgeted Repairs:', 'space_budgeted_repairs');
					$budgetedRepairsData = array(
						'name' => 'space_budgeted_repairs',
						'id' => 'space_budgeted_repairs',
						'class' => 'form-control',
						'value' => set_value('space_budgeted_repairs')
					);
					echo '<div class="input-group">';
					echo '<span class="input-group-addon">&#36;</span>';
					echo form_input($budgetedRepairsData);
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Deposit:', 'space_deposit');
					$depositData = array(
						'name' => 'space_deposit',
						'id' => 'space_deposit',
						'class' => 'form-control',
						'value' => set_value('space_deposit')
					);
					echo '<div class="input-group">';
					echo '<span class="input-group-addon">&#36;</span>';
					echo form_input($depositData);
					echo '</div></div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Closed Date:', 'space_closed_date');
					$closedDateData = array(
						'name' => 'space_closed_date',
						'class' => 'form-control datepicker',
						'value' => set_value('space_closed_date')
					);
					echo form_input($closedDateData);
					echo '</div></div>';
					echo form_fieldset_close();
					echo '</div>';
/**********************************************************************************************************************************/
/***												Space Amenities and Images													***/
/**********************************************************************************************************************************/
					$amenitiesAttr = array('id' => 'space_amenities_fieldset', 'class' => 'col-xs-12 space_amenities');
					echo '<div class="row">';
					echo form_fieldset('Space Amenities', $amenitiesAttr);
					// They can either search a list and add or remove amenities from the large box, or we can list all of the amenities and have them check off the ones that they want.
					echo '<div class="row"><div class="col-xs-12">';
					echo '<label>Choose this space&#39;s amenities:</label>';
					echo '<p class="help-block">Please remember that the amenities you now see are those that apply to only this unit</p>';
					echo '</div></div>';
					// $jsTooltip_targets = array();
					$amenCount = 1;
					$amenMax = count($page_var['amenities']);
					foreach ($page_var['amenities'] as $id => $amens) {
						if ($amenCount == 1)
						{
							echo '<div class="row">';
						}
						$amenity = array();
						// $amenity['data-toggle'] = 'tooltip';
						// $amenity['data-placement'] = 'bottom';
						$amen_desc = '';
						foreach ($amens as $key => $value) {
							if ($key == 'amen_id') {
								$amenity['value'] = $value;
							} else if ($key == 'name') {
								$amenity['name'] = underscore(str_replace(array('.',','), '', $value));
								$amenity['id'] = 'space_' . underscore(str_replace(array('.',','), '', $value));
								// $jsTooltip_targets[] = $amenity['id'];
							// } else if ($key == 'description') {
								// $amen_desc = ucfirst($value);
								// $amen_desc = preg_replace_callback('/[.!?] .*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'), $amen_desc);
								// $amenity['data-original-title'] = $amen_desc;
								// $jsTooltip_targets[] = 'space_' . underscore(str_replace(array('.',','), '', $amens['name']));
							} else {
								continue;
							}
						}
						echo '<div class="col-xs-6 col-md-3 checkbox">';
						echo form_checkbox($amenity);
						// We should actually build out the icon here, and possibly we may use the icon itself as the checkbox rather than an actual checkbox.
						// $checkbox_labelAttr = array(
						// 	'data-placement' => 'bottom',
						// 	'data-toggle' => 'tooltip',
						// 	'data-original-title' => $amen_desc
						// );
						echo form_label(humanize($amens['name']), underscore(str_replace(array('.',','), '', $amens['name'])));//, $checkbox_labelAttr);
						// echo '<div class="tooltip bottom" role="tooltip">';
						// echo '<div class="tooltip-arrow"></div>';
						// echo '<div class="tooltip-inner">' . $amen_desc . '</div>';
						echo '</div>';//</div>';
						if ($amenCount == 4)
						{
							echo '</div>';
							$amenCount = 1;
							continue;
						}
						if ($amenCount != 4 && $id == ($amenMax - 1))
						{
							echo '</div>';
						}
						$amenCount++;
					}
					// $jsonformat = json_encode($jsTooltip_targets);
					// $jsTooltip_data = array(
					// 	'jsTooltips' => $jsonformat
					// );
					// echo form_hidden($jsTooltip_data);
					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Or add new amenities:', 'new_amenities');
					// echo '<input class="btn btn-primary" type="button" data-toggle="modal" data-backdrop="static" data-target="#newCommunityAmenities" value="Add Community Amenities" />';
					// echo '<br/><button type="button" class="btn btn-success" name="new_amenities">Add Amenity</button>';
					echo '<p class="help-block">Coming Soon</p>';
					echo '</div></div>';
					echo form_fieldset_close('</div>');

					// $commImagesAttr = array('id' => 'community_images_fieldset', 'class' => 'col-xs-12 community_images');
					// echo '<div class="row">';
					// echo form_fieldset('Community Images', $commImagesAttr);
					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo '<input class="btn btn-primary" type="button" data-toggle="modal" data-backdrop="static" data-target="#newSpaceImages" value="Add Space Images" />';
					// Do all of the images in a modal, that way they can be stored and added separate from this form
					// $communityImageData = array(
					// 	'name' => 'community_images',
					// 	// 'class' => 'form-control',
					// 	'value' => set_value('community_images')
					// );
					// // echo '<div class="col-xs-12 col-sm-10 col-md-5">';
					// echo form_upload($communityImageData); // Allow user to change certain details about the image like the description.
					// echo '<span class="help-block">The image features are currently unavailable</span>';
					echo '</div></div>';
					// echo form_fieldset_close('</div>');

					// echo 'Submit the word you see below:';
					// echo $page_var['cap']['image'];
					// echo form_input('captcha');
					echo '<div class="row"><div class="col-xs-12 form-group">';
					$buttonSubmitData = 'class="btn btn-primary space-submit-btn"';
					$buttonResetData = 'class="btn btn-primary space-reset-btn"';
					echo form_submit('submit', 'Submit', $buttonSubmitData);
					echo form_reset('reset', 'Clear Form', $buttonResetData);
					echo '</div></div></div>';
					echo form_close();
				?>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function() {
			var comm_json = $.parseJSON($('[name="communities_json"]').val());
			$('.space_communities').change(function() {
				var comm_id = this.value;
				if (comm_id != 0){
					// $('.space_communities option[value="0"]').remove();
					$.each(comm_json, function(index, elem) {
						var found = false;
						$.each(elem, function(key, value) {
							if (key == 'id'){
								if (value == comm_id) {
									$('[name="community_id"]').val(comm_id);
									found = true;
								}
							} else if (key == 'name' && found == true) {
								$('.community_name').html('<h3>' + value + '</h3>');
							} else if (key == 'phone' && found == true) {
								$('.community_phone').html('Phone: ' + value);
							} else if (key == 'first_name' && found == true) {
								$('.community_manager').html('Community Manager: ' + value);
							} else if (key == 'last_name' && found == true) {
								var curCommManHTML = $('.community_manager').html();
								$('.community_manager').html(curCommManHTML + '&nbsp;' + value);
							}
						});
					});
				}
			});
			$('#space_rent_price').number(true, 2);
			$('#space_lease_rate').number(true, 2);
			$('#space_sale_price').number(true, 2);
			$('#space_interest_rate').number(true, 2);
			$('#space_cash_sale_price').number(true, 2);
			$('#space_loc_rate').number(true, 2);
			$('#space_yearly_tax_savings').number(true, 2);
			$('#space_budgeted_repairs').number(true, 2);
			$('#space_deposit').number(true, 2);
		});
		</script>
	</div>
</div>