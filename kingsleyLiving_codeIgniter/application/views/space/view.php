<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="space_view">
			<div class="panel-heading">
				<?php echo heading($page_var['title']); ?>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-3">
						Refine Search Results panel goes here
					</div>
					<div class="col-xs-12 col-md-9">
						<ul class="space-tabs" role="tablist">
							<li class="active"><a href="#home_info" role="tab" data-toggle="tab">Home Info</a></li>
							<li><a href="#photos" role="tab" data-toggle="tab">Photos</a></li>
							<li><a class="maps_nearby" href="#maps_nearby" role="tab" data-toggle="tab">Maps &amp; Nearby</a></li>
							<li><a href="#community_info" role="tab" data-toggle="tab">Community Info</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="home_info">
								<div class="panel" id="home_info_panel">
									<div class="panel-heading">Home Information</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12 col-md-6">
												- Image on the top here -
											</div>
											<div class="col-xs-12 col-md-3">
												<?php
												$tbl_tmp = array(
													'table_open' => '<table class="table table-striped table-bordered">'
												);
												$this->table->set_template($tbl_tmp);
												$purchaseprice_data = array(
													'data' => $page_var['space_pricing']['sale_price'],
													'class' => 'space_purchase_price'
												);
												$this->table->add_row('Purchase Price', $purchaseprice_data);
												$this->table->add_row('Bedrooms', $page_var['space_details']['beds']);
												$this->table->add_row('Bathrooms', $page_var['space_details']['baths']);
												$squarefeet_data = array(
													'data' => $page_var['space_details']['square_feet'],
													'class' => 'space_square_feet'
												);
												$this->table->add_row('Approx. Size', $squarefeet_data);
												echo $this->table->generate();
												?>
											</div>
											<div class="col-xs-12 col-md-3">
												<div class="row">
													<?php
														$home_status_class = 'btn-success';
														$home_status = 'Apply Now';
													?>
													<button class="btn <?php echo $home_status_class; ?>" href="<?php echo index_page('application/'.$page_var['community']['id'].'/'.$page_var['space']['id']); ?>"><?php echo $home_status; ?></button>
												</div>
												<?php
												$flyerlink = 'space/print/'.$page_var['community']['id'] . '/' . $page_var['space']['id'];
												echo anchor($flyerlink, 'Print Flyer', 'title="print_flyer"');
												?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												- Ads here -
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-md-7">
												<?php
													echo heading('Home Description', 4);
													echo '<div class="row"><div class="col-xs-12">';
													echo $page_var['space']['description'];
													echo '<br /><br /><b>Extra Notes:</b><br />';
													echo $page_var['space']['notes'];
													echo '</div></div>';
												?>
											</div>
											<div class="col-xs-12 col-md-5">
												<?php
													echo heading('Features', 4);
													foreach ($page_var['space_amenities'] as $id => $amenity) {
														echo '<div class="row"><div class="col-xs-12">';
														echo humanize($amenity['name']);
														echo '</div></div>';
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="photos">
								<div class="panel" id="photos_panel">
									<div class="panel-heading">Photos</div>
									<div class="panel-body">
										ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! ABSOLUTELY FANTASTIC! 
									</div>
								</div>
							</div>
							<div class="tab-pane" id="maps_nearby">
								<div class="panel" id="maps_nearby_panel">
									<!-- <div class="panel-heading">Maps &amp; Nearby</div> -->
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12 col-md-6 space_maps">
												<div class="community_latitude hidden"><?php echo $page_var['latitude']; ?></div> <!-- Check for Google_id to see if we've already registered Google details for this address, if not, then we neeed to do it. -->
												<div class="community_longitude hidden"><?php echo $page_var['longitude']; ?></div>
												<div id="map_canvas" class="map-canvas-default"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="community_info">
								<div class="panel" id="community_info_panel">
									<div class="panel-heading"><b><?php echo $page_var['community']['name']; ?></b></div>
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12 col-md-8">
												- Image here -
											</div>
											<div class="col-xs-12 col-md-4">
												<?php
													foreach ($page_var['community_amenities'] as $id => $amenity) {
														echo '<div class="row">';
														echo humanize($amenity['name']);

														echo '</div>';
													}
												?>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												Community Contact Information
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-md-6">
												<div class="row">
													<div class="col-xs-12">
														<b>Community Manager</b>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														- Manager Name -
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Phone:&nbsp;
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-md-6">
												<div class="row">
													<div class="col-xs-12">
														Sales Office Hours:
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														- Hours -
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														- Hour Exceptions -
													</div>
												</div>
											</div>
										</div>
										<div class="row top-buffer">
											<div class="col-xs-12 col-md-6">
												<div class="row">
													<div class="col-xs-12">
														<b>Resident Hotline:</b>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Phone:&nbsp;<?php echo $page_var['community']['phone']; ?>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Office Hours:
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														- Hours -
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														- Hour Exceptions -
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-md-6">
												- Button to contact sales here -
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$('.space_purchase_price').number(true, 2);
			var old_pur_price = $('.space_purchase_price').text();
			$('.space_purchase_price').html('&#36;'+old_pur_price);
			$('.space_square_feet').number(true);
		</script>
	</div>
</div>