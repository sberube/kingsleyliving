<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="space_index">
			<div class="panel-heading"><?php echo heading($page_var['title'], 2); ?></div>
			<div class="panel-body">
				<?php
				$loopcount = 1;
				$maxCount = 0;
				// print_r($page_var['spaces']);
				foreach ($page_var['spaces'] as $group => $space_item):
					// function to convert state code to state name here...
					$maxCount = count($space_item);
					?>
					<fieldset class="col-xs-12">
						<legend><?php echo heading($group . ' Locations', 4); ?></legend>
				<?php
					foreach ($space_item as $key => $space):
						if ($maxCount == 1): ?>
							<div class="row">
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $space['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($space['b_addr_2'])?$space['b_addr_1'].'&nbsp;or&nbsp;'.$space['b_addr_2']:$space['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $space['b_addr_city'] . ',&nbsp;' . $space['b_addr_state'] . '&nbsp;' . $space['b_addr_zip']; ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $space['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $space['id']; ?>'">Go to <?php echo ((substr($space['name'], -1) != 's')?$space['name'] . '&#39;s':$space['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
							</div>
						<?php elseif ($loopcount == 1 || $key == 0): ?>
							<div class="row">
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $space['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($space['b_addr_2'])?$space['b_addr_1'].'&nbsp;or&nbsp;'.$space['b_addr_2']:$space['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $space['b_addr_city'] . ',&nbsp;' . $space['b_addr_state'] . '&nbsp;' . $space['b_addr_zip'] ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $space['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $space['id']; ?>'">Go to <?php echo ((substr($space['name'], -1) != 's')?$space['name'] . '&#39;s':$space['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
						<?php elseif ($loopcount == 4): ?>
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $space['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($space['b_addr_2'])?$space['b_addr_1'].'&nbsp;or&nbsp;'.$space['b_addr_2']:$space['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $space['b_addr_city'] . ',&nbsp;' . $space['b_addr_state'] . '&nbsp;' . $space['b_addr_zip'] ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $space['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $space['id']; ?>'">Go to <?php echo ((substr($space['name'], -1) != 's')?$space['name'] . '&#39;s':$space['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
							</div>
						<?php $loopcount=1; continue; elseif ($loopcount != 4 && ($key + 1) == $maxCount): $loopcount=1; ?>
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $space['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($space['b_addr_2'])?$space['b_addr_1'].'&nbsp;or&nbsp;'.$space['b_addr_2']:$space['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $space['b_addr_city'] . ',&nbsp;' . $space['b_addr_state'] . '&nbsp;' . $space['b_addr_zip'] ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $space['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $space['id']; ?>'">Go to <?php echo ((substr($space['name'], -1) != 's')?$space['name'] . '&#39;s':$space['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
							</div>
						<?php else: ?>
							<fieldset class="col-xs-6 col-sm-4 col-md-3">
								<div class="row">
									<div class="col-xs-12">
										<b><?php echo $space['name']; ?></b>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<?php echo (!empty($space['b_addr_2'])?$space['b_addr_1'].'&nbsp;or&nbsp;'.$space['b_addr_2']:$space['b_addr_1']); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<?php echo $space['b_addr_city'] . ',&nbsp;' . $space['b_addr_state'] . '&nbsp;' . $space['b_addr_zip'] ?>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12"><?php echo $space['phone']; ?></div>
								</div>
								<!-- <div class="row"> -->
									<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $space['id']; ?>a'">Go to <?php echo ((substr($space['name'], -1) != 's')?$space['name'] . '&#39;s':$space['name'] . '&#39;'); ?> page</button>
								<!-- </div> -->
							</fieldset>
						<?php endif;
							$loopcount++; ?>
					<?php endforeach; ?>
					</fieldset>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>