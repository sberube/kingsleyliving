<div class="navbar-header">
	<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
		<span class="sr-only">Toggle Navigation</span>
		<span class="icon ion-navicon-round"></span>
	</button>
	<a class="navbar-brand" href="<?php echo base_url("index.php/about"); ?>">Kingsleyliving.com</a>
</div>
<div class="navbar-collapse collapse">
	<ul class="nav navbar-left navbar-nav">
		<li class="active">
			<a href="<?php echo base_url("index.php"); ?>">Home</a>
		</li>
		<li>
			<a href="#">Buy a Home</a>
		</li>
		<li>
			<a href="#">Rent a Home</a>
		</li>
		<li>
			<a href="#">Lease Options</a>
		</li>
		<li>
			<a href="#">Find a Home Site</a>
		</li>
	</ul>
	<!-- <div class="sign-in-register"> -->
		<?php echo $siteHeader_logInRegisterForm; ?>
	<!-- </div> -->
</div>