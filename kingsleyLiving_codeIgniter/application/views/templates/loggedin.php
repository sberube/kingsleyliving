<div class="navbar-form navbar-right sign-in">
	<div class="btn-group">
		<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
			<?php echo $full_name; ?>&nbsp;<span class="caret"></span>
		</button>
		<div class="dropdown-menu">
			<div class="col-sm-12">
				<div class="col-sm-12">
					<a href="<?php echo base_url(); ?>index.php/user/view/<?php echo $user_id; ?>/account_settings">Account</a>
				</div>
				<div class="col-sm-12">
					<a href="<?php echo base_url(); ?>index.php/user/view/<?php echo $user_id; ?>/profile">Profile</a>
				</div>
				<div class="col-sm-12">
					<a href="<?php echo base_url(); ?>index.php/user/logout">Log out</a>
				</div>
			</div>
		</div>
	</div>
</div>