<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- <link rel="shortcut icon" href="<?php //echo base_url(); ?>assets/images/kmc_logo-20140819-favicon.ico" > -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>KingsleyLiving - <?php echo $title ?></title>
		<?php echo $stylesheetLinks; ?>
		<script type="text/javascript">
			CI_ROOT = "<?php echo index_page(); ?>";
		</script>
		<?php echo $jsLinks; ?>
	</head>
	<body>
		<!-- Site header -->
		<div class="navbar navbar-default navbar-fixed-top site-menu" role="navigation">
			<div class="container-fluid">
				<?php echo $siteHeader; ?>
			</div>
		</div>
		<!-- Any site errors should show up here... ideally -->
		<div class="container-fluid under-header-errors">
			<div class="col-xs-12">
				<div class="container-fluid">
					<div class="col-xs-12">
						<div class="panel">
							<div class="panel-body site-errors site-errors-default" data-spy="affix">
								<?php echo validation_errors('<div class="alert alert-danger col-xs-12" role="alert">', '</div>'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Main body content -->
		<div class="container-fluid main-body main-body-default">
			<?php if (!empty($slideout_left)): ?>
			<div class="slideout-left slideout-left-default">
				<?php echo $slideout_left; ?>
			</div>
			<?php endif;
				if (!empty($slideout_right)): ?>
			<div class="slideout-right slideout-right-default">
				<?php echo $slideout_right; ?>
			</div>
			<?php endif; ?>
			<div class="body-content">
				<?php echo $beforeBody_content; ?>
				<div class="main-page-content">
					<?php echo $content; ?>
				</div>
			</div>
		</div>
		<!-- Site footer -->
		<div class="footer">
			<div class="container-fluid">
				<div class="hidden-xs col-md-3">
					<div class="panel footer-links">
						<div class="panel-heading">Explore</div>
						<?php echo $footerLinks; ?>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="panel footer-news">
						<div class="panel-heading">Latest News</div>
						<?php
						if (!empty($latestNews)){
							echo '<div class="news-articles">';
							foreach ($latestNews as $key => $news) {
								echo '<div class="article-title">'.$news['title'].'</div>';
								echo '<div class="article-content">'.$news['content'].'</div>';
							}
							echo '</div>';
						}
						else {
							echo '<div>No Recent News</div>';
						}
						?>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="panel footer-contact">
						<div class="panel-heading">Contact Kingsley Management Corp</div>
						<?php echo $footerContact; ?>
					</div>
				</div>
			</div>
			<div class="container-fluid footer-sliver">
				<div class="col-xs-4"><a href="<?php echo base_url("index.php/terms_and_conditions"); ?>">Terms and Conditions</a></div>
				<div class="col-xs-4"><a href="<?php echo base_url("index.php/privacy_policy"); ?>">Privacy Policy</a></div>
				<div class="col-xs-4"><a href="<?php echo base_url("index.php/sitemap"); ?>">Sitemap</a></div>
			</div>
		</div>
	</body>
</html>