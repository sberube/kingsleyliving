<?php
	$formAttr = array('role' => 'register', 'class' => 'navbar-form navbar-right register_account'); // For Bootstrap
	echo form_open('user/register_account', $formAttr); ?>
	<button class="btn btn-primary" type="submit" name="login_register">Register</button>
</form>
<?php
	$signInFormAttr = array('role' => 'login', 'class' => 'navbar-form navbar-right sign-in'); // For Bootstrap
	echo form_open('user/login', $signInFormAttr); ?>
<!-- <form class="navbar-form navbar-right sign-in" role="login" accept-charset="utf-8" method="post" onsubmit="headerformlogin();"> -->
	<div class="form-group">
		<input class="form-control" type="text" placeholder="Email or Username" id="identity" name="login_identity" />
	</div>
	<div class="form-group">
		<input class="form-control" type="password" placeholder="Password" id="password" name="login_password" />
	</div>
	<input type="submit" name="login_user" id="submit" value="Log In" class="btn btn-primary" />
</form>