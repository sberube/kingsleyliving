<html>
<body>
	<h1>New Password for <?php echo $identity;?></h1>
	
	<p>Your password has been reset! If you did not change this, or if you are otherwise unaware of this and believe that this is in error, click here: <?php echo $reportLink; ?></p>
	<p>If you changed your password and are aware of it, please keep this email for your records.</p>
</body>
</html>