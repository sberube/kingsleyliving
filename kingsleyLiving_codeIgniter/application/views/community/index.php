<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_index">
			<div class="panel-heading"><?php echo heading($page_var['title'], 2); ?></div>
			<div class="panel-body">
				<?php
				$loopcount = 1;
				$maxCount = 0;
				// print_r($page_var['communities']);
				foreach ($page_var['communities'] as $group => $comm_item):
					// function to convert state code to state name here...
					$maxCount = count($comm_item);
					?>
					<fieldset class="col-xs-12">
						<legend><?php echo heading($group . ' Locations', 4); ?></legend>
				<?php
					foreach ($comm_item as $key => $community):
						if ($maxCount == 1): ?>
							<div class="row">
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $community['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($community['b_addr_2'])?$community['b_addr_1'].'&nbsp;or&nbsp;'.$community['b_addr_2']:$community['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $community['b_addr_city'] . ',&nbsp;' . $community['b_addr_state'] . '&nbsp;' . $community['b_addr_zip']; ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $community['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $community['id']; ?>'">Go to <?php echo ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
							</div>
						<?php elseif ($loopcount == 1 || $key == 0): ?>
							<div class="row">
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $community['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($community['b_addr_2'])?$community['b_addr_1'].'&nbsp;or&nbsp;'.$community['b_addr_2']:$community['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $community['b_addr_city'] . ',&nbsp;' . $community['b_addr_state'] . '&nbsp;' . $community['b_addr_zip'] ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $community['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $community['id']; ?>'">Go to <?php echo ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
						<?php elseif ($loopcount == 4): ?>
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $community['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($community['b_addr_2'])?$community['b_addr_1'].'&nbsp;or&nbsp;'.$community['b_addr_2']:$community['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $community['b_addr_city'] . ',&nbsp;' . $community['b_addr_state'] . '&nbsp;' . $community['b_addr_zip'] ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $community['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $community['id']; ?>'">Go to <?php echo ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
							</div>
						<?php $loopcount=1; continue; elseif ($loopcount != 4 && ($key + 1) == $maxCount): $loopcount=1; ?>
								<fieldset class="col-xs-6 col-sm-4 col-md-3">
									<div class="row">
										<div class="col-xs-12">
											<b><?php echo $community['name']; ?></b>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo (!empty($community['b_addr_2'])?$community['b_addr_1'].'&nbsp;or&nbsp;'.$community['b_addr_2']:$community['b_addr_1']); ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<?php echo $community['b_addr_city'] . ',&nbsp;' . $community['b_addr_state'] . '&nbsp;' . $community['b_addr_zip'] ?>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12"><?php echo $community['phone']; ?></div>
									</div>
									<!-- <div class="row"> -->
										<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $community['id']; ?>'">Go to <?php echo ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name'] . '&#39;'); ?> page</button>
									<!-- </div> -->
								</fieldset>
							</div>
						<?php else: ?>
							<fieldset class="col-xs-6 col-sm-4 col-md-3">
								<div class="row">
									<div class="col-xs-12">
										<b><?php echo $community['name']; ?></b>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<?php echo (!empty($community['b_addr_2'])?$community['b_addr_1'].'&nbsp;or&nbsp;'.$community['b_addr_2']:$community['b_addr_1']); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<?php echo $community['b_addr_city'] . ',&nbsp;' . $community['b_addr_state'] . '&nbsp;' . $community['b_addr_zip'] ?>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12"><?php echo $community['phone']; ?></div>
								</div>
								<!-- <div class="row"> -->
									<button class="btn btn-success btn-sm" onclick="location.href='<?php echo current_url();?>/<?php echo $community['id']; ?>a'">Go to <?php echo ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name'] . '&#39;'); ?> page</button>
								<!-- </div> -->
							</fieldset>
						<?php endif;
							$loopcount++; ?>
					<?php endforeach; ?>
					</fieldset>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>