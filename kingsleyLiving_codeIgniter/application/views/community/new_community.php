<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_community-form">
			<div class="panel-heading"><?php echo heading($page_var['title'], 3); ?></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">This form is for admin use only.&nbsp;&nbsp;Access by any other user is forbidden.</p>
						<p class="help-block">The purpose of this creation form is that it simply another way to input community information into the system.</p>
					</div>
				</div>
				<?php
					$formAttr = array('role' => 'form');
					echo form_open('community/create', $formAttr);

					echo '<div class="container-fluid">';
/**********************************************************************************************************************************/
/***													Community Details														***/
/**********************************************************************************************************************************/
					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Community Title:', 'community_name');
					$communityTitleData = array(
						'name' => 'community_name',
						'class' => 'form-control',
						'value' => set_value('community_name')
					);
					echo form_input($communityTitleData);
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Community Phone:', 'community_phone');
					$communityPhoneData = array(
						'name' => 'community_phone',
						'class' => 'form-control',
						'value' => set_value('community_phone')
					);
					echo form_input($communityPhoneData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">'; // For Bootstrap
					echo form_label('Community Manager:', 'community_property_manager');//, $emailLabelData);
					$communityPropertyManagerData = 'class="form-control chosen-select" data-placeholder="Please choose a community manager..."';
					$communityPropertyManagerDDL = array(0 => '');
					foreach ($page_var['property_managers'] as $id => $manager) {
						$communityPropertyManagerDDL[$manager['u_id']] = $manager['first_name'] . ' ' . $manager['last_name'] . ' - ' . $manager['u_email'];
					}
					echo form_dropdown('community_property_manager', $communityPropertyManagerDDL, '', $communityPropertyManagerData);
					echo '</div>'; // For Bootstrap

					echo '<div class="col-xs-12 col-md-6 form-group">'; // For Bootstrap
					echo form_label('Community Assistant Manager:', 'community_assistant_manager');//, $emailLabelData);
					$communityAssistantManagerData = 'class="form-control chosen-select" data-placeholder="Please choose an assistant manager..."';
					$communityAssistantManagerDDL = array(0 => '');
					foreach ($page_var['assistant_managers'] as $id => $man) {
						$communityAssistantManagerDDL[$man['u_id']] = $man['first_name'] . ' ' . $man['last_name'] . ' - ' . $man['u_email'];
					}
					echo form_dropdown('community_assistant_manager', $communityAssistantManagerDDL, '', $communityAssistantManagerData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">'; // For Bootstrap
					echo form_label('Account Manager:', 'community_account_manager');//, $emailLabelData);
					$communityAccountManagerData = 'class="form-control chosen-select" data-placeholder="Please choose an account manager..."';
					$communityAccountManagerDDL = array(0 => '');
					foreach ($page_var['account_managers'] as $id => $acc_manager) {
						$communityAccountManagerDDL[$acc_manager['u_id']] = $acc_manager['first_name'] . ' ' . $acc_manager['last_name'] . ' - ' . $acc_manager['u_email'];
					}
					echo form_dropdown('community_account_manager', $communityAccountManagerDDL, '', $communityAccountManagerData);
					echo '</div></div>'; // For Bootstrap

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Please give a short description / summary, no more than 255 characters:', 'community_short');
					$communityShortData = array(
						'name' => 'community_short',
						'rows' => '2',
						'class' => 'form-control', // For Bootstrap
						'value' => set_value('community_short')
					);
					echo form_textarea($communityShortData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Description:', 'community_description');
					$communityDescriptionData = array(
						'name' => 'community_description',
						'rows' => '6',
						'class' => 'form-control', // For Bootstrap
						'value' => set_value('community_description')
					);
					echo form_textarea($communityDescriptionData);
					echo '</div></div>';

/**********************************************************************************************************************************/
/***													Billing Address															***/
/**********************************************************************************************************************************/
					echo '<div class="row">';
					echo heading('Address Details', 4);
					echo '</div>';

					echo '<div class="row"><div class="col-xs-12 checkbox">';
					$billingSameShippingData = array(
						'name' => 'community_billing_same_shipping',
						'value' => 'community_billing_same_shipping'
					);
					echo form_checkbox($billingSameShippingData);
					echo form_label('Check here if billing and shipping address are the same.', 'community_billing_same_shipping');
					echo '</div></div>';

					$billingAttr = array('id' => 'billing_address_info', 'class' => 'col-xs-12 col-md-6 billing-address-info');
					echo '<div class="row">';
					echo form_fieldset('Billing Address', $billingAttr);

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Address:', 'community_billing_address_1');//, $address1LabelData);
					$billingAddress1Data = array(
						'name' => 'community_billing_address_1',
						'class' => 'form-control',
						'value' => set_value('community_billing_address_1')
					);
					echo form_input($billingAddress1Data);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Alt Address:', 'community_billing_address_2');//, $address2LabelData);
					$billingAddress2Data = array(
						'name' => 'community_billing_address_2',
						'class' => 'form-control',
						'value' => set_value('community_billing_address_2')
					);
					echo form_input($billingAddress2Data);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('City:', 'community_billing_city');//, $cityLabelData);
					$billingCityData = array(
						'name' => 'community_billing_city',
						'class' => 'form-control',
						'value' => set_value('community_billing_city')
					);
					echo form_input($billingCityData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('State:', 'community_billing_state');//, $stateLabelData);
					$billingStateData = array(
						'name' => 'community_billing_state',
						'class' => 'form-control',
						'value' => set_value('community_billing_state')
					);
					echo form_input($billingStateData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Zip:', 'community_billing_postal_code');//, $postalCodeLabelData);
					$billingPostalCodeData = array(
						'name' => 'community_billing_postal_code',
						'class' => 'form-control',
						'value' => set_value('community_billing_postal_code')
					);
					echo '<div class="input-group">';
					echo form_input($billingPostalCodeData);
					echo '<span class="input-group-addon">ft</span>';
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Country:', 'community_billing_country');
					$countryData = 'class="form-control chosen-select" data-placeholder="Please choose a country..."';
					$countryDDL = array(0 => '');
					foreach ($page_var['countries'] as $id => $country) {
						foreach ($country as $key => $value) {
							if ($key == 'id' || $key == 'ISO_code')
							{
								continue;
							}
							$countryDDL[$country['id']] = humanize($value);
						}
					}
					echo form_dropdown('community_billing_country', $countryDDL, '', $countryData);
					echo '</div></div>';
					echo form_fieldset_close();
/**********************************************************************************************************************************/
/***													Shipping Address														***/
/**********************************************************************************************************************************/
					$shippingAttr = array('id' => 'shipping_address_info', 'class' => 'col-xs-12 col-md-6 shipping-address-info');
					echo form_fieldset('Shipping Address', $shippingAttr);

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Address:', 'community_shipping_address_1');//, $address1LabelData);
					$shippingAddress1Data = array(
						'name' => 'community_shipping_address_1',
						'class' => 'form-control',
						'value' => set_value('community_shipping_address_1')
					);
					echo form_input($shippingAddress1Data);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Alt Address:', 'community_shipping_address_2');//, $address2LabelData);
					$shippingAddress2Data = array(
						'name' => 'community_shipping_address_2',
						'class' => 'form-control',
						'value' => set_value('community_shipping_address_2')
					);
					echo form_input($shippingAddress2Data);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('City:', 'community_shipping_city');//, $cityLabelData);
					$shippingCityData = array(
						'name' => 'community_shipping_city',
						'class' => 'form-control',
						'value' => set_value('community_shipping_city')
					);
					echo form_input($shippingCityData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('State:', 'community_shipping_state');//, $stateLabelData);
					$shippingStateData = array(
						'name' => 'community_shipping_state',
						'class' => 'form-control',
						'value' => set_value('community_shipping_state')
					);
					echo form_input($shippingStateData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Zip:', 'community_shipping_postal_code');//, $postalCodeLabelData);
					$shippingPostalCodeData = array(
						'name' => 'community_shipping_postal_code',
						'class' => 'form-control',
						'value' => set_value('community_shipping_postal_code')
					);
					echo form_input($shippingPostalCodeData);
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Country:', 'community_shipping_country');
					$countryData = 'class="form-control chosen-select" data-placeholder="Please choose a country..."';
					$countryDDL = array(0 => '');
					foreach ($page_var['countries'] as $id => $country) {
						foreach ($country as $key => $value) {
							if ($key == 'id' || $key == 'ISO_code')
							{
								continue;
							}
							$countryDDL[$country['id']] = humanize($value);
						}
					}
					echo form_dropdown('community_shipping_country', $countryDDL, '', $countryData);
					echo '</div></div>';
					echo form_fieldset_close('</div>');
/**********************************************************************************************************************************/
/***											Community Amenities and Images													***/
/**********************************************************************************************************************************/
					$amenitiesAttr = array('id' => 'community_amenities_fieldset', 'class' => 'col-xs-12 community_amenities');
					echo '<div class="row">';
					echo form_fieldset('Community Amenities', $amenitiesAttr);
					// They can either search a list and add or remove amenities from the large box, or we can list all of the amenities and have them check off the ones that they want.
					echo '<div class="row"><div class="col-xs-12">';
					echo '<label>Choose this community&#39;s amenities:</label>';
					echo '<p class="help-block">Please remember that the amenities you now see are those that apply to the whole community</p><p class="help-block">(For home amenities visit one of the &#39;space&#39; pages, they will display the amenities available for that home).</p>';
					echo '</div></div>';
					// $jsTooltip_targets = array();
					$amenCount = 1;
					$amenMax = count($page_var['amenities']);
					foreach ($page_var['amenities'] as $id => $amens) {
						if ($amenCount == 1)
						{
							echo '<div class="row">';
						}
						$amenity = array();
						// $amenity['data-toggle'] = 'tooltip';
						// $amenity['data-placement'] = 'bottom';
						$amen_desc = '';
						foreach ($amens as $key => $value) {
							if ($key == 'amen_id') {
								$amenity['value'] = $value;
							} else if ($key == 'name') {
								$amenity['name'] = underscore(str_replace(array('.',','), '', $value));
								$amenity['id'] = 'community_' . underscore(str_replace(array('.',','), '', $value));
								// $jsTooltip_targets[] = $amenity['id'];
							// } else if ($key == 'description') {
								// $amen_desc = ucfirst($value);
								// $amen_desc = preg_replace_callback('/[.!?] .*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'), $amen_desc);
								// $amenity['data-original-title'] = $amen_desc;
								// $jsTooltip_targets[] = 'community_' . underscore(str_replace(array('.',','), '', $amens['name']));
							} else {
								continue;
							}
						}
						echo '<div class="col-xs-6 col-md-3 checkbox">';
						echo form_checkbox($amenity);
						// We should actually build out the icon here, and possibly we may use the icon itself as the checkbox rather than an actual checkbox.
						// $checkbox_labelAttr = array(
						// 	'data-placement' => 'bottom',
						// 	'data-toggle' => 'tooltip',
						// 	'data-original-title' => $amen_desc
						// );
						echo form_label(humanize($amens['name']), underscore(str_replace(array('.',','), '', $amens['name'])));//, $checkbox_labelAttr);
						// echo '<div class="tooltip bottom" role="tooltip">';
						// echo '<div class="tooltip-arrow"></div>';
						// echo '<div class="tooltip-inner">' . $amen_desc . '</div>';
						echo '</div>';//</div>';
						if ($amenCount == 4)
						{
							echo '</div>';
							$amenCount = 1;
							continue;
						}
						if ($amenCount != 4 && $id == ($amenMax - 1))
						{
							echo '</div>';
						}
						$amenCount++;
					}
					// $jsonformat = json_encode($jsTooltip_targets);
					// $jsTooltip_data = array(
					// 	'jsTooltips' => $jsonformat
					// );
					// echo form_hidden($jsTooltip_data);
					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Or add new amenities:', 'new_amenities');
					// echo '<input class="btn btn-primary" type="button" data-toggle="modal" data-backdrop="static" data-target="#newCommunityAmenities" value="Add Community Amenities" />';
					// echo '<br/><button type="button" class="btn btn-success" name="new_amenities">Add Amenity</button>';
					echo '<p class="help-block">Coming Soon</p>';
					echo '</div></div>';
					echo form_fieldset_close('</div>');

					// $commImagesAttr = array('id' => 'community_images_fieldset', 'class' => 'col-xs-12 community_images');
					// echo '<div class="row">';
					// echo form_fieldset('Community Images', $commImagesAttr);
					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo '<input class="btn btn-primary" type="button" data-toggle="modal" data-backdrop="static" data-target="#newCommunityImages" value="Add Community Images" />';
					// Do all of the images in a modal, that way they can be stored and added separate from this form
					// $communityImageData = array(
					// 	'name' => 'community_images',
					// 	// 'class' => 'form-control',
					// 	'value' => set_value('community_images')
					// );
					// // echo '<div class="col-xs-12 col-sm-10 col-md-5">';
					// echo form_upload($communityImageData); // Allow user to change certain details about the image like the description.
					// echo '<span class="help-block">The image features are currently unavailable</span>';
					echo '</div></div>';
					// echo form_fieldset_close('</div>');

					// echo 'Submit the word you see below:';
					// echo $page_var['cap']['image'];
					// echo form_input('captcha');
					echo '<div class="row"><div class="col-xs-12 form-group">';
					$buttonSubmitData = 'class="btn btn-primary community-submit-btn"';
					$buttonResetData = 'class="btn btn-primary community-reset-btn"';
					echo form_submit('submit', 'Next', $buttonSubmitData);
					echo form_reset('reset', 'Clear Form', $buttonResetData);
					echo '</div></div></div>';
					echo form_close();
				?>
			</div>
		</div>
	</div>
</div>