<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_spaces-form">
			<div class="panel-heading"><?php echo heading($page_var['title'], 2); ?></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">This form is for admin use only.&nbsp;&nbsp;Access by any other user is forbidden.</p>
						<p class="help-block">This form is meant to quickly generate large numbers of units in the database for communities with 150+ spaces, as such it does not have the ability to edit or update individual units</p>
					</div>
				</div>
				<?php
					$formAttr = array('role' => 'form');
					echo form_open('community/create_spaces', $formAttr);

					echo '<div class="container-fluid">';
					// print_r($page_var);
					// exit();
/**********************************************************************************************************************************/
/***													Community Details														***/
/**********************************************************************************************************************************/
					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-community_name">';
					echo $page_var['community_details']['name'];
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-offset-3 col-md-3 form-group">';
					echo '<div class="c_s-account_manager">';
					echo '<b>Account Manager:</b><br />';
					echo $page_var['community_details']['account_manager'];
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-3 form-group">';
					echo '<div class="c_s-community_manager">';
					echo '<b>Community Manager:</b><br />';
					echo $page_var['community_details']['property_manager'];
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-3 form-group">';
					echo '<div class="c_s-community_assistant">';
					echo '<b>Community Assistant Manager:</b><br />';
					echo $page_var['community_details']['assistant_property_manager'];
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo 'Main community image goes here.';
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo '<div class="c_s-community_phone">';
					echo 'Main Office Phone: ' . $page_var['community_details']['phone'];
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo '<div class="c_s-community_description">';
					echo $page_var['community_details']['description'];
					echo '</div></div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 checkbox">';
					$comm_amens = array(
						'value' => 'toggle_address',
						'name' => 'toggle_comm_address',
						'id' => 'toggle_comm_address'
					);
					echo form_checkbox($comm_amens);
					echo form_label('Show Address?', 'toggle_comm_address');
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 checkbox">';
					$comm_amens = array(
						'value' => 'toggle_amenities',
						'name' => 'toggle_comm_amenities',
						'id' => 'toggle_comm_amenities'
					);
					echo form_checkbox($comm_amens);
					echo form_label('Show Community Amenities?', 'toggle_comm_amenities');
					echo '</div></div>';

					echo '<div class="row community_address"><div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-billing_address_column">';
					echo heading('Billing Address', 5);
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-shipping_address_column">';
					echo heading('Shipping Address', 5);
					echo '</div></div></div>';

					echo '<div class="row community_address"><div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-b_a-address_1">';
					echo $page_var['community_billing_address']['address_1'] . '&nbsp;&nbsp;&nbsp;&nbsp;or alt: ' . (!empty($page_var['community_billing_address']['address_2'])?$page_var['community_billing_address']['address_2']:'N/A');
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-s_a-address_1">';
					echo $page_var['community_shipping_address']['address_1'] . '&nbsp;&nbsp;&nbsp;&nbsp;or alt: ' . (!empty($page_var['community_shipping_address']['address_2'])?$page_var['community_shipping_address']['address_2']:'N/A');
					echo '</div></div></div>';

					echo '<div class="row community_address"><div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-b_a-city_state_zip">';
					echo $page_var['community_billing_address']['city'] . ',&nbsp;' . $page_var['community_billing_address']['state'] . '&nbsp;' . $page_var['community_billing_address']['postal_code'];
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-s_a-city_state_zip">';
					echo $page_var['community_shipping_address']['city'] . ',&nbsp;' . $page_var['community_shipping_address']['state'] . '&nbsp;' . $page_var['community_shipping_address']['postal_code'];
					echo '</div></div></div>';

					echo '<div class="row community_address"><div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-b_a-country">';
					echo $page_var['community_billing_address']['country'];
					echo '</div></div>';
					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo '<div class="c_s-s_a-country">';
					echo $page_var['community_shipping_address']['country'];
					echo '</div></div></div>';

					echo '<div class="row list_amenities"><div class="col-xs-12 form-group panel">';
					echo '<div class="panel-heading">';
					echo heading('Community Amenities', 5);
					echo '</div>';
					echo '<div class="panel-body">';
					$amenCount = 1;
					$amenMax = count($page_var['community_amenities']);
					foreach ($page_var['community_amenities'] as $id => $amenity) {
						if ($amenCount == 1)
						{
							echo '<div class="row">';
						}
						echo '<div class="col-xs-6 col-md-3 form-group">';
						echo $amenity;
						echo '</div>';
						if ($amenCount == 4)
						{
							echo '</div>';
							$amenCount = 1;
							continue;
						}
						if ($amenCount != 4 && $id == ($amenMax - 1))
						{
							echo '</div>';
						}
						$amenCount++;
					}
					echo '</div></div></div>';
/**********************************************************************************************************************************/
/***													Space Details															***/
/**********************************************************************************************************************************/
					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Number of Spaces in Community:', 'community_spaces');
					$communitySpacesData = array(
						'name' => 'community_spaces',
						'class' => 'form-control',
						'value' => set_value('community_spaces')
					);
					echo form_input($communitySpacesData);
					echo '<p class="help-block">Please input specifically the number of spaces that the community has.</p>';
					echo '</div>';

					echo '<div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Max Unit Number:', 'max_unit_num');
					$communityPhoneData = array(
						'name' => 'max_unit_num',
						'class' => 'form-control',
						'value' => set_value('max_unit_num')
					);
					echo form_input($communityPhoneData);
					echo '<p class="help-block">For example: if a community has 300 spaces, but the unit numbers actually go up to 350, the max unit number would be 350</p>';
					echo '</div></div>';

					echo '<div class="row"><div class="col-xs-12 col-md-6 form-group">';
					echo form_label('Unit Numbers to Skip:', 'skip_unit_num');
					$communitySpacesData = array(
						'name' => 'skip_unit_num',
						'class' => 'form-control',
						'value' => set_value('skip_unit_num'),
						'placeholder' => 'Please provide comma separated values'
					);
					echo form_input($communitySpacesData);
					echo '<p class="help-block">For example: if a community has 300 spaces, but there is no unit 15, 17, or 19, the numbers go from 14 to 16 to 18 and then to 20 and 21, etc...</p>';
					echo '</div></div>';

					$amenitiesAttr = array('id' => 'space_common_amenities_fieldset', 'class' => 'col-xs-12 space_amenities');
					echo '<div class="row">';
					echo form_fieldset('Common Unit Amenities', $amenitiesAttr);
					// They can either search a list and add or remove amenities from the large box, or we can list all of the amenities and have them check off the ones that they want.
					echo '<div class="row"><div class="col-xs-12">';
					echo '<label>Choose the amenities common to most units:</label>';
					echo '<p class="help-block">Please remember that these amenities should apply to all units within the community</p>';
					echo '</div></div>';
					$amenCount = 1;
					$amenMax = count($page_var['amenities']);
					foreach ($page_var['amenities'] as $id => $amens) {
						if ($amenCount == 1)
						{
							echo '<div class="row">';
						}
						$amenity = array();
						$amen_desc = '';
						foreach ($amens as $key => $value) {
							if ($key == 'amen_id') {
								$amenity['value'] = $value;
							} else if ($key == 'name') {
								$amenity['name'] = underscore(str_replace(array('.',','), '', $value));
								$amenity['id'] = 'space_' . underscore(str_replace(array('.',','), '', $value));
							} else {
								continue;
							}
						}
						echo '<div class="col-xs-6 col-md-3 checkbox">';
						echo form_checkbox($amenity);
						echo form_label(humanize($amens['name']), underscore(str_replace(array('.',','), '', $amens['name'])));
						echo '</div>';
						if ($amenCount == 4)
						{
							echo '</div>';
							$amenCount = 1;
							continue;
						}
						if ($amenCount != 4 && $id == ($amenMax - 1))
						{
							echo '</div>';
						}
						$amenCount++;
					}
					echo '<div class="row"><div class="col-xs-12 form-group">';
					echo form_label('Or add new amenities:', 'new_amenities');
					// echo '<br/><button type="button" class="btn btn-success" name="new_amenities">Add Amenity</button>';
					echo '<p class="help-block">Sorry this feature is not yet available</p>';
					echo '</div></div>';
					echo form_fieldset_close('</div>');
					// Get from admin:
					/* 
					 *	Number of spaces
					 *	Max space number (eg. 300 spaces, but numbers go to 430)
					 *	Unit numbers to skip (eg. 300 spaces, but no unit 15, numbers go from 14 to 16), comma separated values
					 *	Address will correspond to space num unless specified, admin will need to go to unit specifically to adjust though
					 *	Amenities? Amenities common to all units?
					 *	Width (Use ranges, eg. Units 1 - 20 are 20 ft wide, Units 40 - 80 are double wide, etc...)
					 *	Length (Ranges)
					 *	Square Feet (Ranges)
					 *	Beds (Ranges)
					 *	Baths (Ranges)
					 *	Rent price (Ranges)
					 *	Sale price (Ranges)
					 *	Cash sale price (Ranges)
					 *	Lease rate (Ranges)
					 *	Interest rate (Ranges)
					 *	Yearly tax savings (Ranges)
					 *	LOC rate (Ranges)
					 *	Budgeted repairs (Ranges)
					 */
					echo form_close('</div>');
					// Request the following from customer (when they log in):
					/*
					 *	VIN
					 *	Amenities (if not set or not correct)
					 *	Appliances (w/ numbers)
					 *	Manufacturer
					 *	Year
					 *	Images
					 *	Deposit
					 *	Closed date
					 */

					// echo 'Submit the word you see below:';
					// echo $page_var['cap']['image'];
					// echo form_input('captcha');
					echo '<div class="row"><div class="col-xs-12 form-group">';
					$buttonSubmitData = 'class="btn btn-primary community-submit-btn"';
					$buttonSubmitData = 'class="btn btn-primary community-later-btn"';
					$buttonResetData = 'class="btn btn-primary community-reset-btn"';
					echo form_submit('submit', 'Generate', $buttonSubmitData);
					echo form_submit('submit', 'Do This Later', $buttonSubmitData);
					echo form_reset('reset', 'Clear Form', $buttonResetData);
					echo '</div></div></div>';
					echo form_close();
				?>
			</div>
		</div>
	</div>
</div>