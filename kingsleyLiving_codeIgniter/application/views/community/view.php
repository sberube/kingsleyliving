<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_view">
			<div class="jumbotron standard_community_jumbotron">
				<div class="container">
					<?php echo heading($page_var['title'], 2, 'class="jumbotron-header"'); ?>
					<p class="jumbotron-body"><?php echo $page_var['community']['short_description']; ?></p>
					<p class="jumbotron-footer"><button class="btn btn-primary" onclick="location.href='#'">Reference community home page?</button></p>
				</div>
			</div>
			<!-- <div class="panel-heading"></div> -->
			<!-- Add hidden fields for the lat and long for Google Map -->
			<div class="panel-body">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-md-7 community_description">
							<div class="panel" id="community_description">
								<div class="panel-heading"><b>Community Summary</b></div>
								<div class="panel-body"><?php echo $page_var['community']['description']; ?></div>
							</div>
						</div>
						<div class="col-xs-12 col-md-5 community_contact">
							<div class="panel" id="community_contact">
								<div class="panel-heading"><b>Contact <?php echo $page_var['title']; ?></b></div>
								<div class="panel-body">
									<div class="col-xs-12">
										<div class="row">
											Community Manager: <?php echo $page_var['community']['property_manager']['first_name'].'&nbsp;'.$page_var['community']['property_manager']['last_name']; ?>
										</div>
										<div class="row">
											<?php echo (!empty($page_var['community']['b_addr_2'])?$page_var['community']['b_addr_1'].'&nbsp;or&nbsp;'.$page_var['community']['b_addr_2']:$page_var['community']['b_addr_1']); ?>
										</div>
										<div class="row">
											<?php echo $page_var['community']['b_addr_city'] . ',&nbsp;' . $page_var['community']['b_addr_state'] . '&nbsp;' . $page_var['community']['b_addr_zip']; ?>
										</div>
										<div class="row">
											<?php echo $page_var['community']['phone']; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="row community_amenities_schools">
								<div class="col-xs-12">
									<div class="panel" id="community_amenities">
										<div class="panel-heading"><b>Community Amenities</b></div>
										<div class="panel-body">
											<?php
											$amenCount = 1;
											$amenMax = count($page_var['community']['amenities']);
											foreach ($page_var['community']['amenities'] as $id => $amenity) {
												if ($amenCount == 1)
												{
													echo '<div class="row">';
												}
												echo '<div class="col-xs-6 col-md-4 form-group">';
												echo humanize($amenity['name']);
												// print_r($amenity);
												// exit();
												echo '</div>';
												if ($amenCount == 3)
												{
													echo '</div>';
													$amenCount = 1;
													continue;
												}
												if ($amenCount != 3 && $id == ($amenMax - 1))
												{
													echo '</div>';
												}
												$amenCount++;
											} ?>
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="panel" id="community_schools">
										<div class="panel-heading"><b>Local Businesses</b></div>
										<div class="panel-body">
											There are good schools here!
										</div>
									</div>
								</div>
							</div>
							<div class="row community_listings_promotions">
								<div class="col-xs-12">
									<div class="panel" id="community_listings">
										<div class="panel-heading"><b>Community Listings</b></div>
										<div class="panel-body">
											Lots and lots of listings... Oh dear...
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="panel" id="community_promotions">
										<div class="panel-heading"><b>Promotions</b></div>
										<div class="panel-body">
											Deals in your area!
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 community_maps">
							<div class="panel" id="community_google_maps">
								<div class="panel-heading"><b>Map</b></div>
								<div class="panel-body">
									<div class="community_latitude hidden"><?php echo $page_var['latitude']; ?></div>
									<div class="community_longitude hidden"><?php echo $page_var['longitude']; ?></div>
									<div id="map_canvas" class="map-canvas-default"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>