<?php
	$formAttr = array('role' => 'form');
	echo form_open('space/create', $formAttr);
?>
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="row">
				<div class="col-xs-12 col-md-4">
<?php
		echo form_label('State', 'search_state');
		$spaceCommunitiesData = 'class="form-control search_state chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_state', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
				</div>
				<div class="col-xs-12 col-md-4">
<?php
		echo form_label('City', 'search_city'); // List of possible cities to change based on state that was clicked
		$spaceCommunitiesData = 'class="form-control search_city chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_city', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
				</div>
				<div class="col-xs-12 col-md-4">
<?php
		echo form_label('Zip', 'search_zip'); // Zip used in conjunction with the radius
		$spaceCommunitiesData = 'class="form-control search_zip chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_zip', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			search now button goes here
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-8">
<?php
		echo form_label('Radius', 'search_radius'); // Radius used in conjunction with zip
		$spaceCommunitiesData = 'class="form-control search_radius chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_radius', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
<?php
		echo form_label('Community Type', 'search_community_type'); // Types are: 55+, RV, etc...
		$spaceCommunitiesData = 'class="form-control search_community_type chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_community_type', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-8">
<?php
		echo form_label('Beds', 'search_beds'); // Number of beds
		$spaceCommunitiesData = 'class="form-control search_beds chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_beds', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
<?php
		echo form_label('Baths', 'search_baths'); // Number of baths
		$spaceCommunitiesData = 'class="form-control search_baths chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_baths', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
<?php
		echo form_label('Price Minimum', 'search_price_min'); // Minimum house price
		$spaceCommunitiesData = 'class="form-control search_price_min chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_price_min', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
<?php
		echo form_label('Price Maximum', 'search_price_max'); // Maximum house price
		$spaceCommunitiesData = 'class="form-control search_price_max chosen-select"';
		$spaceCommunitiesDDL = array(0 => '');
		foreach ($page_var['communities'] as $id => $community) {
			foreach ($community as $key => $info) {
				if ($key != 'name' )
				{
					continue;
				}
				$spaceCommunitiesDDL[$community['id']] = $info;
			}
		}
		echo form_dropdown('search_price_max', $spaceCommunitiesDDL, '', $spaceCommunitiesData);
?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-8">
<?php
		echo heading('Amenities', 4); // Common amenities to search for
		$amenCount = 1;
		$amenMax = count($page_var['amenities']);
		foreach ($page_var['amenities'] as $id => $amens) {
			if ($amenCount == 1)
			{
				echo '<div class="row">';
			}
			$amenity = array();
			$amen_desc = '';
			foreach ($amens as $key => $value) {
				if ($key == 'amen_id') {
					$amenity['value'] = $value;
				} else if ($key == 'name') {
					$amenity['name'] = underscore(str_replace(array('.',','), '', $value));
					$amenity['id'] = 'search_' . underscore(str_replace(array('.',','), '', $value));
				} else {
					continue;
				}
			}
			echo '<div class="col-xs-6 col-md-3 checkbox">';
			echo form_checkbox($amenity);
			// We should actually build out the icon here, and possibly we may use the icon itself as the checkbox rather than an actual checkbox.
			echo form_label(humanize($amens['name']), underscore(str_replace(array('.',','), '', $amens['name'])));
			echo '</div>';
			if ($amenCount == 4)
			{
				echo '</div>';
				$amenCount = 1;
				continue;
			}
			if ($amenCount != 4 && $id == ($amenMax - 1))
			{
				echo '</div>';
			}
			$amenCount++;
		}
?>
<?php
		echo heading('Amenities', 4); // Radio button group
?>
		</div>
	</div>
<?php
	echo form_close();
?>