<div class="floating-infobox-top">
	<div class="park-name">Sherwood Forest</div>
	<div class="space-info">
		<div class="num-spaces">Number of Spaces: 100</div>
		<div class="occupancies">Occupancies: 0</div>
	</div>
	<div class="desc">
		Situated in the beautiful Salt Lake City valley, which was recently rated as one of the safest cities in the nation, Sherwood Forest Mobile Home Park is the ideal place for your family.  In a family friendly environment and close to some of the most advanced health care facilities in the world, you can be sure... See more<!-- that you will always have the peace of mind that you've chosen the right place. -->
	</div>
	<div class="amenities">
		<div class="a-title">Amenities</div>
		<ul class="a-list">
			<li class="tooltip"><a href="#">Image</a><span>Pool</span></li>
			<li class="tooltip"><a href="#">Image</a><span>Ping Pong Table</span></li>
			<li class="tooltip"><a href="#">Image</a><span>Billiards</span></li>
			<li class="tooltip"><a href="#">Image</a><span>Laundromat</span></li>
			<li class="tooltip"><a href="#">Image</a><span>Rec House</span></li>
			<li class="tooltip"><a href="#">Image</a><span>Pet-friendly</span></li>
		</ul>
	</div>
</div>
<div class="floating-mapbox">
	<div class="mapbox-title">Search For Communities</div>
</div>