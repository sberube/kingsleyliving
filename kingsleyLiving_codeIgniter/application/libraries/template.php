<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template
{
    private $data;
    private $js_file;
    private $css_file;
    private $beforeContent;
    private $leftSlide;
    private $rightSlide;
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper(array('url', 'form', 'inflector'));
        $this->CI->load->library('form_validation', 'session');
        $this->CI->load->config('flexi_auth', TRUE);
        $this->CI->auth = new stdClass;
        $this->CI->load->library('flexi_auth');

        // default CSS and JS that must be loaded on every page
        $this->addJS( 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js' );
        $this->addJS( base_url('assets/js/jquery-2.1.1.js') );
        $this->addJS( base_url('assets/bootstrap-3.2.0/js/bootstrap.min.js') );
        $this->addJS( base_url('assets/js/jquery-ui-1.11.0.notheme/jquery-ui.js') );
        $this->addJS( base_url('assets/js/chosen_v1.1.0/chosen.jquery.js') );
        $this->addJS( base_url('assets/js/jquery-number/jquery.number.js') );
        $this->addJS( base_url('assets/js/easySlidePanel/js/jquery.slidePanel.js') );
        $this->addJS( base_url('assets/js/main.js') );
        $this->addCSS( base_url('assets/css/slideout.css') );
        $this->addCSS( base_url('assets/js/chosen_v1.1.0/chosen.css') );
        $this->addCSS( base_url('assets/css/main.css') );
        // $this->addCSS( base_url('assets/css/tooltips.css') );
        $this->addCSS( base_url('assets/bootstrap-3.2.0/css/bootstrap.css') );
        $this->addCSS( base_url('assets/bootstrap-3.2.0/css/elusive-webfont.css') );
        $this->addCSS( base_url('assets/bootstrap-3.2.0/css/ionicons.min.css') );
    }

    public function &__get($key)
    {
        $CI =& get_instance();
        return $CI->$key;
    }

    public function show( $controller, $view, $data=null, $crumbs=false, $slideLeft=true, $slideRight=true, $before=false )
    {
        if ( ! file_exists('application/views/'.$controller.'/'.$view.'.php' ) )
        {
            show_404();
        }
        else
        {
            $this->data['page_var'] = $data;
            $this->load_JS_and_css();
            $this->init_menu($view);
            $this->init_footer();

            if ($crumbs)
            {
            	$this->load_breadcrumbs();
            } else {
            	$this->data['breadcrumbs'] = '';
            }

            // Redo the slideouts maybe?  - Jquery plugin easySlidePanel -
			if ($slideLeft) // if $slideLeft == true
			{
			    if ($this->CI->flexi_auth->is_admin())
			    {
			        // $this->admin_left_slideout_menu();
			        $this->data['slideout_left'] = '';
			    } else {
			        $this->load_left_slideout();
			    }
			}

			if ($slideRight) // if $slideRight == true
			{
				if ($this->CI->flexi_auth->is_admin())
				{
					$this->rightSlide = array(
				        array(
				            'file' => 'templates/links_slideout_right',
				            'data' => ''
				        ),
				        // array(
				        //     'file' => 'templates/notifications_slideout_right',
				        //     'data' => ''
				        // )
				    );
				    $this->load_right_slideout();
				} else {
				    $this->load_right_slideout();
				}
			}

            if ($before) // if $before == true
            {
                $this->load_beforeBody();
            } else {
            	$this->data['beforeBody_content'] = '';
            }
            $this->data['content'] = $this->CI->load->view($controller.'/'.$view.'.php', $this->data, true);
            $this->CI->load->view('templates/template', $this->data);
        }
    }

    public function addJS( $name ) // This function allows us to specify a js file that is not already included by default and include it on the page
    {
        $js = new stdClass();
        $js->file = $name;
        $this->js_file[] = $js;
    }

    public function addCSS( $name, $attribute='' ) // This function allows us to specify a css file that is not already included by default and include it on the page
    {
        $css = new stdClass();
        $css->file = $name;
        if ( !empty($attribute) )
        {
        	$css->attribute = $attribute;
        }
        $this->css_file[] = $css;
    }

    public function addSlideLeft( $name ) // This allows us to change the contents of the left slideout
    {
    	$slideLeft = new stdClass();
    	$slideLeft->file = $name;
    	$this->leftSlide[] = $slideLeft;
    }

    public function addSlideRight( $name ) // This allows us to change the contents of the right slideout
    {
    	$slideRight = new stdClass();
    	$slideRight->file = $name;
    	$this->rightSlide[] = $slideRight;
    }

    public function addBeforeBody( $name ) // This allows us to set or change the contents of the content that goes into the page before the body
    {
    	$beforeBody = new stdClass();
    	$beforeBody->file = $name;
    	$this->beforeContent[] = $beforeBody;
    }

    private function load_JS_and_css()
    {
        $this->data['stylesheetLinks'] = '';
        $this->data['jsLinks'] = '';

        if ( $this->css_file )
        {
            foreach( $this->css_file as $css )
            {
                if (!empty($css->attribute)){
                	$this->data['stylesheetLinks'] .= "<link rel='stylesheet' type='text/css' href=".$css->file." ".$css->attribute.">". "\n"; // Here we are adding all the css for the page
                } else {
	                $this->data['stylesheetLinks'] .= "<link rel='stylesheet' type='text/css' href=".$css->file.">". "\n"; // Here we are adding all the css for the page
	            }
            }
        }

        if ( $this->js_file )
        {
            foreach( $this->js_file as $js )
            {
                $this->data['jsLinks'] .= "<script type='text/javascript' src=".$js->file."></script>". "\n"; // And now we are adding all the js for the page
            }
        }
    }

    private function load_breadcrumbs()
    {
    	$this->data['breadcrumbs'] = get_crumb();
    }

    private function admin_left_slideout_menu()
    {
        // code...
    }

    private function load_left_slideout()
    {
    	$this->data['slideout_left'] = '';
    	if ( $this->leftSlide )
    	{
    		foreach ($this->leftSlide as $slideLeft) {
    			$this->data['slideout_left'] .= $this->CI->load->view($slideLeft['file'], $slideLeft['data'], true);
    		}
    	} else {
    		$this->data['slideout_left'] = $this->CI->load->view('templates/notifications_slideout_left', '', true); // Default settings and links for left side slideout
    	}
    }

    private function load_right_slideout()
    {
    	$this->data['slideout_right'] = '';
    	if ( $this->rightSlide )
    	{
    		foreach ($this->rightSlide as $slideRight) {
    			$this->data['slideout_right'] .= $this->CI->load->view($slideRight['file'], $slideRight['data'], true);
    		}
    	} else {
    		$this->data['slideout_right'] = $this->CI->load->view('templates/links_slideout_right', $this->data, true); // Default settings and links for right side slideout
    	}
    }

    private function load_beforeBody()
    {
    	$this->data['beforeBody_content'] = '';
    	if ( $this->beforeContent )
    	{
    		foreach ($this->beforeContent as $beforeBody) {
    			$this->data['beforeBody_content'] .= $this->CI->load->view($beforeBody->file, '', true); // Modals, or the information panel can go here.
    		}
    	}
    }

    private function init_menu($page) // Logo and Menu
    {
    	$this->data['title'] = humanize($page);
        if ($this->CI->flexi_auth->is_logged_in())
        // if ($this->CI->session->userdata('logged_in'))
        {
            // Need to set username information and other such information to send to page
            $user = $this->CI->flexi_auth->get_user_by_id()->row_array();
            $data['full_name'] = $user['first_name'].' '.$user['last_name'];
            $data['user_id'] = $user['u_id'];
            // exit();
            // $session_data = $this->CI->session->userdata('logged_in');
            // $this->data['username'] = $session_data['username'];
            $this->data['siteHeader_logInRegisterForm'] = $this->CI->load->view('templates/loggedin', $data, true);
            $this->data['siteHeader'] = $this->CI->load->view('templates/sitemenu', $this->data, true);
        } else {
            $this->data['siteHeader_logInRegisterForm'] = $this->CI->load->view('templates/loggedout', '', true);
            $this->data['siteHeader'] = $this->CI->load->view('templates/sitemenu', $this->data, true);
        }
    }

    // Ajax function to reload menu, not using right now.
    // public function reload_menu()
    // {
    //     if ($this->CI->flexi_auth->is_logged_in())
    //     {
    //         // Need to set username information and other such information to send to page
    //         $user = $this->CI->flexi_auth->get_user_by_id()->row_array();
    //         $data['full_name'] = $user['first_name'].' '.$user['last_name'];
    //         $this->data['siteHeader_logInRegisterForm'] = $this->CI->load->view('templates/loggedin', $data, true);
    //         $this->data['siteHeader'] = $this->CI->load->view('templates/sitemenu', $this->data, true);
    //     } else {
    //         $this->data['siteHeader_logInRegisterForm'] = $this->CI->load->view('templates/loggedout', '', true);
    //         $this->data['siteHeader'] = $this->CI->load->view('templates/sitemenu', $this->data, true);
    //     }
    // }

    private function init_footer()
    {
    	$this->CI->load->model('news_model');
		$this->data['latestNews'] = $this->CI->news_model->read_news(); // Middle box on footer
		$this->data['footerContact'] = '';
		$this->data['footerLinks'] = $this->CI->load->view('templates/footer_links', '', true); // Footer links far left box
    }
}