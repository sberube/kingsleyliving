<?php
class Amenity_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function read_amenity_by_class($class_id, $specifier='=')
	{
		$this->db->select('*');
		$this->db->from('amenities');
		$this->db->join('amenities_icons', 'amenities_icons.amenity_id = amenities.amen_id', 'left');
		if ($specifier != '='){
			$this->db->where('amenities.amen_class_id '.$specifier, $class_id);
		} else {
			$this->db->where('amenities.amen_class_id', $class_id);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function read_amenity_classes()
	{
		$query = $this->db->get('amenities_classes');
		return $query->result_array();
	}

	public function read_amenity()
	{
		$this->db->select('*');
		$this->db->from('amenities');
		$this->db->join('amenities_icons', 'amenities_icons.amenity_id = amenities.amen_id', 'left');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function insert_amenity()
	{
		// $this->load->helper('url');

		// $slug = url_title($this->input->post('title'), 'dash', TRUE);

		// $data = array(
		// 	'title' => $this->input->post('title'),
		// 	'slug' => $slug,
		// 	'content' => $this->input->post('content')
		// );

		// return $this->db->insert('news', $data);
	}

	public function update_amenity()
	{
		// code...
	}

	public function delete_amenity()
	{
		// code...
	}

	public function __destruct() {
		$this->db->close();
	}

}