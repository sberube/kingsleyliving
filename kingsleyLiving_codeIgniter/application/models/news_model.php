<?php
class News_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function read_news($slug = FALSE)
	{
		$today = date('Y-m-d');
		$newDate = strtotime('-1 month', strtotime($today));
		$lastMonth = date('Y-m-d', $newDate);
		if ($slug === FALSE)
		{
			$this->db->select('*');
			$this->db->from('news');
			$this->db->where('created >', $lastMonth);
			$query = $this->db->get();
			return $query->result_array();
		}

		$query = $this->db->get_where('news', array('slug' => $slug));
		return $query->row_array();
	}

	public function insert_news()
	{
		$this->load->helper('url');

		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'content' => $this->input->post('content')
		);

		return $this->db->insert('news', $data);
	}

	public function __destruct() {
		$this->db->close();
	}

}