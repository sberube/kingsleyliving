<?php
class Community_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();

		// $this->upload_config['upload_path'] = base_url('/assets/images/community');
		// $this->upload_config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		// $this->upload_config['max_size'] = '2048';
		// $this->upload_config['max_filename'] = '255';
		// $this->upload_config['encrypt_name'] = 'TRUE';
	}

	/**********************************************************/
	/***					Read functions					***/
	/**********************************************************/
	public function read_community($comm_id=FALSE)
	{
		if ($comm_id === FALSE)
		{
			$this->db->select('community.*, b_addr.id as b_addr_id, b_addr.address_1 as b_addr_1, b_addr.address_2 as b_addr_2, b_addr.city as b_addr_city, b_addr.state as b_addr_state, b_addr.postal_code as b_addr_zip, b_addr.country_id as b_addr_country, s_addr.id as s_addr_id, s_addr.address_1 as s_addr_1, s_addr.address_2 as s_addr_2, s_addr.city as s_addr_city, s_addr.state as s_addr_state, s_addr.postal_code as s_addr_zip, s_addr.country_id as s_addr_country');
			// $this->db->select('*');
			$this->db->from('community');
			$this->db->join('community_address', 'community_address.community_id = community.id', 'left');
			$this->db->join('address as b_addr', 'b_addr.id = community_address.billing_address_id', 'left');
			$this->db->join('address as s_addr', 's_addr.id = community_address.shipping_address_id', 'left');
			$this->db->order_by('community.name', 'asc');
			$query = $this->db->get();
			// print_r($query->result_array());
			// exit();
			return $query->result_array();
		} else {
			$this->db->select('community.*, b_addr.id as b_addr_id, b_addr.address_1 as b_addr_1, b_addr.address_2 as b_addr_2, b_addr.city as b_addr_city, b_addr.state as b_addr_state, b_addr.postal_code as b_addr_zip, b_addr.country_id as b_addr_country, s_addr.id as s_addr_id, s_addr.address_1 as s_addr_1, s_addr.address_2 as s_addr_2, s_addr.city as s_addr_city, s_addr.state as s_addr_state, s_addr.postal_code as s_addr_zip, s_addr.country_id as s_addr_country');
			// $this->db->select('*');
			$this->db->from('community');
			$this->db->join('community_address', 'community_address.community_id = community.id', 'left');
			$this->db->join('address as b_addr', 'b_addr.id = community_address.billing_address_id', 'left');
			$this->db->join('address as s_addr', 's_addr.id = community_address.shipping_address_id', 'left');
			// $this->db->join('community_hours', 'community_hours.community_id = community.id', 'left');
			// $this->db->join('hour', 'hour.id = community_hours.hour_id', 'left');
			// $this->db->join('hour_days', 'hour_days.id = hour.days_id', 'left');
			// $this->db->join('hour_times', 'hour_times.id = hour.time_id', 'left');
			// $this->db->join('hour_type', 'hour_type.id = hour.type_id', 'left');
			// $this->db->join('community_images', 'community_images.community_id = community.id', 'left');
			// $this->db->join('image', 'image.id = community_images.hour_id', 'left');
			// $this->db->join('community_main_image', 'community_main_image.community_id = community.id', 'left');
			// $this->db->join('image', 'image.id = community_main_image.hour_id', 'left');
			$this->db->where('community.id', $comm_id);
			$query = $this->db->get();
			// print_r($this->db->last_query());
			// exit();
			return $query->row_array();
		}
	}

	public function read_community_order_state()
	{
		$communities = $this->read_community();
		$ordered_communities = $this->group_community($communities);
		return $ordered_communities;
	}

	public function read_community_amenities($comm_id)
	{
		$this->db->select('*');
		$this->db->from('community_amenities');
		$this->db->where('community_id', $comm_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function read_amenities($id_array)
	{
		$result_array = array();
		foreach ($id_array as $amen_id) {
			$this->db->select('*');
			$this->db->from('amenities');
			$this->db->where('amen_id', $amen_id['amenity_id']);
			$query = $this->db->get();
			$result_array[] = $query->row_array();
		}
		return $result_array;
	}

	public function read_community_user($id)
	{
		$this->db->select('u_id, u_group_id, u_email, u_username, first_name, last_name');
		$this->db->from('user');
		$this->db->join('user_profile', 'user_profile.uprof_user_id = user.u_id', 'left');
		$this->db->where('u_id', $id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function read_community_static($flag=0)
	{
		if ($flag == 1) {
			// Account Manager
			$this->db->select('u_id, u_group_id, u_email, u_username, first_name, last_name');
			$this->db->from('user');
			$this->db->join('user_profile', 'user_profile.uprof_user_id = user.u_id', 'left');
			$this->db->where('u_group_id', '2');
		} else if ($flag == 2) {
			// Community Manager
			$this->db->select('u_id, u_group_id, u_email, u_username, first_name, last_name');
			$this->db->from('user');
			$this->db->join('user_profile', 'user_profile.uprof_user_id = user.u_id', 'left');
			$this->db->where('u_group_id', '3');
		} else if ($flag == 3) {
			// Community Assistant Manager
			$this->db->select('u_id, u_group_id, u_email, u_username, first_name, last_name');
			$this->db->from('user');
			$this->db->join('user_profile', 'user_profile.uprof_user_id = user.u_id', 'left');
			$this->db->where('u_group_id', '4');
		} else if ($flag == 4) {
			// All amenities for communities
			$this->db->select('*');
			$this->db->from('amenities');
			$this->db->join('amenities_icons', 'amenities_icons.amenity_id = amenities.amen_id', 'left');
			$this->db->where('amenities.amen_class_id <', '3');
		} else if ($flag == 5) {
			// All amenities for spaces
			$this->db->select('*');
			$this->db->from('amenities');
			$this->db->join('amenities_icons', 'amenities_icons.amenity_id = amenities.amen_id', 'left');
			$this->db->where('amenities.amen_class_id >=', '3');
		} else if ($flag == 6) {
			// Countries
			$this->db->select('*');
			$this->db->from('address_countries');
		} else {
			return;
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	/**************************************************************/
	/***					Insert functions					***/
	/**************************************************************/
	public function insert_community()
	{
		// $this->load->library('upload', $this->upload_config);
		$viewData = array();
		$communityDetailsData = array(
			'propid' => 0,
			'name' => $this->input->post('community_name'),
			'short_description' => $this->input->post('community_short'),
			'description' => $this->input->post('community_description'),
			'phone' => $this->input->post('community_phone'),
			'fax' => 0,
			'property_manager_id' => $this->input->post('community_property_manager'),
			'assistant_property_manager_id' => $this->input->post('community_assistant_manager'),
			'account_manager_id' => $this->input->post('community_account_manager')
		);
		$viewData['community_details'] = $communityDetailsData;
		$this->db->insert('community', $communityDetailsData);
		$comm_id = $this->db->insert_id(); // Grab and remember the community id we just inserted

		$billingAddressData = array( // Array to insert into the address table
			'address_1' => $this->input->post('community_billing_address_1'),
			'address_2' => $this->input->post('community_billing_address_2'),
			'city' => $this->input->post('community_billing_city'),
			'state' => $this->input->post('community_billing_state'),
			'postal_code' => $this->input->post('community_billing_postal_code'),
			'country_id' => $this->input->post('community_billing_country')
		);
		$viewData['community_billing_address'] = $billingAddressData;
		$this->db->insert('address', $billingAddressData);
		$billing_address_id = $this->db->insert_id(); // Grab and remember the address id we just inserted

		$same_billing_shipping = $this->input->post('community_billing_same_shipping');
		$shipping_address_id = 0;
		if ($same_billing_shipping==false)
		{
			$shippingAddressData = array( // Array to insert into the address table
				'address_1' => $this->input->post('community_shipping_address_1'),
				'address_2' => $this->input->post('community_shipping_address_2'),
				'city' => $this->input->post('community_shipping_city'),
				'state' => $this->input->post('community_shipping_state'),
				'postal_code' => $this->input->post('community_shipping_postal_code'),
				'country_id' => $this->input->post('community_shipping_country')
			);
			$viewData['community_shipping_address'] = $shippingAddressData;
			$this->db->insert('address', $shippingAddressData);
			$shipping_address_id = $this->db->insert_id(); // Grab and remember the address id we just inserted
		} else {
			$viewData['community_shipping_address'] = $billingAddressData;
			$shipping_address_id = $billing_address_id;
		}

		$community_addressData = array(
			'community_id' => $comm_id,
			'billing_address_id' => $billing_address_id,
			'shipping_address_id' => $shipping_address_id
		);
		$this->db->insert('community_address', $community_addressData);

		// Now we take care of amenities
		$amenities = $this->read_community_static(4);
		$amenity_names = array();
		foreach ($amenities as $row_key => $amen_record) {
			$amenity_names[$amen_record['amen_id']] = underscore(str_replace(array('.',','), '', $amen_record['name']));
		}

		foreach ($amenity_names as $id => $name) {
			$amen_post = $this->input->post($name);
			if ($amen_post == false) {
				continue;
			} else {
				$amenityData = array(
					'community_id' => $comm_id,
					'amenity_id' => $id
				);
				$viewData['community_amenities'][] = humanize($name);
				$this->db->insert('community_amenities', $amenityData);
			}
		}

		// now take care of pictures
		return $viewData;
	}

	public function insert_community_spaces()
	{
		// $this->load->library('upload', $this->upload_config);

		$this->load_landingpage();
	}

	/**********************************************************/
	/***					Other functions					***/
	/**********************************************************/
	public function group_community($comm_array)
	{
		$return_array = array();
		foreach ($comm_array as $community) {
			$return_array[$community['b_addr_state']][] = $community;
		}
		return $return_array;
	}

	public function load_landingpage($data=null)
	{
		$this->template->addCSS('assets/css/landing/landing.css');
		$this->template->addCSS('assets/wow_slider/engine1/style.css');
		$this->template->addJS('assets/wow_slider/engine1/jquery.js');
		$this->template->addJS('assets/js/raphael.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/jquery.vmap.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/maps/jquery.vmap.usa.js');
		$this->template->addJS('assets/js/landing/landing.js');
		$data['wow_slider'] = $this->load->file('assets/wow_slider/kingsley_slider.html', true);
		redirect($this->template->show('landing', 'home', $data));
	}

	public function __destruct() {
		$this->db->close();
	}

}