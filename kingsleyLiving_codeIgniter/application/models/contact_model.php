<?php
class Contact_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function read_contact_static($flag=0)
	{
		if ($flag == 1) {
			// Contact interest
			$this->db->select('*');
			$this->db->from('contact_interest');
		} else if ($flag == 2) {
			// Contact means
			$this->db->select('*');
			$this->db->from('contact_mean');
		} else if ($flag == 3) {
			// Contact status
			$this->db->select('*');
			$this->db->from('contact_status');
		} else if ($flag == 4) {
			// Contact type
			$this->db->select('*');
			$this->db->from('contact_type');
		} else {
			return;
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function insert_contact()
	{
		$this->load->helper(array('date', 'email'));
		$this->load->library('email');

		$data = array();

		$now = time();
		$mysqlDateStr = '%Y-%m-%d %h:%i:%s';

		$contactData = array( // Array to insert into the contact table
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'contact_interest_id' => $this->input->post('contact_interest'),
			'contact_means_id' => $this->input->post('contact_means'),
			'contact_status_id' => 1,
			'contact_type_id' => 1,
			'comments' => $this->input->post('comments'),
			'created' => mdate($mysqlDateStr, $now)
		);
		foreach ($contactData as $key => $value) {
			if ($key == 'contact_interest_id') {
				$this->db->select('topic_of_interest');
				$this->db->from('contact_interest');
				$this->db->where('id', $value);
				$query = $this->db->get();
				$result = $query->result_array();
				$data['contact_interest'] = $result[0]['topic_of_interest'];
			} else if ($key == 'contact_means_id') {
				$this->db->select('means');
				$this->db->from('contact_mean');
				$this->db->where('id', $value);
				$query = $this->db->get();
				$result = $query->result_array();
				$data['contact_means'] = $result[0]['means'];
			} else if ($key == 'contact_status_id') {
				continue;
			} else if ($key == 'contact_type_id') {
				continue;
			} else {
				$data[$key] = $value;
			}
		}
		$this->db->insert('contact', $contactData);
		$contact_id = $this->db->insert_id(); // Grab and remember the contact id we just inserted

		$addressData = array( // Array to insert into the address table
			'address_1' => $this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'postal_code' => $this->input->post('postal_code'),
			'country' => $this->input->post('country')
		);
		foreach ($addressData as $key => $value) {
			$data[$key] = $value;
		}
		$this->db->insert('address', $addressData);
		$address_id = $this->db->insert_id(); // Grab and remember the address id we just inserted

		$contactAddressData = array( // Array to insert into the contact_address table
			'contact_id' => $contact_id,
			'address_id' => $address_id
		);
		$this->db->insert('contact_address', $contactAddressData);

		$hourDaysData = array( // Array to insert into the hour_days table
			'days' => $this->input->post('days')
		);
		$this->db->insert('hour_days', $hourDaysData);
		$hour_days_id = $this->db->insert_id(); // Grab and remember the hour days id we just inserted

		$hourTimesData = array( // Array to insert into the hour_times table
			'start_time' => $this->input->post('start_time'),
			'end_time' => $this->input->post('end_time')
		);
		$this->db->insert('hour_times', $hourTimesData);
		$hour_times_id = $this->db->insert_id(); // Grab and remember the hour times id we just inserted

		$hourData = array( // Array to insert into the hour table
			'type_id' => $this->input->post('type_id'),
			'days_id' => $hour_days_id,
			'time_id' => $hour_times_id,
			'notes' => $this->input->post('notes')
		);
		$this->db->insert('hour', $hourData);
		$hour_id = $this->db->insert_id(); // Grab and remember the hour id we just inserted

		$contactHoursData = array(
			'contact_id' => $contact_id,
			'hour_id' => $hour_id
		);
		$this->db->insert('contact_hours', $contactHoursData);

		// Email corporate with contact request, also send a copy to the customer
		$this->email->subject();
		$this->email->from();
		$this->email->to($data['email'], '');
		$htmlMessage = $this->load->view('contact/emails/success', $data, true);

		$altMessage = '';
		$altMessage .= 'Successful Submission\n\n';
		$altMessage .= 'Thank you, ' . $data['first_name'] . ' ' . $data['last_name'] . ', the contact form has been submitted.&nbsp;&nbsp;Please allow up to two business days for us to get to your message and respond.\n\n';
		$altMessage .= 'Below is a summary of the contact request that you submitted:\n\n';
		$altMessage .= 'Contact Name: ' . $data['first_name'] . ' ' . $data['last_name'] . '\n';
		$altMessage .= 'Contact Email: ' . $data['email'] . '\n\n';
		$altMessage .= 'Topic of Interest: ' . $data['contact_interest'] . '\n';
		$altMessage .= 'Best Means of Contact: ' . $data['contact_means'] . '\n\n';
		$altMessage .= 'Address: ' . $data['address_1'] . '\n';
		$altMessage .= 'Phone: ' . $data['phone'] . '\n\n';
		$altMessage .= 'Alt Address: ' . (!empty($data['address_2'])?$data['address_2']:'N/A') . '\n';
		$altMessage .= 'City: ' . $data['city'] . '\n\n';
		$altMessage .= 'Zip: ' . $data['postal_code'] . '\n';
		$altMessage .= 'State: ' . $data['state'] . '\n\n';
		$altMessage .= 'Created: ' . $data['created'] . '\n\n';
		$altMessage .= 'Contact Details: ' . $data['comments'];

		$this->email->message($htmlMessage);
		$this->email->set_alt_message($altMessage);
		$this->email->send();
		
		$this->template->addCSS( base_url('assets/css/communications/communications.css') );
		$this->template->show('contact', 'success', $data);
	}

	public function __destruct() {
		$this->db->close();
	}

}