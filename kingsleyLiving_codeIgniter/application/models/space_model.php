<?php
class Space_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();

		// $this->upload_config['upload_path'] = base_url('/assets/images/community');
		// $this->upload_config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		// $this->upload_config['max_size'] = '2048';
		// $this->upload_config['max_filename'] = '255';
		// $this->upload_config['encrypt_name'] = 'TRUE';
	}

	public function read_space($sp_id=FALSE)
	{
		if ($sp_id === FALSE)
		{
			$this->db->select('space.*, address.*, addr_cntry.id as addr_cntry_id, addr_cntry.name as addr_cntry_name, addr_cntry.ISO_code as addr_cntry_ISO');
			$this->db->from('space');
			$this->db->join('space_address', 'space_address.space_id = space.id', 'left');
			$this->db->join('address', 'address.id = space_address.address_id', 'left');
			$this->db->join('address_countries as addr_cntry', 'addr_cntry.id = address.country_id', 'left');
			$query = $this->db->get();
			return $query->result_array();
		} else {
			$this->db->select('space.*, address.*, addr_cntry.id as addr_cntry_id, addr_cntry.name as addr_cntry_name, addr_cntry.ISO_code as addr_cntry_ISO');
			$this->db->from('space');
			$this->db->join('space_address', 'space_address.space_id = space.id', 'left');
			$this->db->join('address', 'address.id = space_address.address_id', 'left');
			$this->db->join('address_countries as addr_cntry', 'addr_cntry.id = address.country_id', 'left');
			$this->db->where('space.id', $sp_id);
			$query = $this->db->get();
			return $query->row_array();
		}
	}

	public function read_space_details($sp_id)
	{
		$this->db->select('*');
		$this->db->from('space_details');
		$this->db->where('space_details.space_id', $sp_id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function read_space_pricing($sp_id)
	{
		$this->db->select('*');
		$this->db->from('space_pricing');
		$this->db->where('space_pricing.space_id', $sp_id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function read_space_amenities($sp_id)
	{
		$this->db->select('*');
		$this->db->from('space_amenities');
		$this->db->where('space_id', $sp_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function read_space_community($comm_id)
	{
		$this->db->select('community.*, address.*, addr_cntry.id as addr_cntry_id, addr_cntry.name as addr_cntry_name, addr_cntry.ISO_code as addr_cntry_ISO');
		$this->db->from('community');
		$this->db->join('community_address', 'community_address.community_id = community.id', 'left');
		$this->db->join('address', 'address.id = community_address.billing_address_id', 'left');
		$this->db->join('address_countries as addr_cntry', 'addr_cntry.id = address.country_id', 'left');
		$this->db->where('community.id', $comm_id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function read_space_community_amenities($comm_id)
	{
		$this->db->select('*');
		$this->db->from('community_amenities');
		$this->db->where('community_id', $comm_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function read_space_static($flag=0)
	{
		if ($flag == 1) {
			// All amenities
			$this->db->select('*');
			$this->db->from('amenities');
			$this->db->join('amenities_icons', 'amenities_icons.amenity_id = amenities.amen_id', 'left');
			$this->db->where('amenities.amen_class_id >=', '3');
		} else if ($flag == 2) {
			// Space Status
			$this->db->select('*');
			$this->db->from('space_status');
		} else if ($flag == 3) {
			// Communities
			$this->db->select('community.id, community.name, community.phone, user_profile.first_name, user_profile.last_name');
			$this->db->from('community');
			$this->db->join('user', 'user.u_id = community.property_manager_id', 'left');
			$this->db->join('user_profile', 'user_profile.uprof_user_id = user.u_id', 'left');
		} else if ($flag == 4) {
			// Space Countries
			$this->db->select('*');
			$this->db->from('address_countries');
		} else {
			return;
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function insert_space()
	{
		// $this->load->library('upload', $this->upload_config);
		$this->load->helper(array('date'));
		$data = array();

		$now = time();
		$mysqlDateTimeStr = '%Y-%m-%d %h:%i:%s';
		$mysqlDateStr = '%Y-%m-%d';

		$spaceData = array(
			'community_id' => $this->input->post('community_id'),
			'spaceid' => 0,
			'space_num' => $this->input->post('space_number'),
			'space_status_id' => $this->input->post('space_status'),
			'notes' => $this->input->post('space_notes'),
			'VIN' => $this->input->post('space_vin'),
			'created' => mdate($mysqlDateTimeStr, $now)
		);
		$community_id = $this->input->post('community_id');
		$this->db->insert('space', $spaceData);
		$space_id = $this->db->insert_id(); // Grab and remember the community id we just inserted

		$spaceDetailsData = array(
			'space_id' => $space_id,
			'manufacturer' => $this->input->post('space_manufacturer'),
			'width' => $this->input->post('space_width'),
			'length' => $this->input->post('space_length'),
			'square_feet' => $this->input->post('space_square_feet'),
			'beds' => $this->input->post('space_beds'),
			'baths' => $this->input->post('space_baths'),
			'year' => $this->input->post('space_year')
		);
		$this->db->insert('space_details', $spaceDetailsData);

		$cls_date = $this->input->post('space_closed_date');
		$year = substr($cls_date,-4);
		$month_day = str_replace('/', '-', substr($cls_date, 0, 5));
		$closed_date = $year . '-' . $month_day;

		$spacePricingData = array(
			'space_id' => $space_id,
			'rent_price' => str_replace(',', '', $this->input->post('space_rent_price')),
			'sale_price' => str_replace(',', '', $this->input->post('space_sale_price')),
			'cash_sale_price' => str_replace(',', '', $this->input->post('space_cash_sale_price')),
			'lease_rate' => str_replace(',', '', $this->input->post('space_lease_rate')),
			'interest_rate' => str_replace(',', '', $this->input->post('space_interest_rate')),
			'yearly_tax_savings' => str_replace(',', '', $this->input->post('space_yearly_tax_savings')),
			'LOC_rate' => str_replace(',', '', $this->input->post('space_loc_rate')),
			'deposit' => str_replace(',', '', $this->input->post('space_deposit')),
			'budgeted_repairs' => str_replace(',', '', $this->input->post('space_budgeted_repairs')),
			'closed_date' => $closed_date
		);
		$this->db->insert('space_pricing', $spacePricingData);

		$spaceAddressData = array( // Array to insert into the address table
			'address_1' => $this->input->post('space_address_1'),
			'address_2' => $this->input->post('space_address_2'),
			'city' => $this->input->post('space_city'),
			'state' => $this->input->post('space_state'),
			'postal_code' => $this->input->post('space_postal_code'),
			'country_id' => $this->input->post('space_country')
		);
		$this->db->insert('address', $spaceAddressData);
		$space_address_id = $this->db->insert_id(); // Grab and remember the address id we just inserted

		$space_addressData = array(
			'space_id' => $space_id,
			'address_id' => $space_address_id
		);
		$this->db->insert('space_address', $space_addressData);

		// Now we take care of amenities
		$amenities = $this->read_space_static(1);
		$amenity_names = array();
		foreach ($amenities as $row_key => $amen_record) {
			$amenity_names[$amen_record['amen_id']] = underscore(str_replace(array('.',','), '', $amen_record['name']));
		}

		foreach ($amenity_names as $id => $name) {
			$amen_post = $this->input->post($name);
			if ($amen_post == false) {
				continue;
			} else {
				$amenityData = array(
					'space_id' => $space_id,
					'amenity_id' => $id
				);
				$this->db->insert('space_amenities', $amenityData);
			}
		}

		// now take care of pictures
		// Redirect to community page
		$this->template->addCSS( base_url('assets/css/community/community.css') );
		$this->template->addJS( base_url('assets/js/community/community.js'));

		redirect($this->template->show('community', 'index'));
	}

	public function load_landingpage($data=null)
	{
		$this->template->addCSS('assets/css/landing/landing.css');
		$this->template->addCSS('assets/wow_slider/engine1/style.css');
		$this->template->addJS('assets/wow_slider/engine1/jquery.js');
		$this->template->addJS('assets/js/raphael.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/jquery.vmap.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/maps/jquery.vmap.usa.js');
		$this->template->addJS('assets/js/landing/landing.js');
		$data['wow_slider'] = $this->load->file('assets/wow_slider/kingsley_slider.html', true);
		redirect($this->template->show('landing', 'home', $data));
	}

	public function __destruct() {
		$this->db->close();
	}

}