<?php
class User_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();

		$this->upload_config['upload_path'] = base_url('/assets/images/user');
		$this->upload_config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		$this->upload_config['max_size'] = '2048';
		$this->upload_config['max_filename'] = '255';
		$this->upload_config['encrypt_name'] = 'TRUE';
	}

	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

	public function read_user_static($flag=0)
	{
		if ($flag == 1) {
			// User Gender
			$this->db->select('*');
			$this->db->from('user_gender');
		} else if ($flag == 2) {
			// User Marital Status
			$this->db->select('*');
			$this->db->from('user_marital_status');
		} else if ($flag == 3) {
			// User Countries
			$this->db->select('*');
			$this->db->from('address_countries');
		} else {
			return;
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function login()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('login_identity', 'Email or Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_password', 'Password', 'trim|required|xss_clean|callback_check_database');

		if ($this->flexi_auth->ip_login_attempts_exceeded())
		{
			$this->form_validation->set_rules('recaptcha_response_field', 'Captcha Answer', 'required|validate_recaptcha');
		}
		
		if ($this->form_validation->run())
		{
			$this->flexi_auth->login($this->input->post('login_identity'), $this->input->post('login_password'));
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

			$this->load_landingpage();
		}
		else
		{	
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}

	public function login_via_ajax()
	{
		echo "Attempting login";
		exit();
		return $this->flexi_auth->login($this->input->post('login_identity'), $this->input->post('login_password'));
	}

	public function activate_account($user_id, $token = FALSE)
	{
		$this->flexi_auth->activate_user($user_id, $token, TRUE);
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

		$this->load_landingpage();
	}

	public function register_account()
	{
		$this->load->library('form_validation');
		$this->load->library('upload', $this->upload_config);

		$validation_rules = array(
			array(
				'field' => 'register_first_name',
				'label' => 'First Name',
				'rules' => 'required'
			),
			array(
				'field' => 'register_last_name',
				'label' => 'Last Name',
				'rules' => 'required'
			),
			array(
				'field' => 'register_phone',
				'label' => 'Phone Number',
				'rules' => 'required'
			),
			array(
				'field' => 'register_email',
				'label' => 'Email Address',
				'rules' => 'required|valid_email|identity_available'
			),
			array(
				'field' => 'register_username',
				'label' => 'Username',
				'rules' => 'required|min_length[4]|identity_available'
			),
			array(
				'field' => 'register_password',
				'label' => 'Password',
				'rules' => 'required|validate_password'
			),
			array(
				'field' => 'register_confirm_password',
				'label' => 'Confirm Password',
				'rules' => 'required|matches[register_password]'
			),
			array(
				'field' => 'register_billing_address_1',
				'label' => 'Street Address',
				'rules' => 'required'
			),
			array(
				'field' => 'register_billing_city',
				'label' => 'City',
				'rules' => 'required'
			),
			array(
				'field' => 'register_billing_state',
				'label' => 'State',
				'rules' => 'required'
			),
			array(
				'field' => 'register_billing_postal_code',
				'label' => 'Zip Code',
				'rules' => 'required'
			),
			array(
				'field' => 'register_billing_country',
				'label' => 'Country',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			// echo "Validation has been run";
			// exit();
			$email = $this->input->post('register_email');
			$username = $this->input->post('register_username');
			$password = $this->input->post('register_password');
			$billing_address_data = array(
				'address_1' => $this->input->post('register_billing_address_1'),
				'address_2' => $this->input->post('register_billing_address_2'),
				'city' => $this->input->post('register_billing_city'),
				'state' => $this->input->post('register_billing_state'),
				'postal_code' => $this->input->post('register_billing_postal_code'),
				'country' => $this->input->post('register_billing_country')
			);
			$this->db->insert('address', $billing_address_data);
			$billing_address_id = $this->db->insert_id(); // Grab and remember the address id we just inserted

			$same_billing_shipping = $this->input->post('register_billing_same_shipping');
			$shipping_address_id = 0;
			if ($same_billing_shipping==false)
			{
				$shipping_address_data = array(
					'address_1' => $this->input->post('register_shipping_address_1'),
					'address_2' => $this->input->post('register_shipping_address_2'),
					'city' => $this->input->post('register_shipping_city'),
					'state' => $this->input->post('register_shipping_state'),
					'postal_code' => $this->input->post('register_shipping_postal_code'),
					'country' => $this->input->post('register_shipping_country')
				);
				$this->db->insert('address', $shipping_address_data);
				$shipping_address_id = $this->db->insert_id(); // Grab and remember the address id we just inserted
			} else {
				$shipping_address_id = $billing_address_id;
			}

			// $user_addressData = array(
			// 	'uadd_user_id' => 0,
			// 	'billing_address_id' => $billing_address_id,
			// 	'shipping_address_id' => $shipping_address_id
			// );
			// $this->db->insert('user_address', $user_addressData);
			// $address_id = $this->db->insert_id();

			$profile_data = array(
				'marital_status_id' => $this->input->post('register_marital_status'),
				'gender_id' => $this->input->post('register_gender'),
				'phone' => trim($this->input->post('register_phone')),
				'image_id' => 0,
				'uprof_account_id' => 0,
				'first_name' => $this->input->post('register_first_name'),
				'last_name' => $this->input->post('register_last_name'),
				'user_status_id' => 1,
				'billing_address_id' => $billing_address_id,
				'shipping_address_id' => $shipping_address_id
			);
			$instant_activate = FALSE;
			$response = $this->flexi_auth->insert_user($email, $username, $password, $profile_data, '', $instant_activate); // Response to this should be the new user id

			if ($response)
			{
				// $add_update = array(
				// 	'uadd_user_id' => $response
				// );
				// $this->db->update('user_address', $add_update, array('uadd_id' => $address_id));
				$email_data = array('identity' => $email);
				$this->flexi_auth->send_email($email, 'Welcome', 'registration_welcome.php', $email_data);
				$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

				if ($instant_activate && $this->flexi_auth->login($email, $password))
				{
					$this->load_landingpage();
				}
				
				$this->load_landingpage();
			}
		}
		$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');

		return FALSE;
	}

	public function resend_activation_token()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('activation_token_identity', 'Identity (Email / Login)', 'required');

		if ($this->form_validation->run())
		{					
			$response = $this->flexi_auth->resend_activation_token($this->input->post('activation_token_identity'));
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			($response) ? redirect('auth') : redirect('auth/resend_activation_token');
		}
		else
		{	
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Resetting Passwords
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function forgotten_password()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('forgot_password_identity', 'Identity (Email / Login)', 'required');

		if ($this->form_validation->run())
		{
			$response = $this->flexi_auth->forgotten_password($this->input->post('forgot_password_identity'));
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			// redirect('auth');
		}
		else
		{
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}

	public function manual_reset_forgotten_password($user_id, $token)
	{
		$this->load->library('form_validation');

		$validation_rules = array(
			array('field' => 'new_password', 'label' => 'New Password', 'rules' => 'required|validate_password|matches[confirm_new_password]'),
			array('field' => 'confirm_new_password', 'label' => 'Confirm Password', 'rules' => 'required')
		);		
		$this->form_validation->set_rules($validation_rules);

		if ($this->form_validation->run())
		{
			$new_password = $this->input->post('new_password');
			$this->flexi_auth->forgotten_password_complete($user_id, $token, $new_password);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			// redirect('auth');
		}
		else
		{
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	// Manage User Account
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function update_account()
	{
		$this->load->library('form_validation');

		$validation_rules = array(
			array('field' => 'update_first_name', 'label' => 'First Name', 'rules' => 'required'),
			array('field' => 'update_last_name', 'label' => 'Last Name', 'rules' => 'required'),
			array('field' => 'update_phone_number', 'label' => 'Phone Number', 'rules' => 'required'),
			array('field' => 'update_newsletter', 'label' => 'Newsletter', 'rules' => 'integer'),
			array('field' => 'update_email', 'label' => 'Email', 'rules' => 'required|valid_email|identity_available'),
			array('field' => 'update_username', 'label' => 'Username', 'rules' => 'min_length[4]|identity_available')
		);
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			$user_id = $this->flexi_auth->get_user_id();
			$profile_data = array(
				'upro_uacc_fk' => $user_id,
				'upro_first_name' => $this->input->post('update_first_name'),
				'upro_last_name' => $this->input->post('update_last_name'),
				'upro_phone' => $this->input->post('update_phone_number'),
				'upro_newsletter' => $this->input->post('update_newsletter'),
				$this->flexi_auth->db_column('user_acc', 'email') => $this->input->post('update_email'),
				$this->flexi_auth->db_column('user_acc', 'username') => $this->input->post('update_username')
			);
			$response = $this->flexi_auth->update_user($user_id, $profile_data);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

			// ($response) ? redirect('auth_public/dashboard') : redirect('auth_public/update_account');
		}
		else
		{
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}

	public function change_password()
	{
		$this->load->library('form_validation');

		$validation_rules = array(
			array('field' => 'current_password', 'label' => 'Current Password', 'rules' => 'required'),
			array('field' => 'new_password', 'label' => 'New Password', 'rules' => 'required|validate_password|matches[confirm_new_password]'),
			array('field' => 'confirm_new_password', 'label' => 'Confirm Password', 'rules' => 'required')
		);
		$this->form_validation->set_rules($validation_rules);

		if ($this->form_validation->run())
		{
			$identity = $this->flexi_auth->get_user_identity();
			$current_password = $this->input->post('current_password');
			$new_password = $this->input->post('new_password');			
			$response = $this->flexi_auth->change_password($identity, $current_password, $new_password);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

			// ($response) ? redirect('auth_public/dashboard') : redirect('auth_public/change_password');
		}
		else
		{
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}
	
	public function send_new_email_activation()
	{
		$this->load->library('form_validation');

		$validation_rules = array(
			array('field' => 'email_address', 'label' => 'Email', 'rules' => 'required|valid_email|identity_available'),
		);
		$this->form_validation->set_rules($validation_rules);

		if ($this->form_validation->run())
		{
			$user_id = $this->flexi_auth->get_user_id();
			$this->flexi_auth->update_email_via_verification($user_id, $this->input->post('email_address'));
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

			// redirect('auth_public/dashboard');
		}
		else
		{		
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}
	
	public function verify_updated_email($user_id, $token)
	{
		$this->flexi_auth->verify_updated_email($user_id, $token);
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

		if ($this->flexi_auth->is_logged_in())
		{
			// redirect('auth_public/dashboard');
		}
		else
		{
			// redirect('auth/login');
		}
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Manage User Address Book --> not sure if we're going to keep this quite yet
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	public function manage_address_book()
	{
		if ($delete_addresses = $this->input->post('delete_address'))
		{
			foreach($delete_addresses as $address_id => $delete)
			{
				$this->flexi_auth->delete_custom_user_data('demo_user_address', $address_id);
			}
		}

		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

		// redirect('auth_public/manage_address_book');
	}

	public function insert_address()
	{
		$this->load->library('form_validation');

		$validation_rules = array(
			array('field' => 'insert_alias', 'label' => 'Address Alias', 'rules' => 'required'),
			array('field' => 'insert_recipient', 'label' => 'Recipient', 'rules' => 'required'),
			array('field' => 'insert_phone_number', 'label' => 'Phone Number', 'rules' => 'required'),
			array('field' => 'insert_address_01', 'label' => 'Address Line #1', 'rules' => 'required'),
			array('field' => 'insert_city', 'label' => 'City / Town', 'rules' => 'required'),
			array('field' => 'insert_county', 'label' => 'County', 'rules' => 'required'),
			array('field' => 'insert_post_code', 'label' => 'Post Code', 'rules' => 'required'),
			array('field' => 'insert_country', 'label' => 'Country', 'rules' => 'required'),
			array('field' => 'insert_company', 'label' => '', 'rules' => ''),
			array('field' => 'insert_address_02', 'label' => '', 'rules' => '')
		);
		$this->form_validation->set_rules($validation_rules);

		if ($this->form_validation->run())
		{
			$user_id = $this->flexi_auth->get_user_id();
			$address_data = array(
				'uadd_alias' => $this->input->post('insert_alias'),
				'uadd_recipient' => $this->input->post('insert_recipient'),
				'uadd_phone' => $this->input->post('insert_phone_number'),
				'uadd_company' => $this->input->post('insert_company'),
				'uadd_address_01' => $this->input->post('insert_address_01'),
				'uadd_address_02' => $this->input->post('insert_address_02'),
				'uadd_city' => $this->input->post('insert_city'),
				'uadd_county' => $this->input->post('insert_county'),
				'uadd_post_code' => $this->input->post('insert_post_code'),
				'uadd_country' => $this->input->post('insert_country')
			);
			// $response = $this->flexi_auth->insert_custom_user_data($user_id, $address_data);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

			// ($response) ? redirect('auth_public/manage_address_book') : redirect('auth_public/insert_address');
		}
		else
		{
			$this->data['message'] = validation_errors('<p class="error_msg">', '</p>');
			return FALSE;
		}
	}

	public function update_address($address_id)
	{
		$this->load->library('form_validation');

		$validation_rules = array(
			array('field' => 'update_alias', 'label' => 'Address Alias', 'rules' => 'required'),
			array('field' => 'update_recipient', 'label' => 'Recipient', 'rules' => 'required'),
			array('field' => 'update_phone_number', 'label' => 'Phone Number', 'rules' => 'required'),
			array('field' => 'update_address_01', 'label' => 'Address Line #1', 'rules' => 'required'),
			array('field' => 'update_city', 'label' => 'City / Town', 'rules' => 'required'),
			array('field' => 'update_county', 'label' => 'County', 'rules' => 'required'),
			array('field' => 'update_post_code', 'label' => 'Post Code', 'rules' => 'required'),
			array('field' => 'update_country', 'label' => 'Country', 'rules' => 'required'),
			array('field' => 'update_company', 'label' => '', 'rules' => ''),
			array('field' => 'update_address_02', 'label' => '', 'rules' => '')
		);		
		$this->form_validation->set_rules($validation_rules);

		if ($this->form_validation->run())
		{
			$address_id = $this->input->post('update_address_id');
			$address_data = array(
				'uadd_alias' => $this->input->post('update_alias'),
				'uadd_recipient' => $this->input->post('update_recipient'),
				'uadd_phone' => $this->input->post('update_phone_number'),
				'uadd_company' => $this->input->post('update_company'),
				'uadd_address_01' => $this->input->post('update_address_01'),
				'uadd_address_02' => $this->input->post('update_address_02'),
				'uadd_city' => $this->input->post('update_city'),
				'uadd_county' => $this->input->post('update_county'),
				'uadd_post_code' => $this->input->post('update_post_code'),
				'uadd_country' => $this->input->post('update_country')
			);
			// $response = $this->flexi_auth->update_custom_user_data('demo_user_address', $address_id, $address_data);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());

			// ($response) ? redirect('auth_public/manage_address_book') : redirect('auth_public/update_address');
		}
		else
		{
			$this->data['message'] = validation_errors('<div class="alert alert-danger" role="alert">', '</div>');
			return FALSE;
		}
	}

	public function load_landingpage($data=null)
	{
		$this->template->addCSS('assets/css/landing/landing.css');
		$this->template->addCSS('assets/wow_slider/engine1/style.css');
		$this->template->addJS('assets/wow_slider/engine1/jquery.js');
		$this->template->addJS('assets/js/raphael.js');
		// $this->template->addJS('assets/js/us-map-1.0.1/jquery.usmap.js'); // old map, not responsive, not being used.
		// $this->template->addJS('assets/js/landing/kmc_map.js'); // configurations for old map
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/jquery.vmap.js');
		$this->template->addJS('assets/js/jqvmap-stable/jqvmap/maps/jquery.vmap.usa.js');
		$this->template->addJS('assets/js/landing/landing.js');
		$data['wow_slider'] = $this->load->file('assets/wow_slider/kingsley_slider.html', true);
		redirect($this->template->show('landing', 'home', $data));
	}

	public function __destruct() {
		$this->db->close();
	}

}