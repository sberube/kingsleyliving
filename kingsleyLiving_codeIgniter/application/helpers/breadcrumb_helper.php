<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

$CI =& get_instance();
if (!function_exists('get_crumb'))
{
    $CI->load->helper('url');
    function get_crumb()
    {
        echo "Got into get_crumb";
        exit();
        $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
        $breadcrumbs = array();

        $last = end(array_keys($path));
        $breadcrumbs[] = '<ol class="breadcrumb">'
        foreach ($path AS $x => $crumb) {
            $title = ucwords(str_replace(array('.php', '_'), Array('', ' '), $crumb));
            if (strtolower($title) == 'kingsleyliving_codeigniter' || strtolower($title) == 'index') {
                $breadcrumbs[] = '<li><a href="'.base_url("index.php").'">Home</a></li>';
            } else if ($x != $last) {
                $breadcrumbs[] = '<a href="'.base_url("index.php/".$crumb).'">'.$title.'</a>';
            }else {
                $breadcrumbs[] = $title;
            }
        }
        $breadcrumbs[] = '</ol>'
        $crumbString = implode('', $breadcrumbs);
        return $crumbString;
    }
}