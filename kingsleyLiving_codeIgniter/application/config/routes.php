<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// Application pages
$route['application/create'] = 'application/new_application';
$route['application/(:any)'] = 'application/view/$1';
$route['application'] = 'application';

// Space pages
$route['space/create'] = 'space/create_space';
$route['space/(:any)'] = 'space/view/$1/$2';
$route['space'] = 'space';

// Amenities pages
$route['amenity/modal'] = 'amenity/modal_new_amenity';
$route['amenity/create'] = 'amenity/new_amenity';
$route['amenity/(:any)'] = 'amenity/index';
$route['amenity'] = 'amenity';

// Community pages
$route['community/create_spaces'] = 'community/create_community_spaces';
$route['community/create'] = 'community/create_community';
$route['community/(:any)'] = 'community/view/$1';
$route['community'] = 'community';

// User pages
$route['user/logout'] = 'user/logout';
$route['user/login_via_ajax'] = 'user/login_via_ajax';
$route['user/login'] = 'user/login';
$route['user/activate_account/(:any)'] = 'user/activate_account/$1';
$route['user/register_user'] = 'user/register_user';
$route['user/register_account'] = 'user/register_account';
$route['user/(:any)'] = 'user/view/$1';
$route['user'] = 'user';

// Contact pages
$route['contact/success'] = 'contact/success';
$route['contact/contact_us'] = 'contact/create_contact';
$route['contact'] = 'contact';

// News pages
$route['news/create'] = 'news/create';
$route['news/(:any)'] = 'news/view/$1';
$route['news'] = 'news';

// Misc pages
$route['pages'] = 'pages';
$route['(:any)'] = 'pages/view/$1';
$route['default_controller'] = "landing/view";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */