$(document).ready(function() {
	$('.datepicker').datepicker({
		format: 'mm-dd-yyyy',
		changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        yearRange: "-100:+0"
	});
});