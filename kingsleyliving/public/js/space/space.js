$(document).ready(function() {
	if ($('div.site-errors').html().trim().length > 0) {
		$('.under-header-errors').removeClass('hidden');
	} else {
		$('.under-header-errors').addClass('hidden');
	}

	if (typeof google != 'undefined')
	{
		var latit = $('.community_latitude').text();
		var longit = $('.community_longitude').text();
		var myCenter = new google.maps.LatLng(latit,longit);
		map_initialize(myCenter);

		var centered = 0;
		$('.maps_nearby').on('click', function() {
			/* Trigger map resize event */
			setTimeout(function() {
				google.maps.event.trigger(map, 'resize');
				if (centered ==0) {
					map_initialize(myCenter);
					centered = 1;
				}
			}, 200);
		});
	}

	$('.datepicker').datepicker({
		format: 'mm-dd-yyyy',
		changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        yearRange: "-100:+0"
	});

	$('.search-reset-btn').on('click', function() {
		var url = window.location.href;
		var origurl = url.split("?")[0];
		var querystring = url.slice(url.indexOf('?') + 1);
		var queries = parseQueryString(querystring);
		var newqstring = new Array();
		$.each(queries, function(index, value) {
			newqstring.push(index);
		});
		var qstring = newqstring.join('=&');
		var newurl = origurl + '?' + qstring;
		window.location.href = newurl;
	});

	$.each($('.password'), function() {
		$(this).hide();
	});

	$('#searchresults_table tbody').on( 'click', 'tr', function () {
        var community_id = $(this).find('.hidden').children('.community_id').text().trim();
		var space_id = $(this).find('.hidden').children('.space_id').text().trim();
		window.location.href = ROOT + '/' + community_id + '/space/' + space_id + '/view';
    } );

	$('.username').hide();

	$('#email').on('change', function() {
		//  Ajax to check against database and see if the email already exists, if it does, then show the username and fill it in.
		var email = $(this).val();
		var sendURL = ROOT + '/api/v1/internal/user/' + email;

		$.get(sendURL, function( data ) {
			// console.log(data);
			if (data['error'] == false)
			{
				var user = $.parseJSON(data['user']);
				// console.log(user);
				$('label[for=username]').html('Username:');
				$('.username').val(user['username']).show();
				$('.apply_user_name').html('').hide();
				$('input[name=first_name]').val('');
				$('input[name=middle_name]').val('');
				$('input[name=last_name]').val('');
				$('input[name=suffix]').val('');
				$('.phone').val(user['user_profile']['phone']);
				var full_name = user['user_profile']['first_name'] + ( user['user_profile']['middle_name'] != null ? ' ' + user['user_profile']['first_name'] + ' ' : ' ' ) + user['user_profile']['last_name'] + ( user['user_profile']['suffix'] != null ? ' ' + user['user_profile']['suffix'] : '' );
				$('.user_name').val(full_name);
				if (user['user_profile']['gender'] == 'Male')
				{
					$('select[name=gender]').val('mr').trigger('chosen:updated');
				}
				else
				{
					if (user['user_profile']['marital_status_id'] == 2)
					{
						$('select[name=gender]').val('mrs').trigger('chosen:updated');
					}
					else
					{
						$('select[name=gender]').val('miss').trigger('chosen:updated');
					}
				}
				var email_group = $('#email').closest('div');
				email_group.append('<input type="hidden" name="user_id" value="'+ user['id'] +'">');

				if ( user['address'].length > 0 )
				{
					console.log(user['address']);
					var address = user['address'][0];
					$('.address_1').val(address['address_1']);
					$('.address_2').val(address['address_2']);
					$('.city').val(address['city']);
					$('.state').val(address['state']);
					$('.postal_code').val(address['postal_code']);
					$('.country').val(address['country_id']).trigger('chosen:updated');
				}

				$.each($('.password'), function() {
					$(this).remove();
				});
			}
			else
			{
				$('.apply_user_name').html('').hide();
				$('input[name=first_name]').val('');
				$('input[name=middle_name]').val('');
				$('input[name=last_name]').val('');
				$('input[name=suffix]').val('');
				$('label[for=username]').html('Desired username:');
				$('.username').val('').show();
				$('.phone').val('');
				$('.user_name').val('');
				$('input[name=user_id]').remove();
				$('.address_1').val('');
				$('.address_2').val('');
				$('.city').val('');
				$('.state').val('');
				$('.postal_code').val('');
				$('.country').val('').trigger('chosen:updated');
				$('select[name=gender]').val('mr').trigger('chosen:updated');
				if ($('.password').length == 0)
				{
					var password_elem = '<div class="col-xs-12 form-group password" style="display: none;">';
					password_elem += '<label for="password">Password:</label>';
					password_elem += '<input type="password" id="password" value="" name="password" class="form-control">';
					password_elem += '</div>';
					var password_conf_elem = '<div class="col-xs-12 form-group password" style="display: none;">';
					password_conf_elem += '<label for="password_confirmation">Password Confirmation:</label>';
					password_conf_elem += '<input type="password" id="password_confirmation" value="" name="password_confirmation" class="form-control">';
					password_conf_elem += '</div>';
					$('#applicant_info').append(password_elem);
					$('#applicant_info').append(password_conf_elem);
					$.each($('.password'), function() {
						$(this).show();
					});
				}
				else
				{
					$.each($('.password'), function() {
						$(this).show();
					});
				}
			}
		});
	});
});