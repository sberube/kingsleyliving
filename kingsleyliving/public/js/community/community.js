$(document).ready(function() {
	if ($('div.site-errors').html().trim().length > 0) {
		$('.under-header-errors').removeClass('hidden');
	} else {
		$('.under-header-errors').addClass('hidden');
	}

	if ( $('#toggle_comm_address').is(':checked') ) {
		$('.community_address').show();
	} else {
		$('.community_address').hide();
	}

	$('#toggle_comm_address').click(function() {
		if ( $(this).is(':checked') ) {
			$('.community_address').show();
		} else {
			$('.community_address').hide();
		}
	});

	if ( $('#toggle_comm_amenities').is(':checked') ) {
		$('.list_amenities').show();
	} else {
		$('.list_amenities').hide();
	}

	$('#toggle_comm_amenities').click(function() {
		if ( $(this).is(':checked') ) {
			$('.list_amenities').show();
		} else {
			$('.list_amenities').hide();
		}
	});

	$('.community_name').change(function() {
		var curInput = $(this).val().toLowerCase().replace(' ', '_');
		var finalInput = 'http://www.kingsleyliving.com/public/index.php/community/' + curInput;
		$('.home_page').val(finalInput);
	});
	$('#add_images_btn').click(function() {
		if ($('.community_name').val().length == 0)
		{
			alert('Please fill in community name');
		}
		else
		{
			var community_name = $('.community_name').val().toLowerCase().replace(' ', '_');
			$('.modal-body input[name=community_name]').val(community_name);
			var location = '/images/community/' + community_name + '/';
			$('.modal-body input[name=location]').val(location);
			// addImagesToCarousel(); // AddImagesToCarousel needs work, needs to be switched to non-bootstrap plugin (Bootstrap is too much hassle to setup)
			$('#newCommunityImages').modal({
				keyboard: true,
				show: true
			});
		}
	});

	$('#community_hours_btn').on('click', function() {
		$('#community_hours').modal({
			keyboard: true,
			show: true
		});
	});

	var url = window.location.href;
	uSplit = url.split('/');

	if (uSplit.indexOf('create') > 0)
	{
		$('#community_hours').children('.edit').hide();
	}
	else if (uSplit.indexOf('edit') > 0)
	{
		$('#community_hours').children('.create').hide();
	}
	else
	{
		$('#community_hours').children('.edit').hide();
		$('#community_hours').children('.create').hide();
	}

	if (uSplit.indexOf('create') > 0 || uSplit.indexOf('edit') > 0)
	{
		Dropzone.autoDiscover = false;

		var dropzoneObj = new Dropzone('input#comm_images', {
			init: function() {
				this.on('addedfile', function(file) {
					generateNameDesc(file);
				});
				this.on('removedfile', function(file) {
					removeNameDesc(file);
				});
				this.on('complete', function(file) {
					this.removeFile(file);
				});
				this.on('success', function(file, response) {
					var json_image_id = $('input[name=json_image_id]');
					var json = $.parseJSON(json_image_id.val());
					json['add'].push(response['image_id']);
					var imjson = JSON.stringify(json);
					json_image_id.val(imjson)
					json_image_id.trigger('change');
				});
				this.on('sending', function(file, xhr, formData) {
					var location = fetchLocation();
					var filename = file['name'];
					var dot = filename.indexOf('.');
					var assocname = filename.substr(0, dot).replace(/\s/g, '_').toLowerCase();
					var display_name = fetchDisplayName(filename, assocname);
					var description = fetchDescription(assocname);
					formData.append('location', location);
					formData.append('display_name', display_name);
					formData.append('description', description);
				});
			},
			url:ROOT+'/api/v1/internal/image',
			parallelUploads:10,
			maxFilesize:2048,
			paramName: 'image',
			uploadMultiple: false,
			addRemoveLinks:true,
			previewsContainer:'.dropzone-previews',
			clickable:true,
			createImageThumbnails:true,
			maxFiles:10,
			acceptedFiles:'image/*',
			autoProcessQueue:false
		});
	}

	$('.modal-reset-btn').on('click', function() {
		dropzoneObj.removeAllFiles();
	});

	$('.modal-submit-btn').on('click', function() {
		$('#create-community-images').submit();
	});

	$('#create-community-images').submit(function(e) {
		dropzoneObj.processQueue();
		e.preventDefault();
	});

	$('input[name=json_image_id]').on('change', function() {
		// addImagesToCarousel();
	});

	// $('.jumbotron').css('background', 'url(/assets/example/...jpg)', 'no-repeat', 'center', 'center');

	// var amenitiesJSON = $('input:hidden[name="jsTooltips"]').val();
	// var amenities = $.parseJSON(amenitiesJSON);
	// $.each(amenities, function(key, value) {
		// code...$('#example').tooltip(options)
		// $('#'+value).tooltip({
		// 	'title' : value,
		// 	'placement' : 'bottom',
		// 	'trigger' : 'hover'
		// 	// 'selector' : '#'+key
		// });
		// $('#'+value).tooltip();
		// console.log('Element ID: ' + key);
		// console.log('Element ID: ' + value);
	// });
});

function fetchLocation()
{
	var location = $('#create-community-images input[name=location]').val();
	return location;
}

function fetchDisplayName(filename, name)
{
	var display_name = $('#'+name+'_d_n').val();
	if (!display_name.length > 0)
	{
		display_name = filename;
	}
	return display_name;
}

function fetchDescription(name)
{
	var description = $('#'+name+'_desc').val();
	if (!description.length > 0)
	{
		description = 'No description given';
	}
	return description;
}

function generateNameDesc(file)
{
	var filename = file['name'];
	var dot = filename.indexOf('.');
	var assocname = filename.substr(0, dot).replace(/\s/g, '_').toLowerCase();

	var divtemplate = '<div class="col-xs-12 col-md-6 form-group">';
	var label_displayname_template = '<label for="display_name">Give "'+filename.substr(0, dot)+'" a new name:</label>';
	var input_displayname_template = '<input class="form-control display_name" type="text" name="display_name" id="'+assocname+'_d_n">';
	var label_description_template = '<label for="description">Describe "'+filename.substr(0, dot)+'":</label>';
	var input_description_template = '<input class="form-control description" type="text" name="description" id="'+assocname+'_desc">';

	var image_data = $('.image_data');
	var old_data = image_data.html();
	image_data.html(old_data + divtemplate + label_displayname_template + input_displayname_template + '</div>' + divtemplate + label_description_template + input_description_template + '</div>');
}

function removeNameDesc(file)
{
	var filename = file['name'];
	var dot = filename.indexOf('.');
	var assocname = filename.substr(0, dot).replace(/\s/g, '_').toLowerCase();

	$('#'+assocname+'_d_n').parent('.form-group').remove();
	$('#'+assocname+'_desc').parent('.form-group').remove();
}

function addImagesToCarousel()
{
	var json_image_id = $('input[name=json_image_id]').val();
	var json = $.parseJSON(json_image_id);
	console.log(json);
	if (json['images'].length > 0 || json['add'].length > 0)
	{
		for (var i = 0; i < json['images'].length; i++) {
			$.get(
				ROOT+'/api/v1/internal/image/'+json['images'][i],
				function(data) {
					var response = data['image'];
					$.post(ROOT+'/api/v1/internal/image/retrieveImgUrl', {location:response['location']}, function(data) {
						var changeRoot = ROOT.split('/');
						changeRoot.splice(-1,1);
						var newRoot = changeRoot.join('/');
						var location_json = data;
						var img_location = location_json['location'];
						var imgtemplate = '<div><img src="'+newRoot+'/'+img_location+response['file_name']+'" alt="'+response['display_name']+'"></div>';
						$('.community_images_carousel').append(imgtemplate);
					});
			});
		};
		$('.community_images_carousel').parent('.modal-body').show();
	}
	else
	{
		console.log('No images to show in json');
		$('.community_images_carousel').parent('.modal-body').hide();
	}
}