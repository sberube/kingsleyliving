$(window).resize(function() {
	var windowWidth = $(window).width();
	if (windowWidth >= 1200){
		$('div.kingsley_map').removeClass('kingsley_map-md').addClass('kingsley_map-lg');
	} else if (windowWidth >= 992 && windowWidth < 1200){
		$('div.kingsley_map').removeClass('kingsley_map-lg');
		$('div.kingsley_map').removeClass('kingsley_map-sm');
		$('div.kingsley_map').addClass('kingsley_map-md');
	} else if (windowWidth >= 768 && windowWidth < 992){
		$('div.kingsley_map').removeClass('kingsley_map-md');
		$('div.kingsley_map').removeClass('kingsley_map-xs');
		$('div.kingsley_map').addClass('kingsley_map-sm');
	} else {
		$('div.kingsley_map').removeClass('kingsley_map-sm').addClass('kingsley_map-xs');
	}
});
$(document).ready(function() {
	$('.map-legend').hide();
	$('.map-reset').hide();
	$('.map_open_rollover_bar').hide();
	$('.open-search').on('click', function(){
		$('.map_open_rollover_bar').fadeIn();
		$('.map-header').fadeOut();
		$('.map-footer').fadeOut();
		$('.open-search').fadeOut();
	});
	var windowWidth = $(window).width();
	if (windowWidth >= 1200){
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-lg');
	} else if (windowWidth >= 992 && windowWidth < 1200){
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-md');
	} else if (windowWidth >= 768 && windowWidth < 992){
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-sm');
	} else {
		$('div.kingsley_map').removeClass('kingsley_map-default').addClass('kingsley_map-xs');
	}
	var highlighted_states = {
		az : '#28ABDC', //A366E0
		ca : '#28ABDC',
		co : '#28ABDC',
		id : '#28ABDC',
		mi : '#28ABDC',
		nv : '#28ABDC',
		nm : '#28ABDC',
		ny : '#28ABDC',
		tx : '#28ABDC',
		ut : '#28ABDC'
	};

	$('.map-reset').on('click', function() { map_reset(); });
	var operationStates = ['az', 'ca', 'co', 'id', 'mi', 'nv', 'nm', 'ny', 'tx', 'ut'];
	var class_map = {
		AZ : 'arizona',
		CA : 'california',
		CO : 'colorado',
		ID : 'idaho',
		MI : 'michigan',
		NV : 'nevada',
		NM : 'new_mexico',
		NY : 'new_york',
		TX : 'texas',
		UT : 'utah'
	};
	var win_loc = window.location.href;
	var url = '';
	var start = win_loc.length - 1;
	var end = win_loc.length;
	if (win_loc.substring(start,end) == '/')
	{
		var end = win_loc.length - 1;
		url = win_loc.substring(0, end);
	}
	else
	{
		url = win_loc;
	}
	if (url == ROOT)
	{
		$('.kingsley_map').vectorMap({
			map: 'usa_en',
			backgroundColor: null,
			color: '#595C5F', // '#BCBEC0', // 531690
			colors: highlighted_states,
			hoverColor: '#BCBEC0', // '#73C9E8', // '#F3B6FF', // '#F38EFF', // '#DFA2FF',// '#CB8EFF',
			selectedColor: '#A7DDF1', // '#73C9E8', // '#27d4db', // '#FFFF00',
			enableZoom: false,
			showTooltip: false,
			normalizeFunction: 'linear',
			onLabelShow: function(element, label, code)
			{
				if (operationStates.indexOf(code) <= -1)
				{
					element.preventDefault();
				}
			},
			onRegionOver: function (element, code, region)
			{
				if (operationStates.indexOf(code) <= -1)
				{
					element.preventDefault();
				}
			},
			onRegionClick: function(element, code, region)
			{
				if (operationStates.indexOf(code) <= -1)
				{
					element.preventDefault();
				} else {
					var regiontimeout;
					if ($('.map-legend').is(':visible'))
					{
						$('.map-legend').fadeOut(200);
						regiontimeout = setTimeout(function(){
							$('.map-header').fadeOut();
							$('.map-footer').fadeOut();
							$('.open-search').fadeOut();
							// $('.map-reset').fadeIn();
							var ISO_code = code.toUpperCase();
							if ($('.json_last_selected').text().length > 0) {
								var last_selected = $('.json_last_selected').text();
								$('.map-legend').removeClass(class_map[last_selected]);
							}
							$('.json_last_selected').html(ISO_code);
							var state = region.toLowerCase();
							var sendURL = ROOT + '/api/v1/internal/community/' + state;

							$.ajax({
								type: "GET",
								url: sendURL,
								dataType: "json",
								success: function(data) {
									var success = build_us_map_legend(data.communities, region);
									if (success == 'success')
									{
										$('.map-legend').addClass(class_map[ISO_code]).fadeIn();
									}
								}	
							});
						}, 400);
					}
					else
					{
						$('.map-header').fadeOut();
						$('.map-footer').fadeOut();
						$('.open-search').fadeOut();
						// $('.map-reset').fadeIn();
						var ISO_code = code.toUpperCase();
						if ($('.json_last_selected').text().length > 0) {
							var last_selected = $('.json_last_selected').text();
							$('.map-legend').removeClass(class_map[last_selected]);
						}
						$('.json_last_selected').html(ISO_code);
						var state = region.toLowerCase();
						var sendURL = ROOT + '/api/v1/internal/community/' + state;

						$.ajax({
							type: "GET",
							url: sendURL,
							dataType: "json",
							success: function(data) {
								var success = build_us_map_legend(data.communities, region);
								if (success == 'success')
								{
									$('.map-legend').addClass(class_map[ISO_code]).fadeIn();
								}
							}	
						});
					}
					// clearTimeout(regiontimeout);
				}
			}
		});
	}
});

function search_close()
{
	$('.map_open_rollover_bar').fadeOut();
	$('.map-header').fadeIn();
	$('.map-footer').fadeIn();
	$('.open-search').fadeIn();
}

function map_reset()
{
	$('.map-legend').fadeOut();
	$('.map-header').fadeIn();
	$('.map-footer').fadeIn();
	$('.open-search').fadeIn();
	// $('.map-reset').fadeOut();
	var selected_state = $('.json_last_selected').html().toLowerCase();
	$('.kingsley_map').vectorMap('deselect', selected_state);
}

function build_us_map_legend(communities, region)
{
	var default_title = $('.legend-title-default').html();
	var close = '<div class="legend-close" onclick="map_reset()"><span class="icon ion-close-round"></span></div>';// '<button type="button" class="close" data-dismiss="map-legend"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';// '<a class="map-close"><span class="icon ion-close-round"></span></a>';
	var community_href = ROOT + '/community/';
	$('.legend-labels').empty();
	$.each(communities, function(key, community) {
		var list_item = '<li><a href="' + (community_href + community['id']) + '/view">' + community['name'] + '</a></li>';
		var current_contents = $('.legend-labels').html();
		$('.legend-labels').html(current_contents + list_item);
	});
	$('.legend-title').html(region + ' ' + default_title + ' ' + close);
	return 'success';
}