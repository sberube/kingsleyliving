{{ Form::open(array('action' => 'SpaceController@searchresults', 'role' => 'form')) }}
	<fieldset class="col-xs-12 in-page-search" id="in_page_seach">
		<div class="row" style="margin-top:0;">
			<div class="col-xs-12" style="margin-top:0;padding:0;">
				<div class="panel" style="margin:0;">
					<div class="panel-heading"><b>Edit Location</b><div class="pull-right search-form-close" onclick="search_close()"><span class="icon ion-close-round"></span></div></div>
					<div class="panel-body" style="margin-top:0;padding-top:0;">
						<div class="row" style="margin-top:0;">
							<div class="col-xs-12 col-md-3 form-group" style="margin-top:0;">
								{{ Form::label('state', 'State:') }}
								{> $states = array('Arizona', 'California', 'Colorado', 'Idaho', 'Michigan', 'Nevada', 'New Mexico', 'New York', 'Texas', 'Utah') <}
								{{ Form::select('state', $states, '', array('class' => 'form-control chosen-select')) }}
							</div>
							<div class="col-xs-12 col-md-3 form-group" style="margin-top:0;">
								{{ Form::label('city', 'City:') }}
								{> $cities = array('Salt Lake City', 'Denver', 'Dallas') <}
								{{ Form::select('city', $cities, '', array('class' => 'form-control chosen-select')) }}
							</div>
							<div class="col-xs-12 col-md-1 form-group" style="margin-top:0;">
								Or
							</div>
							<div class="col-xs-12 col-md-2 form-group" style="margin-top:0;">
								{{ Form::label('zip', 'Zip:') }}
								{{ Form::text('zip', '', array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-3 form-group" style="margin-top:0;">
								{{ Form::label('radius', 'Radius:') }}
								{> $radius = array('5 miles', '15 miles', '50 miles') <}
								{{ Form::select('radius', $radius, '', array('class' => 'form-control chosen-select')) }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-3 form-group" style="margin-top:5px;">
				{{ Form::label('price_min', 'Price Min:') }}
				{> $price_min = array('$10,000', '$50,000', '$100,000') <}
				{{ Form::select('price_min', $price_min, '', array('class' => 'form-control chosen-select')) }}
			</div>
			<div class="col-xs-12 col-md-3 form-group" style="margin-top:5px;">
				{{ Form::label('price_max', 'Price Max:') }}
				{> $price_max = array('$50,000', '$100,000', '$200,000') <}
				{{ Form::select('price_max', $price_max, '', array('class' => 'form-control chosen-select')) }}
			</div>
			<div class="col-xs-12 col-md-3 form-group" style="margin-top:5px;">
				{{ Form::label('beds', 'Beds:') }}
				{> $beds = array('> 1', '1 - 2', '2+') <}
				{{ Form::select('beds', $beds, '', array('class' => 'form-control chosen-select')) }}
			</div>
			<div class="col-xs-12 col-md-3 form-group" style="margin-top:5px;">
				{{ Form::label('baths', 'Baths:') }}
				{> $baths = array('1', '2', '3') <}
				{{ Form::select('baths', $baths, '', array('class' => 'form-control chosen-select')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group" style="margin-top:0;">
				{{ Form::label('home_type', 'Home Type:') }}
				<div class="radio">
					<label class="radio-inline">
						{{ Form::radio('home_type', 'All', true) }} All
					</label>
					<label class="radio-inline">
						{{ Form::radio('home_type', 'New') }} New
					</label>
					<label class="radio-inline">
						{{ Form::radio('home_type', 'Pre-Owned') }} Pre-Owned
					</label>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group" style="margin-top:0;">
				{{ Form::label('amenities', 'Common Community Amenities:') }}
				{{ Form::text('amenities', '', array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-4 form-group" style="margin-top:0;">
				{{ Form::label('community_type', 'Community Type:') }}
				{> $community_type = array('55+', 'All age') <}
				{{ Form::select('community_type', $community_type, '', array('class' => 'form-control chosen-select')) }}
			</div>
			<div class="col-xs-12 col-md-4 form-group" style="margin-top:0;">
				{{ Form::label('community', 'Community:') }}
				{> $community = array('Shadow Ridge', 'Carefree', 'Country Meadows') <}
				{{ Form::select('community', $community, '', array('class' => 'form-control chosen-select')) }}
			</div>
			<div class="col-xs-12 col-md-4 form-group search-submit">
				<!-- Button to update or begin search -->
				{{ Form::submit('Submit', array('class' => 'btn btn-primary search-submit-btn')) }}
			</div>
		</div>
	</fieldset>
{{ Form::close() }}