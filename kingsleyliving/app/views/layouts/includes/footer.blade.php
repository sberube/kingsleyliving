<div class="footer">
	<div class="container-fluid">
		<div class="hidden-xs col-md-3">
			<div class="panel footer-links">
				<div class="panel-heading">Explore</div>
				@include('layouts.includes.footer_links')
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="panel footer-news">
				<div class="panel-heading">Latest News<div class="pull-right">{{ HTML::link('/news', 'All News') }}</div></div>
				<div class="panel-body">
					@if (!empty($latestNews))
						<div class="news-articles">
						@foreach ($latestNews as $key => $news)
							<div class="article-title">{{ $news['title'] }}</div>
							<div class="article-content">{{ $news['content'] }}</div>
						@endforeach
						</div>
					@else
						<div>No Recent News</div>
					@endif
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-3">
			<div class="panel footer-contact">
				<div class="panel-heading">Contact Kingsley Management Corp</div>
				<div class="panel-body">
					<div class="col-xs-12">
						<div class="row">
							4956 N 300 W Ste 200
						</div>
						<div class="row">
							Provo, UT 84604
						</div>
						<div class="row">
							(801) 228-9702
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid footer-sliver">
		<div class="col-xs-4">{{ HTML::link('/terms_and_conditions', 'Terms and Conditions') }}</div>
		<div class="col-xs-4">{{ HTML::link('/privacy_policy', 'Privacy Policy') }}</div>
		<div class="col-xs-4">{{ HTML::link('/sitemap', 'Sitemap') }}</div>
	</div>
</div>