<div class="navbar-header">
	<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
		<span class="sr-only">Toggle Navigation</span>
		<span class="icon ion-navicon-round"></span>
	</button>
	<a class="header-logo" href="{{ url('/') }}">{{ HTML::image('images/KMC_new_logo.png', 'Kingsleyliving Logo') }}</a>
</div>
<div class="navbar-collapse collapse">
	<ul class="nav navbar-left navbar-nav">
		<li class="home">
			{{ HTML::link('/', 'Home') }}
		</li>
		<li class="buyahome">
			{{ HTML::link('/buyahome', 'Buy a Home') }}
		</li>
		<li class="rentahome">
			{{ HTML::link('/rentahome', 'Rent a Home') }}
		</li>
		<li class="leaseoptions">
			{{ HTML::link('/leaseoptions', 'Lease Options') }}
		</li>
		<li class="findahomesite">
			{{ HTML::link('/findahomesite', 'Find a Home Site') }}
		</li>
	</ul>
	@if (Auth::check())
	<div class="navbar-form navbar-right sign-in">
		<div class="btn-group">
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				{{ $full_name }}&nbsp;<span class="caret"></span>
			</button>
			<div class="dropdown-menu">
				<div class="col-sm-12">
					<div class="col-sm-12">
						 {{-- HTML::link('#', 'Account', array('data-toggle' => 'tooltip', 'title' => 'Coming soon!')) --}}
						{{ HTML::link('/user/'.Auth::user()->id.'/view/account', 'Account', array('data-toggle' => 'tooltip', 'title' => 'Coming soon!')) }}
					</div>
					<div class="col-sm-12">
						{{ HTML::link('/user/'.Auth::user()->id.'/view/profile', 'Profile') }}
					</div>
					<div class="col-sm-12">
						{{ HTML::link('/users/logout?redirectto='.Request::url(), 'Log out') }}
					</div>
				</div>
			</div>
		</div>
	</div>
	@else
	<button type="button" class="btn btn-primary navbar-right navbar-register" onclick="location.href='{{ url('/user/create?redirectto='.Request::url()) }}'">Register</button>
	{{ Form::open(array('url' => 'users/login', 'method' => 'post', 'class' => 'navbar-form navbar-right sign-in', 'role' => 'login')) }}
		<div class="form-group">
			{{ Form::text('identity', '', array('class' => 'form-control', 'id' => 'identity', 'placeholder' => 'Email or Username')) }}
		</div>
		<div class="form-group">
			{{ Form::password('password', array('class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password')) }}
			{{ Form::hidden('redirectto', Request::url()) }}
		</div>
		{{ Form::submit('Log In', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
	@endif
</div>