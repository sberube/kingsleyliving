<link rel="shortcut icon" href="{{ public_path() }}/images/kmc_logo_favicon.ico" type="image/x-icon">
<link rel="icon" href="{{ public_path() }}/images/kmc_logo_favicon.ico" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>KingsleyLiving - {{ $title }}</title>
{{ HTML::style('js/chosen_v1.2.0/chosen.css') }}
{{ HTML::style('css/main.css') }}
{{ HTML::style('bootstrap-3.2.0/css/bootstrap.css') }}
{{ HTML::style('bootstrap-3.2.0/css/elusive-webfont.css') }}
{{ HTML::style('bootstrap-3.2.0/css/ionicons.min.css') }}
@if (!empty($stylesheetLinks))
	@foreach ($stylesheetLinks as $stylesheet)
		{{ $stylesheet }}
	@endforeach
@endif
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<script type="text/javascript">
	var BASE_PATH = '{{ base_path() }}';
	var APP_PATH = '{{ app_path() }}';
	var PUBLIC_PATH = '{{ public_path() }}';
	var ROOT = '{{ URL::to("/") }}';
</script>
{{ HTML::script('js/jquery-2.1.1.js') }}
{{ HTML::script('js/jquery-ui-1.11.0.notheme/jquery-ui.js') }}
{{ HTML::script('bootstrap-3.2.0/js/bootstrap.min.js') }}
{{ HTML::script('js/chosen_v1.2.0/chosen.jquery.js') }}
{{ HTML::script('js/jquery-number/jquery.number.js') }}
{{ HTML::script('js/modernizr-latest.js') }}
{{ HTML::script('js/main.js') }}
@if (!empty($jsLinks))
	@foreach ($jsLinks as $jsFile)
		{{ $jsFile }}
	@endforeach
@endif