<ul class="list-group footer-link-list">
	<li class="list-group-item">
		{{ HTML::link('#', 'Join Our Team') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Acquisitions') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Investors') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('/contact/create', 'Contact Us') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Buy a Home') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Rent a Home') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Lease Options') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Find a Home Site') }}
	</li>
</ul>