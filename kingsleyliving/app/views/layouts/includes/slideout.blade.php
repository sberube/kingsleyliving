{{-- Slide outs for the right side go here --}}
{{ HTML::link('#', 'Links', array('id' => 'linkstrigger', 'class' => 'trigger right')) }}
<div id="linkspanel" class="sl_panel right">
	<ul class="list">
		<li>{{ HTML::link('/join_kingsley/open_positions', 'Open Positions') }}</li>
		<li>{{ HTML::link('#', 'Acquisitions') }}</li>
		<li>{{ HTML::link('#', 'Investors') }}</li>
		<li>{{ HTML::link('/community', 'Our Communities') }}</li>
		<li>{{ HTML::link('/about', 'About Us') }}</li>
		<li>{{ HTML::link('/contact/create', 'Contact Us') }}</li>
	</ul>
</div>
@if (!empty($slide_right))
	@foreach ($slide_right as $sl_right_panel)
		{{ $sl_right_panel }}
	@endforeach
@endif

{{ HTML::link('#', 'Notifications', array('id' => 'notificationstrigger', 'class' => 'trigger left')) }}
<div id="notificationspanel" class="sl_panel left">
	@if (!empty($notifications))
		{{ $notifications }}
	@else
		This panel is currently empty
	@endif
</div>
@if (!empty($slide_left))
	@foreach ($slide_left as $sl_left_panel)
		{{ $sl_left_panel }}
	@endforeach
@endif