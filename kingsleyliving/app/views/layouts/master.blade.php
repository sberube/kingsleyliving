<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.includes.head', array('title' => $page_title, 'stylesheetLinks' => $page_stylesheets, 'jsLinks' => $page_scriptlinks))
	</head>
	<body>
		<!-- Site header -->
		<div class="navbar navbar-default navbar-fixed-top site-menu" role="navigation">
			<div class="container-fluid">
				@include('layouts.includes.header', array('full_name' => $user_name))
				<!-- ('kingsley_logo' => $kingsley_logo) -->
			</div>
		</div>
		<!-- Any site errors should show up here... ideally -->
		<div class="container-fluid under-header-errors">
			<div class="col-xs-12">
				<div class="container-fluid">
					<div class="col-xs-12">
						<div class="panel">
							<div class="panel-body site-errors site-errors-default" data-spy="affix">
								@if (Session::get('error'))
									{> $ses_errors = Session::get('error') <}
								    <div class="alert alert-danger">
								        @if (is_array($ses_errors)))
								        	@foreach ($ses_errors as $error)
								        		{{ $error }}
								        		<br />
								            	{{ head($error) }}
								            @endforeach
								        @else
								        	{{ $ses_errors }}
								        @endif
								    </div>
								@endif

								@if (Session::get('notice'))
									{> $notices = Session::get('notice') <}
							    	<div class="alert alert-info">
								        @if (is_array($notices))
								            @foreach ($notices as $notice)
								            	{{ $notice }}
								        		<br />
								            	{{ head($notice) }}
								            @endforeach
								        @else
								        	{{ $notices }}
								        @endif
								    </div>
								@endif

								@if (Session::get('warning'))
									{> $warnings = Session::get('warning') <}
									<div class="alert alert-warning">
								        @if (is_array($warnings))
								            @foreach ($warnings as $warning)
								            	{{ $warning }}
								        		<br />
								            	{{ head($warning) }}
								            @endforeach
								        @else
								        	{{ $warnings }}
								        @endif
								    </div>
								@endif

								@if (Session::get('success'))
									{> $messages = Session::get('success') <}
									<div class="alert alert-success">
								        @if (is_array($messages))
								            @foreach ($messages as $message)
								            	{{ $message }}
								        		<br />
								            	{{ head($message) }}
								            @endforeach
								        @else
								        	{{ $messages }}
								        @endif
								    </div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Main body content -->
		<div class="container-fluid main-body main-body-default">
			@include('layouts.includes.slideout', array('slide_right' => $slide_right, 'slide_left' => $slide_left, 'notifications' => $notifications))
			<div class="body-content">
				@yield('before_body')
				<div class="main-page-content">
					@yield('content')
				</div>
			</div>
		</div>
		@include('layouts.includes.footer', array('latestNews' => $news))
	</body>
</html>