@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="user_profile_view">
			<div class="panel-heading"><h2><b>{{ $title }}</b></h2></div>
			<div class="panel-body">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-md-6 user_image">
							@if (!empty($location))
							{{ HTML::image($location.$user['user_profile']['image']['file_name']) }}
							@else
							User profile image not set
							@endif
							{{-- ImgProxy::link($location.$user['user_profile']['image']['file_name'], 100, 80) --}}
						</div>
						<div class="col-xs-12 col-md-6 user_information">
							<div class="panel" id="user_login_information">
								<div class="panel-heading"><b>{{ $user['user_profile']['first_name'].' '.$user['user_profile']['last_name'] }}</b></div>
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Username:
										</div>
										<div class="col-xs-12 col-md-6">
											{{ $user['username'] }}
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Email:
										</div>
										<div class="col-xs-12 col-md-6">
											{{ $user['email'] }}
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Date Signed On:
										</div>
										<div class="col-xs-12 col-md-6">
											{{ date('l jS M, Y H:m:s', strtotime($user['created_at'])) }}
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Last Modified:
										</div>
										<div class="col-xs-12 col-md-6">
											{{ date('l jS M, Y H:m:s', strtotime($user['modified'])) }}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="panel" id="user_profile_information">
								<div class="panel-heading"><h4><b>Profile Information</b></h4></div>
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Name:
										</div>
										<div class="col-xs-12 col-md-6">
											{{ $user['user_profile']['first_name'].' '.$user['user_profile']['last_name'] }}
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Phone:
										</div>
										<div class="col-xs-12 col-md-6">
											{{ $user['user_profile']['phone'] }}
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											Gender: {{ $user['user_profile']['gender'] }}
										</div>
										<div class="col-xs-12 col-md-6">
											Marital Status: {{ $user['user_profile']['marital_status']['marital_status'] }}
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="panel" id="user_profile_billing">
												<div class="panel-heading"><h5><b>Address</b></h5></div>
												<div class="panel-body">
													<div class="row billing">
														<div class="col-xs-12">
															<b>Billing:</b>
														</div>
													</div>
													@if(!empty($user['billing']))
													<div class="row">
														<div class="col-xs-12">
															{{ $user['billing']['address_1'] }}
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12">
															{{ $user['billing']['city'] }},&nbsp;{{ $user['billing']['state'] }}&nbsp;{{ $user['billing']['postal_code'] }}
														</div>
													</div>
													@else
													<div class="row">
														<div class="col-xs-12">
															N/A
														</div>
													</div>
													@endif
													<div class="row shipping">
														<div class="col-xs-12">
															<b>Shipping:</b>
														</div>
													</div>
													@if (!empty($user['shipping']))
													<div class="row">
														<div class="col-xs-12">
															{{ $user['shipping']['address_1'] }}
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12">
															{{ $user['shipping']['city'] }},&nbsp;{{ $user['shipping']['state'] }}&nbsp;{{ $user['shipping']['postal_code'] }}
														</div>
													</div>
													@else
													<div class="row">
														<div class="col-xs-12">
															N/A
														</div>
													</div>
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="panel" id="user_associations">
								<div class="panel-heading"><b>Other Information</b></div>
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12">
											<div class="panel" id="user_profile_communities">
												<div class="panel-heading"><b>Associated with the following communities:</b></div>
												<div class="panel-body">
													@if (empty($community))
														This user is not presently associated with any communities
													@else
														@foreach($community as $comm)
															{{ HTML::link('community/'.$comm['id'].'/view', $comm['name']) }}
														@endforeach
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="panel" id="user_profile_applications">
												<div class="panel-heading"><b>User Applications</b></div>
												<div class="panel-body">
													@if (empty($applications))
														This user has not filled out any applications
													@else
														@foreach($applications as $application)
															{{ HTML::link('application/'.$application['id'].'/view', 'Application for '.$application['community']['name']) }}
														@endforeach
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="panel" id="user_profile_contacts">
												<div class="panel-heading"><b>User Contact Requests</b></div>
												<div class="panel-body">
													@if (empty($contacts))
														This user has not submitted any requests to be contacted
													@else
														@foreach($contacts as $contact)
															{{ HTML::link('contact/'.$contact['id'].'/view', date('l F jS, Y', strtotime($contact['created_at'])).' at '.date('g:i A', strtotime($contact['created_at']))) }}
														@endforeach
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop