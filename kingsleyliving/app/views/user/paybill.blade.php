@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="paybill_view">
			<div class="panel-heading"><h2 style="margin-bottom:0;margin-top:10px;"><b>{{ $title }}</b></h2></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						<div class="panel" id="edit_section">
							<div class="panel-heading"><h3 style="margin-top:10px">Payment Account Details</h3></div>
							<div class="panel-body">
								<div class="row" style="margin-bottom:10px">
									<div class="col-xs-12 col-md-3">Account on File: Ref # 22556</div>
									<div class="col-xs-12 col-md-3">Account Type: Recurring Payment</div>
									<div class="col-xs-12 col-md-3">Amount due on next billing cycle: $762.50</div>
									<div class="col-xs-12 col-md-3"><div class="text-center"><button href="#" id="change-main-billing" type="button" class="btn btn-default">Change Accounts</button></div></div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<table id="transaction_history" class="table table-hover">
											<thead>
												<tr>
													<th>Transaction ID</th>
													<th>Account Ref #</th>
													<th>Account Type</th>
													<th>Payment Amount</th>
													<th>Payment Date</th>
													<th>Payment Notes</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Transaction ID</th>
													<th>Account Ref #</th>
													<th>Account Type</th>
													<th>Payment Amount</th>
													<th>Payment Date</th>
													<th>Payment Notes</th>
												</tr>
											</tfoot>
											<tbody>
												<tr>
													<td>1</td>
													<td>123456789</td>
													<td>One-time payment</td>
													<td>$500.00</td>
													<td>09-12-2014</td>
													<td>Catching up</td>
												</tr>
												<tr>
													<td>1256</td>
													<td>22556</td>
													<td>Recurring payment</td>
													<td>$650.00</td>
													<td>09-13-2014</td>
													<td>Recurring payment</td>
												</tr>
												<tr>
													<td>13456</td>
													<td>22556</td>
													<td>Recurring payment</td>
													<td>$112.50</td>
													<td>09-13-2014</td>
													<td>Utilities</td>
												</tr>
												<tr>
													<td>55462</td>
													<td>22556</td>
													<td>Recurring payment</td>
													<td>$0.00</td>
													<td>10-13-2014</td>
													<td>Recurring payment canceled</td>
												</tr>
												<tr>
													<td>98897</td>
													<td>22556</td>
													<td>Recurring payment</td>
													<td>$112.50</td>
													<td>10-13-2014</td>
													<td>Utilities</td>
												</tr>
												<tr>
													<td>133456</td>
													<td>123456789</td>
													<td>One-time payment</td>
													<td>$700.00</td>
													<td>11-13-2014</td>
													<td>Late payment + fee</td>
												</tr>
												<tr>
													<td>167784</td>
													<td>22556</td>
													<td>Recurring payment</td>
													<td>$650.00</td>
													<td>11-13-2014</td>
													<td>Recurring payment</td>
												</tr>
												<tr>
													<td>198709</td>
													<td>22556</td>
													<td>Recurring payment</td>
													<td>$112.50</td>
													<td>11-13-2014</td>
													<td>Utilities</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="panel" id="payment_section">
							<div class="panel-heading"><h4>Pay Bill</h4></div>
							<div class="panel-body">
								Chase Paymentech Form <br />
								(Example form)
								{{ Form::open(array(URL::secure(''), 'role' => 'form')) }}
									<div class="row">
										<div class="col-xs-12 col-md-6">
											{{ Form::label('account_reference', 'Account Ref #') }}
											{> $refs = array('' => '', '12213' => '12213', '22556' => '22556', '123456789' => '123456789') <}
											{{ Form::select('account_reference[id]', $refs, Input::old('account_reference[id]'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please choose your account...')) }}
										</div>
									</div>
								{{ Form::close() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			var searchtable = $('#transaction_history').dataTable({
				"scrollY": "200px",
				"sDom": '<"H"<"transaction_header">fr>t<"F"ip>',
			});
			$('.transaction_header').addClass('pull-left').html('<b>Transaction History</b>');
			$('.transaction_header b').css({
				"font-size":"18px"
			});
		});
	</script>
</div>
@stop