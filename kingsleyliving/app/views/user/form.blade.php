@if ( $errors->count() > 0 )
<div class="row">
	<div class="col-xs-12 alert alert-danger">
		<ul>
			@foreach ( $errors->all() as $message )
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
<div class="row">
	<fieldset class="col-xs-12 user-info" id="user_info">
		<legend>Login Details</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('username', 'Desired username:') }}
				{{ Form::text('username', Input::old('username'), array('class' => 'form-control username')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('email', 'Email:') }}
				{{ Form::text('email', Input::old('email'), array('class' => 'form-control email')) }}
			</div>
		</div>
		<div class="row">
			@if ($url_request != 'user/edit')
				<div class="col-xs-12 col-md-6 form-group">
					{{ Form::label('password', 'Password:') }}
					{{ Form::password('password', array('class' => 'form-control')) }}
				</div>
				<div class="col-xs-12 col-md-6 form-group">
					{{ Form::label('password_confirmation', 'Password Confirmation:') }}
					{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
				</div>
			@else
				{{-- Do a button here to open a modal? --}}
				{{-- @include('user.reset_password') --}}
			@endif
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 manager-info" id="manager_info">
		<legend>User Information</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('user_profile[first_name]', 'First Name:') }}
				{{ Form::text('user_profile[first_name]', Input::old('user_profile[first_name]'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('user_profile[last_name]', 'Last Name:') }}
				{{ Form::text('user_profile[last_name]', Input::old('user_profile[first_name]'), array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('user_profile[phone]', 'Phone:') }}
				{{ Form::text('user_profile[phone]', Input::old('user_profile[phone]'), array('class' => 'form-control phone')) }}
			</div>
			<div class="col-xs-12 col-md-3 form-group">
				{{ Form::label('marital_status', 'Marital Status:') }}
				{{ Form::select('user_profile[marital_status_id]', UserMaritalStatus::lists('marital_status', 'id'), Input::old('user_profile[marital_status_id]'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please choose your marital status...')) }}
			</div>
			<div class="col-xs-12 col-md-3 form-group">
				{{ Form::label('user_profile[gender]', 'Gender:') }}
				{> $user_gender = array('Male' => 'Male', 'Female' => 'Female') <}
				{{ Form::select('user_profile[gender]', $user_gender, Input::old('user_profile[gender]'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please choose your gender...')) }}
				{{ Form::hidden('redirectto', $redirectto) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 community-images" id="community_images">
		<legend>Profile Image</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('profile_image[display_name]', 'Image Name:') }}
				{{ Form::text('profile_image[display_name]', Input::old('profile_image[display_name]'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('profile_image[description]', 'Description:') }}
				{{ Form::text('profile_image[description]', Input::old('profile_image[description]'), array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::file('image', array('class' => 'form-control', 'id' => 'main_image')) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4>Address Details</h4>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 checkbox">
		{{ Form::checkbox('billing_same_shipping', 'billing_same_shipping', Input::old('billing_same_shipping')) }}
		{{ Form::label('billing_same_shipping', 'Check here if billing and shipping address are the same.') }}
	</div>
</div>
<div class="row">
	@include('components.address_component', array('field_prefix' => 'billing', 'size' => 'half'))
	@include('components.address_component', array('field_prefix' => 'shipping', 'add_type' => '2', 'size' => 'half'))
</div>