@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="register_user-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				{{ Form::model($user, array('method' => 'put', 'action' => array('UserController@update', $user['id']), 'files' => true, 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('user.form', array('is_new' => false))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save Changes', array('class' => 'btn btn-primary user-submit-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop