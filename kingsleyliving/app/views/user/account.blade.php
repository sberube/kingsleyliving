@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="user_account_view">
			<div class="panel-heading"><h2><b>{{ $title }}</b></h2><div class="pull-right">{{ link_to('user/'.$user->id.'/paybill', 'Pay Bill', array('class' => 'btn btn-default', 'id' => 'pay_bill')) }}{{ link_to('user/'.$user->id.'/edit/account', 'Edit Account Details', array('class' => 'btn btn-default', 'id' => 'update_account')) }}</div></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						{{-- Danger = red, warning = yellow, info = blue, success = green --}}
						<div class="panel" id="user_ledger_notes">
							<div class="panel-heading"><b>Notes</b></div>
							<div class="panel-body">
								<p class="alert alert-success">Happy Holidays!</p>
								<p class="alert alert-danger">These are some special notes!</p>
								<p class="alert alert-warning"><b>Annual community rate change:</b> $50<br /><b>On:</b> Nov 1st, 2014</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-12 col-md-4">
							<button href="#" id="previous-month" type="button" class="btn btn-default">Previous</button>
						</div>
						<div class="col-xs-12 col-md-4 text-center">
							<label>Now Showing</label>
							<div class="now-showing">10 - 2014</div>
						</div>
						<div class="col-xs-12 col-md-4">
							<button href="#" id="next-month" type="button" class="pull-right btn btn-default">Next</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<!-- These will be panels with buttons to show or hide the next panel (using AJAX) -->
						<div class="panel" id="user_rent_mortgage">
							<div class="panel-heading"><b>Rent &amp; Mortgage</b></div>
							<div class="panel-body">
								<p>Rent: $890</p>
								<p>Morgage: $150</p>
								<p>Security Deposit: $1,200</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="panel" id="user_utilities_taxes">
							<div class="panel-heading"><b>Utilites, Taxes, &amp; Fees</b></div>
							<div class="panel-body">
								<div class="col-xs-12 col-md-6">
									<p>Utilities: $200</p>
									<p>State Taxes: 5%</p>
									<p>Federal Taxes: 2.5%</p>
								</div>
								<div class="col-xs-12 col-md-6">
									<p>Fees:</p>
									<ul>
										<li>Pet Fee: $10</li>
										<li>Laundry Fee: $10</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="panel" id="user_credits">
							<div class="panel-heading"><b>Credits</b></div>
							<div class="panel-body">
								<p>You have received a $50 credit! Notes here:</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="panel" id="user_bill_summary">
							<div class="panel-heading"><b>Summary of the bill</b></div>
							<div class="panel-body">
								- Disclaimer -<br />
								This bill is subject to change and is the best estimate of the actual bill
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<button href="{{ url('user/'.$user['id'].'/view/account/userterms') }}" id="userterms" type="button" class="pull-right btn btn-default">User Terms</button>
						<button href="{{ url('user/'.$user['id'].'/view/account/userprivacypolicy') }}" id="privacypolicy" type="button" class="pull-right btn btn-default">Privacy Policy</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop