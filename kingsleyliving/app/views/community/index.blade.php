@extends('layouts.master')

<?php 
function generate_fieldset($community)
{
	$address_1 = $address_2 = $city = '';
	$state = $postal_code = '';
	foreach ($community['address'] as $address) {
		$address_1 = $address['address_1'];
		// $address_2 = $address['address_2'];
		$city = $address['city'];
		$state = $address['state'];
		$postal_code = $address['postal_code'];
	}
	$comm_fieldset = '<fieldset class="col-xs-6 col-sm-4 col-md-3">'
			. '<div class="row">'
				. '<div class="col-xs-12">'
					. '<b>' . $community['name'] . '</b>'
				. '</div>'
			. '</div>'
			. '<div class="row">'
				. '<div class="col-xs-12">';
	if (!empty($address_2) || $address_2 != '')
	{
		$comm_fieldset .= $address_1.'&nbsp;or&nbsp;'.$address_2;
	}
	else
	{
		$comm_fieldset .= $address_1;
	}
	$comm_fieldset .= '</div>'
			. '</div>'
			. '<div class="row">'
				. '<div class="col-xs-12">'
					. $city . ',&nbsp;' . $state . '&nbsp;' . $postal_code
				. '</div>'
			. '</div>'
			. '<div class="row">'
				. '<div class="col-xs-12">' . $community['phone'] . '</div>'
			. '</div>'
				. '<button class="btn btn-success btn-sm" onclick="window.location=\''
				. url('community/'.$community['id']) . '/view\'">Visit '
				. ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name']
				. '&#39;') . ' page</button>'
		. '</fieldset>';

	return $comm_fieldset;
}
?>

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_index">
			<div class="panel-heading page-header"><h2>Our Communities<br/><small>The Best Communities in the Nation!</small></h2></div>
			<div class="panel-body">
				<div class="call_residentmap"></div>
				{> $loopcount = 1; $maxCount = 0; <}
				@foreach ($communities as $group => $comm_item)
					{> $maxCount = count($comm_item) <}
					<fieldset class="col-xs-12 community_fieldset">
						<legend>{{ $group . ' Locations' }}</legend>

					@foreach ($comm_item as $key => $community)
						@if ($maxCount == 1)
							<div class="row">
								{{ generate_fieldset($community) }}
							</div>
						@elseif ($loopcount == 1 || $key == 0)
							<div class="row">
								{{ generate_fieldset($community) }}
						@elseif ($loopcount == 4)
								{{ generate_fieldset($community) }}
							</div>
						{> $loopcount=1; continue; <}
						@elseif ($loopcount != 4 && ($key + 1) == $maxCount)
						{> $loopcount=1 <}
								{{ generate_fieldset($community) }}
							</div>
						@else
							{{ generate_fieldset($community) }}
						@endif
							{> $loopcount++ <}
					@endforeach
					</fieldset>
				@endforeach
			</div>
		</div>
	</div>
</div>
@stop