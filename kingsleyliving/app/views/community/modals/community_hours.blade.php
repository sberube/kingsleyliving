<div class="modal fade" id="community_hours" tabindex="-1" role="dialog" aria-labelledby="community_hours_modal" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Community Hours
				</h4>
			</div>
			<div class="modal-body current-hours">
				{{-- Existing Hours options --}}
			</div>
			<div class="modal-body edit">
				{{-- Edit form --}}
				{{ Form::open(array(null, 'onclick' => 'return false;', 'role' => 'form', 'id' => 'createorupdate-community-hours')) }}
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12">
								{{ Form::label('hour[hour_day][days]', 'Days:') }}
								{{ Form::text('hour[hour_day][days]', null, array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								{{ Form::label('hour[hour_time][start_time]', 'Open Time:') }}
								{{ Form::text('hour[hour_time][start_time]', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6">
								{{ Form::label('hour[hour_time][end_time]', 'Close Time:') }}
								{{ Form::text('hour[hour_time][end_time]', null, array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								{{ Form::label('hour[notes]', 'Notes:') }}
								{{ Form::textarea('hour[notes]', null, array('class' => 'form-control', 'rows' => '3')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								{{ Form::submit('Submit', array('class' => 'btn btn-success modal-submit-btn')) }}
								{{ Form::reset('Reset', array('class' => 'btn btn-success modal-reset-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<div class="modal-body create">
				{{-- Create form --}}
				{{ Form::open(array(null, 'onclick' => 'return false;', 'role' => 'form', 'id' => 'createorupdate-community-hours')) }}
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12">
								{{ Form::label('hour[hour_day][days]', 'Days:') }}
								{{ Form::text('hour[hour_day][days]', null, array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								{{ Form::label('hour[hour_time][start_time]', 'Open Time:') }}
								{{ Form::text('hour[hour_time][start_time]', null, array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6">
								{{ Form::label('hour[hour_time][end_time]', 'Close Time:') }}
								{{ Form::text('hour[hour_time][end_time]', null, array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								{{ Form::label('hour[notes]', 'Notes:') }}
								{{ Form::textarea('hour[notes]', null, array('class' => 'form-control', 'rows' => '3')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								{{ Form::submit('Submit', array('class' => 'btn btn-success modal-submit-btn')) }}
								{{ Form::reset('Reset', array('class' => 'btn btn-success modal-reset-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">
					Done
				</button>
			</div>
		</div>
	</div>
</div>