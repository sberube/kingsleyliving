<div class="modal fade" id="newCommunityImages" tabindex="-1" role="dialog" aria-labelledby="community_new_images_modal" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Add Community Images
				</h4>
			</div>
			<div class="modal-body">
				<div class="community_images_carousel">
				</div>
			</div>
			<!-- 'onclick' => 'return false;' => this stops the form from opening a second open file dialog when you click the browse button -->
			<div class="modal-body">
				{{ Form::open(array(null, 'files' => true, 'onclick' => 'return false;', 'role' => 'form', 'id' => 'create-community-images')) }}
					{{ Form::hidden('community_name') }}
					{{ Form::hidden('location') }}
					<p class="help-block"><b>Please note:</b> you can only upload up to 10 files at the same time.</p>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::file('image', array('class' => 'form-control', 'id' => 'comm_images', 'multiple')) }}
							</div>
						</div>
						<div class="row">
							<h4>Images preparing to upload</h4>
							<div class="dropzone-previews"></div>
						</div>
						<div class="row image_data"></div>
						<div class="row">
							<div class="col-xs-12">
								{{ Form::submit('Submit', array('class' => 'btn btn-success modal-submit-btn')) }}
								{{ Form::reset('Reset', array('class' => 'btn btn-success modal-reset-btn')) }}
								<p class="help-block">Reset will only reset files in queue to be uploaded, it will not reset files already uploaded.</p>
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">
					Done
				</button>
			</div>
		</div>
	</div>
</div>