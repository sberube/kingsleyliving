@extends('layouts.master')

@section('before_body')
	@include('community.modals.create_images')
	@include('community.modals.community_hours')
@stop

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_community-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">This form is for admin use only.&nbsp;&nbsp;Access by any other user is forbidden.</p>
						<p class="help-block">The purpose of this creation form is that it simply another way to input community information into the system.</p>
					</div>
				</div>
				{{ Form::open(array('action' => 'CommunityController@store', 'files' => true, 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('community.form', array('is_new' => true))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save', array('class' => 'btn btn-primary community-submit-btn')) }}
								{{ Form::reset('Clear Form', array('class' => 'btn btn-primary community-reset-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop