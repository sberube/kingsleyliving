@if ( $errors->count() > 0 )
<div class="row">
	<div class="col-xs-12 alert alert-danger">
		<ul>
			@foreach ( $errors->all() as $message )
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
<div class="row">
	<fieldset class="col-xs-12 community-info" id="community_info">
		<legend>Community Information</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('name', 'Community Name:') }}
				{{ Form::text('name', Input::old('name'), array('class' => 'form-control community_name')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('phone', 'Community Phone:') }}
				{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control phone')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('email', 'Community Email:') }}
				{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('fax', 'Community Fax:') }}
				{{ Form::text('fax', Input::old('fax'), array('class' => 'form-control phone')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('home_page', 'Community Home Page:') }}
				{{ Form::text('home_page', Input::old('home_page'), array('class' => 'form-control home_page')) }}
				<p class="help-block">If different from page listed above</p>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::button('Community Hours', array('class' => 'btn btn-primary', 'id' => 'community_hours_btn')) }}
				{{ Form::hidden('community_hours_associate') }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 manager-info" id="manager_info">
		<legend>Management Information</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('account_manager', 'Account Manager:') }}
				{> $communityAccountManagerDDL = array(0 => '') <}
				@foreach ($account_managers as $id => $account)
					{> $communityAccountManagerDDL[$account['id']] = $account['user_profile']['first_name'] . ' ' . $account['user_profile']['last_name'] . ' - ' . $account['email'] <}
				@endforeach
				{{ Form::select('account_manager_id', $communityAccountManagerDDL, Input::old('account_manager_id'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please choose an account manager...')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('property_manager', 'Community Manager:') }}
				{> $communityPropertyManagerDDL = array(0 => '') <}
				@foreach ($property_managers as $id => $manager)
					{> $communityPropertyManagerDDL[$manager['id']] = $manager['user_profile']['first_name'] . ' ' . $manager['user_profile']['last_name'] . ' - ' . $manager['email'] <}
				@endforeach
				{{ Form::select('property_manager_id', $communityPropertyManagerDDL, Input::old('property_manager_id'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please choose a community manager...')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('assistant_manager', 'Community Assistant Manager:') }}
				{> $communityAssistantManagerDDL = array(0 => '') <}
				@foreach ($assistant_managers as $id => $assist)
					{> $communityAssistantManagerDDL[$assist['id']] = $assist['user_profile']['first_name'] . ' ' . $assist['user_profile']['last_name'] . ' - ' . $assist['email'] <}
				@endforeach
				{{ Form::select('assistant_property_manager_id', $communityAssistantManagerDDL, Input::old('assistant_property_manager_id'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please choose an assistant manager...')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::hidden('json_image_id', $json_image_id) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 community-images" id="community_images">
		<legend>Community Images</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('main_image[display_name]', 'Main Image Name:') }}
				{{ Form::text('main_image[display_name]', Input::old('main_image[display_name]'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('main_image[description]', 'Description:') }}
				{{ Form::text('main_image[description]', Input::old('main_image[description]'), array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::file('image', array('class' => 'form-control', 'id' => 'main_image')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::button('Add Community Images', array('class' => 'btn btn-primary', 'id' => 'add_images_btn')) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{{ Form::label('short_description', 'Please give a short summary, no more than 255 characters:') }}
		{{ Form::textarea('short_description', Input::old('short_description'), array('class' => 'form-control', 'rows' => '2')) }}
	</div>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{{ Form::label('description', 'Description:') }}
		{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control', 'rows' => '6')) }}
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4>Address Details</h4>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 checkbox">
		{{ Form::checkbox('billing_same_shipping', 'billing_same_shipping', Input::old('billing_same_shipping')) }}
		{{ Form::label('billing_same_shipping', 'Check here if billing and shipping address are the same.') }}
	</div>
</div>
<div class="row">
	@include('components.address_component', array('field_prefix' => 'billing', 'size' => 'half'))
	@include('components.address_component', array('field_prefix' => 'shipping', 'add_type' => '2', 'size' => 'half'))
</div>
<div class="row">
	<fieldset class="col-xs-12 community_amenities" id="community_amenities_fieldset">
		<legend>Community Amenities</legend>
		<div class="row">
			<div class="col-xs-12">
				<p class="help-block">Please remember that the amenities you now see are those that apply to the whole community</p>
				<p class="help-block">(For home amenities visit one of the &#39;space&#39; pages, they will display the amenities available for that home).</p>
			</div>
		</div>
		<div class="row">
			@foreach ($amenities as $id => $amenity)
				{> $amenity_name = str_replace(' ', '_', str_replace(array('.',','), '', $amenity['name'])) <}
				<div class="col-xs-6 col-md-3 checkbox">
					@if (isset($community_amenity))
						{{ Form::checkbox('amenity['.$id.'][id]', $amenity['id'], in_array($amenity['id'], $community_amenity)) }}
						{{ Form::label($amenity_name, ucwords($amenity['name'])) }}
					@else
						{{ Form::checkbox('amenity['.$id.'][id]', $amenity['id']) }}
						{{ Form::label($amenity_name, ucwords($amenity['name'])) }}
					@endif
				</div>
			@endforeach
		</div>
		<div class="row">
			<div class="col-xs-12 form-group">
				{{ Form::label('new_amenities', 'Or add new amenities:') }}
				<p class="help-block">Coming Soon</p>
			</div>
		</div>
	</fieldset>
</div>