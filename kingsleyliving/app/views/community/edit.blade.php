@extends('layouts.master')

@section('before_body')
	@include('community.modals.create_images')
	@include('community.modals.community_hours')
@stop

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_community-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">This form is for admin use only.&nbsp;&nbsp;Access by any other user is forbidden.</p>
						<p class="help-block">The purpose of this edit form is that it simply another way to edit community information into the system.</p>
					</div>
				</div>
				{{ Form::model($community, array('method' => 'put', 'action' => array('CommunityController@update', $community['id']), 'files' => true, 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('community.form', array('is_new' => false))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save Changes', array('class' => 'btn btn-primary community-submit-btn')) }}
								{{-- URL::previous() <-- Do a cancel button with this in the href --}}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop