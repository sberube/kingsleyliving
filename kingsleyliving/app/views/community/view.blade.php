@extends('layouts.master')

<?php
	$address_1 = $address_2 = $city = '';
	$state = $postal_code = '';
	$longitude = $latitude = '';
	foreach ($community['address'] as $address) {
		$address_1 = $address['address_1'];
		$address_2 = $address['address_2'];
		$city = $address['city'];
		$state = $address['state'];
		$postal_code = $address['postal_code'];
		$latitude = $address['address_google']['latitude'];
		$longitude = $address['address_google']['longitude'];
	}
?>

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_view">
			<div class="jumbotron standard_community_jumbotron">
				<div class="container">
					<h2 class="jumbotron-header">{{ $community['name'] }}</h2>
					<p class="jumbotron-body">{{ $community['short_description'] }}</p>
					<p class="jumbotron-footer"><button class="btn btn-primary" onclick="location.href='{{ url($community['id'].'/space') }}'">View Available Homes</button></p>
				</div>
			</div>
			<div class="panel-body">
				{{-- <div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-md-5 community_contact">
							<div class="panel" id="community_contact">
								<div class="panel-heading"><b>Contact {{ $community['name'] }}</b></div>
								<div class="panel-body">
									<div class="col-xs-12">
										<div class="row">
											Community Manager: {{ $property_manager['first_name'] }}&nbsp;{{ $property_manager['last_name'] }}
										</div>
										<div class="row">
											@if (!empty($address_2) || $address_2 != '')
												{{ $address_1 }}&nbsp;or&nbsp;{{ $address_2 }}
											@else
												{{ $address_1 }}
											@endif
										</div>
										<div class="row">
											{{ $city }},&nbsp;{{ $state }}&nbsp;{{ $postal_code }}
										</div>
										<div class="row">
											{{ $community['phone'] }}
										</div>
									</div>
								</div>
							</div>
							<div class="panel" id="community_availability">
								<div class="panel-heading"><b>Availability</b></div>
								<div class="panel-body">
									<div class="col-xs-12">
										<div class="row">
											Coming soon...
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> --}}
				<div class="col-xs-12 col-md-6 available_homes">
					<div class="panel-heading"><b>Homes Available</b></div>
					<div class="panel-body">
						Coming soon...
					</div>
				</div>
				<div class="col-xs-12 col-md-6 community_contact">
					<div class="panel col-xs-12 col-md-6" id="community_contact">
						<div class="panel-heading"><b>Contact {{ $community['name'] }}</b></div>
						<div class="panel-body">
							<div class="col-xs-12">
								<div class="row">
									Community Manager: {{ $property_manager['first_name'] }}&nbsp;{{ $property_manager['last_name'] }}
								</div>
								<div class="row">
									@if (!empty($address_2) || $address_2 != '')
										{{ $address_1 }}&nbsp;or&nbsp;{{ $address_2 }}
									@else
										{{ $address_1 }}
									@endif
								</div>
								<div class="row">
									{{ $city }},&nbsp;{{ $state }}&nbsp;{{ $postal_code }}
								</div>
								<div class="row">
									{{ $community['phone'] }}
								</div>
								<div class="row">
									{{ $community['email'] }}
								</div>
							</div>
						</div>
					</div>
					<div class="panel col-xs-12 col-md-6" id="community_availability">
						<div class="panel-heading"><b>Availability</b></div>
						<div class="panel-body">
							<div class="col-xs-12">
								<div class="row">
									Coming soon...
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<ul class="community-tabs" role="tablist">
						<li class="active"><a href="#summary" role="tab" data-toggle="tab">Summary</a></li>
						<!-- <li><a href="#contact_community" role="tab" data-toggle="tab">Contact Community</a></li> -->
						<li><a href="#amenities" role="tab" data-toggle="tab">Amenities</a></li>
						<li><a href="#nearby" role="tab" data-toggle="tab">Nearby</a></li>
						<!-- <li><a href="#promotions" role="tab" data-toggle="tab">Promotions</a></li> -->
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="summary">
							<div class="panel" id="summary_panel">
								<div class="panel-heading"><h4>{{ ((substr($community['name'], -1) != 's')?$community['name'] . '&#39;s':$community['name']	. '&#39;') }} Summary</h4></div>
								<div class="panel-body">
									{{ $community['description'] }}
								</div>
							</div>
						</div>
						{{-- <div class="tab-pane" id="contact_community">
							<div class="panel" id="contact_community_panel">
								<div class="panel-heading"><b>Contact {{ $community['name'] }}</b></div>
								<div class="panel-body">
									<div class="col-xs-12">
										<div class="row">
											Community Manager: {{ $property_manager['first_name'] }}&nbsp;{{ $property_manager['last_name'] }}
										</div>
										<div class="row">
											@if (!empty($address_2) || $address_2 != '')
												{{ $address_1 }}&nbsp;or&nbsp;{{ $address_2 }}
											@else
												{{ $address_1 }}
											@endif
										</div>
										<div class="row">
											{{ $city }},&nbsp;{{ $state }}&nbsp;{{ $postal_code }}
										</div>
										<div class="row">
											{{ $community['phone'] }}
										</div>
									</div>
									<div class="col-xs-12">
										<div class="row">
											<h5>Availability</h5>
										</div>
										<div class="row">
											Coming soon...
										</div>
									</div>
								</div>
							</div>
						</div> --}}
						<div class="tab-pane" id="amenities">
							<div class="panel" id="amenities_panel">
								<div class="panel-heading"><h4>Amenities</h4></div>
								<div class="panel-body">
									<div class="row">
										@foreach ($community['community_amenity'] as $id => $amenity)
											<div class="col-xs-6 form-group">
											{{ ucwords($amenity['name']) }}
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="nearby">
							<div class="panel" id="nearby_panel">
								<div class="panel-heading"><b>Local Businesses</b></div>
								<div class="panel-body">
									Coming soon...
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 community_maps">
					<div class="panel" id="community_google_maps">
						<div class="panel-heading"><b>Map</b></div>
						<div class="panel-body">
							<div class="community_latitude hidden">{{ $latitude }}</div>
							<div class="community_longitude hidden">{{ $longitude }}</div>
							<div id="map_canvas" class="map-canvas-default"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function() {
		var latit = $('.community_latitude').text();
		var longit = $('.community_longitude').text();
		var myCenter = new google.maps.LatLng(latit,longit);
		map_initialize(myCenter);
	});
	</script>
</div>
@stop