@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_view">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12">
						Search Params here.
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-3">
						@include('components.search_component', array('request' => 'vertical'))
					</div>
					<div class="col-xs-12 col-md-9">
						<select id="sort_params" class="sort-chosen" name="sort_params">
							<option value=""></option>
							<option value="name_asc">Community Name (asc)</option>
							<option value="name_desc">Community Name (desc)</option>
							<option value="beds_asc">Beds (asc)</option>
							<option value="beds_desc">Beds (desc)</option>
							<option value="baths_asc">Baths (asc)</option>
							<option value="baths_desc">Baths (desc)</option>
							<option value="footage_asc">Square Footage (asc)</option>
							<option value="footage_desc">Square Footage (desc)</option>
							<option value="rent_price_asc">Rent Price (asc)</option>
							<option value="rent_price_desc">Rent Price (desc)</option>
							<option value="sale_price_asc">Sale Price (asc)</option>
							<option value="sale_price_desc">Sale Price (desc)</option>
						</select>
						<table id="searchresults_table" class="table table-hover">
							<thead>
								<tr>
									<th>Available Homes</th>
									<th>Community Name</th>
									<th>Beds</th>
									<th>Baths</th>
									<th>Square Footage</th>
									<th>Rent Price</th>
									<th>Sale Price</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Available Homes</th>
									<th>Community Name</th>
									<th>Beds</th>
									<th>Baths</th>
									<th>Square Footage</th>
									<th>Rent Price</th>
									<th>Sale Price</th>
								</tr>
							</tfoot>
							<tbody>
								@if (!empty($spaces))
									@foreach($spaces as $space)
										<tr class="home">
											<td>
												<div class="panel" id="search_main">
													<div class="panel-body">
														<div class="row">
															<div class="col-xs-12 col-md-3">
																Coming soon (home image)...
															</div>
															<div class="col-xs-12 col-md-3">
																<div><b>{{ $space['community']['name'] }}</b></div>
																<div>Home Number {{ $space['space_num'] }}</div>
															</div>
															<div class="col-xs-12 col-md-3">
																<div>{{ $space['beds'] }} Beds</div>
																<div>{{ $space['baths'] }} Baths</div>
																<div>{{ $space['square_feet'] }} Square Feet</div>
															</div>
															<div class="col-xs-12 col-md-3">
																<div>Rent for as low as:</div>
																<div><span class="currency">{{ $space['rent_price'] }}</span> / month!</div>
															</div>
															<div class="hidden">
																<div class="space_id">{{ $space['id'] }}</div>
																<div class="community_id">{{ $space['community']['id'] }}</div>
															</div>
														</div>
													</div>
												</div>
											</td>
											<td>{{ $space['community']['name'] }}</td>
											<td>{{ $space['beds'] }}</td>
											<td>{{ $space['baths'] }}</td>
											<td>{{ $space['square_feet'] }}</td>
											<td>{{ $space['sale_price'] }}</td>
											<td>{{ $space['rent_price'] }}</td>
										</tr>
									@endforeach
								@else
									We're sorry, your search returned no results
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		var searchtable = $('#searchresults_table').dataTable({
			"scrollY": "680px",
			"sDom": '<"H"l<"tools">fr>t<"F"ip>',
			"columnDefs": [
			{
				"targets": [ 0 ],
				"searchable": false,
				"orderable": false
			},
			{
				"targets": [ 1 ],
				"visible": false,
			},
			{
				"targets": [ 2 ],
				"visible": false,
			},
			{
				"targets": [ 3 ],
				"visible": false,
			},
			{
				"targets": [ 4 ],
				"visible": false,
			},
			{
				"targets": [ 5 ],
				"visible": false,
			},
			{
				"targets": [ 6 ],
				"visible": false,
			}
			]
		});
		$('.dataTables_scrollHeadInner').find('tr').children('th').removeClass('sorting_asc');
		$('#searchresults_table_length').addClass('col-xs-3');
		$('#searchresults_table_filter').addClass('col-xs-4');
		$('#searchresults_table_wrapper div.tools').addClass('col-xs-5');
		var sortOpts = $('#sort_params').html();
		$('#sort_params').remove();
		$('div.tools').html('Sort By: <select id="sort_params" class="sort-chosen" name="sort_params">'+sortOpts+'</select>');
		$('.sort-chosen').chosen({ width:'80%', disable_search: true });
		$('#sort_params').on('change', function () {
			switch ($(this).val()) {
				case "name_asc":
				searchtable.fnSort( [ [1,'asc'] ] );
				break;

				case "name_desc":
				searchtable.fnSort( [ [1,'desc'] ] );
				break;

				case "beds_asc":
				searchtable.fnSort( [ [2,'asc'] ] );
				break;

				case "beds_desc":
				searchtable.fnSort( [ [2,'desc'] ] );
				break;

				case "baths_asc":
				searchtable.fnSort( [ [3,'asc'] ] );
				break;

				case "baths_desc":
				searchtable.fnSort( [ [3,'desc'] ] );
				break;

				case "footage_asc":
				searchtable.fnSort( [ [4,'asc'] ] );
				break;

				case "footage_desc":
				searchtable.fnSort( [ [4,'desc'] ] );
				break;

				case "rent_price_asc":
				searchtable.fnSort( [ [5,'asc'] ] );
				break;

				case "rent_price_desc":
				searchtable.fnSort( [ [5,'desc'] ] );
				break;

				case "sale_price_asc":
				searchtable.fnSort( [ [6,'asc'] ] );
				break;

				case "sale_price_desc":
				searchtable.fnSort( [ [6,'desc'] ] );
				break;
			}
		});
	});
	</script>
</div>
@stop