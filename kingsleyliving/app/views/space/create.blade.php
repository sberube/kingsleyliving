@extends('layouts.master')

{{--@section('before_body')
	@include('space.modals.create_images')
@stop --}}

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_community-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-xs-12">
						<p class="help-block">This form is for admin use only.&nbsp;&nbsp;Access by any other user is forbidden.</p>
						<p class="help-block">The purpose of this creation form is that it simply another way to input space information into the system.</p>
					</div>
				</div>
				{{ Form::open(array('action' => 'SpaceController@store', 'files' => true, 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('space.form', array('is_new' => true))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save', array('class' => 'btn btn-primary space-submit-btn')) }}
								{{ Form::reset('Clear Form', array('class' => 'btn btn-primary space-reset-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<script type="text/javascript">
			$(document).ready(function() {
				$('#add_images_btn').click(function() {
					if ($('.space_num').val().length == 0)
					{
						alert('No pictures can be input at this time, please fill in space num.');
					}
					else
					{
						var space_num = $('.space_num').val().toLowerCase().replace(' ', '_');
						$('.modal-body input[name=space_num]').val(space_num);
						var location = '/images/space/' + space_num;
						$('.modal-body input[name=location]').val(location);
						$('#newSpaceImages').modal({
							keyboard: true,
							show: true
						});
					}
				});
				$('.square_feet').number(true);
				$('.rent-price').number(true, 2);
				$('.lease-rate').number(true, 2);
				$('.sale-price').number(true, 2);
				$('.interest-rate').number(true, 2);
				$('.cash-sale-price').number(true, 2);
				$('.loc-rate').number(true, 2);
				$('.yearly-tax-savings').number(true, 2);
				$('.budgeted-repairs').number(true, 2);
				$('.deposit').number(true, 2);
			});
			</script>
		</div>
	</div>
</div>
@stop