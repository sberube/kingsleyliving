@if ( $errors->count() > 0 )
<div class="row">
	<div class="col-xs-12 alert alert-danger">
		<ul>
			@foreach ( $errors->all() as $message )
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
<div class="row">
	<fieldset class="col-xs-12 community-info" id="community_info">
		<div class="row">
			<div class="col-xs-12">
				<h3>{{ $community->name }}</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				Community Manager: {{ $community->property_manager->user_profile->first_name }}&nbsp;{{ $community->property_manager->user_profile->last_name }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				Phone: {{$community->phone}}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				&nbsp;
			</div>
		</div>
	</fieldset>
	{{ Form::hidden('community', $community->id) }}
</div>
{{-- print_r($space) --}}
<div class="row">
	<fieldset class="col-xs-12 space-info" id="space_info">
		<legend>Space Information</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_num', 'Unit Number:') }}
				{{ Form::text('space_num', Input::old('space_num'), array('class' => 'form-control space_num')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_status_id', 'Status:') }}
				{> $status = SpaceStatus::lists('status', 'id') <}
				{> $space_statuses = array() <}
				@foreach ($status as $key => $stat)
					{> $space_statuses[$key] = ucwords(str_replace('_', ' ', $stat)) <}
				@endforeach
				{{ Form::select('space_status_id', $space_statuses, Input::old('space_status_id'), array('class' => 'form-control chosen-select')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 form-group">
				{{ Form::label('description', 'Give a summary on the house, it&#39;s condition, etc:') }}
				{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control', 'rows' => '3')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[manufacturer]', 'Manufacturer:') }}
				{{ Form::text('space_detail[manufacturer]', Input::old('space_detail[manufacturer]'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('VIN', 'VIN:') }}
				{{ Form::text('VIN', Input::old('VIN'), array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[square_feet]', 'Square Feet:') }}
				<div class="input-group">
					{{ Form::text('space_detail[square_feet]', Input::old('space_detail[square_feet]'), array('class' => 'form-control square_feet')) }}
					<span class="input-group-addon">ft</span>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[width]', 'Space Width:') }}
				<div class="input-group">
					{{ Form::text('space_detail[width]', Input::old('space_detail[width]'), array('class' => 'form-control')) }}
					<span class="input-group-addon">ft</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[year]', 'Year:') }}
				{{ Form::text('space_detail[year]', Input::old('space_detail[year]'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[length]', 'Space Length:') }}
				<div class="input-group">
					{{ Form::text('space_detail[length]', Input::old('space_detail[length]'), array('class' => 'form-control')) }}
					<span class="input-group-addon">ft</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[beds]', 'Beds:') }}
				{{ Form::text('space_detail[beds]', Input::old('space_detail[beds]'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_detail[baths]', 'Baths:') }}
				{{ Form::text('space_detail[baths]', Input::old('space_detail[baths]'), array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 form-group">
				{{ Form::label('notes', 'Any other items to note?') }}
				{{ Form::textarea('notes', Input::old('notes'), array('class' => 'form-control', 'rows' => '3')) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 space-images" id="space_images">
		<legend>Space Images</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::button('Add Space Images', array('class' => 'btn btn-primary', 'id' => 'add_images_btn')) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4>Address Details</h4>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 checkbox">
		{{ Form::checkbox('billing_same_shipping', 'billing_same_shipping', Input::old('billing_same_shipping')) }}
		{{ Form::label('billing_same_shipping', 'Check here if billing and shipping address are the same.') }}
	</div>
</div>
<div class="row">
	@include('components.address_component', array('field_prefix' => 'billing', 'size' => 'half'))
	@include('components.address_component', array('field_prefix' => 'shipping', 'add_type' => '2', 'size' => 'half'))
</div>
<div class="row">
	<fieldset class="col-xs-12 appliance-info" id="appliance_info">
		<legend>Space Appliances</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				<p class="help-block">Coming Soon</p>
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 space-images" id="space_images">
		<legend>Space Pricing Details</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[rent_price]', 'Rent Price:') }}
				<div class="input-group">
					<span class="input-group-addon">&#36;</span>
					{{ Form::text('space_pricing[rent_price]', Input::old('space_pricing[rent_price]'), array('class' => 'form-control rent-price')) }}
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[lease_rate]', 'Lease Rate:') }}
				<div class="input-group">
					{{ Form::text('space_pricing[lease_rate]', Input::old('space_pricing[lease_rate]'), array('class' => 'form-control lease-rate')) }}
					<span class="input-group-addon">&#37;</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[sale_price]', 'Sale Price:') }}
				<div class="input-group">
					<span class="input-group-addon">&#36;</span>
					{{ Form::text('space_pricing[sale_price]', Input::old('space_pricing[sale_price]'), array('class' => 'form-control sale-price')) }}
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[interest_rate]', 'Interest Rate:') }}
				<div class="input-group">
					{{ Form::text('space_pricing[interest_rate]', Input::old('space_pricing[interest_rate]'), array('class' => 'form-control interest-rate')) }}
					<span class="input-group-addon">&#37;</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[cash_sale_price]', 'Cash Sale Price:') }}
				<div class="input-group">
					<span class="input-group-addon">&#36;</span>
					{{ Form::text('space_pricing[cash_sale_price]', Input::old('space_pricing[cash_sale_price]'), array('class' => 'form-control cash-sale-price')) }}
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[LOC_rate]', 'Line of Credit Rate:') }}
				<div class="input-group">
					{{ Form::text('space_pricing[LOC_rate]', Input::old('space_pricing[LOC_rate]'), array('class' => 'form-control loc-rate')) }}
					<span class="input-group-addon">&#37;</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[yearly_tax_savings]', 'Yearly Tax Savings:') }}
				<div class="input-group">
					<span class="input-group-addon">&#36;</span>
					{{ Form::text('space_pricing[yearly_tax_savings]', Input::old('space_pricing[yearly_tax_savings]'), array('class' => 'form-control yearly-tax-savings')) }}
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[budgeted_repairs]', 'Budgeted Repairs:') }}
				<div class="input-group">
					<span class="input-group-addon">&#36;</span>
					{{ Form::text('space_pricing[budgeted_repairs]', Input::old('space_pricing[budgeted_repairs]'), array('class' => 'form-control budgeted-repairs')) }}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[deposit]', 'Deposit:') }}
				<div class="input-group">
					<span class="input-group-addon">&#36;</span>
					{{ Form::text('space_pricing[deposit]', Input::old('space_pricing[deposit]'), array('class' => 'form-control deposit')) }}
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space_pricing[closed_date]', 'Closed Date:') }}
				{{ Form::text('space_pricing[closed_date]', Input::old('space_pricing[closed_date]'), array('class' => 'form-control datepicker')) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 community_amenities" id="community_amenities_fieldset">
		<legend>Space Amenities</legend>
		<div class="row">
			<div class="col-xs-12">
				<p class="help-block">Please remember that the amenities you now see are those that apply to an individual space</p>
				<p class="help-block">(For community amenities visit one of the community pages, they will display the amenities available for that community).</p>
			</div>
		</div>
		<div class="row">
			@foreach ($amenities as $id => $amenity)
				{> $amenity_name = str_replace(' ', '_', str_replace(array('.',','), '', $amenity['name'])) <}
				<div class="col-xs-6 col-md-3 checkbox">
					@if (isset($space_amenity))
						{{ Form::checkbox('amenity['.$id.'][id]', $amenity['id'], in_array($amenity['id'], $space_amenity)) }}
						{{ Form::label($amenity_name, ucwords($amenity['name'])) }}
					@else
						{{ Form::checkbox('amenity['.$id.'][id]', $amenity['id']) }}
						{{ Form::label($amenity_name, ucwords($amenity['name'])) }}
					@endif
				</div>
			@endforeach
		</div>
		<div class="row">
			<div class="col-xs-12 form-group">
				{{ Form::label('new_amenities', 'Or add new amenities:') }}
				<p class="help-block">Coming Soon</p>
			</div>
		</div>
	</fieldset>
</div>