@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="community_view">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-3">
						@include('components.search_component', array('request' => 'vertical'))
					</div>
					<div class="col-xs-12 col-md-9">
						<ul class="space-tabs" role="tablist">
							<li class="apply_tab"><a href="#apply" role="tab" data-toggle="tab">Apply</a></li>
							<li class="active"><a href="#home_info" role="tab" data-toggle="tab">Home Info</a></li>
							@if (!empty($space['space_image']))
								<li><a href="#photos" role="tab" data-toggle="tab">Photos</a></li>
							@endif
							<li><a class="maps_nearby" href="#maps_nearby" role="tab" data-toggle="tab">Maps &amp; Nearby</a></li>
							<li><a href="#community_info" role="tab" data-toggle="tab">Community Info</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="apply">
								<div class="panel" id="apply_panel">
									<div class="panel-heading"><h4>Inquire Now!</h4></div>
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12">
												@if(Confide::user())
													{{ Form::model($current, array('action' => 'ApplicationController@store', 'role' => 'form')) }}
														{{ Form::hidden('url_request', $url_request) }}
														<div class="container-fluid">
															@include('application.space_form', array('is_new' => true))
															<div class="row">
																<div class="col-xs-12 form-group">
																	{{ Form::submit('Save', array('class' => 'btn btn-primary application-submit-btn')) }}
																	{{ Form::reset('Clear Form', array('class' => 'btn btn-primary application-reset-btn')) }}
																</div>
															</div>
														</div>
													{{ Form::close() }}
												@else
													{{ Form::open(array('action' => 'ApplicationController@store', 'role' => 'form')) }}
														{{ Form::hidden('url_request', $url_request) }}
														<div class="container-fluid">
															@include('application.space_form', array('is_new' => true))
															<div class="row">
																<div class="col-xs-12 form-group">
																	{{ Form::submit('Save', array('class' => 'btn btn-primary application-submit-btn')) }}
																	{{ Form::reset('Clear Form', array('class' => 'btn btn-primary application-reset-btn')) }}
																</div>
															</div>
														</div>
													{{ Form::close() }}
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane active" id="home_info">
								<div class="panel" id="home_info_panel">
									<div class="panel-heading">Home Information</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12 col-md-6">
												@if (!empty($space['main_image']))
													{{ HTML::image($space_location.$space['main_image']['file_name']) }}
												@else
													No images are available at this time
												@endif
											</div>
											<div class="col-xs-12 col-md-3">
												<table class="table table-striped table-bordered">
													<tr>
														<td>Purchase Price</td><td class="currency">{{ $space['space_pricing']['sale_price'] }}</td>
													</tr>
													<tr>
														<td>Bedrooms</td><td>{{ $space['space_detail']['beds'] }}</td>
													</tr>
													<tr>
														<td>Bathrooms</td><td>{{ $space['space_detail']['baths'] }}</td>
													</tr>
													<tr>
														<td>Approx. Size</td><td>{{ $space['space_detail']['square_feet'] }}</td>
													</tr>
												</table>
											</div>
											<div class="col-xs-12 col-md-3">
												{{-- HTML::link() --}} - Flyer link -
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												Community specials coming soon...
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-md-7">
												<h4>Home Description</h4>
												<div class="row">
													<div class="col-xs-12">
														{{ $space['description'] }}
													</div>
												</div>
												@if (!empty($space['notes']))
												<div class="row">
													<div class="col-xs-12">
														<h5><b>Extra Notes:</b></h5>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														{{ $space['notes'] }}
													</div>
												</div>
												@endif
											</div>
											<div class="col-xs-12 col-md-5">
												<div class="row">
													@foreach ($space['space_amenity'] as $amenity)
														<div class="col-xs-6">
															{{ ucwords($amenity['name']) }}
														</div>
													@endforeach
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@if (!empty($space['space_image']))
							<div class="tab-pane" id="photos">
								<div class="panel" id="photos_panel">
									<div class="panel-heading">Photos</div>
									<div class="panel-body">
										Coming soon...
									</div>
								</div>
							</div>
							@endif
							<div class="tab-pane" id="maps_nearby">
								<div class="panel" id="maps_nearby_panel">
									<div class="panel-heading">Maps &amp; Nearby</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12 col-md-6 space_maps">
												<div class="community_latitude hidden">{{ $comm->address[0]->address_google->latitude }}</div> <!-- Check for Google_id to see if we've already registered Google details for this address, if not, then we neeed to do it. -->
												<div class="community_longitude hidden">{{ $comm->address[0]->address_google->longitude }}</div>
												<div id="map_canvas" class="map-canvas-default"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="community_info">
								<div class="panel" id="community_info_panel">
									<div class="panel-heading"><b>{{ $comm->name }}</b></div>
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12 col-md-8">
												{{ HTML::image($community_location.$comm->main_image->file_name) }}
											</div>
											<div class="col-xs-12 col-md-4">
												<div class="row">
													@foreach ($comm->community_amenity as $id => $amenity)
														<div class="col-xs-6">
															{{ ucwords($amenity->name) }}
														</div>
													@endforeach
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												Community Contact Information
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-md-6">
												<div class="row">
													<div class="col-xs-12">
														<b>Community Manager</b>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														{{ $comm->property_manager->user_profile->first_name }}&nbsp;{{ $comm->property_manager->user_profile->last_name }}
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Phone:&nbsp;{{ $comm->phone }}
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-md-6">
												<div class="row">
													<div class="col-xs-12">
														Sales Office Hours:
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Hours coming soon...
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Hour Exceptions coming soon...
													</div>
												</div>
											</div>
										</div>
										<div class="row top-buffer">
											<div class="col-xs-12 col-md-6">
												<div class="row">
													<div class="col-xs-12">
														<b>Resident Hotline:</b>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Phone:&nbsp;{{ $comm->phone }}
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Office Hours:
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Hours coming soon...
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12">
														Hour Exceptions coming soon...
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-md-6">
												Button to contact sales here coming soon...
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.square_feet').number(true);

		$('.user_name').tooltip({placement:'bottom',title:'Last, First Middle'});

		if ($('.apply_user_name').html().length <= 0)
		{
			$('.apply_user_name').hide();
		}
		else
		{
			$('.apply_user_name').show();
		}

		$('.user_name').keyup( function() {
			var user_name = $(this).val();
			var applicant_old = $('.apply_user_name').html();
			if (user_name.indexOf(',') > 0)
			{
				var comma_split = user_name.split(',');
				var last_name_arry = [];
				if (comma_split[0].indexOf(' ') > 0)
				{
					last_name_arry = comma_split[0].trim().split(' ');
				}
				else
				{
					last_name_arry.push(comma_split[0].trim());
				}
				var first_name_arry = comma_split[1].trim().split(' ');
			}
			else
			{
				var space_split = user_name.split(' ');
				var last_name_arry = [];
				var first_name_arry = [];
				first_name_arry.push(space_split[0]);
				first_name_arry.push(space_split[1]);
				if (space_split.length > 3)
				{
					var count = 2;
					while (count < space_split.length)
					{
						last_name_arry.push(space_split[count]);
						count++;
					}
				}
				else
				{
					last_name_arry.push(space_split[2]);
				}
			}
			var first_name = first_name_arry[0];
			if (first_name_arry.length > 1)
			{
				var middle_name = first_name_arry[1];
			}
			else
			{
				var middle_name = '';
			}
			var last_name = last_name_arry[0];
			if (last_name_arry.length > 1)
			{
				var suffix = last_name_arry[1];
			}
			else
			{
				var suffix = '';
			}
			var contents = 'First Name: '+first_name+'<br />Middle Name: '+middle_name+'<br />Last Name: '+last_name+'<br />Suffix: '+suffix;
			$('.apply_user_name').html(contents).show();
			$('input[name=first_name]').val(first_name);
			$('input[name=middle_name]').val(middle_name);
			$('input[name=last_name]').val(last_name);
			$('input[name=suffix]').val(suffix);
		});
	});
	</script>
</div>
@stop