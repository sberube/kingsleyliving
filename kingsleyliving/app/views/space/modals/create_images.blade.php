<div class="modal fade" id="newSpaceImages" tabindex="-1" role="dialog" aria-labelledby="space_new_images_modal" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Add Space Images
				</h4>
			</div>
			<div class="modal-body">
				<div id="space-images-carousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#space-images-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#space-images-carousel" data-slide-to="1"></li>
						<li data-target="#space-images-carousel" data-slide-to="2"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="..." alt="...">
							<div class="carousel-caption">
								<h3>Sample Image</h3>
								<p>This is a sample image!</p>
							</div>
						</div>
						<div class="item">
							<img src="..." alt="...">
							<div class="carousel-caption">
								<h3>Sample Image</h3>
								<p>This is a sample image!</p>
							</div>
						</div>
						...
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>
			</div>
			<div class="modal-body">
				{{ Form::open(array('action' => 'ImageController@store', 'files' => true, 'role' => 'form')) }}
					{{ Form::hidden('space_num') }}
					{{ Form::hidden('location') }}
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::label('image', 'Upload Space Images:') }}
								{{ Form::file('image', array('class' => 'form-control', 'id' => 'current_image')) }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('display_name', 'Give the image a name:') }}
								{{ Form::text('display_name', '', array('class' => 'form-control display_name')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('display_name', 'Give the image a name:') }}
								{{ Form::text('display_name', '', array('class' => 'form-control display_name')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">
					Done
				</button>
			</div>
		</div>
	</div>
</div>