@extends('layouts.master')

<?php 
function generate_fieldset($community, $space)
{
	$spc_fieldset = '<fieldset class="col-xs-6 col-sm-4">'
			. '<div class="row">'
				. '<div class="col-xs-6">';
	// Insert image here
	$spc_fieldset .= '</div>'
				. '<div class="col-xs-6">'
					. '<div class="row">'
						. '<table class="table table-striped table-bordered space_table">'
							. '<tr>'
								. '<td>'
									. 'House Number'
								. '</td>'
								. '<td>'
									. ($space['space_num']?$space['space_num']:'N/A')
								. '</td>'
							. '</tr>'
							. '<tr>'
								. '<td>'
									. 'Beds'
								. '</td>'
								. '<td>'
									. ($space['space_detail']['beds']?$space['space_detail']['beds']:'N/A')
								. '</td>'
							. '</tr>'
							. '<tr>'
								. '<td>'
									. 'Baths'
								. '</td>'
								. '<td>'
									. ($space['space_detail']['baths']?$space['space_detail']['baths']:'N/A')
								. '</td>'
							. '</tr>'
							. '<tr>'
								. '<td>'
									. 'Square Feet'
								. '</td>'
								. '<td class="square_feet">'
									. ($space['space_detail']['square_feet']?$space['space_detail']['square_feet']:'N/A')
								. '</td>'
							. '</tr>'
							. '<tr>'
								. '<td>'
									. 'Sale Price'
								. '</td>'
								. '<td class="sale_price">'
									. ($space['space_pricing']['sale_price']?$space['space_pricing']['sale_price']:'N/A')
								. '</td>'
							. '</tr>'
						. '</table>'
					. '</div>'
					. '<div class="row view_home">'
						. '<div class="col-xs-12">'
							. '<button class="btn btn-success btn-sm" onclick="window.location=\''
							. url($community->id) . '/space/'. $space['id'] .'/view\'">View this home</button>'
						. '</div>'
					. '</div>'
				. '</div>'
			. '</div>'
		. '</fieldset>';

	return $spc_fieldset;
}
?>

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="space_index">
			<div class="panel-heading page-header">
				@if (!Confide::user() || Confide::user()->hasRole(array('current_resident', 'future_resident', 'past_resident')))
					<h3>Homes at {{ $community->name }}<br />
				@else
					<h3>All Homes of {{ $community->name }}<br />
				@endif
				<small><b>Address:</b>&nbsp;{{ $community_address['billing']['address_1'] }},&nbsp;{{ $community_address['billing']['city'] }},&nbsp;{{ $community_address['billing']['state'] }}</small><br /><small><b>Phone:</b>&nbsp;{{ $community->phone }}</small></h3>
			</div>
			<div class="panel-body">
				<!-- <div class="row">
					<div class="col-xs-12">
						<div class="col-xs-6">
							<b>Address:</b>&nbsp;{{ $community_address['billing']['address_1'] }},&nbsp;{{ $community_address['billing']['city'] }},&nbsp;{{ $community_address['billing']['state'] }}
						</div>
					</div>
				</div> -->
				<fieldset class="col-xs-12 space_fieldset">
					<div class="row">
					@foreach ($spaces as $space)
						{{ generate_fieldset($community, $space) }}
					@endforeach
					</div>
				</fieldset>
			</div>
			<script type="text/javascript">
				$(document).ready(function() {
					$.each($('.sale_price'), function() {
						$(this).number(true, 2);
						var old_pur_price = $(this).text();
						$(this).html('&#36;'+old_pur_price);
					});
					$('.square_feet').number(true);
				});
			</script>
		</div>
	</div>
</div>
@stop