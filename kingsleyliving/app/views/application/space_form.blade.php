<div class="row">
	<fieldset class="col-xs-12 col-md-6 applicant-info" id="applicant_info">
		<div class="col-xs-12 form-group">
			{{-- dd($communities) --}}
			{{ Form::label('community', 'Community:') }}
			{{ Form::select('community', $communities, $comm->id, array('class' => 'form-control chosen-select', 'disabled')) }}
			{{ Form::hidden('community_id', $comm->id) }}
		</div>
		<div class="col-xs-12 form-group">
			{{ Form::label('home', 'Home:') }}
			{{ Form::select('home', $spaces, $space['id'], array('class' => 'form-control chosen-select', 'data-placeholder' => 'What space are you applying for?', 'disabled')) }}
			{{ Form::hidden('space', $space['id']) }}
		</div>
		<div class="col-xs-12 form-group">
			{{ Form::label('move_in_date', 'Desired Move In Date:') }}
			{{ Form::text('move_in_date', null, array('class' => 'form-control datepicker')) }}
			{{ Form::hidden('resident_type', 1) }}
		</div>
		@if (Confide::user())
		{> $user = Confide::user() <}
		<div class="col-xs-12 form-group">
			{{ Form::label('email', 'Email:') }}
			{{ Form::text('email', $user->email, array('class' => 'form-control')) }}
			{{ Form::hidden('user_id', $user->id) }}
		</div>
		<div class="col-xs-12 form-group">
			{> $user_profile = UserProfile::where('user_id', $user->id)->first() <}
			{> $first_name = $user_profile->first_name <}
			{> $middle_name = $user_profile->middle_name <}
			{> $last_name = $user_profile->last_name; <}
			{> $suffix = $user_profile->suffix; <}
			{> $full_name = $first_name . (!empty($middle_name)?' ' . $middle_name . ' ': ' ') . $last_name . (!empty($suffix)?' ' . $suffix:'') <}
			{{ Form::label('name', 'Full Name:') }}
			{{ Form::text('name', $full_name, array('class' => 'form-control user_name')) }}
			<span class="help-block apply_user_name"></span>
			{{ Form::hidden('first_name', $first_name) }}
			{{ Form::hidden('middle_name', $middle_name) }}
			{{ Form::hidden('last_name', $last_name) }}
			{{ Form::hidden('suffix', $suffix) }}
		</div>
		<div class="col-xs-12 form-group">
			{{ Form::label('phone', 'Phone:') }}
			{{ Form::text('phone', $user_profile->phone, array('class' => 'form-control phone')) }}
		</div>
		@else
		<div class="col-xs-12 form-group">
			{{ Form::label('email', 'Email:') }}
			{{ Form::text('email', null, array('class' => 'form-control')) }}
		</div>
		<div class="col-xs-12 form-group">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					{{ Form::label('prefix', 'Prefix:') }}
					{{ Form::select('gender', ['mr' => 'Mr.', 'mrs' => 'Mrs.', 'miss' => 'Miss'], null, array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-12 col-md-8">
					{{ Form::label('name', 'Full Name:') }}
					{{ Form::text('name', null, array('class' => 'form-control user_name')) }}
					<span class="help-block apply_user_name"></span>
					{{ Form::hidden('first_name') }}
					{{ Form::hidden('middle_name') }}
					{{ Form::hidden('last_name') }}
					{{ Form::hidden('suffix') }}
				</div>
			</div>
		</div>
		<div class="col-xs-12 form-group">
			{{ Form::label('phone', 'Phone:') }}
			{{ Form::text('phone', null, array('class' => 'form-control phone')) }}
		</div>
		<div class="col-xs-12 form-group username">
			{{ Form::label('username', 'Desired username:') }}
			{{ Form::text('username', Input::old('username'), array('class' => 'form-control username')) }}
		</div>
		<div class="col-xs-12 form-group password">
			{{ Form::label('password', 'Password:') }}
			{{ Form::password('password', array('class' => 'form-control')) }}
		</div>
		<div class="col-xs-12 form-group password">
			{{ Form::label('password_confirmation', 'Password Confirmation:') }}
			{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
		</div>
		@endif
	</fieldset>
	@include('components.address_component', array('field_prefix' => 'current', 'size' => 'half'))
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{{ Form::label('notes', 'Any extra notes:') }}
		{{ Form::textarea('notes', Input::old('notes'), array('class' => 'form-control', 'rows' => '3')) }}
	</div>
</div>