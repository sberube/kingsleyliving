@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_application-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				{{ Form::open(array('action' => 'ApplicationController@store', 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('application.form', array('is_new' => true))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save', array('class' => 'btn btn-primary application-submit-btn')) }}
								{{ Form::reset('Clear Form', array('class' => 'btn btn-primary application-reset-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop