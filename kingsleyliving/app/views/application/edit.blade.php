@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_application-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				{{ Form::model($application, array('method' => 'put', 'action' => array('ApplicationController@update', $application['id']), 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('application.form', array('is_new' => false))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save Changes', array('class' => 'btn btn-primary application-submit-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop