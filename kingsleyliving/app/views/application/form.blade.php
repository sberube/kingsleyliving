@if ( $errors->count() > 0 )
<div class="row">
	<div class="col-xs-12 alert alert-danger">
		<ul>
			@foreach ( $errors->all() as $message )
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
<div class="row">
	<fieldset class="col-xs-12 application-info" id="application_info">
		<legend>Community Information</legend>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{-- dd($communities) --}}
				{{ Form::label('community_id', 'Community:') }}
				@if ($url_request == 'application/create')
					{{ Form::select('community_id', $communities, $app_community['id'], array('class' => 'form-control chosen-select')) }}
				@else
					{{ Form::select('community_id', $communities, Input::old('community_id'), array('class' => 'form-control chosen-select')) }}
				@endif
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('space', 'Space:') }}
				@if (isset($app_space))
					{{ Form::select('space', $spaces, $app_space['id'], array('class' => 'form-control chosen-select', 'data-placeholder' => 'What space are you applying for?')) }}
				@else
					{{ Form::select('space', $spaces, Input::old('space'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'What space are you applying for?')) }}
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('source', 'Sales Associate:') }}
				{{ Form::text('source', Input::old('source'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				{{ Form::label('move_in_date', 'Desired Move In Date:') }}
				{{ Form::text('move_in_date', Input::old('move_in_date'), array('class' => 'form-control datepicker')) }}
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<fieldset class="col-xs-12 community-images" id="community_images">
		<legend>Application Resident</legend>
		<div>
			<div>
				{{ Form::checkbox('current_user_primary', 'current_user_primary', Input::old('current_user_primary')) }}
				{{ Form::label('current_user_primary', 'I&#39;m signed in, put me as the primary.') }}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 form-group">
				<div class="panel" id="primary_resident">
					<div class="panel-heading"><h3>Primary Resident</h3></div>
					<div class="panel-body">
						<div class="row"><!-- first and middle name -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('first_name', 'First Name:') }}
								{{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('phone', 'Phone:') }}
								{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control phone')) }}
							</div>
						</div>
						<div class="row"><!-- last name and phone -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('middle_name', 'Middle Name:') }}
								{{ Form::text('middle_name', Input::old('middle_name'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('marital_status_id', 'Marital Status:') }}
								{{ Form::text('marital_status_id', Input::old('marital_status_id'), array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row"><!-- marital status and birthday -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('last_name', 'Last Name:') }}
								{{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('birthday', 'Birthday:') }}
								{{ Form::text('birthday', Input::old('birthday'), array('class' => 'form-control datepicker')) }}
							</div>
						</div>
						<div class="row"><!-- current address -->
							@include('components.address_component', array('field_prefix' => 'primary_current', 'size' => 'full'))
						</div>
						<div class="row"><!-- how long at current address -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('months', 'Months at Current Address:') }}
								{{ Form::text('months', Input::old('months'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('years', 'Years at Current Address:') }}
								{{ Form::text('years', Input::old('years'), array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row"><!-- previous address -->
							@include('components.address_component', array('field_prefix' => 'primary_previous', 'size' => 'full'))
						</div>
						<div class="row"><!-- how long at previous address -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('months', 'Months at Previous Address:') }}
								{{ Form::text('months', Input::old('months'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('years', 'Years at Previous Address:') }}
								{{ Form::text('years', Input::old('years'), array('class' => 'form-control')) }}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 form-group">
				<div class="panel" id="cosigner_resident">
					<div class="panel-heading"><h3>Co-signer Resident</h3></div>
					<div class="panel-body">
						<div class="row"><!-- first and middle name -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('first_name', 'First Name:') }}
								{{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('phone', 'Phone:') }}
								{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control phone')) }}
							</div>
						</div>
						<div class="row"><!-- last name and phone -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('middle_name', 'Middle Name:') }}
								{{ Form::text('middle_name', Input::old('middle_name'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('marital_status_id', 'Marital Status:') }}
								{{ Form::text('marital_status_id', Input::old('marital_status_id'), array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row"><!-- marital status and birthday -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('last_name', 'Last Name:') }}
								{{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('birthday', 'Birthday:') }}
								{{ Form::text('birthday', Input::old('birthday'), array('class' => 'form-control datepicker')) }}
							</div>
						</div>
						<div class="row"><!-- current address -->
							@include('components.address_component', array('field_prefix' => 'cosigner_current', 'size' => 'full'))
						</div>
						<div class="row"><!-- how long at current address -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('months', 'Months at Current Address:') }}
								{{ Form::text('months', Input::old('months'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('years', 'Years at Current Address:') }}
								{{ Form::text('years', Input::old('years'), array('class' => 'form-control')) }}
							</div>
						</div>
						<div class="row"><!-- previous address -->
							@include('components.address_component', array('field_prefix' => 'cosigner_previous', 'size' => 'full'))
						</div>
						<div class="row"><!-- how long at previous address -->
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('months', 'Months at Previous Address:') }}
								{{ Form::text('months', Input::old('months'), array('class' => 'form-control')) }}
							</div>
							<div class="col-xs-12 col-md-6 form-group">
								{{ Form::label('years', 'Years at Previous Address:') }}
								{{ Form::text('years', Input::old('years'), array('class' => 'form-control')) }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</fieldset>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{{ Form::label('notes', 'Any extra notes:') }}
		{{ Form::textarea('notes', Input::old('notes'), array('class' => 'form-control', 'rows' => '6')) }}
	</div>
</div>