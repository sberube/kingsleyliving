<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Inquiring Form</h2>

		<div>
			<p>Hey Thayne, this is just a test, this time we're testing a new email that will be purpose built for this very reason (sending emails I mean), send an email to Shaine {{{ '<shaineberube@kingsleylivingcorp.com>' }}}, if it comes through properly.</p>
			<h4>User Information</h4>
			<p>Full Name: {{ $full_name }}</p>
			<p>Email: {{ $user['email'] }}</p>
			<p>Phone: {{ $user['user_profile']['phone'] }}</p>
			<h4>Current Address:</h4>
			<p>Street: {{ $user['billing']['address_1'] }}</p>
			<p>City: {{ $user['billing']['city'] }}</p>
			<p>State: {{ $user['billing']['state'] }}</p>
			<p>Zip / Postal Code: {{ $user['billing']['postal_code'] }}</p>
			<p>Country: {{ $user['billing']['country'] }}</p>

			<h4>Application Information:</h4>
			<p>Interested in community: {{ $community->name }}</p>
			<p>Home #: {{ $space->space_num }} </p>
			<p>Desired move in date: {{ $application_resident->move_in_date }} </p>
			<p>Notes: {{ $application->notes }}</p>
		</div>
	</body>
</html>
