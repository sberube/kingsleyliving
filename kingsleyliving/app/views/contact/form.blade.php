@if ( $errors->count() > 0 )
<div class="row">
	<div class="col-xs-12 alert alert-danger">
		<ul>
			@foreach ( $errors->all() as $message )
				<li>{{ $message }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
{{ Form::hidden('contact_status_id', '1') }}
{{ Form::hidden('contact_type_id', '1') }}
<div class="row">
	<fieldset class="col-xs-12 col-md-6 contact-info" id="contact_info">
		<legend>Contact Information</legend>
		<div class="row">
			<div class="col-xs-12 form-group">
				{{ Form::label('first_name', 'First name:') }}
				{{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 form-group">
				{{ Form::label('last_name', 'Last name:') }}
				{{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 form-group">
				{{ Form::label('email', 'Contact Email:') }}
				{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
			</div>
			<div class="col-xs-12 form-group">
				{{ Form::label('phone', 'Phone number:') }}
				{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control phone')) }}
			</div>
			<div class="col-xs-12 form-group">
				{{ Form::label('contact_interest', 'Topic of Interest:') }}
				{> $contactTopicInterestDDL = array(0 => '') <}
				{> $topic_interest = ContactInterest::lists('interests', 'id') <}
				@foreach ($topic_interest as $id => $topic)
					{> $contactTopicInterestDDL[$id] = $topic <}
				@endforeach
				{{ Form::select('contact_interest_id', $contactTopicInterestDDL, Input::old('contact_interest_id'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please select your topic...')) }}
			</div>
			<div class="col-xs-12 form-group">
				{{ Form::label('contact_means', 'Contact Means:') }}
				{> $contactMeansDDL = array(0 => '') <}
				{> $contact_means = ContactMean::lists('means', 'id') <}
				@foreach ($contact_means as $id => $means)
					{> $contactMeansDDL[$id] = $means <}
				@endforeach
				{{ Form::select('contact_means_id', $contactMeansDDL, Input::old('contact_means_id'), array('class' => 'form-control chosen-select', 'data-placeholder' => 'Please select your means of contact...')) }}
			</div>
			<div class="col-xs-12 form-group">
				{{ Form::label('contact_hours', 'Availability:') }}
				<p class="help-block">Coming soon...</p>
				{{-- Widget or component like address? --}}
			</div>
		</div>
	</fieldset>
	<fieldset class="col-xs-12 col-md-6 address-info" id="address_info">
		<legend>Address Details</legend>
		@include('components.address_component', array('field_prefix' => 'contact', 'add_type' => 7, 'size' => 'full'))
	</fieldset>
</div>
<div class="row">
	<div class="col-xs-12 form-group">
		{{ Form::label('extra_notes', 'Any further notes:') }}
		{{ Form::textarea('extra_notes', Input::old('extra_notes'), array('class' => 'form-control', 'rows' => '2')) }}
	</div>
</div>