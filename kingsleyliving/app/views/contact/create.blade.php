@extends('layouts.master')

@section('before_body')
	{{-- @include('contact.modals.attach_files') --}}
@stop

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="new_contact-form">
			<div class="panel-heading"><h3>{{ $title }}</h3></div>
			<div class="panel-body">
				{{ Form::open(array('action' => 'ContactController@store', 'files' => true, 'role' => 'form')) }}
				{{ Form::hidden('url_request', $url_request) }}
					<div class="container-fluid">
						@include('contact.form', array('is_new' => true))
						<div class="row">
							<div class="col-xs-12 form-group">
								{{ Form::submit('Save', array('class' => 'btn btn-primary contact-submit-btn')) }}
								{{ Form::reset('Clear Form', array('class' => 'btn btn-primary contact-reset-btn')) }}
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop