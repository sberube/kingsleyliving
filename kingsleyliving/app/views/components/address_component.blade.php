@if ($size == 'full')
	<fieldset class="col-xs-12 {{ $field_prefix }}-address-info" id="{{ $field_prefix }}_address_info">
@else
	<fieldset class="col-xs-12 col-md-6 {{ $field_prefix }}-address-info" id="{{ $field_prefix }}_address_info">
@endif
	<legend>{{ ucwords(str_replace('_', ' ', $field_prefix)) }}&nbsp;Address</legend>
	@if (empty($add_type))
		{> $add_type = 1 <}
	@endif
	{{ Form::hidden($field_prefix.'[type_id]', $add_type) }}
	<div class="row">
		<div class="col-xs-12 form-group">
			{{ Form::label($field_prefix.'[address_1]', 'Address:') }}
			{{ Form::text($field_prefix.'[address_1]', Input::old($field_prefix.'[address_1]'), array('class' => 'form-control address_1')) }}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			{{ Form::label($field_prefix.'[address_2]', 'Alt Address:') }}
			{{ Form::text($field_prefix.'[address_2]', Input::old($field_prefix.'[address_2]'), array('class' => 'form-control address_2')) }}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			{{ Form::label($field_prefix.'[city]', 'City:') }}
			{{ Form::text($field_prefix.'[city]', Input::old($field_prefix.'[city]'), array('class' => 'form-control city')) }}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			{{ Form::label($field_prefix.'[state]', 'State:') }}
			{{ Form::text($field_prefix.'[state]', Input::old($field_prefix.'[state]'), array('class' => 'form-control state')) }}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			{{ Form::label($field_prefix.'[postal_code]', 'Zip:') }}
			{{ Form::text($field_prefix.'[postal_code]', Input::old($field_prefix.'[postal_code]'), array('class' => 'form-control postal_code')) }}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			{{ Form::label($field_prefix.'[country_id]', 'Country:') }}
			{{ Form::select($field_prefix.'[country_id]', AddressCountry::lists('name', 'id'), Input::old($field_prefix.'[country_id]'), array('class' => 'form-control chosen-select country')) }}
		</div>
	</div>
</fieldset>