<div class='map-legend'>
	<div class="{{ $map_data['legend_style'] }}">
		<div class='legend-title'>{{ $map_data['legend_title'] or '' }}</div>
		<div class="legend-title-default hidden">{{ $map_data['legend_title'] or '' }}</div>
		<div class='legend-scale'>
			<ul class='legend-labels'>
				@if (isset($map_data['legend_labels']) or '')
					@foreach ($map_data['legend_labels'] as $elem_array)
						<?php $li_attr = '' ?>
						@foreach ($elem_array['list_attributes'] as $id => $attr)
							<?php $li_attr .= ' ' . $id . '=' . '"' . $attr . '"'; ?>
						@endforeach
						<li{{ $li_attr }}>
						@if (isset($elem_array['span_after']))
							@if (isset($elem_array['list_contents']))
								{{ $elem_array['list_contents'] }}
							@endif
							@if (isset($elem_array['span_attributes']))
								<?php $span_attr = ''; ?>
								@foreach ($elem_array['span_attributes'] as $id => $attr)
									<?php $span_attr .= ' ' . $id . '=' . '"' . $attr . '"'; ?>
								@endforeach
								<span{{ $span_attr }}>
								@if (isset($elem_array['span_contents']))
									{{ $elem_array['span_contents'] }}
								@endif
								</span>
							@endif
						@else
							@if (isset($elem_array['span_attributes'])){
								<?php $span_attr = ''; ?>
								@foreach ($elem_array['span_attributes'] as $id => $attr)
									<?php $span_attr .= ' ' . $id . '=' . '"' . $attr . '"'; ?>
								@endforeach
								<span{{ $span_attr }}>
								@if (isset($elem_array['span_contents']))
									{{ $elem_array['span_contents'] }}
								@endif
								</span>
							@endif
							@if (isset($elem_array['list_contents']))
								{{ $elem_array['list_contents'] }}
							@endif
						@endif
						</li>
					@endforeach
				@endif
			</ul>
		</div>
		@if (isset($map_data['legend_source']) or '')
			<div class="legend-source">{{ $map_data['legend_source'] }}</div>
		@endif
	</div>
</div>