{{ Form::model($search, array('action' => 'SpaceController@searchresults', 'role' => 'form', 'method' => 'GET')) }}
	<fieldset class="col-xs-12 in-page-search" id="in_page_seach">
		@if($request == 'vertical')
	{{-- **************************************************************************** --}}
	{{-- *						    Vertical Search Form    						* --}}
	{{-- **************************************************************************** --}}
			<legend>Refine Search</legend>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel">
						<div class="panel-heading"><b>Edit Location</b></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 form-group">
									{{ Form::label('state', 'State:') }}
									{{ Form::select('state', $states, Input::old('state'), array('class' => 'form-control chosen-select')) }}
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 form-group">
									{{ Form::label('city', 'City:') }}
									{{ Form::select('city', $cities, Input::old('city'), array('class' => 'form-control chosen-select')) }}
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 form-group">
									- Or -
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 form-group">
									{{ Form::label('zip', 'Zip:') }}<p class="help-block pull-right">Currently not available...</p>
									{{ Form::text('zip', Input::old('zip'), array('class' => 'form-control', 'disabled')) }}
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 form-group">
									{{ Form::label('radius', 'Radius:') }}
									{{ Form::select('radius', $radius, Input::old('radius'), array('class' => 'form-control chosen-select', 'disabled')) }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-6 form-group">
					{{ Form::label('price_min', 'Price Min:') }}
					{{ Form::select('price_min', $price_min, Input::old('price_min'), array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-12 col-md-6 form-group">
					{{ Form::label('price_max', 'Price Max:') }}
					{{ Form::select('price_max', $price_max, Input::old('price_max'), array('class' => 'form-control chosen-select')) }}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-6 form-group">
					{{ Form::label('beds', 'Beds:') }}
					{{ Form::select('beds', $beds, Input::old('beds'), array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-12 col-md-6 form-group">
					{{ Form::label('baths', 'Baths:') }}
					{{ Form::select('baths', $baths, Input::old('baths'), array('class' => 'form-control chosen-select')) }}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{{ Form::label('amenities', 'Common Community Amenities:') }}
					@foreach ($amenities as $id => $name)
						<div class="col-xs-6 checkbox">
							{{ Form::checkbox($name, Input::old($name)) }}
							{{ Form::label($name, ucwords($name)) }}
						</div>
					@endforeach
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{{ Form::label('community_type', 'Community Type:') }}
					{{ Form::select('community_type', $community_type, Input::old('community_type'), array('class' => 'form-control chosen-select')) }}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					{{ Form::label('community', 'Community:') }}
					{{ Form::select('community', $community, Input::old('community'), array('class' => 'form-control chosen-select')) }}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group text-center">
					<!-- Button to update or begin search -->
					@if (Request::is('*/view'))
						{{ Form::submit('Advanced Search', array('class' => 'btn btn-primary search-submit-btn')) }}
					@else
						{{ Form::submit('Update Search', array('class' => 'btn btn-primary search-submit-btn')) }}
						{{ Form::reset('Reset Search', array('class' => 'btn btn-primary search-reset-btn')) }}
					@endif
				</div>
			</div>
	{{-- ************************************************************************ --}}
	{{-- *						   Horizontal Search Form  						* --}}
	{{-- ************************************************************************ --}}
		@else
			<div class="row">
				<div class="col-xs-12">
					<div class="panel">
						<div class="panel-heading"><b>Edit Location</b></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-6 col-md-3 form-group">
									{{ Form::label('state', 'State:') }}
									{{ Form::select('state', $states, '', array('class' => 'form-control chosen-select')) }}
								</div>
								<div class="col-xs-6 col-md-3 form-group">
									{{ Form::label('city', 'City:') }}
									{{ Form::select('city', $cities, '', array('class' => 'form-control chosen-select')) }}
								</div>
								<div class="col-xs-6 col-md-1 form-group">
									- Or -
								</div>
								<div class="col-xs-6 col-md-2 form-group">
									{{ Form::label('zip', 'Zip:') }}
									{{ Form::text('zip', '', array('class' => 'form-control', 'disabled')) }}
									<p class="help-block">Currently not available...</p>
								</div>
								<div class="col-xs-6 col-md-3 form-group">
									{{ Form::label('radius', 'Radius:') }}
									{{ Form::select('radius', $radius, '', array('class' => 'form-control chosen-select', 'disabled')) }}
									{{ Form::hidden('listing_option', $listing_option) }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row other_search_options">
				<div class="col-xs-6 col-md-3 form-group">
					{{ Form::label('price_min', 'Price Min:') }}
					{{ Form::select('price_min', $price_min, '', array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-6 col-md-3 form-group">
					{{ Form::label('price_max', 'Price Max:') }}
					{{ Form::select('price_max', $price_max, '', array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-6 col-md-3 form-group">
					{{ Form::label('beds', 'Beds:') }}
					{{ Form::select('beds', $beds, '', array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-6 col-md-3 form-group">
					{{ Form::label('baths', 'Baths:') }}
					{{ Form::select('baths', $baths, '', array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-6 form-group">
					{{ Form::label('amenities', 'Common Community Amenities:') }}
					<div class="col-xs-12">
						<div class="row search_component_amenities">
							@foreach ($amenities as $id => $name)
								<div class="col-xs-6 col-md-3">
									<label for="{{ $name }}" class="checkbox-inline">
										{{ Form::checkbox($name, Input::old($name)) }}&nbsp;{{ ucwords(str_replace('_', ' ', $name)) }}
									</label>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 form-group">
					{{ Form::label('community_type', 'Community Type:') }}
					{{ Form::select('community_type', $community_type, '', array('class' => 'form-control chosen-select')) }}
				</div>
				<div class="col-xs-6 col-md-3 form-group">
					{{ Form::label('community', 'Community:') }}
					{{ Form::select('community', $community, '', array('class' => 'form-control chosen-select')) }}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<hr />
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 form-group">
					<!-- Button to update or begin search -->
					{{ Form::submit('Advanced Search', array('class' => 'btn btn-primary search-submit-btn')) }}
					{{ Form::button('Show More', array('class' => 'btn btn-link show_btn')) }}
				</div>
			</div>
		@endif
	</fieldset>
{{ Form::close() }}