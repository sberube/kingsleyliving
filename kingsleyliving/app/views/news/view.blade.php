@extends('layouts.master')

@section('content')
<div class="container-fluid">
	@foreach ($news_article as $news_item)
		<div class="col-xs-12 panel">
			<div class="panel-heading">{{ $news_item['title'] }}</div>
			<div class="panel-body">
				{{ $news_item['content'] }}
			</div>
		</div>
	@endforeach
</div>
@stop