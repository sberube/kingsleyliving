@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel">
			<div class="panel-heading">
				<h3>Recent News for Kingsley</h3>
			</div>
			<div class="panel-body">
				@if (empty($news_articles))
					We're sorry, it appears that there are no recent news items...
				@else
				<div class="row">
				@foreach ($news_articles as $news_item)
					<div class="col-xs-12 panel">
						<div class="panel-heading">{{ $news_item['title'] }}</div>
						<div class="panel-body">
							{{ $news_item['content'] }}
						</div>
						<div class="panel-footer"><a href="news/view/{{ $news_item['slug'] }}">View article</a></div>
					</div>
				@endforeach
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@stop