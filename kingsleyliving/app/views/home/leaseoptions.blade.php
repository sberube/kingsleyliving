@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="leaseoptions-page">
			<div class="jumbotron leaseoptions_jumbotron">
				<div class="container">
					<h2 class="jumbotron-header">{{ $title }}</h2>
					<p class="jumbotron-body">We have great communities and wonderful homes to choose from!  Stop by one of your local communities today!</p>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					@include('components.search_component', array('request' => 'horizontal'))
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h4>Lease Options For A Manufactured Home</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Want to buy a home, but don't have all of the money right now? We have lease options that will allow you to start earning credit towards a home today! No external loans and you'll be able to work towards homeownership right away!</p>
						<p>We don't underestimate what a accomplishment it is to own a home so we try and provide as many people as we can with the opportunity to work towards that goal. Call our communities today to find a price that fits your budget!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop