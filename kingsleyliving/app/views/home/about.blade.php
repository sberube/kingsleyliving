@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="about-page">
			<div class="panel-heading"><h2>About Our Communities</h2></div>
			<div class="panel-body">
				<p>From Texas to Idaho and New York to California, and everywhere in between, you'll find more than 45 communities that enhance the cities of which they are a part.</p>
				<p>Home is more than just a place to hang your hat. It's a place to gather loved ones near; a place where you can connect with your neighbors and friends. It's a place where you belong. Home ownership is a significant accomplishment. Our goal is to help everyone seeking that dream to find it. We offer financing and flexible payment terms to help good, hard-working people have a place to call their own. Whether you're looking to rent or buy, we have a place for you!</p>
				<p>Our communities will make you feel right at home. With friendly neighbors and beautiful grounds, you'll never want to leave. In each community you'll find talented personnel who are there to serve the residents of our communities with personal care. Each manager has been trained to deliver the best customer service you can find. Any questions or concerns that you may have, they'll have the answer.</p>
				<p>Our communities strive to provide safe, comfortable and affordable living conditions for all of our residents and, of course, we have fun too. Community activities run throughout the year, allowing for community members come together and connect. In each property we strive for unity so that everyone feels at home and a part of their community.</p>
				<p>Though we've experienced tremendous growth and success during the last three decades, we haven't forgotten the values that got us to this point: integrity, fairness, hard work and doing the right thing. These principles still shape the choices we make every day.</p>
			</div>
		</div>
	</div>
</div>
@stop