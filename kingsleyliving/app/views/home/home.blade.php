@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12 panel panel-default slide-map">
		<div class="col-xs-12 col-md-6 panel">
			@include('home.wow_slider')
		</div>
		<div class="map-header">Select a state to find a community near you!</div>
		<div class="map-reset btn btn-info">Reset</div>
		<div class="json_last_selected hidden"></div>
		@include('components.map_legend', array('map_data' => $legend_data))
		<div class="map-footer">&larr; Or use the search button to do an advanced search.</div>
		<div class="open-search btn btn-primary"><span class="icon ion-search"></span>&nbsp;Search</div>
		<div class="col-xs-12 col-md-6 rollover-area">
			<div id="kingsley_map" class="panel kingsley_map kingsley_map-default"></div>
			<div class="map_open_rollover_bar">&nbsp;
				{{-- <div class="bar-top hidden"></div> --}}
				{{-- <div class="bar-front hidden"></div> --}}
				{{-- <div class="bar-bottom hidden"></div> --}}
				<div class="search-options-rollout">
					@include('widget.search_widget')
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid further-info">
		<div class="col-xs-12 col-md-4">
			<div class="panel family-friendly">
				<div class="panel-heading">
					<h3 class="panel-title">Family Friendly Communities</h3>
				</div>
				<div class="panel-body">
					Looking for a safe and neighborly community to move your family into? Look no further! Our family communities are full of family oriented residents and festivities that will make you feel right at home.
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="panel retirement-info">
				<div class="panel-heading">
					<h3 class="panel-title">Retirement Information</h3>
				</div>
				<div class="panel-body">
					When the time comes to settle into retirement, a quiet and relaxing community is just the thing you&#39;ll need. Find your way into one of our 50+ parks and you will find just the peace you&#39;re looking for.
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="panel specials-info">
				<div class="panel-heading">
					<h3 class="panel-title">Specials Information</h3>
				</div>
				<div class="panel-body">
					We&#39;ll pay you for your move! Lease a space and move your manufactured home into one of our communities today and we&#39;ll pay you to do it!
				</div>
			</div>
		</div>
	</div>
</div>
@stop