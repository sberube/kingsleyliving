@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="findahomesite-page">
			<div class="jumbotron findahomesite_jumbotron">
				<div class="container">
					<h2 class="jumbotron-header">{{ $title }}</h2>
					<p class="jumbotron-body">We have great communities and wonderful homes to choose from!  Stop by one of your local communities today!</p>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					@include('components.search_component', array('request' => 'horizontal'))
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h4>Look At Available Home Sites</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Lease Your Home Site with Kingsley Management Communities Today!</p>
						<p>Our community managers are ready to help you in your search to find a home! Contact one of our communities today to find the perfect fit for you. Our communities will make you feel right at home. With friendly neighbors and beautiful grounds, you'll never want to leave.</p>
						<p>Our communities will also provide you with:
							<ul>
								<li>Great amenities for the whole community to use</li>
								<li>A safe and secure environment</li>
								<li>Privacy in your home and your own yard</li>
								<li>A wonderful staff that will be there to aid you with any needs you may have</li>
							</ul>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop