@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="rentahome-page">
			<div class="jumbotron rentahome_jumbotron">
				<div class="container">
					<h2 class="jumbotron-header">{{ $title }}</h2>
					<p class="jumbotron-body">We have great communities and wonderful homes to choose from!  Stop by one of your local communities today!</p>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					@include('components.search_component', array('request' => 'horizontal'))
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h4>Rent A Beautiful Home</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Are you looking to rent? Why settle for an apartment when you can have a home for even less? Here at Kingsley Management most of our homes have multiple bedrooms and bathrooms and you don't have to worry about a neighbor upstairs! You'll be able to enjoy not only privacy in your home, but you'll have your own yard as well. Plus, most of our communities have amenities including clubhouses, playscapes, pools and basketball courts! </p>
						<p>If you already have a home and want to rent a space in our community, you can do that too! Our beautiful communities are full of united and neighborly residents who will make you feel right at home! If you want to join our community family call our communities today!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop