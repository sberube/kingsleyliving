@extends('layouts.master')

@section('content')
<div class="container-fluid">
	<div class="col-xs-12">
		<div class="panel" id="buyahome-page">
			<div class="jumbotron buyahome_jumbotron">
				<div class="container">
					<h2 class="jumbotron-header">{{ $title }}</h2>
					<p class="jumbotron-body">We have great communities and wonderful homes to choose from!  Stop by one of your local communities today!</p>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					@include('components.search_component', array('request' => 'horizontal'))
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h4>Buy A Manufactured Home</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Most of our communities have homes for sale, fitting every kind of price range. Each property office has a manager who will consult you in your sale. They will make it their top priority to find the perfect home for you. If there isn’t the home you want in their park, they will also be able to refer you to a neighboring Kingsley park to make sure you are well taken care of.</p>
						<p>We strive to provide comfortable, affordable and safe housing options for all who are interested. If you’re interested in finding the perfect place for you, contact one of our many parks all over the United States. We will do our very best to fulfill your housing needs!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop