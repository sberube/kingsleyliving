<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@view');
Route::get('/buyahome', 'HomeController@buyahome');
Route::get('/rentahome', 'HomeController@rentahome');
Route::get('/leaseoptions', 'HomeController@leaseoptions');
Route::get('/findahomesite', 'HomeController@findahomesite');
Route::get('/termsandconditions', 'HomeController@termsandconditions');
Route::get('/privacypolicy', 'HomeController@privacypolicy');
Route::get('/sitemap', 'HomeController@sitemap');
Route::get('/about', 'HomeController@about');

Route::post('/rmaprequest/update', 'ResidentMapController@fullUpdate');
Route::post('/rmaprequest/login', 'ResidentMapController@postLogin');
Route::get('/rmaprequest/users', 'ResidentMapController@getUsers');
Route::get('/rmaprequest/communities', 'ResidentMapController@getCommunities');
Route::get('/rmaprequest/update_community_managers', 'ResidentMapController@getUpdateCommunityManagers');
Route::get('/rmaprequest/managers/{propNum}', 'ResidentMapController@getManagers');
Route::get('/rmaprequest/update_community_residents', 'ResidentMapController@getUpdateCommunityResidents');
Route::get('/rmaprequest/residents/{propNum}', 'ResidentMapController@getResidents');
Route::get('/rmaprequest/update_community_spaces', 'ResidentMapController@getUpdateCommunitySpaces');
Route::get('/rmaprequest/spaces/{propNum}', 'ResidentMapController@getSpaces');

Route::get('/authtest', array('before' => 'auth.basic', function()
{
    return View::make('hello');
}));

// Confide routes
Route::group(array('prefix' => 'users'), function(){
    // Route::get('/create', 'UsersController@create');
    Route::post('/', 'UsersController@store');
    Route::get('/login', 'UsersController@login');
    Route::post('/login', 'UsersController@doLogin');
    Route::get('/confirm/{code}', 'UsersController@confirm');
    Route::get('/forgot_password', 'UsersController@forgotPassword');
    Route::post('/forgot_password', 'UsersController@doForgotPassword');
    Route::get('/reset_password/{token}', 'UsersController@resetPassword');
    Route::post('/reset_password', 'UsersController@doResetPassword');
    Route::get('/logout', 'UsersController@logout');
});

// Our internal api doesn't need authentication, this is the website making calls to itself
Route::group(array('prefix' => 'api/v1/internal'), function()
{
    Route::post('image/retrieveImgUrl', 'ImageController@retrieveImgUrl');

    Route::resource('address', 'AddressController');
    Route::resource('addressgoogle', 'AddressGoogleController');
    Route::resource('amenity', 'AmenityController');
    Route::resource('application', 'ApplicationController');
    Route::resource('community', 'CommunityController');
    Route::resource('contact', 'ContactController');
    Route::resource('employment', 'EmploymentController');
    Route::resource('hour', 'HourController');
    Route::resource('image', 'ImageController');
    Route::resource('importerror', 'ImportErrorController');
    Route::resource('news', 'NewsController');
    Route::resource('space', 'SpaceController');
    Route::resource('user', 'UserController');
});

// Anything outside of these two groups is just default and doesn't need authentication
Route::group(array('prefix' => 'api/v1', 'before' => 'auth.basic'), function()
{
    Route::resource('address', 'AddressController');
    Route::resource('addressgoogle', 'AddressGoogleController');
    Route::resource('amenity', 'AmenityController');
    Route::resource('application', 'ApplicationController');
    Route::resource('community', 'CommunityController');
    Route::resource('contact', 'ContactController');
    Route::resource('employment', 'EmploymentController');
    Route::resource('hour', 'HourController');
    Route::resource('image', 'ImageController');
    Route::resource('importerror', 'ImportErrorController');
    Route::resource('news', 'NewsController');
    Route::resource('space', 'SpaceController');
    Route::resource('user', 'UserController');
});

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{
    // Anything running through admin has to be authenticated
    Route::resource('address', 'AddressController');
    Route::resource('addressgoogle', 'AddressGoogleController');
    Route::resource('amenity', 'AmenityController');
    Route::resource('application', 'ApplicationController');
    Route::resource('community', 'CommunityController');
    Route::resource('contact', 'ContactController');
    Route::resource('employment', 'EmploymentController');
    Route::resource('hour', 'HourController');
    Route::resource('image', 'ImageController');
    Route::resource('importerror', 'ImportErrorController');
    Route::resource('news', 'NewsController');
    Route::resource('space', 'SpaceController');
    Route::resource('user', 'UserController');
});

Route::group(array('prefix' => 'user'), function()
{
    Route::get('/create', 'UserController@create');
    Route::get('{id}/view/{page}', 'UserController@view');
    Route::get('{id}/edit/{page}', 'UserController@edit');
    Route::get('{id}/paybill', 'UserController@paybill');
});

Route::group(array('prefix' => 'news'), function()
{
    Route::get('/', 'NewsController@index');
    Route::get('/{id}/view', 'NewsController@view');
});

Route::group(array('prefix' => 'contact'), function()
{
    Route::get('/', 'ContactController@index');
    Route::get('/create', 'ContactController@create');
    Route::get('/{id}/view', 'ContactController@view');
    Route::get('/{id}/edit', 'ContactController@edit');
});

Route::group(array('prefix' => 'application'), function()
{
    Route::get('/', 'ApplicationController@index');
    Route::get('/{community}/create/{space?}', 'ApplicationController@create');
    Route::get('/{id}/view', 'ApplicationController@view');
});

Route::group(array('prefix' => 'community'), function()
{
    Route::get('/', 'CommunityController@index');
    Route::get('/create', 'CommunityController@create');
    Route::get('{id}/view', 'CommunityController@view');
    Route::get('{id}/edit', 'CommunityController@edit');
});

Route::group(array('prefix' => 'space'), function() {
    Route::get('/searchresults', 'SpaceController@searchresults');
});

Route::group(array('prefix' => '{community}/space'), function()
{
    Route::get('/', 'SpaceController@index');
    Route::get('/{id}/view', 'SpaceController@view');
    Route::get('/create', 'SpaceController@create');
    Route::get('/{id}/edit', 'SpaceController@edit');
});