<?php

class Amenity extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'amenities';
	public $timestamps = false;

	public static function validate($input, $id=null)
	{
		$rules = array(
			'name' => 'required|unique:amenities,name,'.$id,
			'description' => 'required',
			'class_id' => 'required|integer|greater_than:0',
		);

		return Validator::make($input, $rules);
	}

	public function amenity_class()
	{
		return $this->belongsTo('AmenityClass', 'class_id');
	}

	public function amenity_icon()
	{
		return $this->hasMany('AmenityIcon', 'amenity_id');
	}

	public function community_amenity()
	{
		return $this->belongsToMany('Community', 'community_pvt_amenity', 'amenity_id', 'community_id');
	}

	public function space_amenity()
	{
		return $this->belongsToMany('Space', 'space_pvt_amenity', 'amenity_id', 'space_id');
	}

}