<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Contact extends \Eloquent {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contacts';
	protected $dates = ['deleted_at'];

	public function contact_interests()
	{
		return $this->belongsTo('ContactInterest', 'contact_interest_id');
	}

	public function contact_means()
	{
		return $this->belongsTo('ContactMean', 'contact_means_id');
	}

	public function contact_status()
	{
		return $this->belongsTo('ContactStatus', 'contact_status_id');
	}

	public function contact_type()
	{
		return $this->belongsTo('ContactType', 'contact_type_id');
	}

	public function contact_address()
	{
		return $this->belongsToMany('Address', 'contact_pvt_address', 'contact_id', 'address_id');
	}

	public function contact_hour()
	{
		return $this->belongsToMany('Hour', 'contact_pvt_hour', 'contact_id', 'hour_id');
	}
}