<?php

class Address extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'address';
	public $timestamps = false;

	protected $guarded = array();

	public static function validate($input)
	{
		$rules = array(
			'address_1' => 'required',
			'city' => 'required',
			'state' => 'required',
			'postal_code' => 'required',
			'country_id' => 'required|integer|greater_than:0',
			'type_id' => 'required|integer|greater_than:0',
			'google_id' => 'integer'
		);

		return Validator::make($input, $rules);
	}

	public function address_country()
	{
		return $this->belongsTo('AddressCountry', 'country_id');
	}

	public function address_google()
	{
		return $this->belongsTo('AddressGoogle', 'google_id');
	}

	public function address_type()
	{
		return $this->belongsTo('AddressType', 'type_id');
	}

	public function contact_address()
	{
		return $this->belongsToMany('Contact', 'contact_pvt_address', 'address_id', 'contact_id');
	}

	public function community_address()
	{
		return $this->belongsToMany('Community', 'community_pvt_address', 'address_id', 'community_id');
	}

	public function space_address()
	{
		return $this->belongsToMany('Space', 'space_pvt_address', 'address_id', 'space_id');
	}

	public function user_address()
	{
		return $this->belongsToMany('User', 'user_pvt_address', 'address_id', 'user_id');
	}

	public function scopePrimaryBillingAddress($query)
	{
		return $query->where('type_id', '=', 1);
	}

	public function scopePrimaryShippingAddress($query)
	{
		return $query->where('type_id', '=', 2);
	}

	public function scopeSecondaryBillingAddress($query)
	{
		return $query->where('type_id', '=', 3);
	}

	public function scopeSecondaryShippingAddress($query)
	{
		return $query->where('type_id', '=', 4);
	}

	public function scopeAlternateBillingAddress($query)
	{
		return $query->where('type_id', '=', 5);
	}

	public function scopeAlternateShippingAddress($query)
	{
		return $query->where('type_id', '=', 6);
	}

	public function scopeContactAddress($query)
	{
		return $query->where('type_id', '=', 7);
	}

	public function scopeSameAddress($query)
	{
		return $query->where('type_id', '=', 8);
	}

	public static function convertStateToCode($state)
	{
		switch (strtolower($state)) {
			case 'alabama':
				return 'AL';
			case 'alaska':
				return 'AK';
			case 'arizona':
				return 'AZ';
			case 'arkansas':
				return 'AR';
			case 'california':
				return 'CA';
			case 'colorado':
				return 'CO';
			case 'connecticut':
				return 'CT';
			case 'delaware':
				return 'DE';
			case 'district of columbia':
				return 'DC';
			case 'florida':
				return 'FL';
			case 'georgia':
				return 'GA';
			case 'hawaii':
				return 'HI';
			case 'idaho':
				return 'ID';
			case 'illinois':
				return 'IL';
			case 'indiana':
				return 'IN';
			case 'iowa':
				return 'IA';
			case 'kansas':
				return 'KS';
			case 'kentucky':
				return 'KY';
			case 'louisiana':
				return 'LA';
			case 'maine':
				return 'ME';
			case 'maryland':
				return 'MD';
			case 'massachusetts':
				return 'MA';
			case 'michigan':
				return 'MI';
			case 'minnesota':
				return 'MN';
			case 'mississippi':
				return 'MS';
			case 'missouri':
				return 'MO';
			case 'montana':
				return 'MT';
			case 'nebraska':
				return 'NE';
			case 'nevada':
				return 'NV';
			case 'new hampshire':
				return 'NH';
			case 'new jersey':
				return 'NJ';
			case 'new mexico':
				return 'NM';
			case 'new york':
				return 'NY';
			case 'north carolina':
				return 'NC';
			case 'north dakota':
				return 'ND';
			case 'ohio':
				return 'OH';
			case 'oklahoma':
				return 'OK';
			case 'oregon':
				return 'OR';
			case 'pennsylvania':
				return 'PA';
			case 'rhode island':
				return 'RI';
			case 'south carolina':
				return 'SC';
			case 'south dakota':
				return 'SD';
			case 'tennessee':
				return 'TN';
			case 'texas':
				return 'TX';
			case 'utah':
				return 'UT';
			case 'vermont':
				return 'VT';
			case 'virginia':
				return 'VA';
			case 'washington':
				return 'WA';
			case 'west virginia':
				return 'WV';
			case 'wisconsin':
				return 'WI';
			case 'wyoming':
				return 'WY';
		}
	}

	public static function convertCodeToState($code)
	{
		switch (strtolower($code)) {
			case 'al':
				return 'Alabama';
			case 'ak':
				return 'Alaska';
			case 'az':
				return 'Arizona';
			case 'ar':
				return 'Arkansas';
			case 'ca':
				return 'California';
			case 'co':
				return 'Colorado';
			case 'ct':
				return 'Connecticut';
			case 'de':
				return 'Delaware';
			case 'dc':
				return 'District of columbia';
			case 'fl':
				return 'Florida';
			case 'ga':
				return 'Georgia';
			case 'hi':
				return 'Hawaii';
			case 'id':
				return 'Idaho';
			case 'il':
				return 'Illinois';
			case 'in':
				return 'Indiana';
			case 'ia':
				return 'Iowa';
			case 'ks':
				return 'Kansas';
			case 'ky':
				return 'Kentucky';
			case 'la':
				return 'Louisiana';
			case 'me':
				return 'Maine';
			case 'md':
				return 'Maryland';
			case 'ma':
				return 'Massachusetts';
			case 'mi':
				return 'Michigan';
			case 'mn':
				return 'Minnesota';
			case 'ms':
				return 'Mississippi';
			case 'mo':
				return 'Missouri';
			case 'mt':
				return 'Montana';
			case 'ne':
				return 'Nebraska';
			case 'nv':
				return 'Nevada';
			case 'nh':
				return 'New Hampshire';
			case 'nj':
				return 'New Jersey';
			case 'nm':
				return 'New Mexico';
			case 'ny':
				return 'New York';
			case 'nc':
				return 'North Carolina';
			case 'nd':
				return 'North Dakota';
			case 'oh':
				return 'Ohio';
			case 'ok':
				return 'Oklahoma';
			case 'or':
				return 'Oregon';
			case 'pa':
				return 'Pennsylvania';
			case 'ri':
				return 'Rhode Island';
			case 'sc':
				return 'South Carolina';
			case 'sd':
				return 'South Dakota';
			case 'tn':
				return 'Tennessee';
			case 'tx':
				return 'Texas';
			case 'ut':
				return 'Utah';
			case 'vt':
				return 'Vermont';
			case 'va':
				return 'Virginia';
			case 'wa':
				return 'Washington';
			case 'wv':
				return 'West Virginia';
			case 'wi':
				return 'Wisconsin';
			case 'wy':
				return 'Wyoming';
		}
	}
}