<?php

class HourDay extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hour_days';
	public $timestamps = false;

	public function hour()
	{
		return $this->hasMany('Hour', 'days_id');
	}

}