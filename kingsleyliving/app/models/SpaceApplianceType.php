<?php

class SpaceApplianceType extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'space_appliance_types';
	public $timestamps = false;

	public function space_appliance()
	{
		return $this->hasMany('SpaceAppliance', 'time_id');
	}

}