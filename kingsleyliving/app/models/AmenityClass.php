<?php

class AmenityClass extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'amenity_classes';
	public $timestamps = false;

	public function amenity()
	{
		return $this->hasMany('Amenity', 'class_id');
	}

}