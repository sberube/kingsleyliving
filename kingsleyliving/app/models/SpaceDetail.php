<?php

class SpaceDetail extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'space_details';
	public $timestamps = false;

	public function space()
	{
		return $this->belongsTo('Space', 'space_id');
	}

}