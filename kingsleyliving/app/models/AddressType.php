<?php

class AddressType extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'address_types';
	public $timestamps = false;

	public function address()
	{
		return $this->hasMany('Address', 'type_id');
	}

}