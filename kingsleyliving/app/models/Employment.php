<?php

class Employment extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'employment';

	public function employment_address()
	{
		return $this->hasMany('EmploymentAddress', 'employment_id');
	}

	public function employment_dates()
	{
		return $this->hasMany('EmploymentDate', 'employment_id');
	}

	public function application_employment()
	{
		return $this->belongsToMany('Application', 'application_pvt_employment', 'employment_id', 'application_id')->withPivot('user_id');
	}

	public function user_employment()
	{
		return $this->belongsToMany('User', 'user_pvt_employment', 'employment_id', 'user_id');
	}

}