<?php

class SpaceFlyer extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'space_flyers';
	public $timestamps = false;

	public function space()
	{
		return $this->belongsTo('Space', 'space_id');
	}

}