<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Space extends \Eloquent {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'spaces';
	protected $dates = ['deleted_at'];

	public static function validate($input)
	{
		$rules = array(
			'community_id' => 'required|integer',
			'spaceid' => 'integer',
			'space_num' => 'required|integer',
			'space_status_id' => 'required|integer',
		);

		return Validator::make($input, $rules);
	}

	public function community()
	{
		return $this->belongsTo('Community', 'community_id');
	}

	public function space_status()
	{
		return $this->belongsTo('SpaceStatus', 'space_status_id');
	}

	public function address()
	{
		return $this->belongsToMany('Address', 'space_pvt_address', 'space_id', 'address_id');
	}

	public function space_amenity()
	{
		return $this->belongsToMany('Amenity', 'space_pvt_amenity', 'space_id', 'amenity_id');
	}

	public function space_image()
	{
		return $this->belongsToMany('Image', 'space_pvt_image', 'space_id', 'image_id');
	}

	public function application_space()
	{
		return $this->belongsToMany('Application', 'application_pvt_space', 'space_id', 'application_id');
	}

	public function space_appliance()
	{
		return $this->hasOne('SpaceAppliance', 'space_id');
	}

	public function space_detail()
	{
		return $this->hasOne('SpaceDetail', 'space_id');
	}

	public function space_flyer()
	{
		return $this->hasOne('SpaceFlyer', 'space_id');
	}

	public function space_pricing()
	{
		return $this->hasOne('SpacePricing', 'space_id');
	}

	public static function retrieveSpaceBeds()
	{
		$raw_query = DB::table('space_details')
							->select('beds')
							->groupBy('beds')->get();
		$beds = array();
		foreach ($raw_query as $std_id => $std_obj) {
			foreach ($std_obj as $key => $value) {
				$beds[$value] = $value;
			}
		}
		return $beds;
	}

	public static function retrieveSpaceBaths()
	{
		$raw_query = DB::table('space_details')
							->select('baths')
							->groupBy('baths')->get();
		$baths = array();
		foreach ($raw_query as $std_id => $std_obj) {
			foreach ($std_obj as $key => $value) {
				$baths[$value] = $value;
			}
		}
		return $baths;
	}

	public static function retrieveLowestCostAvaliable()
	{
		return DB::table('space_pricing')->min('sale_price');
	}

	public static function retrieveHighestCostAvaliable()
	{
		return DB::table('space_pricing')->max('sale_price');
	}

	public static function retrieveAverageCostAvaliable()
	{
		return DB::table('space_pricing')->avg('sale_price');
	}

	public static function retrievePriceMin($locale='en_US')
	{
		setlocale(LC_MONETARY, $locale);
		$price_arry = array();
		$price_min = self::retrieveLowestCostAvaliable();
		$price_avg = self::retrieveAverageCostAvaliable();

		$price_step = round((($price_avg - $price_min) / 5), 2);
		$count = 0;
		$price_cur = round($price_min, -3);
		while ( $count < 4 )
		{
			$price_arry[round($price_cur, -3)] = money_format('%(#6n', round($price_cur, -3));
			$price_cur = $price_cur + $price_step;
			$count++;
		}

		return $price_arry;
	}

	public static function retrievePriceMax($locale='en_US')
	{
		setlocale(LC_MONETARY, $locale);
		$price_arry = array();
		$price_max = self::retrieveHighestCostAvaliable();
		$price_avg = self::retrieveAverageCostAvaliable();

		$price_step = round((($price_max - $price_avg) / 5), 2);
		$count = 0;
		$price_cur = round($price_max, -3);
		while ( $count < 4 )
		{
			$price_arry[round($price_cur, -3)] = money_format('%(#6n', round($price_cur, -3));
			$price_cur = $price_cur - $price_step;
			$count++;
		}

		return $price_arry;
	}

	public static function retrieveSpaces($tables, $conditions)
	{
		$raw_query = DB::table('spaces');
		$raw_query->join('space_details', 'spaces.id', '=', 'space_details.space_id');
		$raw_query->join('space_pricing', 'spaces.id', '=', 'space_pricing.space_id');
		$raw_query->join('communities', 'spaces.community_id', '=', 'communities.id');
		if ( in_array('address', $tables) == true )
		{
			$raw_query->join('community_pvt_address', 'communities.id', '=', 'community_pvt_address.community_id');
			$raw_query->join('address','community_pvt_address.address_id','=','address.id');
		}
		
		if ( in_array('community_pvt_amenity', $tables) == true )
		{
			$raw_query->join('community_pvt_amenity', 'communities.id', '=', 'community_pvt_amenity.community_id');
		}
		$raw_query->select('spaces.*', 'space_details.*', 'space_pricing.*');
		$first_vacant = SpaceStatus::where('status', 'like', 'vacant_available')->first();
		$vacant_unavailable = SpaceStatus::where('status', 'like', 'vacant_unavailable')->first();
		if (isset($conditions['listing_option']))
		{
			if ($conditions['listing_option'] == 'buy')
			{
				$vacancy = SpaceStatus::where('status', 'like', 'vacant_buy')->first();
				$raw_query->where('spaces.space_status_id', '>=', $vacancy->id);
				$raw_query->where('spaces.space_status_id', '<', $vacant_unavailable->id);
			}
			else if ($conditions['listing_option'] == 'lease')
			{
				$vacancy = SpaceStatus::where('status', 'like', 'vacant_lease')->first();
				$raw_query->where('spaces.space_status_id', '>=', $vacancy->id);
				$raw_query->where('spaces.space_status_id', '<', $vacant_unavailable->id);
			}
			else if ($conditions['listing_option'] == 'rent')
			{
				$vacancy = SpaceStatus::where('status', 'like', 'vacant_rent')->first();
				$raw_query->where('spaces.space_status_id', '>=', $vacancy->id);
				$raw_query->where('spaces.space_status_id', '<', $vacant_unavailable->id);
			}
			else
			{
				$vacancy = SpaceStatus::where('status', 'like', 'vacant_sites')->first();
				$raw_query->where('spaces.space_status_id', '=', $vacancy->id);
			}
		}
		else
		{
			$raw_query->where('spaces.space_status_id', '>=', $first_vacant->id);
			$raw_query->where('spaces.space_status_id', '<', $vacant_unavailable->id);
		}
		foreach ($conditions as $field => $value) {
			switch ($field) {
				case 'listing_option':
					break;
				case 'clubhouse':
				case 'fitness_center':
				case 'swimming_pool':
				case 'playground':
					$amenity = Amenity::where('name', 'like', str_replace('_', ' ', $field))->first();
					$amenity_id = $amenity['id'];
					$raw_query->whereRaw('FIND_IN_SET('.$amenity_id.', community_pvt_amenity.amenity_id)');
					break;
				case 'community_type':
					$amenity = Amenity::where('name', 'like', 'senior community')->first();
					$senior_community = $amenity['id'];
					if ($value == '55_or_over')
					{
						$raw_query->whereRaw('FIND_IN_SET('.$senior_community.', community_pvt_amenity.amenity_id)');
					}
					break;
				case 'state':
					$raw_query->where('address.'.$field, 'like', Address::convertCodeToState($value));
					break;
				case 'city':
					$raw_query->where('address.'.$field, 'like', $value);
					break;
				case 'price_min':
					$raw_query->where('space_pricing.sale_price', '>', $value);
					break;
				case 'price_max':
					$raw_query->where('space_pricing.sale_price', '<', $value);
					break;
				case 'beds':
				case 'baths':
					$raw_query->where('space_details.'.$field, '=', $value);
					break;
				case 'community':
					$raw_query->where('communities.id', '=', $value);
					break;
			}
		}
		$results = $raw_query->get();
		$results = self::super_unique($results);
		$spaces = array();
		// dd(DB::getQueryLog());
		foreach ($results as $std_id => $std_obj) {
			$temp = array();
			foreach ($std_obj as $key => $value) {
				$temp[$key] = $value;
			}
			$spaces[] = $temp;
		}
		// $spaces = self::super_unique($spaces);
		// print_r($results);
		// echo "<br />";
		// echo "<br />";
		// print_r($spaces);
		// exit;
		return $spaces;
	}

	public static function super_unique($array)
	{
		$result = array_map("unserialize", array_unique(array_map("serialize", $array)));

		foreach ($result as $key => $value)
		{
			if ( is_array($value) )
			{
				$result[$key] = self::super_unique($value);
			}
		}

		return $result;
	}

}