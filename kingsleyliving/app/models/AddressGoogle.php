<?php

class AddressGoogle extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'address_google';
	public $timestamps = false;

	public static function validate($input, $id=null)
	{
		$rules = array(
			'street_number' => 'required',
			'route' => 'required',
			'locality' => 'required',
			'administrative_area_level_1' => 'required',
			'country_id' => 'required|integer|greater_than:0',
			'postal_code' => 'required',
			'latitude' => 'required',
			'longitude' => 'required'
		);

		return Validator::make($input, $rules);
	}

	public function address_country()
	{
		return $this->belongsTo('AddressCountry', 'country_id');
	}

	public function address()
	{
		return $this->hasOne('Address', 'google_id');
	}

}