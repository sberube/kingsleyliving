<?php

class ApplicationPricing extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'application_pricing';
	public $timestamps = false;

	public function application()
	{
		return $this->belongsTo('Application', 'application_id');
	}

}