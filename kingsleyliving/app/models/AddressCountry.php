<?php

class AddressCountry extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'address_countries';
	public $timestamps = false;

	public function address()
	{
		return $this->hasMany('Address', 'country_id');
	}

	public function address_google()
	{
		return $this->hasMany('AddressGoogle', 'country_id');
	}

	public function employment_address()
	{
		return $this->hasMany('EmploymentAddress', 'country_id');
	}

}