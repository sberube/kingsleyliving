<?php

class ContactStatus extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contact_status';
	public $timestamps = false;

	public function contact()
	{
		return $this->hasMany('Contact', 'contact_status_id');
	}

}