<?php

class ContactType extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contact_types';
	public $timestamps = false;

	public function contact()
	{
		return $this->hasMany('Contact', 'contact_type_id');
	}

}