<?php

class ApplicationResident extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'application_residents';
	public $timestamps = false;

	public static function validate($input)
	{
		$rules = array(
			'application_id' => 'required|integer|greater_than:0',
			'user_id' => 'required|integer|greater_than:0',
			'resident_type_id' => 'required|integer|greater_than:0',
			'move_in_date' => 'required|date|date_format:m/d/Y'
		);

		return Validator::make($input, $rules);
	}

	public function application()
	{
		return $this->belongsTo('Application', 'application_id');
	}

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function application_resident_type()
	{
		return $this->belongsTo('ApplicationResidentType', 'resident_type_id');
	}

}