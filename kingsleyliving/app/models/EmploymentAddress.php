<?php

class EmploymentAddress extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'employment_address';
	public $timestamps = false;

	public function employment()
	{
		return $this->belongsTo('Employment', 'employment_id');
	}

	public function address_country()
	{
		return $this->belongsTo('AddressCountry', 'country_id');
	}

}