<?php

class Hour extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hours';

	public function hour_day()
	{
		return $this->belongsTo('HourDay', 'days_id');
	}

	public function hour_time()
	{
		return $this->belongsTo('HourTime', 'time_id');
	}

	public function hour_type()
	{
		return $this->belongsTo('HourType', 'type_id');
	}

	public function contacts()
	{
		return $this->belongsToMany('Contact', 'contact_hours', 'hour_id', 'contact_id');
	}

	public function communities()
	{
		return $this->belongsToMany('Community', 'community_hours', 'hour_id', 'community_id');
	}

}