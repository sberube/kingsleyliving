<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ImportError extends \Eloquent {

	use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'import_errors';
	protected $dates = ['deleted_at'];

	public static function validate($input)
	{
		$rules = array(
			'record_table' => 'required',
			'record_field' => 'required',
			'record_id' => 'required|integer',
			'error_message' => 'required'
		);

		return Validator::make($input, $rules);
	}
}