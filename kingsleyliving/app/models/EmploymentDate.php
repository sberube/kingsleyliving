<?php

class EmploymentDate extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'employment_dates';
	public $timestamps = false;

	public function employment()
	{
		return $this->belongsTo('Employment', 'employment_id');
	}

}