<?php

class UserSession extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_sessions';

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function user_request()
	{
		return $this->belongsTo('UserRequest', 'user_request_id');
	}

}