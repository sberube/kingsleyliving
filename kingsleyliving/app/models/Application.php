<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Application extends \Eloquent {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'applications';
	protected $dates = ['deleted_at'];

	public static function validate($input)
	{
		$rules = array(
			'community_id' => 'required|integer|greater_than:0',
		);

		return Validator::make($input, $rules);
	}

	public function community()
	{
		return $this->belongsTo('Community', 'community_id');
	}

	public function application_employment()
	{
		return $this->belongsToMany('Employment', 'application_pvt_employment', 'application_id', 'employment_id')->withPivot('user_id');
	}

	public function application_space()
	{
		return $this->belongsToMany('Space', 'application_pvt_space', 'application_id', 'space_id');
	}

	public function application_pricing()
	{
		return $this->hasOne('ApplicationPricing', 'application_id');
	}

	public function application_resident()
	{
		return $this->hasOne('ApplicationResident', 'application_id');
	}

	public function application_resident_income()
	{
		return $this->hasOne('ApplicationResidentIncome', 'application_id');
	}

	public function application_resident_type()
	{
		return $this->hasOne('ApplicationResidentType', 'application_id');
	}

	public function application_source()
	{
		return $this->hasOne('ApplicationSource', 'application_id');
	}
}