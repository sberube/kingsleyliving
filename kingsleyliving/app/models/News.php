<?php

class News extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news';

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

}