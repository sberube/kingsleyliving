<?php

use Zizaco\Entrust\EntrustPermission;

class UserPermission extends EntrustPermission {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_permissions';

	public function permission_role()
	{
		return $this->belongsToMany('UserRole', 'permission_pvt_role', 'user_permission_id', 'user_role_id');
	}

}