<?php

class ApplicationResidentType extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'application_resident_types';
	public $timestamps = false;

	public function application_resident()
	{
		return $this->hasMany('ApplicationResident', 'resident_type_id');
	}

}