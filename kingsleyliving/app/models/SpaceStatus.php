<?php

class SpaceStatus extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'space_status';
	public $timestamps = false;

	public function space()
	{
		return $this->hasMany('Space', 'space_id');
	}

}