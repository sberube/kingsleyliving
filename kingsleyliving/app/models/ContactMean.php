<?php

class ContactMean extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contact_means';
	public $timestamps = false;

	public function contact()
	{
		return $this->hasMany('Contact', 'contact_means_id');
	}

}