<?php

// use Illuminate\Auth\UserTrait;
// use Illuminate\Auth\UserInterface;
// use Illuminate\Auth\Reminders\RemindableTrait;
// use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;
//UserInterface, RemindableInterface {
class User extends Eloquent implements ConfideUserInterface { 

	// use UserTrait, RemindableTrait;
	use SoftDeletingTrait;
	use ConfideUser;
	use HasRole;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function application_employment()
	{
		return $this->belongsToMany('Employment', 'application_pvt_employment', 'user_id', 'employment_id')->withPivot('application_id');
	}

	public function user_community()
	{
		return $this->belongsToMany('Community', 'user_pvt_community', 'user_id', 'community_id');
	}

	public function address()
	{
		return $this->belongsToMany('Address', 'user_pvt_address', 'user_id', 'address_id');
	}

	public function user_employment()
	{
		return $this->belongsToMany('Employment', 'user_pvt_employment', 'user_id', 'employment_id');
	}

	public function user_role()
	{
		return $this->belongsToMany('UserRole', 'user_pvt_role', 'user_id', 'role_id');
	}

	public function user_space()
	{
		return $this->belongsToMany('Space', 'user_pvt_space', 'user_id', 'space_id');
	}

	public function property_manager()
	{
		return $this->hasMany('Community', 'property_manager_id');
	}

	public function assistant_property_manager()
	{
		return $this->hasMany('Community', 'assistant_property_manager_id');
	}

	public function account_manager()
	{
		return $this->hasMany('Community', 'account_manager_id');
	}

	public function application_resident()
	{
		return $this->hasMany('ApplicationResident', 'user_id');
	}

	public function user_session()
	{
		return $this->hasMany('UserSession', 'user_id');
	}

	public function news()
	{
		return $this->hasMany('News', 'user_id');
	}

	public function user_profile()
	{
		return $this->hasOne('UserProfile', 'user_id');
	}

}
