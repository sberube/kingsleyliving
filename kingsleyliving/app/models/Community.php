<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Community extends \Eloquent {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'communities';
	protected $dates = ['deleted_at'];

	public static function validate($input, $id=null)
	{
		$rules = array(
			'name' => 'required',
			'phone' => 'required',
			'email' => 'required|email|unique:communities,email,'.$id,
			'home_page' => 'required|url',
			'account_manager_id' => 'required|integer|greater_than:0',
			'property_manager_id' => 'required|integer|greater_than:0',
			'assistant_property_manager_id' => 'integer', // not required
			'description' => 'required'
		);

		return Validator::make($input, $rules);
	}

	public function main_image()
	{
		return $this->belongsTo('Image', 'main_image_id');
	}

	public function property_manager()
	{
		return $this->belongsTo('User', 'property_manager_id');
	}

	public function assistant_property_manager()
	{
		return $this->belongsTo('User', 'assistant_property_manager_id');
	}

	public function account_manager()
	{
		return $this->belongsTo('User', 'account_manager_id');
	}

	public function address()
	{
		return $this->belongsToMany('Address', 'community_pvt_address', 'community_id', 'address_id');
	}

	public function community_amenity()
	{
		return $this->belongsToMany('Amenity', 'community_pvt_amenity', 'community_id', 'amenity_id');
	}

	public function community_hour()
	{
		return $this->belongsToMany('Hour', 'community_pvt_hour', 'community_id', 'hour_id');
	}

	public function community_image()
	{
		return $this->belongsToMany('Image', 'community_pvt_image', 'community_id', 'image_id');
	}

	public function user()
	{
		return $this->belongsToMany('User', 'user_pvt_community', 'community_id', 'user_id');
	}

	public function space()
	{
		return $this->hasMany('Space', 'community_id');
	}

	public static function retrieveCommunityStates()
	{
		$raw_query = DB::table('communities')
						->join('community_pvt_address', 'communities.id', '=', 'community_pvt_address.community_id')
						->join('address','community_pvt_address.address_id','=','address.id')
						->select('address.state')
						->where('address.type_id', '=', '1')
						->orWhere('address.type_id', '=', '8')
						->groupBy('address.state')->get();
		$states = array();
		foreach ($raw_query as $std_id => $std_obj) {
			foreach ($std_obj as $key => $value) {
				$code = Address::convertStateToCode($value);
				$states[$code] = $value;
			}
		}
		return $states;
		// select address.state from communities left join community_pvt_address on communities.id = community_pvt_address.community_id left join address on community_pvt_address.address_id = address.id where address.type_id = 1 or address.type_id = 8 group by (address.state)
	}

	public static function retrieveCommunityCities()
	{
		$raw_query = DB::table('communities')
						->join('community_pvt_address', 'communities.id', '=', 'community_pvt_address.community_id')
						->join('address','community_pvt_address.address_id','=','address.id')
						->select('address.city')
						->where('address.type_id', '=', '1')
						->orWhere('address.type_id', '=', '8')
						->groupBy('address.city')->get();
		$cities = array();
		foreach ($raw_query as $std_id => $std_obj) {
			foreach ($std_obj as $key => $value) {
				$cities[strtolower(str_replace(' ', '_', $value))] = $value;
			}
		}
		return $cities;
	}

	public static function retrieveSearchAmenities()
	{
		$amenities = array();
		$search_amenities = array('clubhouse', 'fitness center', 'swimming pool', 'playground');
		foreach ($search_amenities as $search) {
			$result = Amenity::where('name', 'like', $search)->first();
			$amenities[$result['id']] = str_replace(' ', '_', $result['name']);
		}
		return $amenities;
	}

	public static function prependBlankValue($array)
	{
		$array = array_reverse($array, true);
		$array[''] = '';
		return array_reverse($array, true);
	}
	
}