<?php

class UserRequest extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_requests';

	public function user_session()
	{
		return $this->hasMany('UserSession', 'user_request_id');
	}

}