<?php

class UserMaritalStatus extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_marital_status';
	public $timestamps = false;

	public function user_profile()
	{
		return $this->hasMany('UserProfile', 'marital_status_id');
	}

}