<?php

class AmenityIcon extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'amenity_icons';
	public $timestamps = false;

	public static function validate($input, $id=null)
	{
		$rules = array(
			'amenity_id' => 'required|integer|greater_than:0',
			'icon_class' => 'required|unique:amenity_icons,icon_class,'.$id
		);

		return Validator::make($input, $rules);
	}

	public function amenity()
	{
		return $this->belongsTo('Amenity', 'amenity_id');
	}

}