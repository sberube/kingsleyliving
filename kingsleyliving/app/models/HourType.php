<?php

class HourType extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hour_types';
	public $timestamps = false;

	public function hour()
	{
		return $this->hasMany('Hour', 'type_id');
	}

}