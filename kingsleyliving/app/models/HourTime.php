<?php

class HourTime extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hour_times';
	public $timestamps = false;

	public function hour()
	{
		return $this->hasMany('Hour', 'time_id');
	}

}