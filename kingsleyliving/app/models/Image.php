<?php

class Image extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'images';

	public function community_main_image()
	{
		return $this->hasOne('Community', 'main_image_id');
	}

	public function user_profile()
	{
		return $this->hasOne('UserProfile', 'image_id');
	}

	public function community_image()
	{
		return $this->belongsToMany('Community', 'community_pvt_image', 'image_id', 'community_id');
	}

	public function space_image()
	{
		return $this->belongsToMany('Space', 'space_pvt_image', 'image_id', 'space_id');
	}

}