<?php

class UserProfile extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_profiles';

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function image()
	{
		return $this->belongsTo('Image', 'image_id');
	}

	public function user_marital_status()
	{
		return $this->belongsTo('UserMaritalStatus', 'marital_status_id');
	}

}