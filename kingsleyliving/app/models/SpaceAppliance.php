<?php

class SpaceAppliance extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'space_appliances';
	public $timestamps = false;

	public function space()
	{
		return $this->belongsTo('Space', 'space_id');
	}

	public function space_appliance_type()
	{
		return $this->belongsTo('SpaceApplianceType', 'appliance_type_id');
	}

}