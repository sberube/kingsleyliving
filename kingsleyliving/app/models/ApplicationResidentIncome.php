<?php

class ApplicationResidentIncome extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'application_resident_incomes';
	public $timestamps = false;

	public function application()
	{
		return $this->belongsTo('Application', 'application_id');
	}

}