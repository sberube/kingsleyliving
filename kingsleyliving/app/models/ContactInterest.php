<?php

class ContactInterest extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contact_interests';
	public $timestamps = false;

	public function contact()
	{
		return $this->hasMany('Contact', 'contact_interest_id');
	}

}