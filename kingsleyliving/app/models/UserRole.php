<?php

use Zizaco\Entrust\EntrustRole;

class UserRole extends EntrustRole {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_roles';

	public function user_role()
	{
		return $this->belongsToMany('User', 'user_pvt_role', 'role_id', 'user_id');
	}

	public function permission_role()
	{
		return $this->belongsToMany('UserRole', 'permission_pvt_role', 'user_role_id', 'user_permission_id');
	}

}