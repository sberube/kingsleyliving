<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function __construct()
	{
		if (Auth::check()) {
			$user_profile = UserProfile::where('user_id', Auth::user()->id)->first();
			if (!empty($user_profile))
			{
				$this->user_name = $user_profile->first_name.' '.$user_profile->last_name;
			}
			else
			{
				$this->user_name = Auth::user()->username;
			}
		} else {
			$this->user_name = '';
		}
		$this->stylesheets = array();
		$this->stylesheets[] = HTML::style('css/home/home.css');

		$this->javascript = array();
		$this->javascript[] = HTML::script('js/home/home.js');

		$this->states = Community::prependBlankValue(Community::retrieveCommunityStates());
		$this->cities = Community::prependBlankValue(Community::retrieveCommunityCities());
		$this->radius = array('' => '', '5' => '5 miles', '10' => '10 miles', '15' => '15 miles', '20' => '20 miles', '50' => '50 miles');
		$this->price_min = Community::prependBlankValue(Space::retrievePriceMin());
		$this->price_max = Community::prependBlankValue(Space::retrievePriceMax());
		$this->beds = Community::prependBlankValue(Space::retrieveSpaceBeds());
		$this->baths = Community::prependBlankValue(Space::retrieveSpaceBaths());
		$this->community_type = array('' => '', '55_or_over' => '55+', 'all' => 'All Age');
		$this->community = Community::prependBlankValue(Community::lists('name', 'id'));
		$this->amenities = Community::retrieveSearchAmenities();
	}

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function view()
	{
		$this->stylesheets[] = HTML::style('wow_slider/engine1/style.css');
		$this->javascript[] = HTML::script('wow_slider/engine1/jquery.js');
		$this->javascript[] = HTML::script('js/raphael.js');
		$this->javascript[] = HTML::script('js/jqvmap-stable/jqvmap/jquery.vmap.js');
		$this->javascript[] = HTML::script('js/jqvmap-stable/jqvmap/maps/jquery.vmap.usa.js');

		$map_data = array(
			'legend_style' => 'legend-vertical',
			'legend_title' => 'Communities'
		);

		$master_data = array(
			'page_title' => 'Home',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		return View::make('home.home', $master_data, array('legend_data' => $map_data));
	}

	public function about()
	{
		$master_data = array(
			'page_title' => 'About',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		return View::make('home.about', $master_data);
	}

	public function buyahome()
	{
		$master_data = array(
			'page_title' => 'Buy',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Buy A Home';
		$search = '';
		$listing_option = 'buy';

		return View::make('home.buyahome', $master_data, array('title' => $title, 'listing_option' => $listing_option))->with('states', $this->states)->with('cities', $this->cities)->with('search', $search)->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $this->community)->with('amenities', $this->amenities);
	}

	public function rentahome()
	{
		$master_data = array(
			'page_title' => 'Rent',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Rent A Home';
		$search = '';
		$listing_option = 'rent';

		return View::make('home.rentahome', $master_data, array('title' => $title, 'listing_option' => $listing_option))->with('states', $this->states)->with('cities', $this->cities)->with('search', $search)->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $this->community)->with('amenities', $this->amenities);
	}

	public function leaseoptions()
	{
		$master_data = array(
			'page_title' => 'Lease',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Lease Options';
		$search = '';
		$listing_option = 'lease';

		return View::make('home.leaseoptions', $master_data, array('title' => $title, 'listing_option' => $listing_option))->with('states', $this->states)->with('cities', $this->cities)->with('search', $search)->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $this->community)->with('amenities', $this->amenities);
	}

	public function findahomesite()
	{
		$master_data = array(
			'page_title' => 'Home Site',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Find A Home Site';
		$search = '';
		$listing_option = 'site';

		return View::make('home.findahomesite', $master_data, array('title' => $title, 'listing_option' => $listing_option))->with('states', $this->states)->with('cities', $this->cities)->with('search', $search)->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $this->community)->with('amenities', $this->amenities);
	}

	public function termsandconditions()
	{
		$master_data = array(
			'page_title' => 'Terms and Conditions',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		return View::make('home.termsandconditions', $master_data);
	}

	public function privacypolicy()
	{
		$master_data = array(
			'page_title' => 'Privacy Policy',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		return View::make('home.privacypolicy', $master_data);
	}

	public function sitemap()
	{
		$master_data = array(
			'page_title' => 'Sitemap',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		return View::make('home.sitemap', $master_data);
	}

}
