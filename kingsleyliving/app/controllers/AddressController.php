<?php

class AddressController extends \BaseController {

	private function toArray($obj) {
		if (is_object($obj)) {
			$obj = get_object_vars($obj);
		}
 
		if (is_array($obj)) {
			return array_map([__CLASS__, __METHOD__], $obj);
		}
		else {
			return $obj;
		}
	}

	private function toObject($arry) {
		if (is_array($arry)) {
			return (object) array_map([__CLASS__, __METHOD__], $arry);
		}
		else {
			return $arry;
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * Admin only function...
	 *
	 * @return Response
	 */
	public function index()
	{
		//code... 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * Admin only function...
	 *
	 * @return Response
	 */
	public function create()
	{
		//code...
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$address_1 = Request::get('address_1');
		$address_2 = Request::get('address_2');
		$city = Request::get('city');
		$state = Request::get('state');
		$postal_code = Request::get('postal_code');

		if (Request::has('redirectto'))
		{
			$redirectto = Request::get('redirectto');
		}
		else
		{
			$redirectto = '/';
		}
		
		if (Request::has('type_id'))
		{
			$type = AddressType::find(Request::get('type_id'));
		}
		else
		{
			$type = AddressType::where('type', 'like', Request::get('type'))->first();
		}

		if (Request::has('country_id'))
		{
			$country = AddressCountry::find(Request::get('country_id'));
		}
		else
		{
			$country = AddressCountry::where('name', 'like', Request::get('country'))->first();
		}
		$statecodes = array('al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'dc', 'fl', 'ga', 'hi', 'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn', 'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh', 'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa', 'wv', 'wi', 'wy');
		if (in_array(strtolower($state), $statecodes))
		{
			$state = Address::convertCodeToState($state);
		}
		$address = Address::firstOrNew(array('address_1' => $address_1, 'address_2' => $address_2, 'city' => $city, 'state' => $state, 'postal_code' => $postal_code, 'type_id' => $type->id, 'country_id' => $country->id));

		$valid = Address::validate($address->toArray());

		if ( $valid->fails() )
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => true,
					'message' => $valid->messages()->toJson(),
					),
					406
				);
			}
			else
			{
				return Redirect::to($redirectto)->withErrors($valid)->withInput();
			}
		}

		if (!isset($address->id))
		{
			$prep_address = $address->address_1 . ', ' . $address->city . ', ' . $address->state;
			$google_id = HMVC::post('api/v1/internal/addressgoogle', array('prep_address' => $prep_address));
			if ($google_id['error'] == false)
			{
				$google = AddressGoogle::find($google_id['google_id']);

				// associate google address
				$address->address_google()->associate($google);
			}
			else
			{
				return Response::json(array(
					'error' => true,
					'message' => $google_id['message'],
					),
					500
				);
			}
		}

		$address->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Address created!',
			'address_id' => $address->id
			),
			200
		);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$address = Address::where('id', $id)
					->take(1)
					->get();

		return Response::json(array(
			'error' => false,
			'address' => $address->toArray()
			),
			200
		);
	}

	/**
	 * Show the resource.
	 *
	 * Admin only function...
	 *
	 * @return Response
	 */
	public function view($id)
	{
		//code...
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * Admin only function...
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$address = Address::find($id);
		$country;
		if ( Request::get('address_1') )
		{
			$address->address_1 = Request::get('address_1');
		}

		if ( Request::get('address_2') )
		{
			$address->address_2 = Request::get('address_2');
		}

		if ( Request::get('city') )
		{
			$address->city = Request::get('city');
		}

		if ( Request::get('state') )
		{
			$address->state = Request::get('state');
		}

		if ( Request::get('postal_code') )
		{
			$address->postal_code = Request::get('postal_code');
		}

		if ( Request::has('country') )
		{
			$country = Request::get('country');
		}
		else
		{
			$country = Request::get('country_id');
		}

		if (is_numeric($country))
		{
			$address_country = AddressCountry::find($country);
		}
		else
		{
			$address_country = AddressCountry::where('name', 'like', $country)->first();
		}

		$address->address_country()->associate($address_country);
		$address->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Address updated!',
			'address_id' => $address->id
			),
			200
		);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$address = Address::find($id);

		$address->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'Address deleted'
			),
			200
		);
	}


}
