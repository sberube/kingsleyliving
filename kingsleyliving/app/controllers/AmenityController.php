<?php

class AmenityController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Request::is('api/v1/*') || Request::ajax())
		{
			$amenity = Amenity::all();

			return Response::json(array(
				'error' => false,
				'amenity' => $amenity->toArray()),
				200
			);
		}
		else
		{
			$amenities = Amenity::with('amenity_icon')->get();

			if (Auth::check()) {
				// code...
			} else {
				$user_name = '';
			}

			$master_data = array(
				'page_title' => 'Amenity Index',
				'page_stylesheets' => '',
				'page_scriptlinks' => '',
				'user_name' => $user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);
			return View::make('amenity.index', $master_data, array('amenities' => $amenities->toArray()));
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Auth::check()) {
			// code...
		} else {
			$user_name = '';
		}

		$master_data = array(
			'page_title' => 'Amenity Create',
			'page_stylesheets' => '',
			'page_scriptlinks' => '',
			'user_name' => $user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Create New Amenity';

		$classes = AmenityClass::all();

		$url_request = 'amenity/create';
		
		return View::make('amenity.create', $master_data, array('title' => $title, 'url_request' => $url_request))->with('class', $classes);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$amenity = new Amenity;
		$amenity->name = Request::get('name');
		$amenity->description = Request::get('description');

		if (Request::has('class_id'))
		{
			$amenity_class = AmenityClass::find(Request::get('class_id'));
		}
		else
		{
			$amenity_class = AmenityClass::where('class', 'like', Request::get('class'))->first();
		}

		$amenity->amenity_class()->associate($amenity_class);

		$amenity->save();

		if (Request::has('icon_class'))
		{
			$icon_class = new AmenityIcon;
			$icon_class->amenity()->associate($amenity);
			$icon_class->icon_class = Request::get('icon_class');
			$icon_class->save();
		}

		return Response::json(array(
			'error' => false,
			'message' => 'Amenity created!',
			'amenity_id' => $amenity->id
			),
			200
		);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$amenity = Amenity::where('id', $id)
					->take(1)
					->get();

		return Response::json(array(
			'error' => false,
			'amenity' => $amenity->toArray()
			),
			200
		);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check()) {
			// code...
		} else {
			$user_name = '';
		}

		$master_data = array(
			'page_title' => 'Amenity Update',
			'page_stylesheets' => '',
			'page_scriptlinks' => '',
			'user_name' => $user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Update Amenity';

		$classes = AmenityClass::all();

		$url_request = 'amenity/update';

		if (is_numeric($id))
		{
			$amenity = Amenity::with('amenity_icon')->find($id);
		}
		else
		{
			$amenity = Amenity::with('amenity_icon')->where('name', 'like', $id)->first();
		}
		
		return View::make('amenity.edit', $master_data, array('title' => $title, 'url_request' => $url_request))->with('amenity', $amenity)->with('class', $classes);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$amenity = Amenity::find($id);
		if ( Request::get('name') )
		{
			$amenity->name = Request::get('name');
		}

		if ( Request::get('description') )
		{
			$amenity->description = Request::get('description');
		}

		if (Request::has('class_id'))
		{
			$amenity_class = AmenityClass::find(Request::get('class_id'));
		}
		else
		{
			$amenity_class = AmenityClass::where('class', 'like', Request::get('class'))->first();
		}

		$amenity->amenity_class()->associate($amenity_class);

		$amenity->save();

		if (Request::has('icon_class'))
		{
			$class = Request::get('icon_class');
			$icon_class = AmenityIcon::where('amenity_id', $amenity->id)->first();
			$icon_class->icon_class = $class;
			$icon_class->save();
		}

		return Response::json(array(
			'error' => false,
			'message' => 'Amenity updated!'
			),
			200
		);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$amenity = Amenity::find($id);

		$amenity->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'Amenity deleted'
			),
			200
		);
	}


}
