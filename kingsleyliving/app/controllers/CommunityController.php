<?php

class CommunityController extends \BaseController {

	public function __construct()
	{
		$this->stylesheets = array();
		$this->stylesheets[] = HTML::style('js/dropzone_3.10.2/css/dropzone.css');
		$this->stylesheets[] = HTML::style('css/community/community.css');

		$this->javascript = array();
		$this->javascript[] = HTML::script('js/dropzone_3.10.2/dropzone.js');
		$this->javascript[] = HTML::script('js/community/community.js');

		if (Auth::check()) {
			$user_profile = UserProfile::where('user_id', Auth::user()->id)->first();
			if (!empty($user_profile))
			{
				$this->user_name = $user_profile->first_name.' '.$user_profile->last_name;
			}
			else
			{
				$this->user_name = Auth::user()->username;
			}
		} else {
			$this->user_name = '';
		}

		$this->entrust_options = array('validate_all' => true, 'return_type' => 'both');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Request::is('api/v1/*') || Request::ajax() || Input::has('all_communities'))
		{
			$communities = Community::all();

			return Response::json(array(
				'error' => false,
				'communities' => $communities->toArray()),
				200
			);
		}
		else
		{
			$unsorted_communities = Community::with('address')->orderBy('name', 'asc')->get();
			$communities = $this->groupCommunities($unsorted_communities->toArray());

			// $update = HMVC::post('/rmaprequest/update');
			// print_r($update);
			// echo '<br />';
			// echo '<br />';
			// $login = HMVC::post('/rmaprequest/login');
			// print_r($login);
			// echo '<br />';
			// echo '<br />';
			// HMVC::get('/rmaprequest/users');
			// echo 'Login success!';
			// echo '<br />';
			// echo '<br />';
			// $comm_update = HMVC::get('/rmaprequest/communities');
			// $residents = HMVC::get('/rmaprequest/update_community_residents');
			// $spaces = HMVC::get('/rmaprequest/update_community_spaces');
			// exit;

			$master_data = array(
				'page_title' => 'Community Index',
				'page_stylesheets' => $this->stylesheets,
				'page_scriptlinks' => $this->javascript,
				'user_name' => $this->user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);
			return View::make('community.index', $master_data, array('communities' => $communities));
		}
	}

	private function groupCommunities($communities_array)
	{
		$sorted = array();
		foreach ($communities_array as $key => $community) {
			$community_address = $community['address'];
			foreach ($community_address as $id => $address) {
				$sorted[$address['state']][] = $community;
			}
		}
		ksort($sorted);
		return $sorted;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->stylesheets[] = HTML::style('js/slick/slick.css');
		$this->javascript[] = HTML::script('js/slick/slick.js');
		if (!Confide::user())
		{
			return Redirect::to('/community')->with('error', 'Guests cannot access this page!');
		}
		$user = Confide::user();
		if (!$user->ability(array('rm_admin'), array('create_communities')))
		{
			return Redirect::to('/community')->with('error', 'Only admin users can access the community create page!');
		}
		$master_data = array(
			'page_title' => 'Community Create',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Create New Community';

		$amenities = Amenity::where('class_id', '<', 3)->orderBy('id', 'asc')->get();
		$account_managers = User::with('user_role', 'user_profile')->whereHas('user_role', function($query) {
			$query->where('role_id', '=', 51);
		})->get();

		$property_managers = User::with('user_role', 'user_profile')->whereHas('user_role', function($query) {
			$query->where('role_id', '=', 61);
		})->get();

		$assistant_managers = User::with('user_role', 'user_profile')->whereHas('user_role', function($query) {
			$query->where('role_id', '=', 62);
		})->get();

		$url_request = 'community/create';

		$json_image_array = array('add' => array(), 'images' => array(), 'remove' => array());
		$json_image_id = json_encode($json_image_array);
		
		return View::make('community.create', $master_data, array('title' => $title, 'amenities' => $amenities->toArray(), 'url_request' => $url_request))->with('property_managers', $property_managers)->with('assistant_managers', $assistant_managers)->with('account_managers', $account_managers)->with('json_image_id', $json_image_id);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$valid = Community::validate(Input::all());

		if ( $valid->fails() )
		{
			if (Request::has('url_request'))
			{
				return Redirect::to('community/create')->withErrors($valid)->withInput();
			}
			else
			{
				if (Request::is('api/v1/*') || Request::ajax())
				{
					return Response::json(array(
						'error' => true,
						'message' => $valid->messages()->toJson(),
						),
						406
					);
				}
				else
				{
					return Redirect::to('community/create')->withErrors($valid)->withInput();
				}
			}
		}

		// For the main image
		if (Request::has('main_image'))
		{
			$main_image = Request::get('main_image');
			$location = '/images/community/main_image/';
			$file = Input::file('image');
			$comm_image = HMVC::post('api/v1/internal/image', array(
				'display_name' => $main_image['display_name'],
				'description' => $main_image['description'],
				'location' => $location,
				'file' => $file
				)
			);
			$image = Image::find($comm_image['image_id']);
		}
		else
		{
			$main_image_id = Request::get('main_image_id');
			if ($main_image_id > 0)
			{
				$image = Image::find($main_image_id);
			}
			else
			{
				$image = $main_image_id;
			}
		}

		// Community information
		$community = new Community;
		if (Request::has('propid'))
		{
			$community->propid = Request::get('propid');
		}
		else
		{
			$community->propid = 0;
		}
		if (Request::has('propNum'))
		{
			$community->propNum = Request::get('propNum');
		}
		else
		{
			$community->propNum = 0;
		}
		$community->name = Request::get('name');
		$community->home_page = Request::get('home_page');
		if (is_numeric($image))
		{
			$community->main_image_id = $image;
		}
		else
		{
			$community->main_image()->associate($image);
		}
		$community->short_description = Request::get('short_description');
		$community->description = Request::get('description');
		$community->email = Request::get('email');
		$community->phone = Request::get('phone');
		if (Request::has('fax'))
		{
			$community->fax = Request::get('fax');
		}
		else
		{
			$community->fax = 0;
		}

		// Manager information
		$account_manager = User::find(Request::get('account_manager_id'));
		$property_manager = User::find(Request::get('property_manager_id'));
		$community->account_manager()->associate($account_manager);
		$community->property_manager()->associate($property_manager);
		if (Request::get('assistant_property_manager_id') > 0)
		{
			$assistant_property_manager = User::find(Request::get('assistant_property_manager_id'));
			$community->assistant_property_manager()->associate($assistant_property_manager);
		}
		else
		{
			$community->assistant_property_manager_id = Request::get('assistant_property_manager_id');
		}

		$community->save();
		if (!Request::get('billing_same_shipping'))
		{
			$billing_address = Request::get('billing');
			$shipping_address = Request::get('shipping');
			$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
			$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
			if ($billing_address_id['error'] == false)
			{
				$community->address()->attach($billing_address_id['address_id']);
			}
			
			if ($shipping_address_id['error'] == false)
			{
				$community->address()->attach($shipping_address_id['address_id']);
			}
		}
		else
		{
			$billing_address = Request::get('billing');
			$billing_address['type'] = 'both';
			$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
			if ($billing_address_id['error'] == false)
			{
				$community->address()->attach($billing_address_id['address_id']);
			}
		}

		// Amenity information
		if (Request::has('amenity'))
		{
			$amenities = Request::get('amenity');
			foreach ($amenities as $key => $amenity)
			{
				$community->community_amenity()->attach($amenity['id']);
			}
		}

		if (Request::has('url_request'))
		{
			return Redirect::to('community')->with('message', 'Community created!');
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'message' => 'Community created!',
					'community_name' => $community->name
					),
					201
				);
			}
			else
			{
				return Redirect::to('community')->with('message', 'Community created!');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $identifier
	 * @return Response
	 */
	public function show($id)
	{
		if (is_numeric($id))
		{
			$community = Community::with('address.address_google', 'community_amenity')->where('id', $id)->get();

			return Response::json(array(
				'error' => false,
				'community' => $community->toArray()
				),
				200
			);
		}
		else
		{
			$comm = Community::with(array('address' => function($query) use ($id)
			{
				$query->where('state', 'like', $id);
			}), 'community_amenity')->orderBy('name')->get();
			$communities = array();
			foreach ($comm->toArray() as $community) {
				if (empty($community['address']))
				{
					continue;
				}
				else
				{
					$communities[] = $community;
				}
			}
			return Response::json(array(
				'error' => false,
				'communities' => $communities
				),
				200
			);
		}
	}

	public function view($id)
	{
		$this->javascript[] = HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyClw9UO0i-yS0bXOPgGdblmOVxsmVaHUQE&libraries=places');

		$comm;
		if (is_numeric($id))
		{
			$comm = Community::with('address.address_google', 'community_amenity')->where('id', $id)->get();
		}
		else
		{
			$id = str_replace('_', ' ', $id);
			$comm = Community::with('address.address_google', 'community_amenity')->where('name', 'like', $id)->get();
		}

		$local_community;
		foreach ($comm->toArray() as $value) {
			$local_community = $value;
		}
		$prop_man = User::with('user_profile')->where('id', $local_community['property_manager_id'])->get();
		$pm = $prop_man->toArray();
		$property_manager = array();
		foreach ($pm as $row) {
			foreach ($row as $field => $record) {
				if ($field == 'user_profile'){
					continue;
				}
				else
				{
					$property_manager[$field] = $record;
				}
			}
		}
		foreach ($pm as $row) {
			foreach ($row['user_profile'] as $key => $value) {
				if ($key == 'id')
				{
					continue;
				}
				else if ($key == 'user_id')
				{
					continue;
				}
				else if ($key == 'created_at')
				{
					continue;
				}
				else if ($key == 'updated_at')
				{
					continue;
				}
				else
				{
					$property_manager[$key] = $value;
				}
			}
		}

		$master_data = array(
			'page_title' => 'Community View',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);
		return View::make('community.view', $master_data, array('community' => $local_community, 'property_manager' => $property_manager));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $identifier
	 * @return Response
	 */
	public function edit($id)
	{
		$this->stylesheets[] = HTML::style('js/slick/slick.css');
		$this->javascript[] = HTML::script('js/slick/slick.js');
		if (!Confide::user())
		{
			return Redirect::to('/community/'.$id.'/view')->with('error', 'Guests cannot access this page!');
		}
		$user = Confide::user();
		if (!$user->ability(array('rm_admin'), array('edit_communities')))
		{
			return Redirect::to('/community/'.$id.'/view')->with('error', 'Only admin users can access the community edit page!');
		}	
		$master_data = array(
			'page_title' => 'Community Edit',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		if (is_numeric($id))
		{
			$comm = Community::with('main_image')->where('id', $id)->first();
		}
		else
		{
			$id = str_replace('_', ' ', $id);
			$comm = Community::with('main_image')->where('name', 'like', $id)->first();
		}

		$amenities = Amenity::where('class_id', '<', 3)->orderBy('id', 'asc')->get();

		$account_managers = User::with('user_role', 'user_profile')->whereHas('user_role', function($query) {
			$query->where('role_id', '=', 51);
		})->get();

		$property_managers = User::with('user_role', 'user_profile')->whereHas('user_role', function($query) {
			$query->where('role_id', '=', 61);
		})->get();

		$assistant_managers = User::with('user_role', 'user_profile')->whereHas('user_role', function($query) {
			$query->where('role_id', '=', 62);
		})->get();

		$title = 'Edit Community';
		$url_request = 'community/edit';

		$community_amenity = $comm->community_amenity()->lists('amenity_id');

		$community = $comm->toArray();

		// Note: Laravel's form builder does not like collection objects, so it is necessary to package things into an array in order to get things working like they should.  Of course, if it's just checkboxes, its a different story.

		$same = $comm->address()->sameAddress()->first();
		if (empty($same))
		{
			$billing = $comm->address()->primaryBillingAddress()->first();
			$shipping = $comm->address()->primaryShippingAddress()->first();
			$community['billing'] = $billing->toArray();
			$community['shipping'] = $shipping->toArray();
		}
		else
		{
			$community['billing'] = $same->toArray();
			$community['shipping'] = '';
			$community['billing_same_shipping'] = true;
		}

		$images = $comm->community_image()->lists('image_id');

		$json_image_array = array('add' => array(), 'images' => $images, 'remove' => array());
		$json_image_id = json_encode($json_image_array);

		return View::make('community.edit', $master_data, array('title' => $title, 'url_request' => $url_request))->with('community', $community)->with('property_managers', $property_managers->toArray())->with('assistant_managers', $assistant_managers->toArray())->with('account_managers', $account_managers->toArray())->with('community_amenity', $community_amenity)->with('amenities', $amenities)->with('json_image_id', $json_image_id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$valid = Community::validate(Input::all(), $id);

		if ( $valid->fails() )
		{
			if (Request::has('url_request'))
			{
				return Redirect::to('community/'.$id.'/edit')->withErrors($valid)->withInput();
			}
			else
			{
				if (Request::is('api/v1/*') || Request::ajax())
				{
					return Response::json(array(
						'error' => true,
						'message' => $valid->messages()->toJson(),
						),
						406
					);
				}
				else
				{
					return Redirect::to('community/'.$id.'/edit')->withErrors($valid)->withInput();
				}
			}
		}

		$community = Community::with('address', 'community_amenity', 'main_image')->where('id', $id)->first();

		if ( Request::get('main_image') )
		{
			$main_image = Request::get('main_image');
		}

		if ( Input::file('image') )
		{
			$file = Input::file('image');
		}

		if (isset($main_image) || isset($file))
		{
			if (isset($main_image) && !isset($file))
			{
				$image = HMVC::put('api/v1/internal/image/'.$community->main_image_id, array(
					'display_name' => $main_image['display_name'],
					'description' => $main_image['description']
					)
				);
			}
			elseif (isset($file) && !isset($main_image))
			{
				$image = HMVC::put('api/v1/internal/image/'.$community->main_image_id, array(
					'file' => $file
					)
				);
			}
			else
			{
				$image = HMVC::put('api/v1/internal/image/'.$community->main_image_id, array(
					'display_name' => $main_image['display_name'],
					'description' => $main_image['description'],
					'file' => $file
					)
				);
			}
			$new_image = Image::find($image['image_id'])->first();
			$community->main_image()->associate($new_image);
		}

		if ( Request::get('name') )
		{
			$community->name = Request::get('name');
		}

		if ( Request::get('short_description') )
		{
			$community->short_description = Request::get('short_description');
		}

		if ( Request::get('description') )
		{
			$community->description = Request::get('description');
		}

		if ( Request::get('email') )
		{
			$community->email = Request::get('email');
		}

		if ( Request::get('phone') )
		{
			$community->phone = Request::get('phone');
		}

		if ( Request::get('fax') )
		{
			$community->fax = Request::get('fax');
		}

		if ( Request::get('home_page') )
		{
			$community->home_page = Request::get('home_page');
		}

		if ( Request::get('property_manager_id') )
		{
			$property_manager = User::find(Request::get('property_manager_id'));
			$community->property_manager()->associate($property_manager);
		}

		if ( Request::get('assistant_property_manager_id') )
		{
			$assistant_property_manager = User::find(Request::get('assistant_property_manager_id'));
			$community->assistant_property_manager()->associate($assistant_property_manager);
		}

		if ( Request::get('account_manager_id') )
		{
			$account_manager = User::find(Request::get('account_manager_id'));
			$community->account_manager()->associate($account_manager);
		}

		$old_address = $community->address->toArray();

		$community->save();

		if ( !Request::get('billing_same_shipping') )
		{
			$billing_address = Request::get('billing');
			$shipping_address = Request::get('shipping');
			$billing_address_id = HMVC::put('api/v1/internal/address/'.$old_address[0]['id'], $billing_address);
			if (isset($old_address[1]))
			{
				$shipping_address_id = HMVC::put('api/v1/internal/address/'.$old_address[1]['id'], $shipping_address);
			}
			else
			{
				$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
				$community->address()->attach($shipping_address_id['address_id']);
			}
		}
		else
		{
			$billing_address = Request::get('billing');
			$billing_address['type'] = 'both';
			$billing_address_id = HMVC::put('api/v1/internal/address/'.$old_address[0]['id'], $billing_address);
			if (isset($old_address[1]))
			{
				$community->address()->detach($old_address[1]['id']);
			}
		}

		if ( Request::get('amenity') )
		{
			$amenities = Request::get('amenity');
			$old_amenities = $community->community_amenity()->lists('amenity_id');
			foreach ($old_amenities as $key => $id) {
				$community->community_amenity()->detach($id);
			}
		}

		foreach ($amenities as $a_key => $amenity) {
			$community->community_amenity()->attach($amenity['id']);
		}

		if (Request::has('url_request'))
		{
			return Redirect::to('community')->with('message', 'Community updated!');
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'message' => 'Community updated!',
					'community_name' => $community->name
					),
					200
				);
			}
			else
			{
				return Redirect::to('community')->with('message', 'Community updated!');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$community = Community::find($id);

		$community->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'Community deleted'
			),
			200
		);
	}


}
