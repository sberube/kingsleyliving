<?php

class EmploymentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$employment = new Employment;

		$employment->employer = Request::get('employer');
		$employment->phone = Request::get('phone');
		$employment->job_title = Request::get('job_title');

		// Employment Address
		$e_address = new EmploymentAddress;
		$e_address->address_1 = Request::get('address_1');
		$e_address->address_2 = Request::get('address_2');
		$e_address->city = Request::get('city');
		$e_address->state = Request::get('state');
		$e_address->postal_code = Request::get('postal_code');
		if (Request::has('country_id'))
		{
			$country = AddressCountry::find(Request::get('country_id'));
		}
		else
		{
			$country = AddressCountry::where('name', 'like', Request::get('country'))->first();
		}
		$e_address->address_country()->associate($country);
		$employment->employment_address()->save($e_address);

		// Employment Length / Dates
		$e_dates = new EmploymentDate;
		$e_dates->months = Request::get('months');
		$e_dates->days = Request::get('days');
		$e_dates->years = Request::get('years');
		$employment->employment_dates()->save($e_dates);

		$employment->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Hours created!',
			'employment_id' => $employment->id
			),
			200
		);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
