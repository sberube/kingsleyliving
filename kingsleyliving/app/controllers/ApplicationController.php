<?php

class ApplicationController extends \BaseController {

	public function __construct()
	{
		$this->stylesheets = array();
		$this->stylesheets[] = HTML::style('css/application/application.css');

		$this->javascript = array();
		$this->javascript[] = HTML::script('js/application/application.js');

		if (Auth::check()) {
			$user_profile = UserProfile::where('user_id', Auth::user()->id)->first();
			if (!empty($user_profile))
			{
				$this->user_name = $user_profile->first_name.' '.$user_profile->last_name;
			}
			else
			{
				$this->user_name = Auth::user()->username;
			}
		} else {
			$this->user_name = '';
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($community, $space=NULL)
	{
		$this->stylesheets[] = HTML::style('js/jquery-ui-1.11.1.dark_hive/jquery-ui.css');

		$comm = Community::where('id', $community)->first();
		if ($space)
		{
			$spc = Space::where('id', $space)->first();
		}

		$master_data = array(
			'page_title' => 'Application Form',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Fill Out an Application';
		$url_request = 'application/create';

		$communities = Community::lists('name', 'id');
		$spaces = Space::where('community_id', $comm['id'])->where('space_status_id', '>=', 3)->lists('space_num', 'id');

		$new_spaces = array(0 => '');
		foreach ($spaces as $id => $spc_num) {
			$new_spaces[$id] = 'Space '.$spc_num;
		}

		// $topics = ContactInterest::lists('interests', 'id');
		// $means = ContactMean::lists('means', 'id');

		if ($space)
		{
			return View::make('application.create', $master_data, array('title' => $title, 'url_request' => $url_request))->with('communities', $communities)->with('app_community', $comm->toArray())->with('spaces', $new_spaces)->with('app_space', $spc->toArray());
		}
		else
		{
			return View::make('application.create', $master_data, array('title' => $title, 'url_request' => $url_request))->with('communities', $communities)->with('app_community', $comm->toArray())->with('spaces', $new_spaces);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Request::has('url_request'))
		{
			$url_request = Request::get('url_request');
		}
		
		if ($url_request == 'space/view')
		{
			$full_name = Request::get('name');
			$community_id = Request::get('community_id');
			$community = Community::where('id', $community_id)->first();
			$space_id = Request::get('space');
			$space = Space::where('id', $space_id)->first();
			if (Request::has('user_id'))
			{
				$user_id = Request::get('user_id');
				$user_obj = User::with('user_profile')->where('id', $user_id)->first();
				$user = $user_obj->toArray();
				$u_add = $user_obj->address()->get();
				if (empty($u_add->toArray()))
				{
					$user_address = Request::get('current');
					$billing_address_id = HMVC::post('api/v1/internal/address', $user_address);
					$user_obj->address()->attach($billing_address_id['address_id']);
				}
				$success_message = 'Application created!';
			}
			else
			{
				$gender = Request::get('gender');
				$user_data['user']['email'] = Request::get('email');
				$user_data['user']['username'] = Request::get('username');
				$user_data['user']['password'] = Request::get('password');
				$user_data['user']['password_confirmation'] = Request::get('password_confirmation');
				$user_data['user_profile']['first_name'] = Request::get('first_name');
				$user_data['user_profile']['middle_name'] = Request::get('middle_name');
				$user_data['user_profile']['last_name'] = Request::get('last_name');
				$user_data['user_profile']['suffix'] = Request::get('suffix');
				$user_data['user_profile']['phone'] = Request::get('phone');
				$user_address = Request::get('current');
				$user_data['billing'] = $user_address;
				if ($gender == 'mr')
				{
					$user_data['user_profile']['gender'] = 'Male';
				}
				else
				{
					$user_data['user_profile']['gender'] = 'Female';
				}
				$user_data['application_user_create'] = true;
				$user_response = HMVC::post('api/v1/internal/user', $user_data);
				$user_obj = User::with('user_profile')->where('id', $user_response->id)->first();
				$user = $user_obj->toArray();
				$success_message = 'Application &amp; User created!';
			}
			$same = $user_obj->address()->sameAddress()->first();
			if (empty($same))
			{
				$billing = $user_obj->address()->primaryBillingAddress()->first();
				$user['billing'] = $billing->toArray();
				$country = AddressCountry::where('id', $user['billing']['country_id'])->first();
				$user['billing']['country'] = $country->name;
			}
			else
			{
				$user['billing'] = $same->toArray();
				$country = AddressCountry::where('id', $user['billing']['country_id'])->first();
				$user['billing']['country'] = $country->name;
			}

			$application = new Application;
			$application->community()->associate($community);
			$application->notes = Request::get('notes');

			$application->save();

			$application->application_space()->attach($space->id);
			$resident_type_id = Request::get('resident_type');
			$resident_type = ApplicationResidentType::where('id', $resident_type_id)->first();

			$application_resident = new ApplicationResident;
			$application_resident->application()->associate($application);
			$application_resident->user()->associate($user_obj);
			$application_resident->application_resident_type()->associate($resident_type);
			$application_resident->move_in_date = date('Y-m-d', strtotime(Request::get('move_in_date')));

			$application_resident->save();

			$application_source = new ApplicationSource;
			$application_source->application()->associate($application);
			$application_source->source = 'KingsleyLiving.com Application';

			$application_source->save();

			Mail::send('emails.application.initapplication', array('application' => $application, 'application_resident' => $application_resident, 'community' => $community, 'space' => $space, 'full_name' => $full_name, 'user' => $user), function($message) use ($user, $full_name)
			{
				$message->to('thaynetrevenen@kingsleylivingcorp.com', 'KingsleyLiving Customer Support')->cc($user['email'], $full_name)->subject('Initial Application');
			});

			return Redirect::to($community->id.'/space/'.$space->id.'/view')->with('success', $success_message);
		}
		else
		{
			// $comm = Community::where('id', $community)->first();
			// if ($space)
			// {
			// 	$spc = Space::where('id', $space)->first();
			// }
			// Application information
			$application = new Application;
			// $application->community()->associate($comm);
			$application->notes = Request::get('notes');

			$application->save();

			$app_pricing = new ApplicationPricing;
			$app_resident = new ApplicationResident;
			$app_resident_income = new ApplicationResidentIncome;
			$app_source = new ApplicationSource;
			// if (!Request::get('billing_same_shipping'))
			// {
			// 	$billing_address = Request::get('billing');
			// 	$shipping_address = Request::get('shipping');
			// 	$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
			// 	$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
			// 	$application->address()->attach($billing_address_id['address_id']);
			// 	$application->address()->attach($shipping_address_id['address_id']);
			// }
			// else
			// {
			// 	$billing_address = Request::get('billing');
			// 	$billing_address['type'] = 'both';
			// 	$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
			// 	$application->address()->attach($billing_address_id['address_id']);
			// }

			// $validator = Validator::make($community, array(
			// 	'propid' => 'integer',
			// 	'name' => 'required',
			// 	'main_image_id' => 'integer',
			// 	'email' => 'required|email|unique:communities',
			// 	'property_manager_id' => 'integer',
			// 	'assistant_property_manager_id' => 'integer',
			// 	'account_manager_id' => 'integer',
			// 	'home_page' => 'url'
			// 	)
			// );

			// if ($validator->fails())
			// {
			// 	// The given data did not pass validation
			// }
		}

		if (isset($url_request))
		{
			return Redirect::to('community/'.$comm->id.'/view')->with('success', 'Application created!');
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'message' => 'Community created!',
					'community_name' => $community->name
					),
					200
				);
			}
			else
			{
				return Redirect::to('community')->with('message', 'Community created!');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->stylesheets[] = HTML::style('js/jquery-ui-1.11.1.dark_hive/jquery-ui.css');

		$master_data = array(
			'page_title' => 'Application Form',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
