<?php

class ContactController extends \BaseController {

	public function __construct()
	{
		$this->stylesheets = array();
		$this->stylesheets[] = HTML::style('css/communications/communications.css');

		$this->javascript = array();
		$this->javascript[] = HTML::script('js/communications/communications.js');

		if (Auth::check()) {
			$user_profile = UserProfile::where('user_id', Auth::user()->id)->first();
			if (!empty($user_profile))
			{
				$this->user_name = $user_profile->first_name.' '.$user_profile->last_name;
			}
			else
			{
				$this->user_name = Auth::user()->username;
			}
		} else {
			$this->user_name = '';
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$master_data = array(
			'page_title' => 'Contact Kingsley',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Contact Us';
		$url_request = 'contact/create';

		$topics = ContactInterest::lists('interests', 'id');
		$means = ContactMean::lists('means', 'id');

		return View::make('contact.create', $master_data, array('title' => $title, 'url_request' => $url_request))->with('topic_interest', $topics)->with('contact_means', $means);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$contact = new Contact;
		$contact->first_name = Request::get('first_name');
		$contact->last_name = Request::get('last_name');
		$contact->email = Request::get('email');
		$contact->phone = Request::get('phone');

		$contact_interest = ContactInterest::find(Request::get('contact_interest_id'));
		$contact_means = ContactMean::find(Request::get('contact_means_id'));
		$contact_status = ContactStatus::find(Request::get('contact_status_id'));
		$contact_type = ContactType::find(Request::get('contact_type_id'));

		$contact->contact_interests()->associate($contact_interest);
		$contact->contact_means()->associate($contact_means);
		$contact->contact_status()->associate($contact_status);
		$contact->contact_type()->associate($contact_type);

		$contact->extra_notes = Request::get('extra_notes');

		// $notes = Request::get('notes');
		// $days = Request::get('days_id');
		// $time = Request::get('time_id');
		// $type = Request::get('type_id');
		// $hours = HMVC::post('api/v1/internal/hour', array(
		// 	'notes' => $notes,
		// 	'days_id' => $days,
		// 	'time_id' => $time,
		// 	'type_id' => $type
		// 	)
		// );

		// $contact->contact_hour()->attach($hours['hour_id']);

		$contact_address = Request::get('contact');

		$contact_address_id = HMVC::post('api/v1/internal/address', $contact_address);

		$contact->save();

		$contact->contact_address()->attach($contact_address_id['address_id']);

		// create email and send to appropriate place depending on the topic of interest

		if (Request::has('url_request'))
		{
			return Redirect::to('/')->with('message', 'Contact created!');
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'message' => 'Contact created!',
					'contact_id' => $contact->id
					),
					200
				);
			}
			else
			{
				return Redirect::to('/')->with('message', 'Contact created!');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
