<?php

class ImportErrors {
	public function __construct()
	{
		$this->failed = null;
		$this->http_code = null;
		$this->record_table = null;
		$this->record_field = null;
		$this->record_id = null;
		$this->error_message = null;
	}

	public function isEmpty()
	{
		if (!empty($this->failed))
		{
			return false;
		}
		else if (!empty($this->http_code))
		{
			return false;
		}
		else if (!empty($this->table))
		{
			return false;
		}
		else if (!empty($this->field))
		{
			return false;
		}
		else if (!empty($this->identity))
		{
			return false;
		}
		else if (!empty($this->error_message))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function resetImportError()
	{
		$this->failed = null;
		$this->http_code = null;
		$this->table = null;
		$this->field = null;
		$this->identity = null;
		$this->error_message = null;
		return;
	}
}

class ResidentMapController extends \BaseController {
// For all things to do with connecting to, retrieving from, or pushing to ResidentMap
	public function __construct()
	{
		$this->curl = null;
		$this->json_response = null;
	}

	private function toArray($obj) {
		if (is_object($obj)) {
			$obj = get_object_vars($obj);
		}
 
		if (is_array($obj)) {
			return array_map([__CLASS__, __METHOD__], $obj);
		}
		else {
			return $obj;
		}
	}

	private function toObject($arry) {
		if (is_array($arry)) {
			return (object) array_map([__CLASS__, __METHOD__], $arry);
		}
		else {
			return $arry;
		}
	}

	private function setCurlOpts()
	{
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
	    // curl_setopt($this->curl, CURLOPT_USERAGENT, $this->useragent);
	    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($this->curl, CURLOPT_COOKIE, $this->cookie);
		curl_setopt($this->curl, CURLOPT_COOKIEJAR, "samplecookie.txt");
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, "samplecookie.txt");
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
		return;
	}

	private function reset()
	{
		$this->curl = null;
		$this->json_response = null;
	}

	public function fullUpdate()
	{
		$update = new Update;
		$login = $this->toObject($this->postLogin());
		if (strtolower($login->message) == 'success')
		{
			$users = $this->toObject($this->getUsers());
			$communities = $this->toObject($this->getCommunities());
			$residents = $this->getUpdateCommunityResidents();
			$spaces = $this->getUpdateCommunitySpaces();
		}
		else
		{
			// Log this --v ?
			$update->failed = true;
			$update->http_code = 500;
			$update->record_table = 'users';
			$update->record_field = 'id';
			$update->record_id = 0;
			$update->error_message = 'fullUpdate() failed at login';
			$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($update));
			$update->resetImportError();
		}
		return;
	}

	public function properAddress($address)
	{
		// Return a proper address
		$periods = str_replace('.', '', $address);
		$commas = str_replace(',', '', $periods);
		$north = str_replace('North', 'N', $commas);
		$south = str_replace('South', 'S', $north);
		$west = str_replace('West', 'W', $south);
		$east = str_replace('East', 'E', $west);
		$suite = str_replace('Suite', 'Ste', $east);
		$avenue = str_replace('Avenue', 'Ave', $suite);
		$blvd = str_replace('Boulevard', 'Blvd', $avenue);
		$street = str_replace('Street', 'St', $blvd);
		$road = str_replace('Road', 'Rd', $street);
		$drive = str_replace('Drive', 'Dr', $road);
		$lane = str_replace('Lane', 'Ln', $drive);
		$trim = trim($lane);
		// $return_val = preg_replace('#(?<=\d)(?=[a-z])#i', ' ', $trim);
		return $trim;
	}

	public function postLogin()
	{
		// $this->json_response = HMVC::invokeRemote('https://kingsley.residentmap.com/kingsleylivingapi.php', 'get', array('module' => 'user', 'cmd' => 'login', 'username' => 'kingsleylivingapi', 'password' => md5('cr3dTigr1942')));

		session_write_close();
		$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=user&cmd=login&username=kingsleylivingapi&password='.md5('cr3dTigr1942'));
		$this->setCurlOpts();
		$this->json_response = curl_exec($this->curl);
		curl_close($this->curl); // This is the alternate method to the HMVC laravel plugin for now

		$login = json_decode($this->json_response);
		$this->reset();
		return $this->toArray($login);
	}

	public function getUsers()
	{
		session_write_close();
		$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=user&type=corporate&cmd=list');
		$this->setCurlOpts();
		$this->json_response = curl_exec($this->curl);
		curl_close($this->curl);
		$rmap_corp_users = json_decode($this->json_response);

		$users = array();
		$user_update = new ImportErrors();
		$json_report = array();
		$user_roles = array();
		if ($rmap_corp_users->statuscode == 200)
		{
			$corp_users = $this->toArray($rmap_corp_users);
			foreach ($corp_users['response'] as $position => $user_array) {
				$temp = array();
				foreach ($user_array as $field => $record) {
					if ($field == 'billingAddress')
					{
						$address = array();
						foreach ($record as $id => $value) {
							if ($id == 'address1')
							{
								if (strpos($value, ',') != false)
								{
									print_r($user_array);
									$user_update->failed = true;
									$user_update->http_code = 500;
									$user_update->record_table = 'address';
									$user_update->record_field = 'address_1';
									$user_update->record_id = $user_array['id'];
									$user_update->error_message = 'Address_1 contained illegal character(s): \',\'';
									$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
									$user_update->resetImportError();
								}
								
								if (strpos($value, '--') != false)
								{
									$user_update->failed = true;
									$user_update->http_code = 500;
									$user_update->record_table = 'address';
									$user_update->record_field = 'address_1';
									$user_update->record_id = $user_array['id'];
									$user_update->error_message = 'Address_1 contained illegal character(s): \'--\'';
									$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
									$user_update->resetImportError();
								}

								if (strpos(strtolower($value), 'office') != false)
								{
									$user_update->failed = true;
									$user_update->http_code = 200;
									$user_update->record_table = 'address';
									$user_update->record_field = 'address_1';
									$user_update->record_id = $user_array['id'];
									$user_update->error_message = 'Warning: address_1 contains the word: \'office\'';
									$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
									$user_update->resetImportError();
								}
								$addr1 = $this->properAddress($value);
								$address['address_1'] = $addr1;
							}
							else if ($id == 'address2')
							{
								$address['address_2'] = $value;
							}
							else if ($id == 'zipcode')
							{
								$address['postal_code'] = $value;
							}
							else if ($id == 'country')
							{
								continue;
							}
							else
							{
								$address[$id] = $value;
							}
						}
						if (!empty($address))
						{
							$address['type_id'] = 1;
							$country = AddressCountry::where('name', 'like', 'United States')->first();
							$address['country_id'] = $country->id;
						}
						$temp['billing'] = $address;
						$temp['billing_same_shipping'] = 1;
					}
					else if ($field == 'firstName')
					{
						if (strpos($record, '&') != false)
						{
							$user_update->failed = true;
							$user_update->http_code = 500;
							$user_update->record_table = 'user_profile';
							$user_update->record_field = 'first_name';
							$user_update->record_id = $user_array['id'];
							$user_update->error_message = 'User\'s name contained illegal character \'&amp;\'';
							$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
							$user_update->resetImportError();
						}
						$temp['user_profile']['first_name'] = $record;
					}
					else if ($field == 'lastName')
					{
						if (empty($record))
						{
							$user_update->failed = true;
							$user_update->http_code = 500;
							$user_update->record_table = 'user_profile';
							$user_update->record_field = 'last_name';
							$user_update->record_id = $user_array['id'];
							$user_update->error_message = 'No last name field found';
							$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
							$user_update->resetImportError();
						}
						$temp['user_profile']['last_name'] = $record;
					}
					else if ($field == 'userLevel')
					{
						switch ($record) {
							case '1':
								$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '1');
								break;

							case '2':
								$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '51');
								break;

							case '3':
								$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '61');
								break;

							case '7':
								$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '18');
								break;

							case '8':
								$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '12');
								break;
							
							default:
								$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '63');
								break;
						}
					}
					else if ($field == 'isAccountManager')
					{
						continue;
					}
					else if ($field == 'isCommunityManager')
					{
						continue;
					}
					else if ($field == 'id')
					{
						$temp['rmap_id'] = $record;
					}
					else if ($field == 'contact')
					{
						if (empty($record['email']))
						{
							$user_update->failed = true;
							$user_update->http_code = 500;
							$user_update->record_table = 'users';
							$user_update->record_field = 'email';
							$user_update->record_id = $user_array['id'];
							$user_update->error_message = 'No email field found';
							$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
							$user_update->resetImportError();
							$temp['email'] = '';
						}
						else
						{
							if (strpos($record['email'], '@') != false)
							{
								$temp['email'] = $record['email'];
							}
							else
							{
								$temp['email'] = '';
								$user_update->failed = true;
								$user_update->http_code = 500;
								$user_update->record_table = 'users';
								$user_update->record_field = 'email';
								$user_update->record_id = $user_array['id'];
								$user_update->error_message = 'Invalid email: '.$record['email'];
								$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
								$user_update->resetImportError();
							}
						}
						$phone = $this->formatPhone($record['phone']);
						$temp['user_profile']['phone'] = $phone;
						$temp['user_profile']['gender'] = 'Male';
					}
					else
					{
						$temp[$field] = $record;
					}
				}
				$temp['import_user_create'] = true;
				$users[] = $temp;
			}
			$timed_groups = $this->groupsOfFive($users);
			foreach ($timed_groups as $key => $group) {
				foreach ($group as $position => $user) {
					if (empty($user['email']))
					{
						$user_update->failed = true;
						$user_update->http_code = 500;
						$user_update->record_table = 'users';
						$user_update->record_field = 'email';
						$user_update->record_id = $user['rmap_id'];
						$user_update->error_message = 'Unable to create user, email is required field';
						$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
						$user_update->resetImportError();
						continue;
					}
					else
					{
						$email_check = User::where('email', 'like', $user['email'])->first();
						if (!empty($email_check))
						{
							$user_update->failed = true;
							$user_update->http_code = 500;
							$user_update->record_table = 'users';
							$user_update->record_field = 'email';
							$user_update->record_id = $user['rmap_id'];
							$user_update->error_message = 'This user has a duplicate email with another user, this is not allowed.';
							$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
							$user_update->resetImportError();
							continue;
						}
						else
						{
							$results = HMVC::post('api/v1/internal/user', $user);
							$rmap = DB::table('residentmap_users_pvt_kingsleyliving_users')->where('user_id', $results->id)->first();
							foreach ($user_roles as $key => $rmap_vals) {
								if ($rmap_vals['rmap_id'] == $rmap->rmap_id)
								{
									$role = UserRole::where('id', $rmap_vals['role_id'])->first();

									$results->attachRole( $role );
								}
							}
						}
					}
				}
				sleep(1);
			}
			return;
		}
		else
		{
			$this->reset();
			$user_update->failed = true;
			$user_update->http_code = 500;
			$user_update->record_table = 'residentmap_connection';
			$user_update->record_field = 'getcommunities';
			$user_update->record_id = 0;
			$user_update->error_message = $rmap_corp_users->message;
			$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($user_update));
			$user_update->resetImportError();
			return $rmap_corp_users->message;
		}
	}

	private function groupsOfFive($array)
	{
		$return_array = array();
		$group5 = array();
		$count = 1;
		foreach ($array as $key => $vals) {
			if ($count <= 5)
			{
				$group5[] = $vals;
				$count += 1;
			}
			else
			{
				$return_array[] = $group5;
				$group5 = array();
				$group5[] = $vals;
				$count = 2;
			}
		}
		return $return_array;
	}

	private function formatPhone($phone) {
		// note: making sure we have something
		if(!isset($phone{3}))
		{
			return '';
		}
		// note: strip out everything but numbers
		$phone = preg_replace("/[^0-9]/", "", $phone);
		$length = strlen($phone);
		switch($length) {
			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
				break;
			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
				break;
			case 11:
				return preg_replace("/([0-9{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
				break;
			default:
				return $phone;
				break;
		}
	}

	public function getCommunities($propNum=null)
	{
		// $response = HMVC::invokeRemote('https://kingsley.residentmap.com/kingsleylivingapi.php', 'get', array('module' => 'community', 'cmd' => 'list', 'type' => 'managers', 'propnum' => '130'));
		$comm_update = new ImportErrors();
		session_write_close();
		if (!empty($propNum))
		{
			$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=community&cmd=list&propNum='.$propNum);
		    $this->setCurlOpts();
		    $this->json_response = curl_exec($this->curl);
		    curl_close($this->curl);
		}
		else
	    {
	    	$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=community&cmd=list');
		    $this->setCurlOpts();
		    $this->json_response = curl_exec($this->curl);
		    curl_close($this->curl);
		}

		$rmap_communities = json_decode($this->json_response);
		if ($rmap_communities->statuscode == 200)
		{
			$communities = array();
			$temp = $this->toArray($rmap_communities);
			foreach ($temp as $id => $community) {
				if (is_array($community))
				{
					$inner_community = array();
					foreach ($community as $comm_id => $comm) {
						$inner_array = array();
						foreach ($comm as $field => $record) {
							if ($field == 'name')
							{
								$inner_array[$field] = $record;
								$comm_name = str_replace(' ','_', strtolower($record));
								$inner_array['home_page'] = 'http://www.kingsleyliving.com/public/index.php/community/'.$comm_name;
							}
							else if ($field == 'address')
							{
								$address_array = array();
								foreach ($record as $inner_field => $inner_record) {
									if ($inner_field == 'address1')
									{
										$addr_1 = strtolower($inner_record);
										if (strpos($addr_1, ',') != false)
										{
											$comm_update->failed = true;
											$comm_update->http_code = 500;
											$comm_update->record_table = 'address';
											$comm_update->record_field = 'address_1';
											$comm_update->record_id = $comm['propNum'];
											$comm_update->error_message = 'Address_1 contained illegal character(s): \',\'';
											$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($comm_update));
											$comm_update->resetImportError();
										}
										
										if (strpos($addr_1, '--') != false)
										{
											$comm_update->failed = true;
											$comm_update->http_code = 500;
											$comm_update->record_table = 'address';
											$comm_update->record_field = 'address_1';
											$comm_update->record_id = $comm['propNum'];
											$comm_update->error_message = 'Address_1 contained illegal character(s): \'--\'';
											$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($comm_update));
											$comm_update->resetImportError();
										}

										if (strpos($addr_1, 'office') != false)
										{
											$comm_update->failed = true;
											$comm_update->http_code = 200;
											$comm_update->record_table = 'address';
											$comm_update->record_field = 'address_1';
											$comm_update->record_id = $comm['propNum'];
											$comm_update->error_message = 'Warning: address_1 contains the word: \'office\'';
											$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($comm_update));
											$comm_update->resetImportError();
										}
										$addr1 = $this->properAddress($inner_record);
										$address_array['address_1'] = $addr1;
									}
									else if ($inner_field == 'address2')
									{
										$address_array['address_2'] = $inner_record;
									}
									else if ($inner_field == 'zipcode')
									{
										$address_array['postal_code'] = $inner_record;
									}
									else if ($inner_field == 'country')
									{
										continue;
									}
									else
									{
										$address_array[$inner_field] = $inner_record;
									}
								}
								if (!empty($address_array))
								{
									$address_array['type_id'] = 1;
									$country = AddressCountry::where('name', 'like', 'United States')->first();
									$address_array['country_id'] = $country->id;
								}
								$inner_array['billing'] = $address_array;
							}
							else if ($field == 'fax')
							{
								if (empty($record))
								{
									$inner_array[$field] = 0;
								}
								else
								{
									$inner_array[$field] = $this->formatPhone($record);
								}
							}
							else if ($field == 'phone')
							{
								$inner_array[$field] = $this->formatPhone($record);
							}
							else
							{
								$inner_array[$field] = $record;
							}
						}
						$inner_array['main_image_id'] = 0;
						$inner_array['short_description'] = 'None';
						$inner_array['description'] = 'None';
						$inner_array['billing_same_shipping'] = true;
						if ($inner_array['propNum'] == 177 || $inner_array['propNum'] == 178 || $inner_array['propNum'] == 170 || $inner_array['propNum'] == 174)
						{
							continue;
						}
						$inner_community[$comm_id] = $inner_array;
					}
					$communities[$id] = $inner_community;
				}
				else
				{
					$communities[$id] = $community;
				}
			}
			$comm_store = array();
			foreach ($communities['response'] as $community) {
				$managers = $this->getManagers($community['propNum']);
				if ($managers == 406)
				{
					$comm_update->failed = true;
					$comm_update->http_code = 500;
					$comm_update->record_table = 'community';
					$comm_update->record_field = 'managers';
					$comm_update->record_id = $community['propNum'];
					$comm_update->error_message = 'Error, community manager fields do not contain valid users';
					$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($comm_update));
					$comm_update->resetImportError();
					continue;
				}
				else if ($managers != 'Unauthorized')
				{
					// print_r($managers);
					// echo '<br />';
					// echo '<br />';
					$valid_users = $this->userValid(array('account_manager_id' => $managers['accountManagerId'], 'property_manager_id' => $managers['communityManagerId']));
					// print_r($valid_users);
					// echo '<br />';
					// echo '<br />';
					$valid = true;
					if (is_array($valid_users) == true)
					{
						foreach ($valid_users as $identity => $bool) {
							if ($bool['user_found'] == true)
							{
								continue;
							}
							else
							{
								$valid = false;
							}
						}
					}
					else
					{
						$valid = false;
					}
					if ($valid == true)
					{
						$community['account_manager_id'] = $valid_users[$managers['accountManagerId']]['account_manager_id'];
						$community['property_manager_id'] = $valid_users[$managers['communityManagerId']]['property_manager_id'];
						$community['assistant_property_manager_id'] = (!empty($managers['assistantManagerId'])?$managers['assistantManagerId']:0);
						$comm_store[] = $community;
					}
					else
					{
						$comm_update->failed = true;
						$comm_update->http_code = 500;
						$comm_update->record_table = 'community';
						$comm_update->record_field = ($valid_users[$managers['accountManagerId']]['user_found'] == false?'account_manager_id':'property_manager_id');
						$comm_update->record_id = $community['propNum'];
						$comm_update->error_message = 'Error, community field \''.($valid_users[$managers['accountManagerId']]['user_found'] == false?'account_manager_id':'property_manager_id').'\' does not contain a valid user';
						$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($comm_update));
						$comm_update->resetImportError();
						continue;
					}
				}
				else
				{
					$this->reset();
					return $this->toArray($managers);
				}
					
			}
			$timed_groups = $this->groupsOfFive($comm_store);
			foreach ($timed_groups as $key => $group) {
				// put these into groups of five and run one group every two seconds?
				foreach ($group as $position => $community) {
					// print_r($community);
					// echo '<br />';
					// echo '<br />';
					$results = HMVC::post('api/v1/internal/community', $community);
				}

				sleep(1);
			}
			return Response::json(array(
				'error' => false,
				'message' => 'Communities updated!  Check import errors to see what went wrong',
				),
				200
			);
		}
		else
		{
			$this->reset();
			$comm_update->failed = true;
			$comm_update->http_code = 500;
			$comm_update->record_table = 'residentmap_connection';
			$comm_update->record_field = 'getcommunities';
			$comm_update->record_id = 0;
			$comm_update->error_message = $rmap_communities->message;
			$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($comm_update));
			$comm_update->resetImportError();
			return $rmap_communities->message;
		}
	}

	private function userValid($identity)
	{
		$return_array = array();
		if (is_array($identity))
		{
			foreach ($identity as $key => $id) {
				$temp = DB::table('residentmap_users_pvt_kingsleyliving_users')->where('rmap_id', $id)->first();
				$rmap_relation = $this->toArray($temp);
				$user = User::find($rmap_relation['user_id']);
				if (!empty($user))
				{
					$return_array[$id] = array('user_found' => true, $key => $user->id);
				}
				else
				{
					$return_array[$id] = array('user_found' => false);
				}
			}
			return $return_array;
		}
		else
		{
			$temp = DB::table('residentmap_users_pvt_kingsleyliving_users')->where('rmap_id', $identity)->first();
			$rmap_relation = $this->toArray($temp);
			$user = User::find($rmap_relation['user_id']);
			if (!empty($user))
			{
				return $user->id;
			}
			else
			{
				return false;
			}
		}
	}

	public function getUpdateCommunityManagers()
	{
		$manager_update = new ImportErrors();
		$communities = Community::lists('propNum', 'id');
	}

	public function getManagers($propNum)
	{
		$manager_update = new ImportErrors();
		session_write_close();
		$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=community&type=managers&cmd=list&propnum='.$propNum);
	    $this->setCurlOpts();
	    $this->json_response = curl_exec($this->curl);
	    curl_close($this->curl);
		$rmap_managers = json_decode($this->json_response);
		if (!is_object($rmap_managers))
		{
			$manager_update->failed = true;
			$manager_update->http_code = 500;
			$manager_update->record_table = 'residentmap_connection';
			$manager_update->record_field = 'getmanagers';
			$manager_update->record_id = 0;
			$manager_update->error_message = 'incorrectly formatted response';
			$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($manager_update));
			$manager_update->resetImportError();
			return 406;
		}

		if ($rmap_managers->statuscode == 200)
		{
			$managers = $this->toArray($rmap_managers);
			$this->reset();
			return $managers['response'];
		}
		else
		{
			$this->reset();
			$manager_update->failed = true;
			$manager_update->http_code = 500;
			$manager_update->record_table = 'residentmap_connection';
			$manager_update->record_field = 'getmanagers';
			$manager_update->record_id = 0;
			$manager_update->error_message = $rmap_managers->message;
			$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($manager_update));
			$manager_update->resetImportError();
			return $rmap_managers->message;
		}
	}

	// public function checkAccountManagers($id)
	// {
	// 	$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=user&type=accountmanager&cmd=listproperties&id='.$id);
	//     $this->setCurlOpts();
	//     $this->json_response = curl_exec($this->curl);
	//     curl_close($this->curl);
	// }

	// public function checkPropertyManagers($id)
	// {
	// 	$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=user&type=residentmanager&cmd=listproperties&id='.$id);
	//     $this->setCurlOpts();
	//     $this->json_response = curl_exec($this->curl);
	//     curl_close($this->curl);
	// }

	public function getUpdateCommunityResidents()
	{
		// call community model and iterate through the table
		$communities = Community::lists('propNum', 'id');
		$errors = array();
		foreach ($communities as $id => $propNum) {
			if ($propNum == 130 || $propNum == 115 || $propNum == 162)
			{
				continue;
			}
			$residents = $this->getResidents($propNum);
		}
		return;
	}

	public function getResidents($propNum)
	{
		$resident_update = new ImportErrors();
		session_write_close();
		$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=user&type=resident&cmd=list&propnum='.$propNum);
		$this->setCurlOpts();
		$this->json_response = curl_exec($this->curl);
		curl_close($this->curl);
		$rmap_residents = json_decode($this->json_response);

		$community = Community::where('propNum', $propNum)->first();

		$comm_addr = '';
		$same = $community->address()->sameAddress()->first();
		if (empty($same))
		{
			$comm_addr = $community->address()->primaryBillingAddress()->first();
		}
		else
		{
			$comm_addr = $same;
		}

		$residents = array();
		$json_report = array();
		$resident_roles = array();
		if ($rmap_residents->statuscode == 200)
		{
			$resident_users = $this->toArray($rmap_residents);
			foreach ($resident_users['response'] as $position => $resident_array) {
				$temp = array();
				// print_r($resident_array);
				// echo '<br />';
				// echo '<br />';
				foreach ($resident_array as $field => $record) {
					// break;
					if ($field == 'billingAddress')
					{
						$address = array();
						foreach ($record as $id => $value) {
							if ($id == 'address1')
							{
								// if (strpos($value, ',') != false)
								// {
								// 	$resident_update->failed = true;
								// 	$resident_update->http_code = 500;
								// 	$resident_update->record_table = 'address';
								// 	$resident_update->record_field = 'address_1';
								// 	$resident_update->record_id = $resident_array['id'];
								// 	$resident_update->error_message = 'Address_1 contained illegal character(s): \',\'';
								// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
								// 	$resident_update->resetImportError();
								// }
								
								// if (strpos($value, '--') != false)
								// {
								// 	$resident_update->failed = true;
								// 	$resident_update->http_code = 500;
								// 	$resident_update->record_table = 'address';
								// 	$resident_update->record_field = 'address_1';
								// 	$resident_update->record_id = $resident_array['id'];
								// 	$resident_update->error_message = 'Address_1 contained illegal character(s): \'--\'';
								// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
								// 	$resident_update->resetImportError();
								// }

								// if (strpos(strtolower($value), 'office') != false)
								// {
								// 	$resident_update->failed = true;
								// 	$resident_update->http_code = 200;
								// 	$resident_update->record_table = 'address';
								// 	$resident_update->record_field = 'address_1';
								// 	$resident_update->record_id = $resident_array['id'];
								// 	$resident_update->error_message = 'Warning: address_1 contains the word: \'office\'';
								// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
								// 	$resident_update->resetImportError();
								// }
								$addr1 = $this->properAddress($value);
								$pound = explode('#', $addr1);
								if (strcmp(trim($pound[0]), $comm_addr->address_1) !== 0)
								{
									echo 'Invalid address';
									echo '<br />';
									print_r($addr1);
								}
								else
								{
									echo 'Valid address';
									echo '<br />';
									print_r($addr1);
								}
								echo '<br />';
								echo '<br />';
								$address['address_1'] = $value;
							}
							else if ($id == 'address2')
							{
								$address['address_2'] = $value;
							}
							else if ($id == 'zipcode')
							{
								$address['postal_code'] = $value;
							}
							else if ($id == 'country')
							{
								continue;
							}
							else
							{
								$address[$id] = $value;
							}
						}
						if (!empty($address))
						{
							$address['type_id'] = 1;
							$country = AddressCountry::where('name', 'like', 'United States')->first();
							$address['country_id'] = $country->id;
						}
						$temp['billing'] = $address;
					}
					else if ($field == 'mailingAddress')
					{
						$address = array();
						if (!empty($record))
						{
							foreach ($record as $id => $value) {
								if ($id == 'address1')
								{
									// if (strpos($value, ',') != false)
									// {
									// 	$resident_update->failed = true;
									// 	$resident_update->http_code = 500;
									// 	$resident_update->record_table = 'address';
									// 	$resident_update->record_field = 'address_1';
									// 	$resident_update->record_id = $resident_array['id'];
									// 	$resident_update->error_message = 'Address_1 contained illegal character(s): \',\'';
									// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
									// 	$resident_update->resetImportError();
									// }
									
									// if (strpos($value, '--') != false)
									// {
									// 	$resident_update->failed = true;
									// 	$resident_update->http_code = 500;
									// 	$resident_update->record_table = 'address';
									// 	$resident_update->record_field = 'address_1';
									// 	$resident_update->record_id = $resident_array['id'];
									// 	$resident_update->error_message = 'Address_1 contained illegal character(s): \'--\'';
									// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
									// 	$resident_update->resetImportError();
									// }

									// if (strpos(strtolower($value), 'office') != false)
									// {
									// 	$resident_update->failed = true;
									// 	$resident_update->http_code = 200;
									// 	$resident_update->record_table = 'address';
									// 	$resident_update->record_field = 'address_1';
									// 	$resident_update->record_id = $resident_array['id'];
									// 	$resident_update->error_message = 'Warning: address_1 contains the word: \'office\'';
									// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
									// 	$resident_update->resetImportError();
									// }
									$addr1 = $this->properAddress($value);
									$pound = explode('#', $addr1);
									if (strcmp(trim($pound[0]), $comm_addr->address_1) !== 0)
									{
										echo 'Invalid address';
										echo '<br />';
										print_r($addr1);
									}
									else
									{
										echo 'Valid address';
										echo '<br />';
										print_r($addr1);
									}
									echo '<br />';
									echo '<br />';
									$address['address_1'] = $value;
								}
								else if ($id == 'address2')
								{
									$address['address_2'] = $value;
								}
								else if ($id == 'zipcode')
								{
									$address['postal_code'] = $value;
								}
								else if ($id == 'country')
								{
									continue;
								}
								else
								{
									$address[$id] = $value;
								}
							}
						}
						else
						{
							continue;
						}
						if (!empty($address))
						{
							$address['type_id'] = 1;
							$country = AddressCountry::where('name', 'like', 'United States')->first();
							$address['country_id'] = $country->id;
						}
						$temp['shipping'] = $address;
					}
					else if ($field == 'firstName')
					{
						$name = str_replace(' ', '_', trim($record));
						// if (strpos($record, '&') != false)
						// {
						// 	$resident_update->failed = true;
						// 	$resident_update->http_code = 500;
						// 	$resident_update->record_table = 'user_profile';
						// 	$resident_update->record_field = 'first_name';
						// 	$resident_update->record_id = $resident_array['id'];
						// 	$resident_update->error_message = 'User\'s name contained illegal character \'&amp;\'';
						// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
						// 	$resident_update->resetImportError();
						// }
						// if (strpos($name, '_') != false)
						// {
						// 	$resident_update->failed = true;
						// 	$resident_update->http_code = 500;
						// 	$resident_update->record_table = 'user_profile';
						// 	$resident_update->record_field = 'first_name';
						// 	$resident_update->record_id = $resident_array['id'];
						// 	$resident_update->error_message = 'User\'s name contained illegal character \'_\' (spaces are converted to underscores)';
						// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
						// 	$resident_update->resetImportError();
						// }
						print_r($name);
						echo '<br />';
						echo '<br />';
						$temp['user_profile']['first_name'] = $record;
					}
					else if ($field == 'sameAsBilling')
					{
						$temp['billing_same_shipping'] = $record;
					}
					else if ($field == 'lastName')
					{
						// if (empty($record))
						// {
						// 	$resident_update->failed = true;
						// 	$resident_update->http_code = 500;
						// 	$resident_update->record_table = 'user_profile';
						// 	$resident_update->record_field = 'last_name';
						// 	$resident_update->record_id = $resident_array['id'];
						// 	$resident_update->error_message = 'No last name field found';
						// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
						// 	$resident_update->resetImportError();
						// }
						$temp['user_profile']['last_name'] = $record;
					}
					// else if ($field == 'userLevel')
					// {
					// 	switch ($record) {
					// 		case '1':
					// 			$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '1');
					// 			break;

					// 		case '2':
					// 			$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '51');
					// 			break;

					// 		case '3':
					// 			$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '61');
					// 			break;

					// 		case '7':
					// 			$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '18');
					// 			break;

					// 		case '8':
					// 			$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '12');
					// 			break;
							
					// 		default:
					// 			$user_roles[] = array('rmap_id' => $user_array['id'], 'role_id' => '63');
					// 			break;
					// 	}
					// }
					else if ($field == 'id')
					{
						$temp['rmap_id'] = $record;
					}
					else if ($field == 'contact')
					{
						if (empty($record['email']))
						{
							echo 'no email';
							echo '<br />';
							echo '<br />';
							// $resident_update->failed = true;
							// $resident_update->http_code = 500;
							// $resident_update->record_table = 'users';
							// $resident_update->record_field = 'email';
							// $resident_update->record_id = $resident_array['id'];
							// $resident_update->error_message = 'No email field found';
							// $json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
							// $resident_update->resetImportError();
							// $temp['email'] = '';
						}
						else
						{
							// if (strpos($record['email'], '@') != false)
							// {
								$temp['email'] = $record['email'];
							// }
							// else
							// {
							// 	$temp['email'] = '';
							// 	$resident_update->failed = true;
							// 	$resident_update->http_code = 500;
							// 	$resident_update->record_table = 'users';
							// 	$resident_update->record_field = 'email';
							// 	$resident_update->record_id = $resident_array['id'];
							// 	$resident_update->error_message = 'Invalid email: '.$record['email'];
							// 	$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
							// 	$resident_update->resetImportError();
							// }
						}
						$phone = $this->formatPhone($record['phone']);
						$temp['user_profile']['phone'] = $phone;
						$temp['user_profile']['gender'] = 'Male';
					}
					else
					{
						$temp[$field] = $record;
					}
				}
				$temp['import_user_create'] = true;
				$residents[] = $temp;
			}
			foreach ($residents as $key => $resident) {
				print_r($resident);
				echo '<br />';
				echo '<br />';
			}
			exit;
			foreach ($residents as $key => $resident) {
				if (empty($resident['email']))
				{
					$resident_update->failed = true;
					$resident_update->http_code = 500;
					$resident_update->record_table = 'users';
					$resident_update->record_field = 'email';
					$resident_update->record_id = $resident['rmap_id'];
					$resident_update->error_message = 'Unable to create resident, email is required field';
					$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
					$resident_update->resetImportError();
					continue;
				}
				else
				{
					$email_check = User::where('email', 'like', $resident['email'])->first();
					if (!empty($email_check))
					{
						$resident_update->failed = true;
						$resident_update->http_code = 500;
						$resident_update->record_table = 'users';
						$resident_update->record_field = 'email';
						$resident_update->record_id = $resident['rmap_id'];
						$resident_update->error_message = 'This resident has a duplicate email with another resident, this is not allowed.';
						$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
						$resident_update->resetImportError();
						continue;
					}
					else
					{
						// echo 'Creating User';
						// echo '<br />';
						// echo '<br />';
						$results = HMVC::post('api/v1/internal/user', $resident);
						$rmap = DB::table('residentmap_users_pvt_kingsleyliving_users')->where('user_id', $results->id)->first();
						foreach ($user_roles as $key => $rmap_vals) {
							if ($rmap_vals['rmap_id'] == $rmap->rmap_id)
							{
								$role = UserRole::where('id', $rmap_vals['role_id'])->first();

								$results->attachRole( $role );
							}
						}
					}
				}
			}
			return;
		}
		else
		{
			$this->reset();
			$resident_update->failed = true;
			$resident_update->http_code = 500;
			$resident_update->record_table = 'residentmap_connection';
			$resident_update->record_field = 'getmanagers';
			$resident_update->record_id = 0;
			$resident_update->error_message = $rmap_residents->message;
			$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($resident_update));
			$resident_update->resetImportError();
			return $rmap_residents->message;
		}
	}

	public function getUpdateCommunitySpaces()
	{
		// call community model and iterate through the table
		$communities = Community::lists('propNum', 'id');
		$errors = array();
		foreach ($communities as $id => $propNum) {
			$spaces = $this->getSpaces($propNum);
		}
		return;
	}

	public function getSpaces($propNum, $spacenum=null)
	{
		if (!empty($spacenum))
		{
			$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=space&cmd=list&propnum='.$propNum.'&spacenum='.$spacenum);
		    $this->setCurlOpts();
		    $this->json_response = curl_exec($this->curl);
		    curl_close($this->curl);
		}
		else
		{
			$this->curl = curl_init('https://kingsley.residentmap.com/kingsleylivingapi.php?module=space&cmd=list&propnum='.$propNum);
		    $this->setCurlOpts();
		    $this->json_response = curl_exec($this->curl);
		    curl_close($this->curl);

		    $community = Community::where('propNum', $propNum)->first();

			$comm_addr = '';
			$same = $community->address()->sameAddress()->first();
			if (empty($same))
			{
				$comm_addr = $community->address()->primaryBillingAddress()->first();
			}
			else
			{
				$comm_addr = $same;
			}

			$space_update = new ImportErrors();
			$rmap_spaces = json_decode($this->json_response);
			$spaces = array();

			if ($rmap_spaces->statuscode == 200)
			{
				$space_array = $this->toArray($rmap_spaces);
				foreach ($space_array['response'] as $position => $record) {
					$temp = array();
					foreach ($record as $prop => $val) {
						$temp['community_id'] = $community->id;
						if ($prop == 'spaceNum')
						{
							$temp['space_num'] = $val;
						}
						else if ($prop == 'vin')
						{
							$temp['VIN'] = $val;
						}
						else if ($prop == 'spaceStatus')
						{
							$space_status = null;
							switch ($val) {
								case 1:
								case 2:
								case 4:
								case 16:
								case 18:
								case 25:
								case 29:
								case 31:
								case 32:
								case 35:
								case 37:
								case 38:
								case 39:
								case 41:
								case 42:
								case 43:
								case 46:
								case 47:
								case 48:
								case 50:
								case 53:
								case 54:
								case 55:
								case 56:
								case 63:
								case 64:
								case 67:
									$space_status = SpaceStatus::where('status', 'like', 'occupied')->first();
									break;
								case 3:
								case 5:
								case 24:
									$space_status = SpaceStatus::where('status', 'like', 'occupied_nobill')->first();
									break;
								case 8:
								case 9:
								case 28:
									$space_status = SpaceStatus::where('status', 'like', 'vacant_available')->first();
									break;
								case 17:
								case 40:
								case 66:
								case 59:
								case 49:
								case 51:
								case 12:
								case 36:
								case 44:
								case 45:
								case 52:
								case 57:
								case 58:
								case 60:
								case 61:
								case 62:
									$space_status = SpaceStatus::where('status', 'like', 'vacant_rent')->first();
									break;
								case 14:
								case 65:
								case 11:
								case 10:
								case 7:
								case 13:
								case 26:
								case 27:
								case 15:
									$space_status = SpaceStatus::where('status', 'like', 'vacant_unavailable')->first();
									break;
								case 6:
									$space_status = SpaceStatus::where('status', 'like', 'vacant_rent')->first();
									break;
							}
							$temp['space_status_id'] = $space_status->id;
						}
						else if ($prop == 'appliances')
						{
							if (!empty($val))
							{
								foreach ($val as $appliance => $number) {
									$app = SpaceApplianceType::where('type', 'like', $appliance)->first();
									$temp['space_appliance'][] = array('appliance_type_id' => $app->id, 'appliance_num' => $number);
								}
							}
						}
						else if ($prop == 'sqft')
						{
							$temp['space_detail']['square_feet'] = str_replace(',', '', $val);
						}
						else if ($prop == 'closedDate')
						{
							$temp['space_pricing']['closed_date'] = $val;
						}
						else if ($prop == 'budgetedRepairs')
						{
							$temp['space_pricing']['budgeted_repairs'] = $val;
						}
						else if ($prop == 'cashPrice')
						{
							$temp['cash_sale_price'] = $val;
						}
						else if ($prop == 'deposit')
						{
							$temp['space_pricing'][$prop] = $val;
						}
						else if ($prop == 'interest')
						{
							$temp['space_pricing']['interest_rate'] = $val;
						}
						else if ($prop == 'lease')
						{
							$temp['space_pricing']['lease_rate'] = $val;
						}
						else if ($prop == 'lineOfCredit')
						{
							$temp['space_pricing']['LOC_rate'] = $val;
						}
						else if ($prop == 'rent')
						{
							$temp['space_pricing']['rent_price'] = $val;
						}
						else if ($prop == 'salePrice')
						{
							continue;
						}
						else if ($prop == 'taxSavings')
						{
							$temp['space_pricing']['yearly_tax_savings'] = $val;
						}
						else if ($prop == 'salesPrice')
						{
							$temp['space_pricing']['sale_price'] = $val;
						}
						else if ($prop == 'ssid')
						{
							continue;
						}
						else if ($prop == 'sellable')
						{
							continue;
						}
						else if ($prop == 'rentable')
						{
							continue;
						}
						else if ($prop == 'images')
						{
							continue;
						}
						else if ($prop == 'resident_id')
						{
							continue;
						}
						else
						{
							$temp['space_detail'][$prop] = $val;
						}
					}
					$spaces[] = $temp;
				}
				foreach ($spaces as $key => $value) {
					print_r($value);
					echo '<br />';
					echo '<br />';
				}
				exit;
			}
			else
			{
				$this->reset();
				$space_update->failed = true;
				$space_update->http_code = 500;
				$space_update->record_table = 'residentmap_connection';
				$space_update->record_field = 'getmanagers';
				$space_update->record_id = 0;
				$space_update->error_message = $rmap_spaces->message;
				$json_report[] = HMVC::post('api/v1/internal/importerror', $this->toArray($space_update));
				$space_update->resetImportError();
				return $rmap_spaces->message;
			}
		}
	}

}