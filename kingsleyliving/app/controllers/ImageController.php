<?php

class ImageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function retrieveImgUrl()
	{
		$loc = Request::get('location');
		$loc_split = explode('/', $loc);
		$doContinue = false;
		$count = count($loc_split) - 1;
		$loc_reverse = array();
		while ($doContinue == false)
		{
			if ($loc_split[$count] == 'images')
			{
				$loc_reverse[] = $loc_split[$count];
				$doContinue = true;
			}
			else
			{
				$loc_reverse[] = $loc_split[$count];
			}
			$count--;
		}
		$location = implode('/', array_reverse($loc_reverse));

		return Response::json(array(
			'error' => false,
			'message' => 'Returning image url',
			'location' => $location,
			),
			200
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$width = 0;
		$height = 0;
		$image = new Image;
		$image->display_name = Request::get('display_name');
		$image->description = Request::get('description');
		if (Request::has('file'))
		{
			$file = Request::get('file');
		}
		else
		{
			$file = Input::file('image');
		}
		$image->file_name = Str::random(20) . '.' . $file->getClientOriginalExtension();
		$image->location = public_path() . Request::get('location');
		if (!File::exists($image->location))
		{
			File::makeDirectory($image->location, $mode=0777, true, true);
		}
		$file->move($image->location, $image->file_name);
		list($width, $height) = getimagesize($image->location . $image->file_name);
		$image->width = $width;
		$image->height = $height;

		$image->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Image created!',
			'image_id' => $image->id,
			'image_name' => $image->display_name
			),
			200
		);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$image = Image::where('id', $id)
					->take(1)
					->first();

		return Response::json(array(
			'error' => false,
			'image' => $image->toArray()
			),
			200
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$image = Image::find($id);

		if ( Request::get('display_name') )
		{
			$image->display_name = Request::get('display_name');
		}

		if ( Request::get('description') )
		{
			$image->description = Request::get('description');
		}

		if ( Request::get('file') || Request::get('image') )
		{
			if (Request::has('file'))
			{
				$file = Request::get('file');
			}
			else
			{
				$file = Input::file('image');
			}
			$image->file_name = Str::random(20) . '.' . $file->getClientOriginalExtension();
			list($width, $height) = getimagesize($image->location . $image->file_name);
			$image->width = $width;
			$image->height = $height;
		}

		if ( Request::get('location') )
		{
			$image->location = public_path() . Request::get('location');
			$file->move($image->location, $image->file_name);
		}

		$image->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Image updated!',
			'image_id' => $image->id,
			'image_name' => $image->display_name
			),
			200
		);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$image = Image::find($id);

		$image->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'Image deleted'
			),
			200
		);
	}


}
