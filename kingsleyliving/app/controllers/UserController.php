<?php

class UserController extends \BaseController {

	public function __construct()
	{
		$this->stylesheets = array();
		$this->stylesheets[] = HTML::style('css/user/user.css');

		$this->javascript = array();
		$this->javascript[] = HTML::script('js/user/user.js');

		if (Auth::check()) {
			$user_profile = UserProfile::where('user_id', Auth::user()->id)->first();
			if (!empty($user_profile))
			{
				$this->user_name = $user_profile->first_name.' '.$user_profile->last_name;
			}
			else
			{
				$this->user_name = Auth::user()->username;
			}
		} else {
			$this->user_name = '';
		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$master_data = array(
			'page_title' => 'Sign Up',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => '',
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$redirectto;
		if (Input::has('redirectto'))
		{
			$redirectto = Input::get('redirectto');
		}
		else
		{
			$redirectto = '/';
		}

		$title = 'Register New User';

		$url_request = 'user/create';

		return View::make('user.create', $master_data, array('title' => $title, 'url_request' => $url_request))->with('redirectto', $redirectto);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Request::has('application_user_create'))
		{
			$repo = App::make('UserRepository');
			$user = $repo->signup(Request::get('user'));

			if ($user->id) {
				if (Config::get('confide::signup_email')) {
					Mail::queueOn(
						Config::get('confide::email_queue'),
						Config::get('confide::email_account_confirmation'),
						compact('user'),
						function ($message) use ($user) {
							$message
								->to($user->email, $user->username)
								->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
						}
					);
				}
			}

			$billing_address = Request::get('billing');
			$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
			$user->address()->attach($billing_address_id['address_id']);

			$user_profile = Request::get('user_profile');
			$mrstat = UserMaritalStatus::where('id', 1)->first();

			$profile = new UserProfile;
			$profile->user()->associate($user);
			$profile->account_id = 0;
			$profile->first_name = $user_profile['first_name'];
			$profile->middle_name = $user_profile['middle_name'];
			$profile->last_name = $user_profile['last_name'];
			$profile->suffix = $user_profile['suffix'];
			$profile->phone = $user_profile['phone'];
			$profile->image_id = 0;
			$profile->user_marital_status()->associate($mrstat);
			$profile->gender = $user_profile['gender'];
			$profile->save();

			return $user;
		}
		else if (Request::has('import_user_create'))
		{
			$repo = App::make('UserRepository');

			$user_array = array();
			$user_array['username'] = Request::get('username');
			$user_array['email'] = Request::get('email');
			$user_array['password'] = 'password123';
			$user_array['password_confirmation'] = 'password123';

			$user = $repo->signup($user_array);

			$rmap_id;
			if (Request::has('rmap_id'))
			{
				$rmap_id = Request::get('rmap_id');
				DB::insert('insert into residentmap_users_pvt_kingsleyliving_users (user_id, rmap_id) values (?, ?)', array($user->id, $rmap_id));
			}

			if (!Request::get('billing_same_shipping'))
			{
				$billing_address = Request::get('billing');
				$shipping_address = Request::get('shipping');
				$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
				$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
				if ($billing_address_id['error'] == false)
				{
					$user->address()->attach($billing_address_id['address_id']);
				}
				
				if ($shipping_address_id['error'] == false)
				{
					$user->address()->attach($shipping_address_id['address_id']);
				}
			}
			else
			{
				$billing_address = Request::get('billing');
				$billing_address['type'] = 'both';
				$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
				if ($billing_address_id['error'] == false)
				{
					$user->address()->attach($billing_address_id['address_id']);
				}
			}

			$user_profile = Request::get('user_profile');
			$mrstat = UserMaritalStatus::where('id', 1)->first();

			$profile = new UserProfile;
			$profile->user()->associate($user);
			$profile->account_id = 0;
			$profile->first_name = $user_profile['first_name'];
			if (isset($user_profile['middle_name']))
			{
				$profile->middle_name = $user_profile['middle_name'];
			}
			$profile->last_name = $user_profile['last_name'];
			if (isset($user_profile['suffix']))
			{
				$profile->suffix = $user_profile['suffix'];
			}
			$profile->phone = $user_profile['phone'];
			$profile->image_id = 0;
			$profile->user_marital_status()->associate($mrstat);
			$profile->gender = $user_profile['gender'];
			$profile->save();

			return $user;
		}
		else
		{
			$user = User::where('email', 'like', Request::get('email'))->first();

			$rmap_id;
			if (Request::has('rmap_id'))
			{
				$rmap_id = Request::get('rmap_id');
				DB::insert('insert into residentmap_users_pvt_kingsleyliving_users (user_id, rmap_id) values (?, ?)', array($user->id, $rmap_id));
			}

			// Create profile image
			$image;
			if (Request::has('profile_image'))
			{
				$profile_image = Request::get('profile_image');
				$location = '/images/user/';
				$file = Input::file('image');
				$prof_image = HMVC::post('api/v1/internal/image', array(
					'display_name' => $profile_image['display_name'],
					'description' => $profile_image['description'],
					'location' => $location,
					'file' => $file
					)
				);
				$image = Image::find($prof_image['image_id']);
			}

			$user_profile = Request::get('user_profile');
			// Get marital status value for association
			$mrstat = UserMaritalStatus::where('id', $user_profile['marital_status_id'])->first();

			$profile = new UserProfile;
			$profile->user()->associate($user);
			$profile->account_id = 0;
			$profile->first_name = $user_profile['first_name'];
			$profile->last_name = $user_profile['last_name'];
			$profile->phone = $user_profile['phone'];
			if (!empty($image))
			{
				$profile->image()->associate($image);
			}
			else
			{
				$profile->image = 0;
			}
			$profile->user_marital_status()->associate($mrstat);
			$profile->gender = $user_profile['gender'];
			$profile->save();

			// Address information
			if (!Request::get('billing_same_shipping'))
			{
				$billing_address = Request::get('billing');
				$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
				$user->address()->attach($billing_address_id['address_id']);

				$shipping_address = Request::get('shipping');
				$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
				$user->address()->attach($shipping_address_id['address_id']);
			}
			else
			{
				$billing_address = Request::get('billing');
				$billing_address['type'] = 'both';
				$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
				$user->address()->attach($billing_address_id['address_id']);
			}
		}

		if (Request::has('url_request'))
		{
			return Response::json(array(
				'error' => false,
				'notice' => 'User created!',
				'user_id' => $user->id
				),
				200
			);
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'notice' => 'User created!',
					'user_id' => $user->id
					),
					200
				);
			}
			else
			{
				return Redirect::to('/')->with('success', 'User created!');
			}
		}
	}

	// public function login()
	// {
	// 	$master_data = array(
	// 		'page_title' => 'Sign Up',
	// 		'page_stylesheets' => $this->stylesheets,
	// 		'page_scriptlinks' => $this->javascript,
	// 		'user_name' => '',
	// 		'news' => array(),
	// 		'slide_right' => '',
	// 		'slide_left' => '',
	// 		'notifications' => ''
	// 	);

	// 	$title = 'Register New User';

	// 	$url_request = 'user/create';

	// 	return View::make('user.create', $master_data, array('title' => $title, 'url_request' => $url_request));
	// }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// echo 'Posted to show method';
		if (is_numeric($id))
		{
			$user = User::with('user_profile.image', 'address')->where('id', $id)->first();
		}
		else
		{
			if (filter_var($id, FILTER_VALIDATE_EMAIL) == false)
			{
				$user = User::with('user_profile.image', 'address')->where('username', 'like', $id)->first();
			}
			else
			{
				$user = User::with('user_profile.image', 'address')->where('email', 'like', $id)->first();
			}
		}
		if($user)
		{
			return Response::json(array(
				'error' => false,
				'user' => json_encode($user->toArray())
				),
				200
			);
		}
		else
		{
			return Response::json(array(
				'error' => 'User not found!',
				),
				200
			);
		}
	}

	public function view($id, $page)
	{
		if (!Confide::user())
		{
			return Redirect::to('/')->with('error', 'No user logged in!');
		}
		$confide_user = Confide::user();

		$user_record = User::with('user_profile.image')->where('id', $confide_user->id)->first();
		$user = $user_record->toArray();
		if ($page == 'profile')
		{
			$master_data = array(
				'page_title' => 'User Profile',
				'page_stylesheets' => $this->stylesheets,
				'page_scriptlinks' => $this->javascript,
				'user_name' => $this->user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);

			$same = $user_record->address()->sameAddress()->first();
			if (empty($same))
			{
				$billing = $user_record->address()->primaryBillingAddress()->first();
				$shipping = $user_record->address()->primaryShippingAddress()->first();
				if (!empty($billing))
				{
					$user['billing'] = $billing->toArray();
				}
				else
				{
					$user['billing'] = '';
				}
				if (!empty($shipping))
				{
					$user['shipping'] = $shipping->toArray();
				}
				else
				{
					$user['shipping'] = '';
				}
			}
			else
			{
				$user['billing'] = $same->toArray();
				$user['shipping'] = '';
			}

			$title = $user_record->user_profile->first_name.' '.$user_record->user_profile->last_name.'&#39;s Profile';

			if (!empty($user_record->user_profile->image->location))
			{
				$location = HMVC::post('api/v1/internal/image/retrieveImgUrl', array('location' => $user_record->user_profile->image->location));
			}
			else
			{
				$location = '';
			}
			$user_mrstat = UserMaritalStatus::where('id', $user_record->user_profile->marital_status_id)->first();
			$user['user_profile']['marital_status'] = $user_mrstat->toArray();

			if (strtotime($user['updated_at']) > strtotime($user['user_profile']['updated_at']))
			{
				$user['modified'] = $user['updated_at'];
			}
			else
			{
				$user['modified'] = $user['user_profile']['updated_at'];
			}

			$application_resident = $user_record->application_resident()->get();
			$user_applications = array();
			if (!empty($application_resident->toArray()))
			{
				foreach ($application_resident->toArray() as $key => $app_res_record) {
					$application = Application::with('community', 'application_space')->where('id', $app_res_record['application_id'])->first();
					$user_applications[] = $application->toArray();
				}
			}

			$contact_attempts = Contact::where('email', $user_record->email)->get();
			$user_contacts = array();
			if (!empty($contact_attempts->toArray()))
			{
				foreach ($contact_attempts->toArray() as $key => $contact) {
					$user_contacts[] = $contact;
				}
			}

			$community = $user_record->user_community()->get();
			$user_community = array();
			if (!empty($community->toArray()))
			{
				foreach ($community->toArray() as $key => $comm) {
					$user_community[] = $comm;
				}
			}
			// print_r($user_applications);
			// echo '<br />';
			// echo '<br />';
			// print_r($user_contacts);
			// echo '<br />';
			// echo '<br />';
			// print_r($user_contacts);
			// exit;

			if (!empty($user_record->user_profile->image->location))
			{
				return View::make('user.profile', $master_data, array('title' => $title))->with('user', $user)->with('location', $location['location'])->with('applications', $user_applications)->with('contacts', $user_contacts)->with('community', $user_community);
			}
			else
			{
				return View::make('user.profile', $master_data, array('title' => $title))->with('user', $user)->with('location', $location)->with('applications', $user_applications)->with('contacts', $user_contacts)->with('community', $user_community);
			}
		}
		if ($page == 'account')
		{
			// This is for payments etc...
			$master_data = array(
				'page_title' => 'User Account',
				'page_stylesheets' => $this->stylesheets,
				'page_scriptlinks' => $this->javascript,
				'user_name' => $this->user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);

			$title = 'Account for '.$user_record->user_profile->first_name.' '.$user_record->user_profile->last_name;

			return View::make('user.account', $master_data, array('title' => $title))->with('user', $confide_user);
		}
	}

	public function paybill($id)
	{
		if (Auth::guest())
		{
			return Redirect::to('/')->with('error', 'No editing unless logged in!');
		}
		$confide_user = Confide::user();

		$this->stylesheets[] = HTML::style('css/media/css/jquery.dataTables.css');
		$this->stylesheets[] = HTML::style('css/media/plugins/integration/bootstrap/3/dataTables.bootstrap.css');
		$this->javascript[] = HTML::script('css/media/js/jquery.dataTables.js');
		$this->javascript[] = HTML::script('css/media/plugins/integration/bootstrap/3/dataTables.bootstrap.js');
		// This is for payments etc...
		$master_data = array(
			'page_title' => 'Pay Your Bill',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Payment Center';

		return View::make('user.paybill', $master_data, array('title' => $title))->with('user', $confide_user);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id, $page)
	{
		if (Auth::guest())
		{
			return Redirect::to('/')->with('error', 'No editing unless logged in!');
		}
		$master_data = array(
			'page_title' => 'Update User',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		if ($page == 'account')
		{
			$title = 'Edit User';

			$url_request = 'user/edit';

			$user_record = User::with('user_profile')->where('id', $id)->first();
			$user = $user_record->toArray();

			$same = $user_record->address()->sameAddress()->first();
			if (empty($same))
			{
				$billing = $user_record->address()->primaryBillingAddress()->first();
				$shipping = $user_record->address()->primaryShippingAddress()->first();
				$user['billing'] = $billing->toArray();
				$user['shipping'] = $shipping->toArray();
			}
			else
			{
				$user['billing'] = $same->toArray();
				$user['shipping'] = '';
				$user['billing_same_shipping'] = true;
			}

			$prof_image = Image::where('id', $user['user_profile']['image_id'])->first();

			$user['profile_image'] = $prof_image->toArray();

			$redirectto = '';

			return View::make('user.edit', $master_data, array('title' => $title, 'url_request' => $url_request))->with('user', $user)->with('redirectto', $redirectto);
		}
		if ($page == 'profile')
		{
			$title = 'Edit User';

			$url_request = 'user/edit';

			$user_record = User::with('user_profile')->where('id', $id)->first();
			$user = $user_record->toArray();

			$same = $user_record->address()->sameAddress()->first();
			if (empty($same))
			{
				$billing = $user_record->address()->primaryBillingAddress()->first();
				$shipping = $user_record->address()->primaryShippingAddress()->first();
				$user['billing'] = $billing->toArray();
				$user['shipping'] = $shipping->toArray();
			}
			else
			{
				$user['billing'] = $same->toArray();
				$user['shipping'] = '';
				$user['billing_same_shipping'] = true;
			}

			$prof_image = Image::where('id', $user['user_profile']['image_id'])->first();

			$user['profile_image'] = $prof_image->toArray();

			$redirectto = '';

			return View::make('user.edit', $master_data, array('title' => $title, 'url_request' => $url_request))->with('user', $user)->with('redirectto', $redirectto);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
