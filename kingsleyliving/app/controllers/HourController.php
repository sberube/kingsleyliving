<?php

class HourController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$hour = new Hour;
		if (!empty(Request::get('notes')))
		{
			$hour->notes = Request::get('notes');
		}
		else
		{
			$hour->notes = NULL;
		}
		$hour_days = HourDay::find(Request::get('days_id'));
		$hour_time = HourTime::find(Request::get('time_id'));
		$hour_type = HourType::find(Request::get('type_id'));

		$hour->hour_day()->associate($hour_days);
		$hour->hour_time()->associate($hour_time);
		$hour->hour_type()->associate($hour_type);

		$hour->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Hours created!',
			'hour_id' => $hour->id
			),
			200
		);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
