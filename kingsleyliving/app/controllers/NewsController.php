<?php

class NewsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	// Detect whether or not this function is called via ajax, the api, or by the site itself.
	public function index()
	{
		if (Auth::check()) {
			// code...
		} else {
			$user_name = '';
		}

		if (Request::is('api/v1/*') || Request::ajax())
		{
			$news = News::all();

			return Response::json(array(
				'error' => false,
				'news' => $news->toArray()),
				200
			);
		}
		else
		{
			$news = News::all();

			// print_r($news->toArray());
			// exit();

			$master_data = array(
				'page_title' => 'News Index',
				'page_stylesheets' => NULL,
				'page_scriptlinks' => NULL,
				'user_name' => $user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);
			return View::make('news.index', $master_data, array('news_articles' => $news->toArray()));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Auth::check()) {
			// code...
		} else {
			$user_name = '';
		}

		$master_data = array(
			'page_title' => 'News Index',
			'page_stylesheets' => NULL,
			'page_scriptlinks' => NULL,
			'user_name' => $user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		return View::make('news.create', $master_data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$news = new News;
		$news->title = Request::get('title');
		$news->content = Request::get('content');
		$news->user_id = Auth::user()->id;
		$news->slug = $this->create_slug($news->title);

		$news->save();

		return Response::json(array(
			'error' => false,
			'message' => 'News article created!',
			'news_title' => $news->title
			),
			200
		);
	}

	private function create_slug($title)
	{
		$slug = strtolower($title);

		$find = array(' ', '&', '+',',');
		$slug = str_replace ($find, '-', $slug);

		$find = array('/[^a-z0-9-<>]/', '/[-]+/', '/<[^>]*>/');
		$repl = array('', '-', '');
		$slug = preg_replace ($find, $repl, $slug);

		return $slug;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// Detect whether or not this function is called via ajax, the api, or by the site itself.
	public function show($id)
	{
		$news = News::where('id', $id)
					->take(1)
					->get();

		return Response::json(array(
			'error' => false,
			'news' => $news->toArray()
			),
			200
		);
	}

	public function view($identifier)
	{
		if (Auth::check()) {
			// code...
		} else {
			$user_name = '';
		}

		if (is_numeric($identifier))
		{
			$news = News::find($identifier);
			$news_dummy = array();
			$news_dummy[] = $news->toArray();

			$master_data = array(
				'page_title' => 'News Index',
				'page_stylesheets' => NULL,
				'page_scriptlinks' => NULL,
				'user_name' => $user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);
			return View::make('news.view', $master_data, array('news_article' => $news_dummy));
		}
		else
		{
			$news = News::where('slug', $identifier)
						->take(1)
						->get();

			$master_data = array(
				'page_title' => 'News Index',
				'page_stylesheets' => NULL,
				'page_scriptlinks' => NULL,
				'user_name' => $user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);
			return View::make('news.view', $master_data, array('news_article' => $news->toArray()));
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$news = News::find($id);
		if ( Request::get('title') )
		{
			$news->title = Request::get('title');
		}

		if ( Request::get('content') )
		{
			$news->content = Request::get('content');
		}

		$news->slug = $this->create_slug($news->title);

		$news->save();

		return Response::json(array(
			'error' => false,
			'message' => 'News article updated!'
			),
			200
		);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// Detect whether or not this function is called via ajax, the api, or by the site itself.
	public function destroy($id)
	{
		$news = News::find($id);

		$news->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'News article deleted'
			),
			200
		);
	}


}
