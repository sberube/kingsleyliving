<?php

class AddressGoogleController extends \BaseController {

	private function toArray($obj) {
		if (is_object($obj)) {
			$obj = get_object_vars($obj);
		}
 
		if (is_array($obj)) {
			return array_map([__CLASS__, __METHOD__], $obj);
		}
		else {
			return $obj;
		}
	}

	private function toObject($arry) {
		if (is_array($arry)) {
			return (object) array_map([__CLASS__, __METHOD__], $arry);
		}
		else {
			return $arry;
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// prep_address format: 4956+N+300+W+Ste+200,+Provo,+Utah
		$google_address = Request::get('prep_address');
		$prep_address = str_replace(' ', '+', $google_address);
		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prep_address.'&sensor=false');
		$output = json_decode($geocode);

		// print_r($this->toArray($output->results));
		// echo '<br />';
		// echo '<br />';

		if (empty($output->results))
		{
			return Response::json(array(
				'error' => true,
				'message' => 'Running too many addresses',
				),
				500
			);
		}
		$subpremise = $street_number = $route = $neighborhood = null;
		$locality = $administrative_area_level_2 = $administrative_area_level_1 = null;
		$country = $postal_code = null;

		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('subpremise', $geoaddress->types))
			{
				$subpremise = $geoaddress->long_name;
			}
		}

		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('street_number', $geoaddress->types))
			{
				$street_number = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('route', $geoaddress->types))
			{
				$route = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('neighborhood', $geoaddress->types))
			{
				$neighborhood = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('locality', $geoaddress->types))
			{
				$locality = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('administrative_area_level_2', $geoaddress->types))
			{
				$administrative_area_level_2 = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('administrative_area_level_1', $geoaddress->types))
			{
				$administrative_area_level_1 = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('country', $geoaddress->types))
			{
				$country = $geoaddress->long_name;
			}
		}
		
		foreach ($output->results[0]->address_components as $geoaddress) {
			if (in_array('postal_code', $geoaddress->types))
			{
				$postal_code = $geoaddress->long_name;
			}
		}

		$google_country = AddressCountry::where('name', 'like', $country)->first();

		$google = new AddressGoogle;
		$google->subpremise = $subpremise;
		$google->street_number = $street_number;
		$google->route = $route;
		$google->neighborhood = $neighborhood;
		$google->locality = $locality;
		$google->administrative_area_level_2 = $administrative_area_level_2;
		$google->administrative_area_level_1 = $administrative_area_level_1;
		$google->country_id = $google_country->id;
		$google->postal_code = $postal_code;
		$google->latitude = number_format($output->results[0]->geometry->location->lat, 5);
		$google->longitude = number_format($output->results[0]->geometry->location->lng, 5);

		$valid = AddressGoogle::validate($google->toArray());

		if ( $valid->fails() )
		{
			return Response::json(array(
				'error' => true,
				'message' => $valid->messages()->toJson(),
				),
				406
			);
		}

		$google->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Google address created!',
			'google_id' => $google->id
			),
			200
		);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$address_google = AddressGoogle::where('id', $id)
					->take(1)
					->get();

		return Response::json(array(
			'error' => false,
			'address_google' => $address_google->toArray()
			),
			200
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$address_google = AddressGoogle::find($id);
		$country;
		if ( Request::get('street_number') )
		{
			$address_google->street_number = Request::get('street_number');
		}

		if ( Request::get('route') )
		{
			$address_google->route = Request::get('route');
		}

		if ( Request::get('neighborhood') )
		{
			$address_google->neighborhood = Request::get('neighborhood');
		}

		if ( Request::get('locality') )
		{
			$address_google->locality = Request::get('locality');
		}

		if ( Request::get('administrative_area_level_2') )
		{
			$address_google->administrative_area_level_2 = Request::get('administrative_area_level_2');
		}

		if ( Request::get('administrative_area_level_1') )
		{
			$address_google->administrative_area_level_1 = Request::get('administrative_area_level_1');
		}

		if ( Request::get('country') )
		{
			$country = Request::get('country');
		}

		if ( Request::get('postal_code') )
		{
			$address_google->postal_code = Request::get('postal_code');
		}

		if (is_numeric($country))
		{
			$address_google->country = AddressCountry::find($country);
		}
		else
		{
			$address_google->country = AddressCountry::where('name', 'like', $country)->first();
		}

		if ( Request::get('latitude') )
		{
			$address_google->latitude = Request::get('latitude');
		}

		if ( Request::get('longitude') )
		{
			$address_google->longitude = Request::get('longitude');
		}


		$address_google->save();

		return Response::json(array(
			'error' => false,
			'message' => 'Google address updated!'
			),
			200
		);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$address_google = AddressGoogle::find($id);

		$address_google->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'Google address deleted'
			),
			200
		);
	}


}
