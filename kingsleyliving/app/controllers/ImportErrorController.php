<?php

class ImportErrorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$errors = ImportError::all();
		$json = json_encode($errors->toArray());
		return Response::json(array(
				'error' => false,
				'message' => $json
			),
			200
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$valid = ImportError::validate(Input::all());
		if ( $valid->fails() )
		{
			return Response::json(array(
				'error' => true,
				'message' => $valid->messages()->toJson(),
				),
				406
			);
		}
		
		$import_error = new ImportError;

		$import_error->record_table = Request::get('record_table');
		$import_error->record_field = Request::get('record_field');
		$import_error->record_id = Request::get('record_id');
		$import_error->error_message = Request::get('error_message');

		$import_error->save();

		$failed = Request::get('failed');
		$http_code = Request::get('http_code');

		return Response::json(array(
				'error' => $failed,
				'message' => $import_error->error_message
			),
			$http_code
		);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
