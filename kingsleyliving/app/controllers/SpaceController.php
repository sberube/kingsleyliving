<?php

class SpaceController extends \BaseController {

	public function __construct()
	{
		$this->stylesheets = array();
		$this->stylesheets[] = HTML::style('css/space/space.css');

		$this->javascript = array();
		$this->javascript[] = HTML::script('js/space/space.js');

		if (Auth::check()) {
			$user_profile = UserProfile::where('user_id', Auth::user()->id)->first();
			if (!empty($user_profile))
			{
				$this->user_name = $user_profile->first_name.' '.$user_profile->last_name;
			}
			else
			{
				$this->user_name = Auth::user()->username;
			}
		} else {
			$this->user_name = '';
		}

		$this->entrust_options = array('validate_all' => true, 'return_type' => 'both');

		$this->states = Community::prependBlankValue(Community::retrieveCommunityStates());
		$this->cities = Community::prependBlankValue(Community::retrieveCommunityCities());
		$this->radius = array('' => '', '5' => '5 miles', '10' => '10 miles', '15' => '15 miles', '20' => '20 miles', '50' => '50 miles');
		$this->price_min = Community::prependBlankValue(Space::retrievePriceMin());
		$this->price_max = Community::prependBlankValue(Space::retrievePriceMax());
		$this->beds = Community::prependBlankValue(Space::retrieveSpaceBeds());
		$this->baths = Community::prependBlankValue(Space::retrieveSpaceBaths());
		$this->community_type = array('' => '', '55_or_over' => '55+', 'all' => 'All Age');
		$this->amenities = Community::retrieveSearchAmenities();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param  int or string $community
	 *
	 * @return Response
	 */
	public function index($community)
	{
		$comm_id;
		if (is_numeric($community))
		{
			$comm = Community::find($community);
			$comm_id = $community;
		}
		else
		{
			$comm = Community::where('name', 'like', $community)->first();
			$comm_id = $comm->id;
		}

		if (Request::is('api/v1/*') || Request::ajax())
		{
			$spaces = Space::all();

			return Response::json(array(
				'error' => false,
				'news' => $spaces->toArray()),
				200
			);
		}
		else
		{
			// print_r(Input::all());
			// exit;
			if (!Confide::user() || Confide::user()->hasRole(array('current_resident', 'future_resident', 'past_resident')))
			{
				$spaces = Space::with('space_detail', 'space_pricing')
							->where('community_id', $comm_id)
							->where('space_status_id', '>=', 3)
							->get();
			}
			else
			{
				$spaces = Space::with('space_detail', 'space_pricing')->where('community_id', $comm_id)->get();
			}

			$master_data = array(
				'page_title' => 'Homes Index',
				'page_stylesheets' => $this->stylesheets,
				'page_scriptlinks' => $this->javascript,
				'user_name' => $this->user_name,
				'news' => array(),
				'slide_right' => '',
				'slide_left' => '',
				'notifications' => ''
			);

			$community_address;
			$same = $comm->address()->sameAddress()->first();
			if (empty($same))
			{
				// $addresses = $comm->address()->get();
				// print_r($comm->toArray());
				// echo '<br />';
				// echo '<br />';
				// print_r($addresses->toArray());
				// exit;
				$billing = $comm->address()->primaryBillingAddress()->first();
				$community_address['billing'] = $billing->toArray();
			}
			else
			{
				$community_address['billing'] = $same->toArray();
			}

			// Get a thumbnail pic of house here

			return View::make('space.index', $master_data, array('spaces' => $spaces->toArray(), 'community' => $comm))->with('community_address', $community_address);
		}
	}

	public function searchresults()
	{
		$qstr = Input::all();

		$this->stylesheets[] = HTML::style('css/media/css/jquery.dataTables.css');
		$this->stylesheets[] = HTML::style('css/media/plugins/integration/bootstrap/3/dataTables.bootstrap.css');
		$this->javascript[] = HTML::script('css/media/js/jquery.dataTables.js');
		$this->javascript[] = HTML::script('css/media/plugins/integration/bootstrap/3/dataTables.bootstrap.js');

		$master_data = array(
			'page_title' => 'Search Results',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Search Results';

		$community = Community::prependBlankValue(Community::lists('name', 'id'));

		$conditions = array();
		$tables = array();
		foreach ($qstr as $field => $record) {
			if (!empty($record))
			{
				switch ($field) {
					case 'listing_option':
						$conditions[$field] = $record;
						break;
					case 'state':
					case 'city':
						$tables[] = 'address';
						$conditions[$field] = $record;
						break;
					case 'zip':
					case 'radius':
						// This will be unavailable until a future update
						$tables[] = 'address';
						$conditions[$field] = $record;
						break;
					case 'price_min':
					case 'price_max':
						$tables[] = 'space_pricing';
						$conditions[$field] = $record;
						break;
					case 'beds':
					case 'baths':
						$tables[] = 'space_details';
						$conditions[$field] = $record;
						break;
					case 'clubhouse':
					case 'fitness_center':
					case 'swimming_pool':
					case 'playground':
					case 'community_type':
						$tables[] = 'community_pvt_amenity';
						$conditions[$field] = $record;
						break;
					case 'community':
						$tables[] = 'communities';
						$conditions[$field] = $record;
						break;
				}
			}
		}
		$results = Space::retrieveSpaces($tables, $conditions);
		$spaces = array();
		foreach ($results as $key => $space) {
			$space_community = Community::where('id', $space['community_id'])->first();
			$new_space = $space;
			$new_space['community'] = $space_community->toArray();
			$spaces[] = $new_space;
		}

		// print_r($spaces);
		// exit;
		
		return View::make('space.searchresults', $master_data, array('title' => $title))->with('spaces', $spaces)->with('states', $this->states)->with('cities', $this->cities)->with('search', $qstr)->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $community)->with('amenities', $this->amenities);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param  int or string $community
	 *
	 * @return Response
	 */
	public function create($community)
	{
		$this->stylesheets[] = HTML::style('js/jquery-ui-1.11.1.dark_hive/jquery-ui.css');

		// $this->javascript[] = HTML::script('js/dropzone.js');
		$comm;
		if (is_numeric($community))
		{
			$comm = Community::with('property_manager.user_profile')->where('id', $community)->first();
		}
		else
		{
			$comm = Community::with('property_manager.user_profile')->where('name', 'like', $community)->first();
		}

		if (!Confide::user())
		{
			return Redirect::to($comm->id.'/space')->with('error', 'Guests cannot access this page!');
		}
		$user = Confide::user();
		if (!$user->ability(array('rm_admin'), array('create_spaces')))
		{
			return Redirect::to($comm->id.'/space')->with('error', 'Only admin users can access the home create page!');
		}

		$master_data = array(
			'page_title' => 'Home Create',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Create New Home';

		$amenities = Amenity::where('class_id', '>=', 3)->orderBy('id', 'asc')->get();

		$url_request = 'space/create';
		
		return View::make('space.create', $master_data, array('title' => $title, 'amenities' => $amenities->toArray(), 'url_request' => $url_request))->with('community', $comm);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  int or string $community
	 *
	 * @return Response
	 */
	public function store()
	{
		print_r(Input::all());
		exit;
		// Community
		$comm = Request::get('community');
		$community;
		if (is_numeric($comm))
		{
			$community = Community::find($comm);
		}
		else
		{
			$community = Community::where('name', 'like', $comm)->first();
		}
		$space_status = SpaceStatus::find(Request::get('space_status_id'));

		// Space information
		$space = new Space;
		$space->community()->associate($community);
		$space->spaceid = 0;
		$space->space_num = Request::get('space_num');
		$space->space_status()->associate($space_status);
		$space->description = Request::get('description');
		$space->notes = Request::get('notes');
		$space->VIN = Request::get('VIN');

		$space->save();

		if (Request::has('billing'))
		{
			// Address information
			if (!Request::get('billing_same_shipping'))
			{
				$billing_address = Request::get('billing');
				$shipping_address = Request::get('shipping');
				$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
				$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
				$space->address()->attach($billing_address_id['address_id']);
				$space->address()->attach($shipping_address_id['address_id']);
			}
			else
			{
				$billing_address = Request::get('billing');
				$billing_address['type'] = 'both';
				$billing_address_id = HMVC::post('api/v1/internal/address', $billing_address);
				$space->address()->attach($billing_address_id['address_id']);
			}
		}

		if (Request::has('amenity'))
		{
			// Amenity information
			$amenities = Request::get('amenity');
			foreach ($amenities as $key => $amenity)
			{
				$space->space_amenity()->attach($amenity['id']);
			}
		}

		// Space Details information
		$space_details = new SpaceDetail;
		$details = Request::get('space_detail');
		$space_details->space()->associate($space);
		$space_details->manufacturer = $details['manufacturer'];
		$space_details->width = $details['width'];
		$space_details->length = $details['length'];
		$space_details->square_feet = str_replace(',', '', $details['square_feet']);
		$space_details->beds = $details['beds'];
		$space_details->baths = $details['baths'];
		$space_details->year = $details['year'];

		$space_details->save();

		// Space Flyer information
		// $space_flyer = new SpaceFlyer;
		// $space_flyer->space()->associate($space);
		// if (Request::has('flyer_link'))
		// {
		// 	$space_flyer->flyer_link = Request::get('flyer_link');
		// }
		// else
		// {
			// Generate flyer link?
		// }

		// $space_flyer->save();

		// Space Pricing information
		$space_pricing = new SpacePricing;
		$pricing = Request::get('space_pricing');
		$space_pricing->space()->associate($space);
		$space_pricing->rent_price = str_replace(',', '', $pricing['rent_price']);
		$space_pricing->sale_price = str_replace(',', '', $pricing['sale_price']);
		$space_pricing->cash_sale_price = str_replace(',', '', $pricing['cash_sale_price']);
		$space_pricing->lease_rate = $pricing['lease_rate'];
		$space_pricing->interest_rate = $pricing['interest_rate'];
		$space_pricing->yearly_tax_savings = str_replace(',', '', $pricing['yearly_tax_savings']);
		$space_pricing->LOC_rate = $pricing['LOC_rate'];
		$space_pricing->deposit = str_replace(',', '', $pricing['deposit']);
		$space_pricing->budgeted_repairs = str_replace(',', '', $pricing['budgeted_repairs']);
		$space_pricing->closed_date = date('Y-m-d', strtotime($pricing['closed_date']));

		$space_pricing->save();

		if (Request::has('url_request'))
		{
			return Redirect::to($community->id.'/space')->with('success', 'Space created!');
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'message' => 'Space created!',
					'space_num' => $space->space_num
					),
					200
				);
			}
			else
			{
				return Redirect::to($community->id.'/space')->with('success', 'Space created!');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int or string $community, int  $id
	 * @return Response
	 */
	public function show($community, $id)
	{
		if (is_numeric($community))
		{
			$space = Space::with('community', 'space_status', 'space_amenity', 'space_image', 'space_appliance', 'space_detail', 'space_flyer', 'space_pricing')->where('id', $id)->get();

			return Response::json(array(
				'error' => false,
				'space' => $community->toArray()
				),
				200
			);
		}
		else
		{
			// Not sure what else wants to happen here... yet...
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int or string $community, int  $id
	 * @return Response
	 */
	public function view($community, $id)
	{
		$this->javascript[] = HTML::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyClw9UO0i-yS0bXOPgGdblmOVxsmVaHUQE&libraries=places');
		$this->stylesheets[] = HTML::style('js/jquery-ui-1.11.1.dark_hive/jquery-ui.css');

		$comm;
		if (is_numeric($community))
		{
			$comm = Community::with('address.address_google', 'property_manager.user_profile', 'community_amenity', 'main_image')->where('id', $community)->first();
		}
		else
		{
			$community = str_replace('_', ' ', $community);
			$comm = Community::with('address.address_google', 'property_manager.user_profile', 'community_amenity', 'main_image')->where('name', 'like', $community)->first();
		}

		$space = Space::with('space_status', 'space_amenity', 'space_appliance', 'space_detail', 'space_flyer', 'space_pricing')->where('id', $id)->first();

		$main_image = $space->space_image()->first();
		$space_images = $space->space_image()->get();

		$space->main_image = $main_image;
		$space->space_image = $space_images->toArray();

		$master_data = array(
			'page_title' => 'Home View',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$title = 'Home '.$space->space_num;

		$url_request = 'space/view';

		$communities = Community::prependBlankValue(Community::lists('name', 'id'));
		$spaces = Space::where('community_id', $comm->id)->lists('space_num', 'id');
		$space_list = array();
		foreach ($spaces as $id => $space_num) {
			$space_list[$id] = 'Home '.$space_num;
		}
		$current = array();
		if (Confide::user())
		{
			$user_obj = User::with('user_profile')->where('id', Confide::user()->id)->first();
			$u_add = $user_obj->address()->get();
			if (!empty($u_add->toArray()))
			{
				$same = $user_obj->address()->sameAddress()->first();
				if (empty($same))
				{
					$billing = $user_obj->address()->primaryBillingAddress()->first();
					$current['current'] = $billing->toArray();
				}
				else
				{
					$current['current'] = $same->toArray();
				}
			}
		}

		// This turns the image url retrieved from the database into a url that laravel can use to spit images out to a page
		$comm_location = HMVC::post('api/v1/internal/image/retrieveImgUrl', array('location' => $comm->main_image->location));
		if (!empty($main_image))
		{
			$space_location = HMVC::post('api/v1/internal/image/retrieveImgUrl', array('location' => $main_image->location));
			return View::make('space.view', $master_data, array('space' => $space->toArray(), 'comm' => $comm, 'title' => $title, 'community_location' => $comm_location['location'], 'space_location' => $space_location['location'], 'url_request' => $url_request))->with('communities', $communities)->with('spaces', $space_list)->with('states', $this->states)->with('cities', $this->cities)->with('search', '')->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $communities)->with('amenities', $this->amenities)->with('current', $current);
		}
		else
		{
			return View::make('space.view', $master_data, array('space' => $space->toArray(), 'comm' => $comm, 'title' => $title, 'community_location' => $comm_location['location'], 'url_request' => $url_request))->with('communities', $communities)->with('spaces', $space_list)->with('states', $this->states)->with('cities', $this->cities)->with('search', '')->with('radius', $this->radius)->with('price_min', $this->price_min)->with('price_max', $this->price_max)->with('beds', $this->beds)->with('baths', $this->baths)->with('community_type', $this->community_type)->with('community', $communities)->with('amenities', $this->amenities)->with('current', $current);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int or string $community, int  $id
	 * @return Response
	 */
	public function edit($community, $id)
	{
		$this->stylesheets[] = HTML::style('js/jquery-ui-1.11.1.dark_hive/jquery-ui.css');

		$comm;
		if (is_numeric($community))
		{
			$comm = Community::with('property_manager.user_profile')->where('id', $community)->first();
		}
		else
		{
			$comm = Community::with('property_manager.user_profile')->where('name', 'like', $community)->first();
		}

		if (!Confide::user())
		{
			return Redirect::to($comm->id.'/space')->with('error', 'Guests cannot access this page!');
		}
		$user = Confide::user();
		if (!$user->ability(array('rm_admin'), array('edit_spaces')))
		{
			return Redirect::to($comm->id.'/space')->with('error', 'Only admin users can access the home edit page!');
		}

		$master_data = array(
			'page_title' => 'Home Edit',
			'page_stylesheets' => $this->stylesheets,
			'page_scriptlinks' => $this->javascript,
			'user_name' => $this->user_name,
			'news' => array(),
			'slide_right' => '',
			'slide_left' => '',
			'notifications' => ''
		);

		$amenities = Amenity::where('class_id', '>=', 3)->orderBy('id', 'asc')->get();

		$spc = Space::with('space_status', 'space_image', 'space_appliance', 'space_detail', 'space_flyer', 'space_pricing')->where('id', $id)->first();

		$title = 'Edit Home '.$spc->space_num;

		$space_amenity = $spc->space_amenity()->lists('amenity_id');

		$url_request = 'space/update';

		$spc->space_pricing->closed_date = date('m/d/Y', strtotime($spc->space_pricing->closed_date));

		$space = $spc->toArray();

		$same = $spc->address()->sameAddress()->first();
		if (empty($same))
		{
			$billing = $spc->address()->primaryBillingAddress()->first();
			$shipping = $spc->address()->primaryShippingAddress()->first();
			$space['billing'] = $billing->toArray();
			$space['shipping'] = $shipping->toArray();
		}
		else
		{
			$space['billing'] = $same->toArray();
			$space['shipping'] = '';
			$space['billing_same_shipping'] = true;
		}
		
		return View::make('space.edit', $master_data, array('title' => $title, 'amenities' => $amenities->toArray(), 'url_request' => $url_request))->with('space', $space)->with('community', $comm)->with('space_amenity', $space_amenity);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int or string $community, int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Community
		$comm_id = Request::get('community');
		$comm;
		if (is_numeric($comm_id))
		{
			$comm = Community::find($comm_id);
		}
		else
		{
			$comm = Community::where('name', 'like', $comm_id)->first();
		}

		// Space information
		$space = Space::find($id);
		$space = Space::with('address', 'space_status', 'space_image', 'space_appliance')->where('id', $id)->first();

		if (Request::has('space_status_id'))
		{
			$space_status = SpaceStatus::find(Request::get('space_status_id'));
			$space->space_status()->associate($space_status);
		}
		if ($space->community_id != $comm->id)
		{
			$space->community()->associate($community);
		}
		if (Request::has('spaceid'))
		{
			$space->spaceid = Request::get('spaceid');
		}
		if (Request::has('space_num'))
		{
			$space->space_num = Request::get('space_num');
		}
		if (Request::has('description'))
		{
			$space->description = Request::get('description');
		}
		if (Request::has('notes'))
		{
			$space->notes = Request::get('notes');
		}
		if (Request::has('VIN'))
		{
			$space->VIN = Request::get('VIN');
		}

		$space->save();

		$old_address = $space->address->toArray();

		// Address information
		if ( !Request::get('billing_same_shipping') )
		{
			$billing_address = Request::get('billing');
			$shipping_address = Request::get('shipping');
			$billing_address_id = HMVC::put('api/v1/internal/address/'.$old_address[0]['id'], $billing_address);
			if (isset($old_address[1]))
			{
				$shipping_address_id = HMVC::put('api/v1/internal/address/'.$old_address[1]['id'], $shipping_address);
			}
			else
			{
				$shipping_address_id = HMVC::post('api/v1/internal/address', $shipping_address);
				$space->address()->attach($shipping_address_id['address_id']);
			}
		}
		else
		{
			$billing_address = Request::get('billing');
			$billing_address['type'] = 'both';
			$billing_address_id = HMVC::put('api/v1/internal/address/'.$old_address[0]['id'], $billing_address);
			if (isset($old_address[1]))
			{
				$space->address()->detach($old_address[1]['id']);
			}
		}

		// Amenity information
		if ( Request::get('amenity') )
		{
			$amenities = Request::get('amenity');
			$old_amenities = $space->space_amenity()->lists('amenity_id');
			foreach ($old_amenities as $key => $id) {
				$space->space_amenity()->detach($id);
			}
		}

		foreach ($amenities as $a_key => $amenity) {
			$space->space_amenity()->attach($amenity['id']);
		}

		$request_space_details = Request::get('space_detail');

		// Space Details information
		$space_details = SpaceDetail::where('space_id', $space->id)->first();

		if (!empty($request_space_details['manufacturer']))
		{
			$space_details->manufacturer = $request_space_details['manufacturer'];
		}
		if (!empty($request_space_details['width']))
		{
			$space_details->width = $request_space_details['width'];
		}
		if (!empty($request_space_details['length']))
		{
			$space_details->length = $request_space_details['length'];
		}
		if (!empty($request_space_details['square_feet']))
		{
			$space_details->square_feet = $request_space_details['square_feet'];
		}
		if (!empty($request_space_details['beds']))
		{
			$space_details->beds = $request_space_details['beds'];
		}
		if (!empty($request_space_details['baths']))
		{
			$space_details->baths = $request_space_details['baths'];
		}
		if (!empty($request_space_details['year']))
		{
			$space_details->year = $request_space_details['year'];
		}

		$space_details->save();

		// Space Flyer information
		// $space_flyer = SpaceFlyer::where('space_id', $space->id)->first();

		// if (Request::has('flyer_link'))
		// {
		// 	$space_flyer->flyer_link = Request::get('flyer_link');
		// }

		// $space_flyer->save();

		$request_space_pricing = Request::get('space_pricing');

		// Space Pricing information
		$space_pricing = SpacePricing::where('space_id', $space->id)->first();

		if (!empty($request_space_pricing['rent_price']))
		{
			$space_pricing->rent_price = str_replace(',', '', $request_space_pricing['rent_price']);
		}
		if (!empty($request_space_pricing['sale_price']))
		{
			$space_pricing->sale_price = str_replace(',', '', $request_space_pricing['sale_price']);
		}
		if (!empty($request_space_pricing['cash_sale_price']))
		{
			$space_pricing->cash_sale_price = str_replace(',', '', $request_space_pricing['cash_sale_price']);
		}
		if (!empty($request_space_pricing['lease_rate']))
		{
			$space_pricing->lease_rate = $request_space_pricing['lease_rate'];
		}
		if (!empty($request_space_pricing['interest_rate']))
		{
			$space_pricing->interest_rate = $request_space_pricing['interest_rate'];
		}
		if (!empty($request_space_pricing['yearly_tax_savings']))
		{
			$space_pricing->yearly_tax_savings = str_replace(',', '', $request_space_pricing['yearly_tax_savings']);
		}
		if (!empty($request_space_pricing['LOC_rate']))
		{
			$space_pricing->LOC_rate = $request_space_pricing['LOC_rate'];
		}
		if (!empty($request_space_pricing['deposit']))
		{
			$space_pricing->deposit = str_replace(',', '', $request_space_pricing['deposit']);
		}
		if (!empty($request_space_pricing['budgeted_repairs']))
		{
			$space_pricing->budgeted_repairs = str_replace(',', '', $request_space_pricing['budgeted_repairs']);
		}
		if (!empty($request_space_pricing['closed_date']))
		{
			$space_pricing->closed_date = date('Y-m-d', strtotime($request_space_pricing['closed_date']));
		}

		$space_pricing->save();

		if (Request::has('url_request'))
		{
			return Redirect::to($comm->id.'/space/'.$space->id.'/view')->with('success', 'Home updated!');
		}
		else
		{
			if (Request::is('api/v1/*') || Request::ajax())
			{
				return Response::json(array(
					'error' => false,
					'message' => 'Space updated!',
					'space_num' => $space->space_num
					),
					200
				);
			}
			else
			{
				return Redirect::to($comm->id.'/space/'.$space->id.'/view')->with('message', 'Home updated!');
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int or string $community, int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$space = Space::find($id);

		$space->delete();

		return Response::json(array(
			'error' => false,
			'message' => 'Space deleted'
			),
			200
		);
	}


}
