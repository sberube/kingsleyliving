<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Creates the roles table
        Schema::create('user_roles', function ($table) {
            $table->engine = 'MyISAM';

            $table->increments('id')->unsigned();
            $table->string('name')->unique();
            $table->text('description')->nullable();
            $table->tinyInteger('admin')->default(0);
            $table->timestamps();
        });

        // Creates the assigned_roles (Many-to-Many relation) table
        Schema::create('user_pvt_role', function ($table) {
            $table->engine = 'MyISAM';

            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('user_roles');
        });

        // Creates the permissions table
        Schema::create('user_permissions', function ($table) {
            $table->engine = 'MyISAM';

            $table->increments('id')->unsigned();
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->timestamps();
        });

        // Creates the permission_role (Many-to-Many relation) table
        Schema::create('permission_pvt_role', function ($table) {
            $table->engine = 'MyISAM';

            $table->increments('id')->unsigned();
            $table->integer('user_permission_id')->unsigned();
            $table->integer('user_role_id')->unsigned();
            $table->foreign('user_permission_id')->references('id')->on('permissions'); // assumes a users table
            $table->foreign('user_role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::table('user_pvt_role', function (Blueprint $table) {
            $table->dropForeign('assigned_roles_user_id_foreign');
            $table->dropForeign('assigned_roles_role_id_foreign');
        });

        Schema::table('permission_pvt_role', function (Blueprint $table) {
            $table->dropForeign('permission_role_permission_id_foreign');
            $table->dropForeign('permission_role_role_id_foreign');
        });

        Schema::drop('user_pvt_role');
        Schema::drop('permission_pvt_role');
        Schema::drop('user_roles');
        Schema::drop('user_permissions');
    }

}
