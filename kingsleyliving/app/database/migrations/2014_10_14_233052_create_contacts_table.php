<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('phone');
			$table->integer('contact_interest_id')->unsigned();
			$table->foreign('contact_interest_id')->references('id')->on('contact_interests');
			$table->integer('contact_means_id')->unsigned();
			$table->foreign('contact_means_id')->references('id')->on('contact_means');
			$table->integer('contact_status_id')->unsigned();
			$table->foreign('contact_status_id')->references('id')->on('contact_status');
			$table->integer('contact_type_id')->unsigned();
			$table->foreign('contact_type_id')->references('id')->on('contact_types');
			$table->text('extra_notes')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('contact_pvt_address', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('contact_id')->unsigned();
			$table->foreign('contact_id')->references('id')->on('contacts');
			$table->integer('address_id')->unsigned();
			$table->foreign('address_id')->references('id')->on('address');
		});

		Schema::create('contact_pvt_hour', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('contact_id')->unsigned();
			$table->foreign('contact_id')->references('id')->on('contacts');
			$table->integer('hour_id')->unsigned();
			$table->foreign('hour_id')->references('id')->on('hours');
		});

		Schema::create('contact_interests', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('interests');
		});

		Schema::create('contact_means', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('means');
		});

		Schema::create('contact_status', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('status');
		});

		Schema::create('contact_types', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
		Schema::drop('contact_pvt_address');
		Schema::drop('contact_pvt_hour');
		Schema::drop('contact_interests');
		Schema::drop('contact_means');
		Schema::drop('contact_status');
		Schema::drop('contact_types');
	}

}
