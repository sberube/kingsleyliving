<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpacesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('spaces', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
			$table->integer('spaceid')->unsigned();
			$table->integer('space_num');
			$table->integer('space_status_id')->unsigned();
			$table->foreign('space_status_id')->references('id')->on('space_status');
			$table->text('description')->nullable();
			$table->text('notes')->nullable();
			$table->string('VIN');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('space_pvt_address', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->integer('address_id')->unsigned();
			$table->foreign('address_id')->references('id')->on('address');
		});

		Schema::create('space_pvt_amenity', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->integer('amenity_id')->unsigned();
			$table->foreign('amenity_id')->references('id')->on('amenities');
		});

		Schema::create('space_appliances', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->integer('appliance_type_id')->unsigned();
			$table->foreign('appliance_type_id')->references('id')->on('space_appliance_types');
			$table->integer('appliance_num');
		});

		Schema::create('space_appliance_types', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('type');
		});

		Schema::create('space_details', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->string('manufacturer')->nullable();
			$table->integer('width')->nullable();
			$table->integer('length')->nullable();
			$table->integer('square_feet')->nullable();
			$table->integer('beds')->nullable();
			$table->integer('baths')->nullable();
			$table->integer('year')->nullable();
		});

		Schema::create('space_flyers', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->text('flyer_link');
		});

		Schema::create('space_pvt_image', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->integer('image_id')->unsigned();
			$table->foreign('image_id')->references('id')->on('images');
		});

		Schema::create('space_pricing', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
			$table->decimal('rent_price', 11, 2);
			$table->decimal('sale_price', 11, 2);
			$table->decimal('cash_sale_price', 11, 2);
			$table->double('lease_rate', 5, 2)->nullable();
			$table->double('interest_rate', 5, 2)->nullable();
			$table->decimal('yearly_tax_savings', 11, 2)->nullable();
			$table->double('LOC_rate', 5, 2)->nullable();
			$table->decimal('deposit', 11, 2);
			$table->decimal('budgeted_repairs', 11, 2)->nullable();
			$table->date('closed_date')->nullable();
		});

		Schema::create('space_status', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('spaces');
		Schema::drop('space_pvt_address');
		Schema::drop('space_pvt_amenity');
		Schema::drop('space_appliances');
		Schema::drop('space_appliance_types');
		Schema::drop('space_details');
		Schema::drop('space_flyers');
		Schema::drop('space_pvt_image');
		Schema::drop('space_pricing');
		Schema::drop('space_status');
	}

}
