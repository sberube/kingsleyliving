<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applications', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
			$table->text('notes')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('application_pvt_employment', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('application_id')->unsigned();
			$table->foreign('application_id')->references('id')->on('applications');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('employment_id')->unsigned();
			$table->foreign('employment_id')->references('id')->on('employment');
		});

		Schema::create('application_pricing', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('application_id')->unsigned();
			$table->foreign('application_id')->references('id')->on('applications');
			$table->decimal('current_housing_cost', 11, 2);
			$table->integer('property_tax');
			$table->decimal('land_payment', 11, 2)->nullable();
			$table->decimal('cash_sales_price', 11, 2);
			$table->decimal('down_payment', 11, 2);
			$table->date('sold_before');
		});

		Schema::create('application_residents', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('application_id')->unsigned();
			$table->foreign('application_id')->references('id')->on('applications');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('resident_type_id')->unsigned();
			$table->foreign('resident_type_id')->references('id')->on('application_resident_types');
			$table->date('move_in_date');
		});

		Schema::create('application_resident_incomes', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('application_resident_id')->unsigned();
			$table->foreign('application_resident_id')->references('id')->on('application_residents');
			$table->decimal('gross_income', 11, 2);
			$table->decimal('additional_income', 11, 2)->nullable();
			$table->string('additional_income_source')->nullable();
		});

		Schema::create('application_resident_types', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('type');
		});

		Schema::create('application_sources', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('application_id')->unsigned();
			$table->foreign('application_id')->references('id')->on('applications');
			$table->string('source');
		});

		Schema::create('application_pvt_space', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('application_id')->unsigned();
			$table->foreign('application_id')->references('id')->on('applications');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applications');
		Schema::drop('application_pvt_employment');
		Schema::drop('application_pricing');
		Schema::drop('application_residents');
		Schema::drop('application_resident_incomes');
		Schema::drop('application_resident_types');
		Schema::drop('application_sources');
		Schema::drop('application_pvt_space');
	}

}
