<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_pvt_address', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('address_id')->unsigned();
			$table->foreign('address_id')->references('id')->on('address');
		});

		Schema::create('user_pvt_community', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
		});

		Schema::create('user_pvt_employment', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('employment_id')->unsigned();
			$table->foreign('employment_id')->references('id')->on('employment');
		});

		Schema::create('user_marital_status', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('marital_status');
		});

		Schema::create('user_pvt_space', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('space_id')->unsigned();
			$table->foreign('space_id')->references('id')->on('spaces');
		});

		Schema::create('user_profiles', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('account_id')->unsigned();
			$table->string('first_name');
			$table->string('middle_name')->nullable();
			$table->string('last_name');
			$table->string('suffix')->nullable();
			$table->string('phone');
			$table->integer('image_id')->unsigned();
			$table->foreign('image_id')->references('id')->on('images');
			$table->integer('marital_status_id')->unsigned();
			$table->foreign('marital_status_id')->references('id')->on('user_marital_status');
			$table->enum('gender', array('Male', 'Female'));
			$table->timestamps();
		});

		Schema::create('user_defaults', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->timestamps();
		});

		Schema::create('user_sessions', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('user_request_id')->unsigned();
			$table->foreign('user_request_id')->references('id')->on('user_requests');
			$table->string('public_key')->unique();
			$table->string('private_key')->unique();
			$table->decimal('duration', 11, 4);
			$table->timestamps();
		});

		Schema::create('user_requests', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->text('url');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_pvt_address');
		Schema::drop('user_pvt_community');
		Schema::drop('user_pvt_employment');
		Schema::drop('user_marital_status');
		Schema::drop('user_pvt_space');
		Schema::drop('user_profiles');
		Schema::drop('user_defaults');
		Schema::drop('user_sessions');
		Schema::drop('user_requests');
	}

}
