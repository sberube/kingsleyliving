<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('communities', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('propid')->unsigned();
			$table->integer('propNum')->unsigned();
			$table->string('name');
			$table->integer('main_image_id')->unsigned()->nullable();
			$table->foreign('main_image_id')->references('id')->on('images');
			$table->string('short_description')->nullable();
			$table->text('description')->nullable();
			$table->string('email');
			$table->string('phone');
			$table->string('fax');
			$table->integer('property_manager_id')->unsigned();
			$table->foreign('property_manager_id')->references('id')->on('users');
			$table->integer('assistant_property_manager_id')->unsigned()->nullable();
			$table->foreign('assistant_property_manager_id')->references('id')->on('users');
			$table->integer('account_manager_id')->unsigned();
			$table->foreign('account_manager_id')->references('id')->on('users');
			$table->text('home_page');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('community_pvt_address', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
			$table->integer('address_id')->unsigned();
			$table->foreign('address_id')->references('id')->on('address');
		});

		Schema::create('community_pvt_amenity', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
			$table->integer('amenity_id')->unsigned();
			$table->foreign('amenity_id')->references('id')->on('amenities');
		});

		Schema::create('community_pvt_hour', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
			$table->integer('hour_id')->unsigned();
			$table->foreign('hour_id')->references('id')->on('hours');
		});

		Schema::create('community_pvt_image', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('community_id')->unsigned();
			$table->foreign('community_id')->references('id')->on('communities');
			$table->integer('image_id')->unsigned();
			$table->foreign('image_id')->references('id')->on('images');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('communities');
		Schema::drop('community_pvt_address');
		Schema::drop('community_pvt_amenity');
		Schema::drop('community_pvt_hour');
		Schema::drop('community_pvt_image');
	}

}
