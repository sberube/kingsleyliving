<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportErrorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('import_errors', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('record_table');
			$table->string('record_field');
			$table->integer('record_id');
			$table->text('error_message');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('residentmap_users_pvt_kingsleyliving_users', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('rmap_id')->unsigned();
		});

		Schema::create('rmap_pvt_residents', function (Blueprint $table) {
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('rmap_id')->unsigned();
			$table->foreign('rmap_id')->references('rmap_id')->on('residentmap_users_pvt_kingsleyliving_users');
			$table->integer('rsid')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('import_errors');
		Schema::drop('residentmap_users_pvt_kingsleyliving_users');
		Schema::drop('rmap_pvt_residents');
	}

}
