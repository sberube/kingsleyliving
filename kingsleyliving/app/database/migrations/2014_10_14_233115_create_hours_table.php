<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hours', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('days_id')->unsigned();
			$table->foreign('days_id')->references('id')->on('hour_days');
			$table->integer('time_id')->unsigned();
			$table->foreign('time_id')->references('id')->on('hour_times');
			$table->integer('type_id')->unsigned();
			$table->foreign('type_id')->references('id')->on('hour_types');
			$table->text('notes');
			$table->timestamps();
		});

		Schema::create('hour_days', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('days');
		});

		Schema::create('hour_times', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->time('start_time');
			$table->time('end_time');
		});

		Schema::create('hour_types', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hours');
		Schema::drop('hour_days');
		Schema::drop('hour_times');
		Schema::drop('hour_types');
	}

}
