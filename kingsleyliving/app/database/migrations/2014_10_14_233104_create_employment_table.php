<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employment', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('employer');
			$table->string('phone');
			$table->string('job_title');
			$table->timestamps();
		});

		Schema::create('employment_address', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('employment_id')->unsigned();
			$table->foreign('employment_id')->references('id')->on('employment');
			$table->string('address_1');
			$table->string('address_2')->nullable();
			$table->string('city');
			$table->string('state');
			$table->integer('country_id')->unsigned();
			$table->foreign('country_id')->references('id')->on('address_countries');
			$table->string('postal_code');
		});

		Schema::create('employment_dates', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('employment_id')->unsigned();
			$table->foreign('employment_id')->references('id')->on('employment');
			$table->integer('months');
			$table->integer('days');
			$table->integer('years');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employment');
		Schema::drop('employment_address');
		Schema::drop('employment_dates');
	}

}
