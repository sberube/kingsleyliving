<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('amenity_classes', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('class');
		});

		Schema::create('amenities', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->integer('class_id')->unsigned();
			$table->foreign('class_id')->references('id')->on('amenity_classes');
		});

		Schema::create('amenity_icons', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->integer('amenity_id')->unsigned();
			$table->foreign('amenity_id')->references('id')->on('amenities');
			$table->string('icon_class');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('amenity_classes');
		Schema::drop('amenities');
		Schema::drop('amenity_icons');
	}

}
