<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address_countries', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('name');
			$table->string('ISO_code');
		});

		Schema::create('address_google', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('subpremise')->nullable();
			$table->string('street_number'); // street
			$table->string('route'); // street
			$table->string('neighborhood')->nullable(); // district
			$table->string('locality'); // city
			$table->string('administrative_area_level_2')->nullable(); // county
			$table->string('administrative_area_level_1'); // state
			$table->integer('country_id')->unsigned();
			$table->foreign('country_id')->references('id')->on('address_countries');
			$table->string('postal_code');
			$table->float('latitude', 8, 5);
			$table->float('longitude', 8, 5);
		});

		Schema::create('address_types', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('type');
		});

		Schema::create('address', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

			$table->increments('id');
			$table->string('address_1');
			$table->string('address_2')->nullable();
			$table->string('city');
			$table->string('state');
			$table->string('postal_code');
			$table->integer('country_id')->unsigned();
			$table->foreign('country_id')->references('id')->on('address_countries');
			$table->integer('type_id')->unsigned();
			$table->foreign('type_id')->references('id')->on('address_type');
			$table->integer('google_id')->unsigned()->nullable();
			$table->foreign('google_id')->references('id')->on('address_google');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address_countries');
		Schema::drop('address_google');
		Schema::drop('address_types');
		Schema::drop('address');
	}

}
