<?php

class SpaceStatusTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('space_status')->truncate();
        
		\DB::table('space_status')->insert(array (
			0 => 
			array (
				'status' => 'occupied',
			),
			1 => 
			array (
				'status' => 'occupied_nobill',
			),
			2 => 
			array (
				'status' => 'vacant_available',
			),
			3 => 
			array (
				'status' => 'vacant_lease',
			),
			4 => 
			array (
				'status' => 'vacant_rent',
			),
			5 => 
			array (
				'status' => 'vacant_sites',
			),
			6 => 
			array (
				'status' => 'vacant_unavailable',
			),
		));
	}

}
