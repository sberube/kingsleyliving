<?php

class ContactStatusTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('contact_status')->truncate();
        
		\DB::table('contact_status')->insert(array (
			0 => 
			array (
				'status' => 'New',
			),
			1 => 
			array (
				'status' => 'Corp Responded',
			),
			2 => 
			array (
				'status' => 'Unable to Contact',
			),
			3 => 
			array (
				'status' => 'Resolved',
			),
			4 => 
			array (
				'status' => 'Closed',
			),
		));
	}

}
