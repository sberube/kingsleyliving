<?php

class UserPvtAddressTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('user_pvt_address')->truncate();
        
		\DB::table('user_pvt_address')->insert(array (
			0 => 
			array (
				'id' => 1,
				'user_id' => 5,
				'address_id' => 1,
			),
			1 => 
			array (
				'id' => 2,
				'user_id' => 6,
				'address_id' => 1,
			),
			2 => 
			array (
				'id' => 3,
				'user_id' => 7,
				'address_id' => 1,
			),
			3 => 
			array (
				'id' => 4,
				'user_id' => 8,
				'address_id' => 1,
			),
			4 => 
			array (
				'id' => 5,
				'user_id' => 9,
				'address_id' => 1,
			),
			5 => 
			array (
				'id' => 6,
				'user_id' => 10,
				'address_id' => 1,
			),
			6 => 
			array (
				'id' => 7,
				'user_id' => 11,
				'address_id' => 1,
			),
			7 => 
			array (
				'id' => 8,
				'user_id' => 12,
				'address_id' => 1,
			),
			8 => 
			array (
				'id' => 9,
				'user_id' => 13,
				'address_id' => 1,
			),
			9 => 
			array (
				'id' => 10,
				'user_id' => 14,
				'address_id' => 1,
			),
			10 => 
			array (
				'id' => 11,
				'user_id' => 15,
				'address_id' => 1,
			),
			11 => 
			array (
				'id' => 12,
				'user_id' => 16,
				'address_id' => 1,
			),
			12 => 
			array (
				'id' => 13,
				'user_id' => 17,
				'address_id' => 1,
			),
			13 => 
			array (
				'id' => 14,
				'user_id' => 18,
				'address_id' => 1,
			),
			14 => 
			array (
				'id' => 15,
				'user_id' => 19,
				'address_id' => 1,
			),
			15 => 
			array (
				'id' => 16,
				'user_id' => 20,
				'address_id' => 1,
			),
			16 => 
			array (
				'id' => 17,
				'user_id' => 21,
				'address_id' => 1,
			),
			17 => 
			array (
				'id' => 18,
				'user_id' => 22,
				'address_id' => 1,
			),
			18 => 
			array (
				'id' => 19,
				'user_id' => 23,
				'address_id' => 1,
			),
			19 => 
			array (
				'id' => 20,
				'user_id' => 24,
				'address_id' => 1,
			),
			20 => 
			array (
				'id' => 21,
				'user_id' => 25,
				'address_id' => 1,
			),
			21 => 
			array (
				'id' => 22,
				'user_id' => 26,
				'address_id' => 2,
			),
			22 => 
			array (
				'id' => 23,
				'user_id' => 27,
				'address_id' => 1,
			),
			23 => 
			array (
				'id' => 24,
				'user_id' => 28,
				'address_id' => 1,
			),
			24 => 
			array (
				'id' => 25,
				'user_id' => 29,
				'address_id' => 1,
			),
			25 => 
			array (
				'id' => 26,
				'user_id' => 30,
				'address_id' => 1,
			),
			26 => 
			array (
				'id' => 27,
				'user_id' => 31,
				'address_id' => 2,
			),
			27 => 
			array (
				'id' => 28,
				'user_id' => 32,
				'address_id' => 1,
			),
			28 => 
			array (
				'id' => 29,
				'user_id' => 33,
				'address_id' => 1,
			),
			29 => 
			array (
				'id' => 30,
				'user_id' => 34,
				'address_id' => 1,
			),
			30 => 
			array (
				'id' => 31,
				'user_id' => 35,
				'address_id' => 1,
			),
			31 => 
			array (
				'id' => 32,
				'user_id' => 36,
				'address_id' => 1,
			),
			32 => 
			array (
				'id' => 33,
				'user_id' => 37,
				'address_id' => 3,
			),
			33 => 
			array (
				'id' => 34,
				'user_id' => 38,
				'address_id' => 4,
			),
			34 => 
			array (
				'id' => 35,
				'user_id' => 39,
				'address_id' => 1,
			),
			35 => 
			array (
				'id' => 36,
				'user_id' => 40,
				'address_id' => 1,
			),
			36 => 
			array (
				'id' => 37,
				'user_id' => 41,
				'address_id' => 1,
			),
			37 => 
			array (
				'id' => 38,
				'user_id' => 42,
				'address_id' => 1,
			),
			38 => 
			array (
				'id' => 39,
				'user_id' => 43,
				'address_id' => 5,
			),
			39 => 
			array (
				'id' => 40,
				'user_id' => 44,
				'address_id' => 1,
			),
			40 => 
			array (
				'id' => 41,
				'user_id' => 45,
				'address_id' => 1,
			),
			41 => 
			array (
				'id' => 42,
				'user_id' => 46,
				'address_id' => 6,
			),
			42 => 
			array (
				'id' => 43,
				'user_id' => 47,
				'address_id' => 7,
			),
			43 => 
			array (
				'id' => 44,
				'user_id' => 48,
				'address_id' => 1,
			),
			44 => 
			array (
				'id' => 45,
				'user_id' => 49,
				'address_id' => 1,
			),
			45 => 
			array (
				'id' => 46,
				'user_id' => 50,
				'address_id' => 1,
			),
			46 => 
			array (
				'id' => 47,
				'user_id' => 51,
				'address_id' => 8,
			),
			47 => 
			array (
				'id' => 48,
				'user_id' => 52,
				'address_id' => 1,
			),
			48 => 
			array (
				'id' => 49,
				'user_id' => 53,
				'address_id' => 1,
			),
			49 => 
			array (
				'id' => 50,
				'user_id' => 54,
				'address_id' => 1,
			),
			50 => 
			array (
				'id' => 51,
				'user_id' => 55,
				'address_id' => 1,
			),
			51 => 
			array (
				'id' => 52,
				'user_id' => 56,
				'address_id' => 1,
			),
			52 => 
			array (
				'id' => 53,
				'user_id' => 57,
				'address_id' => 9,
			),
			53 => 
			array (
				'id' => 54,
				'user_id' => 58,
				'address_id' => 1,
			),
			54 => 
			array (
				'id' => 55,
				'user_id' => 59,
				'address_id' => 1,
			),
			55 => 
			array (
				'id' => 56,
				'user_id' => 60,
				'address_id' => 1,
			),
			56 => 
			array (
				'id' => 57,
				'user_id' => 61,
				'address_id' => 1,
			),
			57 => 
			array (
				'id' => 58,
				'user_id' => 62,
				'address_id' => 1,
			),
			58 => 
			array (
				'id' => 59,
				'user_id' => 63,
				'address_id' => 1,
			),
			59 => 
			array (
				'id' => 60,
				'user_id' => 64,
				'address_id' => 1,
			),
			60 => 
			array (
				'id' => 61,
				'user_id' => 65,
				'address_id' => 1,
			),
			61 => 
			array (
				'id' => 62,
				'user_id' => 66,
				'address_id' => 1,
			),
			62 => 
			array (
				'id' => 63,
				'user_id' => 67,
				'address_id' => 1,
			),
			63 => 
			array (
				'id' => 64,
				'user_id' => 68,
				'address_id' => 10,
			),
			64 => 
			array (
				'id' => 65,
				'user_id' => 69,
				'address_id' => 1,
			),
			65 => 
			array (
				'id' => 66,
				'user_id' => 70,
				'address_id' => 11,
			),
			66 => 
			array (
				'id' => 67,
				'user_id' => 71,
				'address_id' => 1,
			),
			67 => 
			array (
				'id' => 68,
				'user_id' => 72,
				'address_id' => 1,
			),
			68 => 
			array (
				'id' => 69,
				'user_id' => 73,
				'address_id' => 1,
			),
			69 => 
			array (
				'id' => 70,
				'user_id' => 74,
				'address_id' => 1,
			),
			70 => 
			array (
				'id' => 71,
				'user_id' => 75,
				'address_id' => 1,
			),
			71 => 
			array (
				'id' => 72,
				'user_id' => 76,
				'address_id' => 1,
			),
			72 => 
			array (
				'id' => 73,
				'user_id' => 77,
				'address_id' => 1,
			),
			73 => 
			array (
				'id' => 74,
				'user_id' => 78,
				'address_id' => 1,
			),
		));
	}

}
