<?php

class AddressGoogleTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('address_google')->truncate();
        
		\DB::table('address_google')->insert(array (
			0 => 
			array (
				'id' => 1,
				'subpremise' => '210',
				'street_number' => '5314',
				'route' => 'Heritage School Drive',
				'neighborhood' => 'River Bottoms',
				'locality' => 'Provo',
				'administrative_area_level_2' => 'Utah County',
				'administrative_area_level_1' => 'Utah',
				'country_id' => 227,
				'postal_code' => '84604',
				'latitude' => 40.305,
				'longitude' => -111.66,
			),
			1 => 
			array (
				'id' => 2,
				'subpremise' => NULL,
				'street_number' => '5314',
				'route' => 'Heritage School Drive',
				'neighborhood' => 'River Bottoms',
				'locality' => 'Provo',
				'administrative_area_level_2' => 'Utah County',
				'administrative_area_level_1' => 'Utah',
				'country_id' => 227,
				'postal_code' => '84604',
				'latitude' => 40.305,
				'longitude' => -111.66,
			),
			2 => 
			array (
				'id' => 3,
				'subpremise' => NULL,
				'street_number' => '507',
				'route' => 'East Spaulding Street',
				'neighborhood' => NULL,
				'locality' => 'Lafayette',
				'administrative_area_level_2' => 'Boulder County',
				'administrative_area_level_1' => 'Colorado',
				'country_id' => 227,
				'postal_code' => '80026',
				'latitude' => 39.991999999999997,
				'longitude' => -105.08,
			),
			3 => 
			array (
				'id' => 4,
				'subpremise' => NULL,
				'street_number' => '1711',
				'route' => 'Roosevelt Way',
				'neighborhood' => 'Friendly Village',
				'locality' => 'Aurora',
				'administrative_area_level_2' => 'Adams County',
				'administrative_area_level_1' => 'Colorado',
				'country_id' => 227,
				'postal_code' => '80011',
				'latitude' => 39.744999999999997,
				'longitude' => -104.79000000000001,
			),
			4 => 
			array (
				'id' => 5,
				'subpremise' => NULL,
				'street_number' => '2771',
				'route' => 'South 2670 West',
				'neighborhood' => NULL,
				'locality' => 'West Valley City',
				'administrative_area_level_2' => 'Salt Lake County',
				'administrative_area_level_1' => 'Utah',
				'country_id' => 227,
				'postal_code' => '84119',
				'latitude' => 40.710000000000001,
				'longitude' => -111.95999999999999,
			),
			5 => 
			array (
				'id' => 6,
				'subpremise' => NULL,
				'street_number' => '1855',
				'route' => 'East Riverside Drive',
				'neighborhood' => NULL,
				'locality' => 'Ontario',
				'administrative_area_level_2' => 'San Bernardino County',
				'administrative_area_level_1' => 'California',
				'country_id' => 227,
				'postal_code' => '91761',
				'latitude' => 34.021999999999998,
				'longitude' => -117.61,
			),
			6 => 
			array (
				'id' => 7,
				'subpremise' => NULL,
				'street_number' => '303',
				'route' => 'South Recker Road',
				'neighborhood' => NULL,
				'locality' => 'Mesa',
				'administrative_area_level_2' => 'Maricopa County',
				'administrative_area_level_1' => 'Arizona',
				'country_id' => 227,
				'postal_code' => '85206',
				'latitude' => 33.409999999999997,
				'longitude' => -111.7,
			),
			7 => 
			array (
				'id' => 8,
				'subpremise' => NULL,
				'street_number' => '2885',
				'route' => 'East Midway Boulevard',
				'neighborhood' => NULL,
				'locality' => 'Denver',
				'administrative_area_level_2' => 'Broomfield County',
				'administrative_area_level_1' => 'Colorado',
				'country_id' => 227,
				'postal_code' => '80234',
				'latitude' => 39.929000000000002,
				'longitude' => -105.02,
			),
			8 => 
			array (
				'id' => 9,
				'subpremise' => NULL,
				'street_number' => '1218',
				'route' => 'East Cleveland Avenue',
				'neighborhood' => NULL,
				'locality' => 'Madera',
				'administrative_area_level_2' => 'Madera County',
				'administrative_area_level_1' => 'California',
				'country_id' => 227,
				'postal_code' => '93638',
				'latitude' => 36.972999999999999,
				'longitude' => -120.05,
			),
			9 => 
			array (
				'id' => 10,
				'subpremise' => NULL,
				'street_number' => '1201',
				'route' => 'South Harbor Boulevard',
				'neighborhood' => NULL,
				'locality' => 'La Habra',
				'administrative_area_level_2' => 'Orange County',
				'administrative_area_level_1' => 'California',
				'country_id' => 227,
				'postal_code' => '90631',
				'latitude' => 33.917000000000002,
				'longitude' => -117.93000000000001,
			),
			10 => 
			array (
				'id' => 11,
				'subpremise' => '200',
				'street_number' => '4956',
				'route' => 'North 300 West',
				'neighborhood' => 'River Bottoms',
				'locality' => 'Provo',
				'administrative_area_level_2' => 'Utah County',
				'administrative_area_level_1' => 'Utah',
				'country_id' => 227,
				'postal_code' => '84604',
				'latitude' => 40.299999999999997,
				'longitude' => -111.66,
			),
		));
	}

}
