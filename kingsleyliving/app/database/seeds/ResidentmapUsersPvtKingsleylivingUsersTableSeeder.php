<?php

class ResidentmapUsersPvtKingsleylivingUsersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('residentmap_users_pvt_kingsleyliving_users')->truncate();
        
		\DB::table('residentmap_users_pvt_kingsleyliving_users')->insert(array (
			0 => 
			array (
				'id' => 1,
				'user_id' => 5,
				'rmap_id' => 113,
			),
			1 => 
			array (
				'id' => 2,
				'user_id' => 6,
				'rmap_id' => 351,
			),
			2 => 
			array (
				'id' => 3,
				'user_id' => 7,
				'rmap_id' => 338,
			),
			3 => 
			array (
				'id' => 4,
				'user_id' => 8,
				'rmap_id' => 320,
			),
			4 => 
			array (
				'id' => 5,
				'user_id' => 9,
				'rmap_id' => 335,
			),
			5 => 
			array (
				'id' => 6,
				'user_id' => 10,
				'rmap_id' => 345,
			),
			6 => 
			array (
				'id' => 7,
				'user_id' => 11,
				'rmap_id' => 333,
			),
			7 => 
			array (
				'id' => 8,
				'user_id' => 12,
				'rmap_id' => 247,
			),
			8 => 
			array (
				'id' => 9,
				'user_id' => 13,
				'rmap_id' => 323,
			),
			9 => 
			array (
				'id' => 10,
				'user_id' => 14,
				'rmap_id' => 130,
			),
			10 => 
			array (
				'id' => 11,
				'user_id' => 15,
				'rmap_id' => 237,
			),
			11 => 
			array (
				'id' => 12,
				'user_id' => 16,
				'rmap_id' => 300,
			),
			12 => 
			array (
				'id' => 13,
				'user_id' => 17,
				'rmap_id' => 186,
			),
			13 => 
			array (
				'id' => 14,
				'user_id' => 18,
				'rmap_id' => 380,
			),
			14 => 
			array (
				'id' => 15,
				'user_id' => 19,
				'rmap_id' => 267,
			),
			15 => 
			array (
				'id' => 16,
				'user_id' => 20,
				'rmap_id' => 332,
			),
			16 => 
			array (
				'id' => 17,
				'user_id' => 21,
				'rmap_id' => 321,
			),
			17 => 
			array (
				'id' => 18,
				'user_id' => 22,
				'rmap_id' => 379,
			),
			18 => 
			array (
				'id' => 19,
				'user_id' => 23,
				'rmap_id' => 362,
			),
			19 => 
			array (
				'id' => 20,
				'user_id' => 24,
				'rmap_id' => 26,
			),
			20 => 
			array (
				'id' => 21,
				'user_id' => 25,
				'rmap_id' => 364,
			),
			21 => 
			array (
				'id' => 22,
				'user_id' => 26,
				'rmap_id' => 3,
			),
			22 => 
			array (
				'id' => 23,
				'user_id' => 27,
				'rmap_id' => 25,
			),
			23 => 
			array (
				'id' => 24,
				'user_id' => 28,
				'rmap_id' => 386,
			),
			24 => 
			array (
				'id' => 25,
				'user_id' => 29,
				'rmap_id' => 358,
			),
			25 => 
			array (
				'id' => 26,
				'user_id' => 30,
				'rmap_id' => 309,
			),
			26 => 
			array (
				'id' => 27,
				'user_id' => 31,
				'rmap_id' => 44,
			),
			27 => 
			array (
				'id' => 28,
				'user_id' => 32,
				'rmap_id' => 233,
			),
			28 => 
			array (
				'id' => 29,
				'user_id' => 33,
				'rmap_id' => 372,
			),
			29 => 
			array (
				'id' => 30,
				'user_id' => 34,
				'rmap_id' => 111,
			),
			30 => 
			array (
				'id' => 31,
				'user_id' => 35,
				'rmap_id' => 330,
			),
			31 => 
			array (
				'id' => 32,
				'user_id' => 36,
				'rmap_id' => 388,
			),
			32 => 
			array (
				'id' => 33,
				'user_id' => 37,
				'rmap_id' => 14,
			),
			33 => 
			array (
				'id' => 34,
				'user_id' => 38,
				'rmap_id' => 30,
			),
			34 => 
			array (
				'id' => 35,
				'user_id' => 39,
				'rmap_id' => 365,
			),
			35 => 
			array (
				'id' => 36,
				'user_id' => 40,
				'rmap_id' => 115,
			),
			36 => 
			array (
				'id' => 37,
				'user_id' => 41,
				'rmap_id' => 156,
			),
			37 => 
			array (
				'id' => 38,
				'user_id' => 42,
				'rmap_id' => 348,
			),
			38 => 
			array (
				'id' => 39,
				'user_id' => 43,
				'rmap_id' => 197,
			),
			39 => 
			array (
				'id' => 40,
				'user_id' => 44,
				'rmap_id' => 289,
			),
			40 => 
			array (
				'id' => 41,
				'user_id' => 45,
				'rmap_id' => 296,
			),
			41 => 
			array (
				'id' => 42,
				'user_id' => 46,
				'rmap_id' => 41,
			),
			42 => 
			array (
				'id' => 43,
				'user_id' => 47,
				'rmap_id' => 75,
			),
			43 => 
			array (
				'id' => 44,
				'user_id' => 48,
				'rmap_id' => 97,
			),
			44 => 
			array (
				'id' => 45,
				'user_id' => 49,
				'rmap_id' => 350,
			),
			45 => 
			array (
				'id' => 46,
				'user_id' => 50,
				'rmap_id' => 316,
			),
			46 => 
			array (
				'id' => 47,
				'user_id' => 51,
				'rmap_id' => 53,
			),
			47 => 
			array (
				'id' => 48,
				'user_id' => 52,
				'rmap_id' => 352,
			),
			48 => 
			array (
				'id' => 49,
				'user_id' => 53,
				'rmap_id' => 295,
			),
			49 => 
			array (
				'id' => 50,
				'user_id' => 54,
				'rmap_id' => 283,
			),
			50 => 
			array (
				'id' => 51,
				'user_id' => 55,
				'rmap_id' => 108,
			),
			51 => 
			array (
				'id' => 52,
				'user_id' => 56,
				'rmap_id' => 238,
			),
			52 => 
			array (
				'id' => 53,
				'user_id' => 57,
				'rmap_id' => 353,
			),
			53 => 
			array (
				'id' => 54,
				'user_id' => 58,
				'rmap_id' => 284,
			),
			54 => 
			array (
				'id' => 55,
				'user_id' => 59,
				'rmap_id' => 301,
			),
			55 => 
			array (
				'id' => 56,
				'user_id' => 60,
				'rmap_id' => 336,
			),
			56 => 
			array (
				'id' => 57,
				'user_id' => 61,
				'rmap_id' => 246,
			),
			57 => 
			array (
				'id' => 58,
				'user_id' => 62,
				'rmap_id' => 373,
			),
			58 => 
			array (
				'id' => 59,
				'user_id' => 63,
				'rmap_id' => 286,
			),
			59 => 
			array (
				'id' => 60,
				'user_id' => 64,
				'rmap_id' => 234,
			),
			60 => 
			array (
				'id' => 61,
				'user_id' => 65,
				'rmap_id' => 209,
			),
			61 => 
			array (
				'id' => 62,
				'user_id' => 66,
				'rmap_id' => 344,
			),
			62 => 
			array (
				'id' => 63,
				'user_id' => 67,
				'rmap_id' => 382,
			),
			63 => 
			array (
				'id' => 64,
				'user_id' => 68,
				'rmap_id' => 47,
			),
			64 => 
			array (
				'id' => 65,
				'user_id' => 69,
				'rmap_id' => 241,
			),
			65 => 
			array (
				'id' => 66,
				'user_id' => 70,
				'rmap_id' => 244,
			),
			66 => 
			array (
				'id' => 67,
				'user_id' => 71,
				'rmap_id' => 243,
			),
			67 => 
			array (
				'id' => 68,
				'user_id' => 72,
				'rmap_id' => 281,
			),
			68 => 
			array (
				'id' => 69,
				'user_id' => 73,
				'rmap_id' => 275,
			),
			69 => 
			array (
				'id' => 70,
				'user_id' => 74,
				'rmap_id' => 334,
			),
			70 => 
			array (
				'id' => 71,
				'user_id' => 75,
				'rmap_id' => 159,
			),
			71 => 
			array (
				'id' => 72,
				'user_id' => 76,
				'rmap_id' => 222,
			),
			72 => 
			array (
				'id' => 73,
				'user_id' => 77,
				'rmap_id' => 24,
			),
			73 => 
			array (
				'id' => 74,
				'user_id' => 78,
				'rmap_id' => 110,
			),
		));
	}

}
