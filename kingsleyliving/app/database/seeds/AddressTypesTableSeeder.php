<?php

class AddressTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('address_types')->truncate();
        
		\DB::table('address_types')->insert(array (
			0 => 
			array (
				'id' => 1,
				'type' => 'billing - primary',
			),
			1 => 
			array (
				'id' => 2,
				'type' => 'shipping - primary',
			),
			2 => 
			array (
				'id' => 3,
				'type' => 'billing - secondary',
			),
			3 => 
			array (
				'id' => 4,
				'type' => 'shipping - secondary',
			),
			4 => 
			array (
				'id' => 5,
				'type' => 'billing - alternate',
			),
			5 => 
			array (
				'id' => 6,
				'type' => 'shipping - alternate',
			),
			6 => 
			array (
				'id' => 7,
				'type' => 'contact',
			),
			7 => 
			array (
				'id' => 8,
				'type' => 'both',
			),
		));
	}

}
