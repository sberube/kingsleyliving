<?php

class AddressCountriesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('address_countries')->truncate();
        
		\DB::table('address_countries')->insert(array (
			0 => 
			array (
				'id' => 1,
				'name' => 'Andorra',
				'ISO_code' => 'AD',
			),
			1 => 
			array (
				'id' => 2,
				'name' => 'United Arab Emirates',
				'ISO_code' => 'AE',
			),
			2 => 
			array (
				'id' => 3,
				'name' => 'Afghanistan',
				'ISO_code' => 'AF',
			),
			3 => 
			array (
				'id' => 4,
				'name' => 'Antigua and Barbuda',
				'ISO_code' => 'AG',
			),
			4 => 
			array (
				'id' => 5,
				'name' => 'Anguilla',
				'ISO_code' => 'AI',
			),
			5 => 
			array (
				'id' => 6,
				'name' => 'Albania',
				'ISO_code' => 'AL',
			),
			6 => 
			array (
				'id' => 7,
				'name' => 'Armenia',
				'ISO_code' => 'AM',
			),
			7 => 
			array (
				'id' => 8,
				'name' => 'Netherlands Antilles',
				'ISO_code' => 'AN',
			),
			8 => 
			array (
				'id' => 9,
				'name' => 'Angola',
				'ISO_code' => 'AO',
			),
			9 => 
			array (
				'id' => 10,
				'name' => 'Antarctica',
				'ISO_code' => 'AQ',
			),
			10 => 
			array (
				'id' => 11,
				'name' => 'Argentina',
				'ISO_code' => 'AR',
			),
			11 => 
			array (
				'id' => 12,
				'name' => 'American Samoa',
				'ISO_code' => 'AS',
			),
			12 => 
			array (
				'id' => 13,
				'name' => 'Austria',
				'ISO_code' => 'AT',
			),
			13 => 
			array (
				'id' => 14,
				'name' => 'Australia',
				'ISO_code' => 'AU',
			),
			14 => 
			array (
				'id' => 15,
				'name' => 'Aruba',
				'ISO_code' => 'AW',
			),
			15 => 
			array (
				'id' => 16,
				'name' => 'Aland Islands',
				'ISO_code' => 'AX',
			),
			16 => 
			array (
				'id' => 17,
				'name' => 'Azerbaija',
				'ISO_code' => 'AZ',
			),
			17 => 
			array (
				'id' => 18,
				'name' => 'Bosnia and Herzegovina',
				'ISO_code' => 'BA',
			),
			18 => 
			array (
				'id' => 19,
				'name' => 'Barbados',
				'ISO_code' => 'BB',
			),
			19 => 
			array (
				'id' => 20,
				'name' => 'Bangladesh',
				'ISO_code' => 'BD',
			),
			20 => 
			array (
				'id' => 21,
				'name' => 'Belgium',
				'ISO_code' => 'BE',
			),
			21 => 
			array (
				'id' => 22,
				'name' => 'Burkina Faso',
				'ISO_code' => 'BF',
			),
			22 => 
			array (
				'id' => 23,
				'name' => 'Bulgaria',
				'ISO_code' => 'BG',
			),
			23 => 
			array (
				'id' => 24,
				'name' => 'Bahrai',
				'ISO_code' => 'BH',
			),
			24 => 
			array (
				'id' => 25,
				'name' => 'Burundi',
				'ISO_code' => 'BI',
			),
			25 => 
			array (
				'id' => 26,
				'name' => 'Beni',
				'ISO_code' => 'BJ',
			),
			26 => 
			array (
				'id' => 27,
				'name' => 'Bermuda',
				'ISO_code' => 'BM',
			),
			27 => 
			array (
				'id' => 28,
				'name' => 'Brunei',
				'ISO_code' => 'BN',
			),
			28 => 
			array (
				'id' => 29,
				'name' => 'Bolivia',
				'ISO_code' => 'BO',
			),
			29 => 
			array (
				'id' => 30,
				'name' => 'Brazil',
				'ISO_code' => 'BR',
			),
			30 => 
			array (
				'id' => 31,
				'name' => 'Bahamas',
				'ISO_code' => 'BS',
			),
			31 => 
			array (
				'id' => 32,
				'name' => 'Bhuta',
				'ISO_code' => 'BT',
			),
			32 => 
			array (
				'id' => 33,
				'name' => 'Bouvet Island',
				'ISO_code' => 'BV',
			),
			33 => 
			array (
				'id' => 34,
				'name' => 'Botswana',
				'ISO_code' => 'BW',
			),
			34 => 
			array (
				'id' => 35,
				'name' => 'Belarus',
				'ISO_code' => 'BY',
			),
			35 => 
			array (
				'id' => 36,
				'name' => 'Belize',
				'ISO_code' => 'BZ',
			),
			36 => 
			array (
				'id' => 37,
				'name' => 'Canada',
				'ISO_code' => 'CA',
			),
			37 => 
			array (
				'id' => 38,
			'name' => 'Cocos (Keeling), Islands',
				'ISO_code' => 'CC',
			),
			38 => 
			array (
				'id' => 39,
			'name' => 'Congo (Kinshasa),',
				'ISO_code' => 'CD',
			),
			39 => 
			array (
				'id' => 40,
				'name' => 'Central African Republic',
				'ISO_code' => 'CF',
			),
			40 => 
			array (
				'id' => 41,
			'name' => 'Congo (Brazzaville),',
				'ISO_code' => 'CG',
			),
			41 => 
			array (
				'id' => 42,
				'name' => 'Switzerland',
				'ISO_code' => 'CH',
			),
			42 => 
			array (
				'id' => 43,
				'name' => 'Ivory Coast',
				'ISO_code' => 'CI',
			),
			43 => 
			array (
				'id' => 44,
				'name' => 'Cook Islands',
				'ISO_code' => 'CK',
			),
			44 => 
			array (
				'id' => 45,
				'name' => 'Chile',
				'ISO_code' => 'CL',
			),
			45 => 
			array (
				'id' => 46,
				'name' => 'Cameroo',
				'ISO_code' => 'CM',
			),
			46 => 
			array (
				'id' => 47,
				'name' => 'China',
				'ISO_code' => 'CN',
			),
			47 => 
			array (
				'id' => 48,
				'name' => 'Colombia',
				'ISO_code' => 'CO',
			),
			48 => 
			array (
				'id' => 49,
				'name' => 'Costa Rica',
				'ISO_code' => 'CR',
			),
			49 => 
			array (
				'id' => 50,
				'name' => 'Serbia And Montenegro',
				'ISO_code' => 'CS',
			),
			50 => 
			array (
				'id' => 51,
				'name' => 'Cuba',
				'ISO_code' => 'CU',
			),
			51 => 
			array (
				'id' => 52,
				'name' => 'Cape Verde',
				'ISO_code' => 'CV',
			),
			52 => 
			array (
				'id' => 53,
				'name' => 'Christmas Island',
				'ISO_code' => 'CX',
			),
			53 => 
			array (
				'id' => 54,
				'name' => 'Cyprus',
				'ISO_code' => 'CY',
			),
			54 => 
			array (
				'id' => 55,
				'name' => 'Czech Republic',
				'ISO_code' => 'CZ',
			),
			55 => 
			array (
				'id' => 56,
				'name' => 'Germany',
				'ISO_code' => 'DE',
			),
			56 => 
			array (
				'id' => 57,
				'name' => 'Djibouti',
				'ISO_code' => 'DJ',
			),
			57 => 
			array (
				'id' => 58,
				'name' => 'Denmark',
				'ISO_code' => 'DK',
			),
			58 => 
			array (
				'id' => 59,
				'name' => 'Dominica',
				'ISO_code' => 'DM',
			),
			59 => 
			array (
				'id' => 60,
				'name' => 'Dominican Republic',
				'ISO_code' => 'DO',
			),
			60 => 
			array (
				'id' => 61,
				'name' => 'Algeria',
				'ISO_code' => 'DZ',
			),
			61 => 
			array (
				'id' => 62,
				'name' => 'Ecuador',
				'ISO_code' => 'EC',
			),
			62 => 
			array (
				'id' => 63,
				'name' => 'Estonia',
				'ISO_code' => 'EE',
			),
			63 => 
			array (
				'id' => 64,
				'name' => 'Egypt',
				'ISO_code' => 'EG',
			),
			64 => 
			array (
				'id' => 65,
				'name' => 'Western Sahara',
				'ISO_code' => 'EH',
			),
			65 => 
			array (
				'id' => 66,
				'name' => 'Eritrea',
				'ISO_code' => 'ER',
			),
			66 => 
			array (
				'id' => 67,
				'name' => 'Spai',
				'ISO_code' => 'ES',
			),
			67 => 
			array (
				'id' => 68,
				'name' => 'Ethiopia',
				'ISO_code' => 'ET',
			),
			68 => 
			array (
				'id' => 69,
				'name' => 'Finland',
				'ISO_code' => 'FI',
			),
			69 => 
			array (
				'id' => 70,
				'name' => 'Fiji',
				'ISO_code' => 'FJ',
			),
			70 => 
			array (
				'id' => 71,
				'name' => 'Falkland Islands',
				'ISO_code' => 'FK',
			),
			71 => 
			array (
				'id' => 72,
				'name' => 'Micronesia',
				'ISO_code' => 'FM',
			),
			72 => 
			array (
				'id' => 73,
				'name' => 'Faroe Islands',
				'ISO_code' => 'FO',
			),
			73 => 
			array (
				'id' => 74,
				'name' => 'France',
				'ISO_code' => 'FR',
			),
			74 => 
			array (
				'id' => 75,
				'name' => 'Gabo',
				'ISO_code' => 'GA',
			),
			75 => 
			array (
				'id' => 76,
				'name' => 'United Kingdom',
				'ISO_code' => 'GB',
			),
			76 => 
			array (
				'id' => 77,
				'name' => 'Grenada',
				'ISO_code' => 'GD',
			),
			77 => 
			array (
				'id' => 78,
				'name' => 'Georgia',
				'ISO_code' => 'GE',
			),
			78 => 
			array (
				'id' => 79,
				'name' => 'French Guiana',
				'ISO_code' => 'GF',
			),
			79 => 
			array (
				'id' => 80,
				'name' => 'Guernsey',
				'ISO_code' => 'GG',
			),
			80 => 
			array (
				'id' => 81,
				'name' => 'Ghana',
				'ISO_code' => 'GH',
			),
			81 => 
			array (
				'id' => 82,
				'name' => 'Gibraltar',
				'ISO_code' => 'GI',
			),
			82 => 
			array (
				'id' => 83,
				'name' => 'Greenland',
				'ISO_code' => 'GL',
			),
			83 => 
			array (
				'id' => 84,
				'name' => 'Gambia',
				'ISO_code' => 'GM',
			),
			84 => 
			array (
				'id' => 85,
				'name' => 'Guinea',
				'ISO_code' => 'GN',
			),
			85 => 
			array (
				'id' => 86,
				'name' => 'Guadeloupe',
				'ISO_code' => 'GP',
			),
			86 => 
			array (
				'id' => 87,
				'name' => 'Equatorial Guinea',
				'ISO_code' => 'GQ',
			),
			87 => 
			array (
				'id' => 88,
				'name' => 'Greece',
				'ISO_code' => 'GR',
			),
			88 => 
			array (
				'id' => 89,
				'name' => 'South Georgia and the South Sandwich Islands',
				'ISO_code' => 'GS',
			),
			89 => 
			array (
				'id' => 90,
				'name' => 'Guatemala',
				'ISO_code' => 'GT',
			),
			90 => 
			array (
				'id' => 91,
				'name' => 'Guam',
				'ISO_code' => 'GU',
			),
			91 => 
			array (
				'id' => 92,
				'name' => 'Guinea-Bissau',
				'ISO_code' => 'GW',
			),
			92 => 
			array (
				'id' => 93,
				'name' => 'Guyana',
				'ISO_code' => 'GY',
			),
			93 => 
			array (
				'id' => 94,
				'name' => 'Hong Kong S.A.R., China',
				'ISO_code' => 'HK',
			),
			94 => 
			array (
				'id' => 95,
				'name' => 'Heard Island and McDonald Islands',
				'ISO_code' => 'HM',
			),
			95 => 
			array (
				'id' => 96,
				'name' => 'Honduras',
				'ISO_code' => 'HN',
			),
			96 => 
			array (
				'id' => 97,
				'name' => 'Croatia',
				'ISO_code' => 'HR',
			),
			97 => 
			array (
				'id' => 98,
				'name' => 'Haiti',
				'ISO_code' => 'HT',
			),
			98 => 
			array (
				'id' => 99,
				'name' => 'Hungary',
				'ISO_code' => 'HU',
			),
			99 => 
			array (
				'id' => 100,
				'name' => 'Indonesia',
				'ISO_code' => 'ID',
			),
			100 => 
			array (
				'id' => 101,
				'name' => 'Ireland',
				'ISO_code' => 'IE',
			),
			101 => 
			array (
				'id' => 102,
				'name' => 'Israel',
				'ISO_code' => 'IL',
			),
			102 => 
			array (
				'id' => 103,
				'name' => 'Isle of Ma',
				'ISO_code' => 'IM',
			),
			103 => 
			array (
				'id' => 104,
				'name' => 'India',
				'ISO_code' => 'IN',
			),
			104 => 
			array (
				'id' => 105,
				'name' => 'British Indian Ocean Territory',
				'ISO_code' => 'IO',
			),
			105 => 
			array (
				'id' => 106,
				'name' => 'Iraq',
				'ISO_code' => 'IQ',
			),
			106 => 
			array (
				'id' => 107,
				'name' => 'Ira',
				'ISO_code' => 'IR',
			),
			107 => 
			array (
				'id' => 108,
				'name' => 'Iceland',
				'ISO_code' => 'IS',
			),
			108 => 
			array (
				'id' => 109,
				'name' => 'Italy',
				'ISO_code' => 'IT',
			),
			109 => 
			array (
				'id' => 110,
				'name' => 'Jersey',
				'ISO_code' => 'JE',
			),
			110 => 
			array (
				'id' => 111,
				'name' => 'Jamaica',
				'ISO_code' => 'JM',
			),
			111 => 
			array (
				'id' => 112,
				'name' => 'Jorda',
				'ISO_code' => 'JO',
			),
			112 => 
			array (
				'id' => 113,
				'name' => 'Japa',
				'ISO_code' => 'JP',
			),
			113 => 
			array (
				'id' => 114,
				'name' => 'Kenya',
				'ISO_code' => 'KE',
			),
			114 => 
			array (
				'id' => 115,
				'name' => 'Kyrgyzsta',
				'ISO_code' => 'KG',
			),
			115 => 
			array (
				'id' => 116,
				'name' => 'Cambodia',
				'ISO_code' => 'KH',
			),
			116 => 
			array (
				'id' => 117,
				'name' => 'Kiribati',
				'ISO_code' => 'KI',
			),
			117 => 
			array (
				'id' => 118,
				'name' => 'Comoros',
				'ISO_code' => 'KM',
			),
			118 => 
			array (
				'id' => 119,
				'name' => 'Saint Kitts and Nevis',
				'ISO_code' => 'KN',
			),
			119 => 
			array (
				'id' => 120,
				'name' => 'North Korea',
				'ISO_code' => 'KP',
			),
			120 => 
			array (
				'id' => 121,
				'name' => 'South Korea',
				'ISO_code' => 'KR',
			),
			121 => 
			array (
				'id' => 122,
				'name' => 'Kuwait',
				'ISO_code' => 'KW',
			),
			122 => 
			array (
				'id' => 123,
				'name' => 'Cayman Islands',
				'ISO_code' => 'KY',
			),
			123 => 
			array (
				'id' => 124,
				'name' => 'Kazakhsta',
				'ISO_code' => 'KZ',
			),
			124 => 
			array (
				'id' => 125,
				'name' => 'Laos',
				'ISO_code' => 'LA',
			),
			125 => 
			array (
				'id' => 126,
				'name' => 'Lebano',
				'ISO_code' => 'LB',
			),
			126 => 
			array (
				'id' => 127,
				'name' => 'Saint Lucia',
				'ISO_code' => 'LC',
			),
			127 => 
			array (
				'id' => 128,
				'name' => 'Liechtenstei',
				'ISO_code' => 'LI',
			),
			128 => 
			array (
				'id' => 129,
				'name' => 'Sri Lanka',
				'ISO_code' => 'LK',
			),
			129 => 
			array (
				'id' => 130,
				'name' => 'Liberia',
				'ISO_code' => 'LR',
			),
			130 => 
			array (
				'id' => 131,
				'name' => 'Lesotho',
				'ISO_code' => 'LS',
			),
			131 => 
			array (
				'id' => 132,
				'name' => 'Lithuania',
				'ISO_code' => 'LT',
			),
			132 => 
			array (
				'id' => 133,
				'name' => 'Luxembourg',
				'ISO_code' => 'LU',
			),
			133 => 
			array (
				'id' => 134,
				'name' => 'Latvia',
				'ISO_code' => 'LV',
			),
			134 => 
			array (
				'id' => 135,
				'name' => 'Libya',
				'ISO_code' => 'LY',
			),
			135 => 
			array (
				'id' => 136,
				'name' => 'Morocco',
				'ISO_code' => 'MA',
			),
			136 => 
			array (
				'id' => 137,
				'name' => 'Monaco',
				'ISO_code' => 'MC',
			),
			137 => 
			array (
				'id' => 138,
				'name' => 'Moldova',
				'ISO_code' => 'MD',
			),
			138 => 
			array (
				'id' => 139,
				'name' => 'Madagascar',
				'ISO_code' => 'MG',
			),
			139 => 
			array (
				'id' => 140,
				'name' => 'Marshall Islands',
				'ISO_code' => 'MH',
			),
			140 => 
			array (
				'id' => 141,
				'name' => 'Macedonia',
				'ISO_code' => 'MK',
			),
			141 => 
			array (
				'id' => 142,
				'name' => 'Mali',
				'ISO_code' => 'ML',
			),
			142 => 
			array (
				'id' => 143,
				'name' => 'Myanmar',
				'ISO_code' => 'MM',
			),
			143 => 
			array (
				'id' => 144,
				'name' => 'Mongolia',
				'ISO_code' => 'MN',
			),
			144 => 
			array (
				'id' => 145,
				'name' => 'Macao S.A.R., China',
				'ISO_code' => 'MO',
			),
			145 => 
			array (
				'id' => 146,
				'name' => 'Northern Mariana Islands',
				'ISO_code' => 'MP',
			),
			146 => 
			array (
				'id' => 147,
				'name' => 'Martinique',
				'ISO_code' => 'MQ',
			),
			147 => 
			array (
				'id' => 148,
				'name' => 'Mauritania',
				'ISO_code' => 'MR',
			),
			148 => 
			array (
				'id' => 149,
				'name' => 'Montserrat',
				'ISO_code' => 'MS',
			),
			149 => 
			array (
				'id' => 150,
				'name' => 'Malta',
				'ISO_code' => 'MT',
			),
			150 => 
			array (
				'id' => 151,
				'name' => 'Mauritius',
				'ISO_code' => 'MU',
			),
			151 => 
			array (
				'id' => 152,
				'name' => 'Maldives',
				'ISO_code' => 'MV',
			),
			152 => 
			array (
				'id' => 153,
				'name' => 'Malawi',
				'ISO_code' => 'MW',
			),
			153 => 
			array (
				'id' => 154,
				'name' => 'Mexico',
				'ISO_code' => 'MX',
			),
			154 => 
			array (
				'id' => 155,
				'name' => 'Malaysia',
				'ISO_code' => 'MY',
			),
			155 => 
			array (
				'id' => 156,
				'name' => 'Mozambique',
				'ISO_code' => 'MZ',
			),
			156 => 
			array (
				'id' => 157,
				'name' => 'Namibia',
				'ISO_code' => 'NA',
			),
			157 => 
			array (
				'id' => 158,
				'name' => 'New Caledonia',
				'ISO_code' => 'NC',
			),
			158 => 
			array (
				'id' => 159,
				'name' => 'Niger',
				'ISO_code' => 'NE',
			),
			159 => 
			array (
				'id' => 160,
				'name' => 'Norfolk Island',
				'ISO_code' => 'NF',
			),
			160 => 
			array (
				'id' => 161,
				'name' => 'Nigeria',
				'ISO_code' => 'NG',
			),
			161 => 
			array (
				'id' => 162,
				'name' => 'Nicaragua',
				'ISO_code' => 'NI',
			),
			162 => 
			array (
				'id' => 163,
				'name' => 'Netherlands',
				'ISO_code' => 'NL',
			),
			163 => 
			array (
				'id' => 164,
				'name' => 'Norway',
				'ISO_code' => 'NO',
			),
			164 => 
			array (
				'id' => 165,
				'name' => 'Nepal',
				'ISO_code' => 'NP',
			),
			165 => 
			array (
				'id' => 166,
				'name' => 'Nauru',
				'ISO_code' => 'NR',
			),
			166 => 
			array (
				'id' => 167,
				'name' => 'Niue',
				'ISO_code' => 'NU',
			),
			167 => 
			array (
				'id' => 168,
				'name' => 'New Zealand',
				'ISO_code' => 'NZ',
			),
			168 => 
			array (
				'id' => 169,
				'name' => 'Oma',
				'ISO_code' => 'OM',
			),
			169 => 
			array (
				'id' => 170,
				'name' => 'Panama',
				'ISO_code' => 'PA',
			),
			170 => 
			array (
				'id' => 171,
				'name' => 'Peru',
				'ISO_code' => 'PE',
			),
			171 => 
			array (
				'id' => 172,
				'name' => 'French Polynesia',
				'ISO_code' => 'PF',
			),
			172 => 
			array (
				'id' => 173,
				'name' => 'Papua New Guinea',
				'ISO_code' => 'PG',
			),
			173 => 
			array (
				'id' => 174,
				'name' => 'Philippines',
				'ISO_code' => 'PH',
			),
			174 => 
			array (
				'id' => 175,
				'name' => 'Pakista',
				'ISO_code' => 'PK',
			),
			175 => 
			array (
				'id' => 176,
				'name' => 'Poland',
				'ISO_code' => 'PL',
			),
			176 => 
			array (
				'id' => 177,
				'name' => 'Saint Pierre and Miquelo',
				'ISO_code' => 'PM',
			),
			177 => 
			array (
				'id' => 178,
				'name' => 'Pitcair',
				'ISO_code' => 'PN',
			),
			178 => 
			array (
				'id' => 179,
				'name' => 'Puerto Rico',
				'ISO_code' => 'PR',
			),
			179 => 
			array (
				'id' => 180,
				'name' => 'Palestinian Territory',
				'ISO_code' => 'PS',
			),
			180 => 
			array (
				'id' => 181,
				'name' => 'Portugal',
				'ISO_code' => 'PT',
			),
			181 => 
			array (
				'id' => 182,
				'name' => 'Palau',
				'ISO_code' => 'PW',
			),
			182 => 
			array (
				'id' => 183,
				'name' => 'Paraguay',
				'ISO_code' => 'PY',
			),
			183 => 
			array (
				'id' => 184,
				'name' => 'Qatar',
				'ISO_code' => 'QA',
			),
			184 => 
			array (
				'id' => 185,
				'name' => 'Reunio',
				'ISO_code' => 'RE',
			),
			185 => 
			array (
				'id' => 186,
				'name' => 'Romania',
				'ISO_code' => 'RO',
			),
			186 => 
			array (
				'id' => 187,
				'name' => 'Russia',
				'ISO_code' => 'RU',
			),
			187 => 
			array (
				'id' => 188,
				'name' => 'Rwanda',
				'ISO_code' => 'RW',
			),
			188 => 
			array (
				'id' => 189,
				'name' => 'Saudi Arabia',
				'ISO_code' => 'SA',
			),
			189 => 
			array (
				'id' => 190,
				'name' => 'Solomon Islands',
				'ISO_code' => 'SB',
			),
			190 => 
			array (
				'id' => 191,
				'name' => 'Seychelles',
				'ISO_code' => 'SC',
			),
			191 => 
			array (
				'id' => 192,
				'name' => 'Suda',
				'ISO_code' => 'SD',
			),
			192 => 
			array (
				'id' => 193,
				'name' => 'Swede',
				'ISO_code' => 'SE',
			),
			193 => 
			array (
				'id' => 194,
				'name' => 'Singapore',
				'ISO_code' => 'SG',
			),
			194 => 
			array (
				'id' => 195,
				'name' => 'Saint Helena',
				'ISO_code' => 'SH',
			),
			195 => 
			array (
				'id' => 196,
				'name' => 'Slovenia',
				'ISO_code' => 'SI',
			),
			196 => 
			array (
				'id' => 197,
				'name' => 'Svalbard and Jan Maye',
				'ISO_code' => 'SJ',
			),
			197 => 
			array (
				'id' => 198,
				'name' => 'Slovakia',
				'ISO_code' => 'SK',
			),
			198 => 
			array (
				'id' => 199,
				'name' => 'Sierra Leone',
				'ISO_code' => 'SL',
			),
			199 => 
			array (
				'id' => 200,
				'name' => 'San Marino',
				'ISO_code' => 'SM',
			),
			200 => 
			array (
				'id' => 201,
				'name' => 'Senegal',
				'ISO_code' => 'SN',
			),
			201 => 
			array (
				'id' => 202,
				'name' => 'Somalia',
				'ISO_code' => 'SO',
			),
			202 => 
			array (
				'id' => 203,
				'name' => 'Suriname',
				'ISO_code' => 'SR',
			),
			203 => 
			array (
				'id' => 204,
				'name' => 'Sao Tome and Principe',
				'ISO_code' => 'ST',
			),
			204 => 
			array (
				'id' => 205,
				'name' => 'El Salvador',
				'ISO_code' => 'SV',
			),
			205 => 
			array (
				'id' => 206,
				'name' => 'Syria',
				'ISO_code' => 'SY',
			),
			206 => 
			array (
				'id' => 207,
				'name' => 'Swaziland',
				'ISO_code' => 'SZ',
			),
			207 => 
			array (
				'id' => 208,
				'name' => 'Turks and Caicos Islands',
				'ISO_code' => 'TC',
			),
			208 => 
			array (
				'id' => 209,
				'name' => 'Chad',
				'ISO_code' => 'TD',
			),
			209 => 
			array (
				'id' => 210,
				'name' => 'French Southern Territories',
				'ISO_code' => 'TF',
			),
			210 => 
			array (
				'id' => 211,
				'name' => 'Togo',
				'ISO_code' => 'TG',
			),
			211 => 
			array (
				'id' => 212,
				'name' => 'Thailand',
				'ISO_code' => 'TH',
			),
			212 => 
			array (
				'id' => 213,
				'name' => 'Tajikista',
				'ISO_code' => 'TJ',
			),
			213 => 
			array (
				'id' => 214,
				'name' => 'Tokelau',
				'ISO_code' => 'TK',
			),
			214 => 
			array (
				'id' => 215,
				'name' => 'East Timor',
				'ISO_code' => 'TL',
			),
			215 => 
			array (
				'id' => 216,
				'name' => 'Turkmenista',
				'ISO_code' => 'TM',
			),
			216 => 
			array (
				'id' => 217,
				'name' => 'Tunisia',
				'ISO_code' => 'TN',
			),
			217 => 
			array (
				'id' => 218,
				'name' => 'Tonga',
				'ISO_code' => 'TO',
			),
			218 => 
			array (
				'id' => 219,
				'name' => 'Turkey',
				'ISO_code' => 'TR',
			),
			219 => 
			array (
				'id' => 220,
				'name' => 'Trinidad and Tobago',
				'ISO_code' => 'TT',
			),
			220 => 
			array (
				'id' => 221,
				'name' => 'Tuvalu',
				'ISO_code' => 'TV',
			),
			221 => 
			array (
				'id' => 222,
				'name' => 'Taiwa',
				'ISO_code' => 'TW',
			),
			222 => 
			array (
				'id' => 223,
				'name' => 'Tanzania',
				'ISO_code' => 'TZ',
			),
			223 => 
			array (
				'id' => 224,
				'name' => 'Ukraine',
				'ISO_code' => 'UA',
			),
			224 => 
			array (
				'id' => 225,
				'name' => 'Uganda',
				'ISO_code' => 'UG',
			),
			225 => 
			array (
				'id' => 226,
				'name' => 'United States Minor Outlying Islands',
				'ISO_code' => 'UM',
			),
			226 => 
			array (
				'id' => 227,
				'name' => 'United States',
				'ISO_code' => 'US',
			),
			227 => 
			array (
				'id' => 228,
				'name' => 'Uruguay',
				'ISO_code' => 'UY',
			),
			228 => 
			array (
				'id' => 229,
				'name' => 'Uzbekista',
				'ISO_code' => 'UZ',
			),
			229 => 
			array (
				'id' => 230,
				'name' => 'Vatica',
				'ISO_code' => 'VA',
			),
			230 => 
			array (
				'id' => 231,
				'name' => 'Saint Vincent and the Grenadines',
				'ISO_code' => 'VC',
			),
			231 => 
			array (
				'id' => 232,
				'name' => 'Venezuela',
				'ISO_code' => 'VE',
			),
			232 => 
			array (
				'id' => 233,
				'name' => 'British Virgin Islands',
				'ISO_code' => 'VG',
			),
			233 => 
			array (
				'id' => 234,
				'name' => 'U.S. Virgin Islands',
				'ISO_code' => 'VI',
			),
			234 => 
			array (
				'id' => 235,
				'name' => 'Vietnam',
				'ISO_code' => 'VN',
			),
			235 => 
			array (
				'id' => 236,
				'name' => 'Vanuatu',
				'ISO_code' => 'VU',
			),
			236 => 
			array (
				'id' => 237,
				'name' => 'Wallis and Futuna',
				'ISO_code' => 'WF',
			),
			237 => 
			array (
				'id' => 238,
				'name' => 'Samoa',
				'ISO_code' => 'WS',
			),
			238 => 
			array (
				'id' => 239,
				'name' => 'Yeme',
				'ISO_code' => 'YE',
			),
			239 => 
			array (
				'id' => 240,
				'name' => 'Mayotte',
				'ISO_code' => 'YT',
			),
			240 => 
			array (
				'id' => 241,
				'name' => 'South Africa',
				'ISO_code' => 'ZA',
			),
			241 => 
			array (
				'id' => 242,
				'name' => 'Zambia',
				'ISO_code' => 'ZM',
			),
			242 => 
			array (
				'id' => 243,
				'name' => 'Zimbabwe',
				'ISO_code' => 'ZW',
			),
		));
	}

}
