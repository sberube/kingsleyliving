<?php

class ApplicationResidentTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('application_resident_types')->truncate();
        
		\DB::table('application_resident_types')->insert(array (
			0 => 
			array (
				'type' => 'Primary',
			),
			1 => 
			array (
				'type' => 'Co-signer',
			),
		));
	}

}
