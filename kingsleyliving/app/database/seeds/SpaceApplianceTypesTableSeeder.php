<?php

class SpaceApplianceTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('space_appliance_types')->truncate();
        
		\DB::table('space_appliance_types')->insert(array (
			0 => 
			array (
				'type' => 'Furnace',
			),
			1 => 
			array (
				'type' => 'Dishwasher',
			),
			2 => 
			array (
				'type' => 'Fridge',
			),
			3 => 
			array (
				'type' => 'Dryer',
			),
			4 => 
			array (
				'type' => 'Washer',
			),
		));
	}

}
