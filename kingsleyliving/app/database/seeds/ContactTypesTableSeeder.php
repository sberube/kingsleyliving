<?php

class ContactTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('contact_types')->truncate();
        
		\DB::table('contact_types')->insert(array (
			0 => 
			array (
				'id' => 1,
				'type' => 'Kingsley Management Corporation',
			),
			1 => 
			array (
				'id' => 2,
				'type' => 'Arbordale Acres',
			),
			2 => 
			array (
				'id' => 3,
				'type' => 'Brentwood',
			),
			3 => 
			array (
				'id' => 4,
				'type' => 'Carefree',
			),
			4 => 
			array (
				'id' => 5,
				'type' => 'Casa de Francisco',
			),
			5 => 
			array (
				'id' => 6,
				'type' => 'Casa Estates',
			),
			6 => 
			array (
				'id' => 7,
				'type' => 'Cielo Grande',
			),
			7 => 
			array (
				'id' => 8,
				'type' => 'Coach Royal',
			),
			8 => 
			array (
				'id' => 9,
				'type' => 'Country Club',
			),
			9 => 
			array (
				'id' => 10,
				'type' => 'Country Meadows',
			),
			10 => 
			array (
				'id' => 11,
				'type' => 'Fountain East',
			),
			11 => 
			array (
				'id' => 12,
				'type' => 'Friendly Village of Aurora',
			),
			12 => 
			array (
				'id' => 13,
				'type' => 'Friendly Village of Orangewood',
			),
			13 => 
			array (
				'id' => 14,
				'type' => 'Friendly Village of the Rockies',
			),
			14 => 
			array (
				'id' => 15,
				'type' => 'Front Range',
			),
			15 => 
			array (
				'id' => 16,
				'type' => 'Grandview',
			),
			16 => 
			array (
				'id' => 17,
				'type' => 'Green Acres',
			),
			17 => 
			array (
				'id' => 18,
				'type' => 'Highland Meadows',
			),
			18 => 
			array (
				'id' => 19,
				'type' => 'Independence Woods',
			),
			19 => 
			array (
				'id' => 20,
				'type' => 'Kimberly Hills',
			),
			20 => 
			array (
				'id' => 21,
				'type' => 'King Arthur Draper',
			),
			21 => 
			array (
				'id' => 22,
				'type' => 'King Arthur WVC',
			),
			22 => 
			array (
				'id' => 23,
				'type' => 'Kings River',
			),
			23 => 
			array (
				'id' => 24,
				'type' => 'La Montana del Sur',
			),
			24 => 
			array (
				'id' => 25,
				'type' => 'Lake Haven Estates',
			),
			25 => 
			array (
				'id' => 26,
				'type' => 'Lakewood',
			),
			26 => 
			array (
				'id' => 27,
				'type' => 'Lamplighter Village',
			),
			27 => 
			array (
				'id' => 28,
				'type' => 'Majestic Meadows',
			),
			28 => 
			array (
				'id' => 29,
				'type' => 'Majestic Oaks',
			),
			29 => 
			array (
				'id' => 30,
				'type' => 'Mountain View',
			),
			30 => 
			array (
				'id' => 31,
				'type' => 'Oakland Estates',
			),
			31 => 
			array (
				'id' => 32,
				'type' => 'Pecan Lake',
			),
			32 => 
			array (
				'id' => 33,
				'type' => 'Pleasant Valley',
			),
			33 => 
			array (
				'id' => 34,
				'type' => 'Quail Run',
			),
			34 => 
			array (
				'id' => 35,
				'type' => 'Riverhaven',
			),
			35 => 
			array (
				'id' => 36,
				'type' => 'Riverwoods',
			),
			36 => 
			array (
				'id' => 37,
				'type' => 'Shadow Ridge',
			),
			37 => 
			array (
				'id' => 38,
				'type' => 'Sherwood Forest',
			),
			38 => 
			array (
				'id' => 39,
				'type' => 'Stoneridge',
			),
			39 => 
			array (
				'id' => 40,
				'type' => 'Sugarberry Place',
			),
			40 => 
			array (
				'id' => 41,
				'type' => 'Sunny Crest',
			),
			41 => 
			array (
				'id' => 42,
				'type' => 'The Cliffs',
			),
			42 => 
			array (
				'id' => 43,
				'type' => 'The Meadows',
			),
			43 => 
			array (
				'id' => 44,
				'type' => 'The Woods',
			),
			44 => 
			array (
				'id' => 45,
				'type' => 'Villa Cajon',
			),
			45 => 
			array (
				'id' => 46,
				'type' => 'Westcrest',
			),
			46 => 
			array (
				'id' => 47,
				'type' => 'Westwood Estates',
			),
			47 => 
			array (
				'id' => 48,
				'type' => 'Wintergreen',
			),
			48 => 
			array (
				'id' => 49,
				'type' => 'Wyoming Terrace',
			),
		));
	}

}
