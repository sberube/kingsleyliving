<?php

class UserMaritalStatusTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('user_marital_status')->truncate();
        
		\DB::table('user_marital_status')->insert(array (
			0 => 
			array (
				'marital_status' => 'Single',
			),
			1 => 
			array (
				'marital_status' => 'Married',
			),
		));
	}

}
