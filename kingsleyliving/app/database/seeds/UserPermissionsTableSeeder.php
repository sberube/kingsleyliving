<?php

class UserPermissionsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('user_permissions')->truncate();

		$admin = UserRole::where('name', 'like', 'rm_admin')->first();
		$acc_man = UserRole::where('name', 'like', 'account_manager')->first();

		$createCommunities = new UserPermission;
		$createCommunities->name = 'create_communities';
		$createCommunities->display_name = 'Create Communities';
		$createCommunities->save();

		$editCommunities = new UserPermission;
		$editCommunities->name = 'edit_communities';
		$editCommunities->display_name = 'Edit Communities';
		$editCommunities->save();

		$createSpace = new UserPermission;
		$createSpace->name = 'create_space';
		$createSpace->display_name = 'Create Space';
		$createSpace->save();

		$editSpace = new UserPermission;
		$editSpace->name = 'edit_space';
		$editSpace->display_name = 'Edit Space';
		$editSpace->save();

		$manageUsers = new UserPermission;
		$manageUsers->name = 'manage_users';
		$manageUsers->display_name = 'Manage Users';
		$manageUsers->save();

		$acc_man->permission_role()->sync(array($editCommunities->id, $editSpace->id));
	}

}
