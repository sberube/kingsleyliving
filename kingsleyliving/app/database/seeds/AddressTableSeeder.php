<?php

class AddressTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('address')->truncate();
        
		\DB::table('address')->insert(array (
			0 => 
			array (
				'id' => 1,
				'address_1' => '5314 N 250 W Ste 210',
				'address_2' => '',
				'city' => 'Provo',
				'state' => 'Utah',
				'postal_code' => '84604',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 1,
			),
			1 => 
			array (
				'id' => 2,
				'address_1' => '5314 N 250 W',
				'address_2' => '',
				'city' => 'Provo',
				'state' => 'Utah',
				'postal_code' => '84604',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 2,
			),
			2 => 
			array (
				'id' => 3,
				'address_1' => '507 E Spaulding',
				'address_2' => '',
				'city' => 'Lafayette',
				'state' => 'Colorado',
				'postal_code' => '80026',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 3,
			),
			3 => 
			array (
				'id' => 4,
				'address_1' => '1711 Roosevelt Way',
				'address_2' => '',
				'city' => 'Aurora',
				'state' => 'Colorado',
				'postal_code' => '80011',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 4,
			),
			4 => 
			array (
				'id' => 5,
				'address_1' => '2771 S 2670 W',
				'address_2' => '',
				'city' => 'West Valley City',
				'state' => 'Utah',
				'postal_code' => '84119',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 5,
			),
			5 => 
			array (
				'id' => 6,
				'address_1' => '1855 E Riverside Dr',
				'address_2' => '',
				'city' => 'Ontario',
				'state' => 'California',
				'postal_code' => '91761',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 6,
			),
			6 => 
			array (
				'id' => 7,
				'address_1' => '303 S Recker Rd',
				'address_2' => '',
				'city' => 'Mesa',
				'state' => 'Arizona',
				'postal_code' => '85206',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 7,
			),
			7 => 
			array (
				'id' => 8,
				'address_1' => '2885 E Midway Blvd',
				'address_2' => '',
				'city' => 'Denver',
				'state' => 'Colorado',
				'postal_code' => '80234',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 8,
			),
			8 => 
			array (
				'id' => 9,
				'address_1' => '1218 E Cleveland Ave',
				'address_2' => '',
				'city' => 'Madera',
				'state' => 'California',
				'postal_code' => '93638',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 9,
			),
			9 => 
			array (
				'id' => 10,
				'address_1' => '1201 S Harbor Blvd #304',
				'address_2' => '',
				'city' => 'Santa Ana',
				'state' => 'California',
				'postal_code' => '92704',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 10,
			),
			10 => 
			array (
				'id' => 11,
				'address_1' => '4956 N 300 W Ste 200',
				'address_2' => '',
				'city' => 'Provo',
				'state' => 'Utah',
				'postal_code' => '84604',
				'country_id' => 227,
				'type_id' => 1,
				'google_id' => 11,
			),
		));
	}

}
