<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('AddressCountriesTableSeeder');
		$this->call('AmenitiesTableSeeder');
		$this->call('AmenityClassesTableSeeder');
		$this->call('ApplicationResidentTypesTableSeeder');
		$this->call('ContactMeansTableSeeder');
		$this->call('ContactStatusTableSeeder');
		$this->call('ContactTypesTableSeeder');
		$this->call('HourTypesTableSeeder');
		$this->call('SpaceApplianceTypesTableSeeder');
		$this->call('SpaceStatusTableSeeder');
		$this->call('UserRolesTableSeeder');
		$this->call('UserMaritalStatusTableSeeder');
		$this->call('AddressTypesTableSeeder');
		$this->call('AddressTableSeeder');
		$this->call('AddressGoogleTableSeeder');
		$this->call('UserProfilesTableSeeder');
		$this->call('UserPermissionsTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('UserPvtAddressTableSeeder');
		$this->call('UserPvtRoleTableSeeder');
		$this->call('ResidentmapUsersPvtKingsleylivingUsersTableSeeder');
	}

}
