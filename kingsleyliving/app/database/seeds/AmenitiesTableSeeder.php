<?php

class AmenitiesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('amenities')->truncate();
        
		\DB::table('amenities')->insert(array (
			0 => 
			array (
				'name' => 'senior community',
				'description' => '55+ retirement community, move in with other seniors.',
				'class_id' => 1,
			),
			1 => 
			array (
				'name' => 'activities, groups and clubs',
				'description' => 'regular community activities and community clubs to participate in.',
				'class_id' => 2,
			),
			2 => 
			array (
				'name' => 'automatic sprinklers',
				'description' => 'your yard features a sprinkler system with a control box to set the times.',
				'class_id' => 4,
			),
			3 => 
			array (
				'name' => 'basketball court',
				'description' => 'community basketball court, play whenever you like, call main office for more details.',
				'class_id' => 2,
			),
			4 => 
			array (
				'name' => 'beauty salon',
				'description' => 'on-site salon services to help take care of your needs without needing to go somewhere else.',
				'class_id' => 2,
			),
			5 => 
			array (
				'name' => 'billiards room',
				'description' => 'play with your family or neighbors, call main office for hours.',
				'class_id' => 2,
			),
			6 => 
			array (
				'name' => 'close to public transportation',
				'description' => 'public transit is near entrance or exit to the community, so no need to drive to where you need to go.',
				'class_id' => 1,
			),
			7 => 
			array (
				'name' => 'clubhouse',
				'description' => 'community clubhouse for activities and socializing, call main office for other details such as reservations and other available options.',
				'class_id' => 2,
			),
			8 => 
			array (
				'name' => 'courtyard',
				'description' => 'beautiful community courtyard, and yet another place to enjoy and socialize with your community family.',
				'class_id' => 2,
			),
			9 => 
			array (
				'name' => 'exercise room',
				'description' => 'call main office for further details to see if previous customer is including some equipment in the purchase of the home.',
				'class_id' => 6,
			),
			10 => 
			array (
				'name' => 'fire pit',
				'description' => 'this house includes an outdoor barbeque or fire pit.',
				'class_id' => 4,
			),
			11 => 
			array (
				'name' => 'fishing pond',
				'description' => 'community fishing pond is available to the residents, call main office for more details.',
				'class_id' => 1,
			),
			12 => 
			array (
				'name' => 'fitness center',
				'description' => 'community fitness center open to all residents, well furnished with equipment.',
				'class_id' => 2,
			),
			13 => 
			array (
				'name' => 'game room',
				'description' => 'enjoy your favorites from card games to board games and more!',
				'class_id' => 2,
			),
			14 => 
			array (
				'name' => 'garden',
				'description' => 'there is a garden in the yard of this property.',
				'class_id' => 4,
			),
			15 => 
			array (
				'name' => 'gated entry',
				'description' => 'feel the safety of having complete privacy in a gated community.',
				'class_id' => 1,
			),
			16 => 
			array (
				'name' => 'golf course',
				'description' => 'this community has a private golf course open to all residents.',
				'class_id' => 1,
			),
			17 => 
			array (
				'name' => 'heated pool pond',
				'description' => 'community has a heated pool / pond for use by residents.',
				'class_id' => 2,
			),
			18 => 
			array (
				'name' => 'laundry',
				'description' => 'this community features on-site laundromat services.',
				'class_id' => 2,
			),
			19 => 
			array (
				'name' => 'swimming pool',
				'description' => 'let your kids swim in the pool or go swim a couple laps yourself, for hour or weather restrictions please contact the main office, or for more details.',
				'class_id' => 2,
			),
			20 => 
			array (
				'name' => 'steam room',
				'description' => 'relax and enjoy some time in our on-site steam room, included with the pool facilities.',
				'class_id' => 2,
			),
			21 => 
			array (
				'name' => 'library',
				'description' => 'this community has its own library, call main office for more details such as how to check out books or book returning policies.',
				'class_id' => 2,
			),
			22 => 
			array (
				'name' => 'on-site management',
				'description' => 'to provide you with the best possible service, our on-site management will assist you with whatever they can.',
				'class_id' => 2,
			),
			23 => 
			array (
				'name' => 'pets allowed',
				'description' => 'call main office to see if there are any breed restrictions and for further details.',
				'class_id' => 5,
			),
			24 => 
			array (
				'name' => 'picnic area',
				'description' => 'this community offers well maintained picnic areas for residents to enjoy.',
				'class_id' => 2,
			),
			25 => 
			array (
				'name' => 'ping pong',
				'description' => 'enjoy a game or two against your family or neighbors.',
				'class_id' => 2,
			),
			26 => 
			array (
				'name' => 'playground',
				'description' => 'let your kids have fun outside in our community playground, your kids can make new friends and you can enjoy some peace of mind.',
				'class_id' => 2,
			),
			27 => 
			array (
				'name' => 'rv parking',
				'description' => 'we have room for your RV here too, no need to store it somewhere else.',
				'class_id' => 1,
			),
			28 => 
			array (
				'name' => 'sauna',
				'description' => 'the perfect way to relax, enjoy the sauna available for residents.',
				'class_id' => 2,
			),
			29 => 
			array (
				'name' => 'shuffleboard',
				'description' => 'tables or courts, call main office for more details.',
				'class_id' => 2,
			),
			30 => 
			array (
				'name' => 'small pets welcome',
				'description' => 'call main office for any breed restrictions or for further details.',
				'class_id' => 5,
			),
			31 => 
			array (
				'name' => 'spa',
				'description' => 'save on gas and still pamper yourself with on-site spa services.',
				'class_id' => 2,
			),
			32 => 
			array (
				'name' => 'tennis court',
				'description' => 'on-site tennis courts available, call main office for possible equipment rentals',
				'class_id' => 2,
			),
			33 => 
			array (
				'name' => 'air conditioning',
				'description' => 'this home is equipped with central heating and air.',
				'class_id' => 5,
			),
			34 => 
			array (
				'name' => 'ceiling fans',
				'description' => 'this home has several energy efficient ceiling fans to help keep you cool.',
				'class_id' => 12,
			),
			35 => 
			array (
				'name' => 'deck (or patio)',
				'description' => 'this home has an added on deck or patio for your convenience.',
				'class_id' => 4,
			),
			36 => 
			array (
				'name' => 'dishwasher',
				'description' => 'kitchen has a dishwasher.',
				'class_id' => 8,
			),
			37 => 
			array (
				'name' => 'dryer',
				'description' => 'this home has an included dryer unit.',
				'class_id' => 5,
			),
			38 => 
			array (
				'name' => 'fireplace',
				'description' => 'this home has an electric or gas fireplace, wood burning also a possibility, call main office for more details.',
				'class_id' => 6,
			),
			39 => 
			array (
				'name' => 'garbage disposal',
				'description' => 'this home has garbage disposal.',
				'class_id' => 8,
			),
			40 => 
			array (
				'name' => 'hardwood floors',
				'description' => 'this home has hardwood floors, call main office for details regarding which areas have hardwood flooring.',
				'class_id' => 7,
			),
			41 => 
			array (
				'name' => 'island counter',
				'description' => 'kitchen has an island counter, to help your divide your work or serve more guests.',
				'class_id' => 8,
			),
			42 => 
			array (
				'name' => 'microwave',
				'description' => 'kitchen has microwave appliance.',
				'class_id' => 8,
			),
			43 => 
			array (
				'name' => 'oven',
				'description' => 'kitchen features oven appliance.',
				'class_id' => 8,
			),
			44 => 
			array (
				'name' => 'programmable thermostats',
				'description' => 'this home has energy efficient and programmable thermostats, never let your home get too hot or too cold.',
				'class_id' => 12,
			),
			45 => 
			array (
				'name' => 'range',
				'description' => 'this kitchen has a stove top / range.',
				'class_id' => 8,
			),
			46 => 
			array (
				'name' => 'recessed lighting',
				'description' => 'kitchen cupboards and drawers feature recessed lighting, to help you see what you need to see.',
				'class_id' => 8,
			),
			47 => 
			array (
				'name' => 'refrigerator',
				'description' => 'kitchen has refrigerator appliance.',
				'class_id' => 8,
			),
			48 => 
			array (
				'name' => 'shed',
				'description' => 'this home comes with an outdoor shed for storing tools or yard equipment.',
				'class_id' => 4,
			),
			49 => 
			array (
				'name' => 'storm windows',
				'description' => 'heavy duty windows to withstand the storms and help protect your family.',
				'class_id' => 12,
			),
			50 => 
			array (
				'name' => 'trash compactor',
				'description' => 'kitchen features a trash compactor to help you save space.',
				'class_id' => 8,
			),
			51 => 
			array (
				'name' => 'walk-in closet',
				'description' => 'master bedroom features walk-in closets that you would expect in full homes.',
				'class_id' => 10,
			),
			52 => 
			array (
				'name' => 'washer',
				'description' => 'this home has a washing machine for your clothing, no need to go to the local laundromat.',
				'class_id' => 5,
			),
			53 => 
			array (
				'name' => 'wet bar',
				'description' => 'this home features a wet bar for all your beverage needs.',
				'class_id' => 10,
			),
			54 => 
			array (
				'name' => 'window shutters',
				'description' => 'shutters on most windows to help keep your home at a nice temperature all year round.',
				'class_id' => 12,
			),
		));
	}

}
