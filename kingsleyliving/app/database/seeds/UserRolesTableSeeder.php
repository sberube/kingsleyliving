<?php

class UserRolesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('user_roles')->truncate();

		$admin = new UserRole;
		$admin->name = 'rm_admin';
		$admin->description = 'This user group has access and rights to view everything on the site, they can also edit everything.';
		$admin->admin = 1;
		$admin->save();

		$srdev = new UserRole;
		$srdev->id = 11;
		$srdev->name = 'senior_developer';
		$srdev->description = 'Code developer of senior rank.';
		$srdev->admin = 1;
		$srdev->save();

		$dev = new UserRole;
		$dev->id = 12;
		$dev->name = 'developer';
		$dev->description = 'Regular code developer, above junior developers.';
		$dev->admin = 1;
		$dev->save();

		$jrdev = new UserRole;
		$jrdev->id = 13;
		$jrdev->name = 'junior_developer';
		$jrdev->description = 'Code developer of junior rank.';
		$jrdev->admin = 1;
		$jrdev->save();

		$sracc = new UserRole;
		$sracc->id = 14;
		$sracc->name = 'senior_accounting';
		$sracc->description = 'Accountant of senior rank.';
		$sracc->admin = 0;
		$sracc->save();

		$acc = new UserRole;
		$acc->id = 15;
		$acc->name = 'accounting';
		$acc->description = 'Regular accountant, above junior accountants.';
		$acc->admin = 0;
		$acc->save();

		$jracc = new UserRole;
		$jracc->id = 16;
		$jracc->name = 'junior_accounting';
		$jracc->description = 'Accountant of junior rank.';
		$jracc->admin = 0;
		$jracc->save();

		$hr = new UserRole;
		$hr->id = 17;
		$hr->name = 'human_resources';
		$hr->description = 'HR and company policy compliance.';
		$hr->admin = 0;
		$hr->save();

		$uw = new UserRole;
		$uw->id = 18;
		$uw->name = 'underwriter';
		$uw->description = 'This group is for legal matters.';
		$uw->admin = 0;
		$uw->save();

		$in = new UserRole;
		$in->id = 19;
		$in->name = 'inventory';
		$in->description = 'Community or corporate inventory and product management.';
		$in->admin = 0;
		$in->save();

		$app = new UserRole;
		$app->id = 20;
		$app->name = 'app_user';
		$app->description = 'A user from an application.';
		$app->admin = 0;
		$app->save();

		$re = new UserRole;
		$re->id = 21;
		$re->name = 'receivables';
		$re->description = 'Generic description.';
		$re->admin = 0;
		$re->save();

		$pay = new UserRole;
		$pay->id = 22;
		$pay->name = 'payables';
		$pay->description = 'Generic description.';
		$pay->admin = 0;
		$pay->save();

		$titles = new UserRole;
		$titles->id = 23;
		$titles->name = 'titles';
		$titles->description = 'Generic description.';
		$titles->admin = 0;
		$titles->save();

		$util = new UserRole;
		$util->id = 24;
		$util->name = 'utilities';
		$util->description = 'Generic description.';
		$util->admin = 0;
		$util->save();

		$cs = new UserRole;
		$cs->id = 25;
		$cs->name = 'customer_service';
		$cs->description = 'Handling customer complaints and concerns.';
		$cs->admin = 0;
		$cs->save();

		$api = new UserRole;
		$api->id = 50;
		$api->name = 'api_user';
		$api->description = 'This group should enable resident map to add or edit records in kingsleyliving.';
		$api->admin = 0;
		$api->save();

		$acc_man = new UserRole;
		$acc_man->id = 51;
		$acc_man->name = 'account_manager';
		$acc_man->description = 'This user group has extended permissions to view and edit information, but not full access.';
		$acc_man->admin = 0;
		$acc_man->save();

		$prop_man = new UserRole;
		$prop_man->id = 61;
		$prop_man->name = 'property_manager';
		$prop_man->description = 'This group is for managers of KMC communities.';
		$prop_man->admin = 0;
		$prop_man->save();

		$assist_prop_man = new UserRole;
		$assist_prop_man->id = 62;
		$assist_prop_man->name = 'assistant_property_manager';
		$assist_prop_man->description = 'Assistant to the property manager';
		$assist_prop_man->admin = 0;
		$assist_prop_man->save();

		$prop_emp = new UserRole;
		$prop_emp->id = 63;
		$prop_emp->name = 'property_employee';
		$prop_emp->description = 'This group is for any other community employees that are not managers.';
		$prop_emp->admin = 0;
		$prop_emp->save();

		$cur_res = new UserRole;
		$cur_res->id = 71;
		$cur_res->name = 'current_resident';
		$cur_res->description = 'This group is for current residents of one of our communities.';
		$cur_res->admin = 0;
		$cur_res->save();

		$future_res = new UserRole;
		$future_res->id = 72;
		$future_res->name = 'future_resident';
		$future_res->description = 'This group is for those who are not yet residents and want to live in one of our communities.';
		$future_res->admin = 0;
		$future_res->save();

		$past_res = new UserRole;
		$past_res->id = 73;
		$past_res->name = 'past_resident';
		$past_res->description = 'This group is for those who have been residents and no longer live in one of our communities.';
		$past_res->admin = 0;
		$past_res->save();
	}

}
