<?php

class AmenityClassesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('amenity_classes')->truncate();
        
		\DB::table('amenity_classes')->insert(array (
			0 => 
			array (
				'class' => 'community - location',
			),
			1 => 
			array (
				'class' => 'community - perks',
			),
			2 => 
			array (
				'class' => 'home exterior - exterior',
			),
			3 => 
			array (
				'class' => 'home exterior - yard',
			),
			4 => 
			array (
				'class' => 'home interior - features',
			),
			5 => 
			array (
				'class' => 'home interior - rooms',
			),
			6 => 
			array (
				'class' => 'home interior - flooring',
			),
			7 => 
			array (
				'class' => 'home interior - kitchen',
			),
			8 => 
			array (
				'class' => 'home interior - dining',
			),
			9 => 
			array (
				'class' => 'home interior - master',
			),
			10 => 
			array (
				'class' => 'home interior - bath',
			),
			11 => 
			array (
				'class' => 'home interior - energy',
			),
			12 => 
			array (
				'class' => 'home interior - disability',
			),
		));
	}

}
