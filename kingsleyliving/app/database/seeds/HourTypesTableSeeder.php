<?php

class HourTypesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('hour_types')->truncate();
        
		\DB::table('hour_types')->insert(array (
			0 => 
			array (
				'type' => 'Weekdays',
			),
			1 => 
			array (
				'type' => 'Weekends',
			),
			2 => 
			array (
				'type' => 'Holidays',
			),
		));
	}

}
