<?php

class ContactMeansTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('contact_means')->truncate();
        
		\DB::table('contact_means')->insert(array (
			0 => 
			array (
				'means' => 'Phone (call)',
			),
			1 => 
			array (
				'means' => 'Phone (text)',
			),
			2 => 
			array (
				'means' => 'Email',
			),
			3 => 
			array (
				'means' => 'Old School (Post Mail)',
			),
		));
	}

}
