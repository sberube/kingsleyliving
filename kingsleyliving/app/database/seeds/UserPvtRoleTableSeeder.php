<?php

class UserPvtRoleTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('user_pvt_role')->truncate();
        
		\DB::table('user_pvt_role')->insert(array (
			0 => 
			array (
				'id' => 1,
				'user_id' => 1,
				'role_id' => 1,
			),
			1 => 
			array (
				'id' => 2,
				'user_id' => 2,
				'role_id' => 51,
			),
			2 => 
			array (
				'id' => 3,
				'user_id' => 3,
				'role_id' => 61,
			),
			3 => 
			array (
				'id' => 4,
				'user_id' => 4,
				'role_id' => 62,
			),
			4 => 
			array (
				'id' => 5,
				'user_id' => 5,
				'role_id' => 51,
			),
			5 => 
			array (
				'id' => 6,
				'user_id' => 6,
				'role_id' => 12,
			),
			6 => 
			array (
				'id' => 7,
				'user_id' => 7,
				'role_id' => 12,
			),
			7 => 
			array (
				'id' => 8,
				'user_id' => 8,
				'role_id' => 51,
			),
			8 => 
			array (
				'id' => 9,
				'user_id' => 9,
				'role_id' => 12,
			),
			9 => 
			array (
				'id' => 10,
				'user_id' => 10,
				'role_id' => 12,
			),
			10 => 
			array (
				'id' => 11,
				'user_id' => 11,
				'role_id' => 51,
			),
			11 => 
			array (
				'id' => 12,
				'user_id' => 12,
				'role_id' => 51,
			),
			12 => 
			array (
				'id' => 13,
				'user_id' => 13,
				'role_id' => 12,
			),
			13 => 
			array (
				'id' => 14,
				'user_id' => 14,
				'role_id' => 61,
			),
			14 => 
			array (
				'id' => 15,
				'user_id' => 15,
				'role_id' => 12,
			),
			15 => 
			array (
				'id' => 16,
				'user_id' => 16,
				'role_id' => 51,
			),
			16 => 
			array (
				'id' => 17,
				'user_id' => 17,
				'role_id' => 12,
			),
			17 => 
			array (
				'id' => 18,
				'user_id' => 18,
				'role_id' => 12,
			),
			18 => 
			array (
				'id' => 19,
				'user_id' => 19,
				'role_id' => 61,
			),
			19 => 
			array (
				'id' => 20,
				'user_id' => 20,
				'role_id' => 61,
			),
			20 => 
			array (
				'id' => 21,
				'user_id' => 21,
				'role_id' => 61,
			),
			21 => 
			array (
				'id' => 22,
				'user_id' => 22,
				'role_id' => 12,
			),
			22 => 
			array (
				'id' => 23,
				'user_id' => 23,
				'role_id' => 12,
			),
			23 => 
			array (
				'id' => 24,
				'user_id' => 24,
				'role_id' => 51,
			),
			24 => 
			array (
				'id' => 25,
				'user_id' => 25,
				'role_id' => 1,
			),
			25 => 
			array (
				'id' => 26,
				'user_id' => 26,
				'role_id' => 1,
			),
			26 => 
			array (
				'id' => 27,
				'user_id' => 27,
				'role_id' => 51,
			),
			27 => 
			array (
				'id' => 28,
				'user_id' => 28,
				'role_id' => 12,
			),
			28 => 
			array (
				'id' => 29,
				'user_id' => 29,
				'role_id' => 12,
			),
			29 => 
			array (
				'id' => 30,
				'user_id' => 30,
				'role_id' => 12,
			),
			30 => 
			array (
				'id' => 31,
				'user_id' => 31,
				'role_id' => 51,
			),
			31 => 
			array (
				'id' => 32,
				'user_id' => 32,
				'role_id' => 51,
			),
			32 => 
			array (
				'id' => 33,
				'user_id' => 33,
				'role_id' => 51,
			),
			33 => 
			array (
				'id' => 34,
				'user_id' => 34,
				'role_id' => 1,
			),
			34 => 
			array (
				'id' => 35,
				'user_id' => 35,
				'role_id' => 12,
			),
			35 => 
			array (
				'id' => 36,
				'user_id' => 36,
				'role_id' => 12,
			),
			36 => 
			array (
				'id' => 37,
				'user_id' => 37,
				'role_id' => 61,
			),
			37 => 
			array (
				'id' => 38,
				'user_id' => 38,
				'role_id' => 61,
			),
			38 => 
			array (
				'id' => 39,
				'user_id' => 39,
				'role_id' => 61,
			),
			39 => 
			array (
				'id' => 40,
				'user_id' => 40,
				'role_id' => 61,
			),
			40 => 
			array (
				'id' => 41,
				'user_id' => 41,
				'role_id' => 61,
			),
			41 => 
			array (
				'id' => 42,
				'user_id' => 42,
				'role_id' => 61,
			),
			42 => 
			array (
				'id' => 43,
				'user_id' => 43,
				'role_id' => 61,
			),
			43 => 
			array (
				'id' => 44,
				'user_id' => 44,
				'role_id' => 61,
			),
			44 => 
			array (
				'id' => 45,
				'user_id' => 45,
				'role_id' => 61,
			),
			45 => 
			array (
				'id' => 46,
				'user_id' => 46,
				'role_id' => 61,
			),
			46 => 
			array (
				'id' => 47,
				'user_id' => 47,
				'role_id' => 61,
			),
			47 => 
			array (
				'id' => 48,
				'user_id' => 48,
				'role_id' => 61,
			),
			48 => 
			array (
				'id' => 49,
				'user_id' => 49,
				'role_id' => 61,
			),
			49 => 
			array (
				'id' => 50,
				'user_id' => 50,
				'role_id' => 61,
			),
			50 => 
			array (
				'id' => 51,
				'user_id' => 51,
				'role_id' => 61,
			),
			51 => 
			array (
				'id' => 52,
				'user_id' => 52,
				'role_id' => 61,
			),
			52 => 
			array (
				'id' => 53,
				'user_id' => 53,
				'role_id' => 61,
			),
			53 => 
			array (
				'id' => 54,
				'user_id' => 54,
				'role_id' => 61,
			),
			54 => 
			array (
				'id' => 55,
				'user_id' => 55,
				'role_id' => 61,
			),
			55 => 
			array (
				'id' => 56,
				'user_id' => 56,
				'role_id' => 61,
			),
			56 => 
			array (
				'id' => 57,
				'user_id' => 57,
				'role_id' => 61,
			),
			57 => 
			array (
				'id' => 58,
				'user_id' => 58,
				'role_id' => 61,
			),
			58 => 
			array (
				'id' => 59,
				'user_id' => 59,
				'role_id' => 61,
			),
			59 => 
			array (
				'id' => 60,
				'user_id' => 60,
				'role_id' => 61,
			),
			60 => 
			array (
				'id' => 61,
				'user_id' => 61,
				'role_id' => 61,
			),
			61 => 
			array (
				'id' => 62,
				'user_id' => 62,
				'role_id' => 61,
			),
			62 => 
			array (
				'id' => 63,
				'user_id' => 63,
				'role_id' => 61,
			),
			63 => 
			array (
				'id' => 64,
				'user_id' => 64,
				'role_id' => 61,
			),
			64 => 
			array (
				'id' => 65,
				'user_id' => 65,
				'role_id' => 61,
			),
			65 => 
			array (
				'id' => 66,
				'user_id' => 66,
				'role_id' => 61,
			),
			66 => 
			array (
				'id' => 67,
				'user_id' => 67,
				'role_id' => 61,
			),
			67 => 
			array (
				'id' => 68,
				'user_id' => 68,
				'role_id' => 61,
			),
			68 => 
			array (
				'id' => 69,
				'user_id' => 69,
				'role_id' => 61,
			),
			69 => 
			array (
				'id' => 70,
				'user_id' => 70,
				'role_id' => 61,
			),
			70 => 
			array (
				'id' => 71,
				'user_id' => 71,
				'role_id' => 61,
			),
			71 => 
			array (
				'id' => 72,
				'user_id' => 72,
				'role_id' => 61,
			),
			72 => 
			array (
				'id' => 73,
				'user_id' => 73,
				'role_id' => 61,
			),
			73 => 
			array (
				'id' => 74,
				'user_id' => 74,
				'role_id' => 61,
			),
			74 => 
			array (
				'id' => 75,
				'user_id' => 75,
				'role_id' => 61,
			),
			75 => 
			array (
				'id' => 76,
				'user_id' => 76,
				'role_id' => 61,
			),
			76 => 
			array (
				'id' => 77,
				'user_id' => 77,
				'role_id' => 51,
			),
			77 => 
			array (
				'id' => 78,
				'user_id' => 78,
				'role_id' => 12,
			),
		));
	}

}
