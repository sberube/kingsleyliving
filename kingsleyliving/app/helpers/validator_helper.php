<?php

Validator::extend('greater_than', function($attribute, $value, $parameters) {
	if (!is_numeric($parameters[0]))
	{
		$other = Input::get($parameters[0]);
	}
	else
	{
		$other = $parameters[0];
	}
	return isset($other) and intval($value) > intval($other);
});

Validator::extend('less_than', function($attribute, $value, $parameters) {
	if (!is_numeric($parameters[0]))
	{
		$other = Input::get($parameters[0]);
	}
	else
	{
		$other = $parameters[0];
	}
	return isset($other) and intval($value) < intval($other);
});