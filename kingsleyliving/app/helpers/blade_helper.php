<?php

Blade::extend(function($value) {
    return preg_replace('/\{\>(.+)\<\}/', '<?php ${1} ?>', $value);
});

Blade::extend(function($view, $compiler)
{
    $pattern = $compiler->createPlainMatcher('break');

    return preg_replace($pattern, '$1<?php break; ?>$2', $view);
});

Blade::extend(function($view, $compiler)
{
    $pattern = $compiler->createPlainMatcher('continue');

    return preg_replace($pattern, '$1<?php continue; ?>$2', $view);
});

Blade::extend(function($view, $compiler)
{
    $pattern = $compiler->createMatcher('datetime');

    return preg_replace($pattern, '$1<?php echo $2->format(\'m/d/Y H:i\'); ?>', $view);
});

// Blade::extend(function($view, $compiler)
// {
// 	$view = preg_replace('/(?<=\s)@switch\((.*)\)(\s*)@case\((.*)\)(?=\s)/', '$1<?php switch($2):$3case $4: ?', $view);
//     $view = preg_replace('/(?<=\s)@endswitch(?=\s)/', '<?php endswitch; ?', $view);

//     $view = preg_replace('/(?<=\s)@case\((.*)\)(?=\s)/', '<?php case $1: ?', $view);
//     $view = preg_replace('/(?<=\s)@default(?=\s)/', '<?php default: ?', $view);

//     return $view;
// });