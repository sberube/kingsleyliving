$(document).ready(function() {
	if ($('div.site-errors').html().trim().length > 0) {
		$('.under-header-errors').removeClass('hidden');
	} else {
		$('.under-header-errors').addClass('hidden');
	}

	var latit = $('.community_latitude').text();
	var longit = $('.community_longitude').text();
	var myCenter = new google.maps.LatLng(latit,longit);
	map_initialize(myCenter);

	var centered = 0;
	$('.maps_nearby').on('click', function() {
		/* Trigger map resize event */
		setTimeout(function() {
			google.maps.event.trigger(map, 'resize');
			if (centered ==0) {
				map_initialize(myCenter);
				centered = 1;
			}
		}, 200);
	});

	$('.datepicker').datepicker({
		format: 'mm-dd-yyyy'
	});
});