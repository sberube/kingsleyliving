$(document).ready(function() {
	if ($('div.site-errors').html().trim().length > 0) {
		$('.under-header-errors').removeClass('hidden');
	} else {
		$('.under-header-errors').addClass('hidden');
	}

	if ( $('#toggle_comm_address').is(':checked') ) {
		$('.community_address').show();
	} else {
		$('.community_address').hide();
	}

	$('#toggle_comm_address').click(function() {
		if ( $(this).is(':checked') ) {
			$('.community_address').show();
		} else {
			$('.community_address').hide();
		}
	});

	if ( $('#toggle_comm_amenities').is(':checked') ) {
		$('.list_amenities').show();
	} else {
		$('.list_amenities').hide();
	}

	$('#toggle_comm_amenities').click(function() {
		if ( $(this).is(':checked') ) {
			$('.list_amenities').show();
		} else {
			$('.list_amenities').hide();
		}
	});

	// $('.jumbotron').css('background', 'url(/assets/example/...jpg)', 'no-repeat', 'center', 'center');

	// var amenitiesJSON = $('input:hidden[name="jsTooltips"]').val();
	// var amenities = $.parseJSON(amenitiesJSON);
	// $.each(amenities, function(key, value) {
		// code...$('#example').tooltip(options)
		// $('#'+value).tooltip({
		// 	'title' : value,
		// 	'placement' : 'bottom',
		// 	'trigger' : 'hover'
		// 	// 'selector' : '#'+key
		// });
		// $('#'+value).tooltip();
		// console.log('Element ID: ' + key);
		// console.log('Element ID: ' + value);
	// });

	var latit = $('.community_latitude').text();
	var longit = $('.community_longitude').text();
	var myCenter = new google.maps.LatLng(latit,longit);
	map_initialize(myCenter);
});