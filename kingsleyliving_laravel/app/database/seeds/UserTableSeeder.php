<?php
 
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('user')->delete();
 
        User::create(array(
            // 'email' => 'firstuser@test.com',
            'username' => 'firstuser',
            'password' => Hash::make('first_password'),
            'first_name' => 'Jane',
            'last_name' => 'Test',
            'phone' => '(801) 225-6677'
        ));
 
        User::create(array(
            // 'email' => 'seconduser@test.com',
            'username' => 'seconduser',
            'password' => Hash::make('second_password'),
            'first_name' => 'Jack',
            'last_name' => 'Sparrow',
            'phone' => '(801) 334-8799'
        ));
    }
 
}