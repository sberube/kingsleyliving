<div class="navbar-header">
	<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
		<span class="sr-only">Toggle Navigation</span>
		<span class="icon ion-navicon-round"></span>
	</button>
		{{ HTML::link('/about', 'KingsleyLiving.com') }}
		<!-- $kingsley_logo -->
</div>
<div class="navbar-collapse collapse">
	<ul class="nav navbar-left navbar-nav">
		<li class="active">
			{{ HTML::link('/', 'Home') }}
		</li>
		<li>
			<a href="#">Buy a Home</a>
		</li>
		<li>
			<a href="#">Rent a Home</a>
		</li>
		<li>
			<a href="#">Lease Options</a>
		</li>
		<li>
			<a href="#">Find a Home Site</a>
		</li>
	</ul>
	@if (Auth::check())
	<div class="navbar-form navbar-right sign-in">
		<div class="btn-group">
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				{{ $full_name }}&nbsp;<span class="caret"></span>
			</button>
			<div class="dropdown-menu">
				<div class="col-sm-12">
					<div class="col-sm-12">
						{{ HTML::link('/user/view/'.Auth::user()->id.'/account_settings', 'Account') }}
					</div>
					<div class="col-sm-12">
						{{ HTML::link('/user/view/'.Auth::user()->id.'/profile', 'Profile') }}
					</div>
					<div class="col-sm-12">
						{{ HTML::link('/user/logout', 'Log out') }}
					</div>
				</div>
			</div>
		</div>
	</div>
	@else
	{{ Form::open(array('url' => 'user/register_user', 'method' => 'post', 'class' => 'navbar-form navbar-right register_account', 'role' => 'register')) }}
		{{ Form::submit('Register', array('class' => 'btn btn-primary', 'name' => 'login_register')) }}
	{{ Form::close() }}
	{{ Form::open(array('url' => 'user/login', 'method' => 'post', 'class' => 'navbar-form navbar-right sign-in', 'role' => 'login')) }}
		<div class="form-group">
			{{ Form::text('login_identity', 'Email or Username', array('class' => 'form-control', 'id' => 'identity')) }}
		</div>
		<div class="form-group">
			{{ Form::password('login_password', array('class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password')) }}
		</div>
		{{ Form::submit('Log In', array('class' => 'btn btn-primary', 'name' => 'login_user')) }}
	{{ Form::close() }}
	@endif
</div>