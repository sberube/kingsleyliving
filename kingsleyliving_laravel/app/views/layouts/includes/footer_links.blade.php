<ul class="list-group footer-link-list">
	<li class="list-group-item">
		{{ HTML::link('/join_kingsley', 'Join Our Team') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Acquisitions') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Investors') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('/contact/contact_us', 'Contact Us') }}
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Buy a Home') }}
		<a href="#">Buy a Home</a>
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Rent a Home') }}
		<a href="#">Rent a Home</a>
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Lease Options') }}
		<a href="#">Lease Options</a>
	</li>
	<li class="list-group-item">
		{{ HTML::link('#', 'Find a Home Site') }}
		<a href="#">Find a Home Site</a>
	</li>
</ul>