<!-- <link rel="shortcut icon" href="<?php //echo base_url(); ?>assets/images/kmc_logo-20140819-favicon.ico" > -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>KingsleyLiving - {{ $title }}</title>
{{ HTML::style('js/chosen_v1.1.0/chosen.css') }}
{{ HTML::style('css/main.css') }}
{{ HTML::style('bootstrap-3.2.0/css/bootstrap.css') }}
{{ HTML::style('bootstrap-3.2.0/css/elusive-webfont.css') }}
{{ HTML::style('bootstrap-3.2.0/css/ionicons.min.css') }}
@foreach ($stylesheetLinks as $stylesheet)
	{{ $stylesheet }}
@endforeach
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
{{ HTML::script('js/jquery-2.1.1.js') }}
{{ HTML::script('bootstrap-3.2.0/js/bootstrap.min.js') }}
{{ HTML::script('js/jquery-ui-1.11.0.notheme/jquery-ui.js') }}
{{ HTML::script('js/chosen_v1.1.0/chosen.jquery.js') }}
{{ HTML::script('js/jquery-number/jquery.number.js') }}
{{ HTML::script('js/easySlidePanel/js/jquery.slidePanel.js') }}
{{ HTML::script('js/main.js') }}
@foreach ($jsLinks as $jsFile)
	{{ $jsFile }}
@endforeach