<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.includes.head', array('title' => $page_title, 'stylesheetLinks' => $page_stylesheets, 'jsLinks' => $page_scriptlinks))
	</head>
	<body>
		<!-- Site header -->
		<div class="navbar navbar-default navbar-fixed-top site-menu" role="navigation">
			<div class="container-fluid">
				@include('layouts.includes.header', array('full_name' => $user_name))
				<!-- ('kingsley_logo' => $kingsley_logo) -->
			</div>
		</div>
		<!-- Any site errors should show up here... ideally -->
		<div class="container-fluid under-header-errors">
			<div class="col-xs-12">
				<div class="container-fluid">
					<div class="col-xs-12">
						<div class="panel">
							<div class="panel-body site-errors site-errors-default" data-spy="affix">
								@yield('errors')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Main body content -->
		<div class="container-fluid main-body main-body-default">
			@yield('slideouts')
			<div class="body-content">
				{{{ $beforeBody_content or '' }}}
				<div class="main-page-content">
					@yield('content')
				</div>
			</div>
		</div>
		@include('layouts.includes.footer', array('latestNews' => $news, 'footerContact' => $footer_contact))
	</body>
</html>