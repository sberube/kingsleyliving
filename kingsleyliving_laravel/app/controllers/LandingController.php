<?php

class LandingController extends BaseController {

	// protected $layout = 'layouts.master';
	
	public function view()
	{
		$stylesheets = array();
		$stylesheets[] = HTML::style('css/landing/landing.css');
		$stylesheets[] = HTML::style('wow_slider/engine1/style.css');
		$javascript = array();
		$javascript[] = HTML::script('wow_slider/engine1/jquery.js');
		$javascript[] = HTML::script('js/raphael.js');
		$javascript[] = HTML::script('js/jqvmap-stable/jqvmap/jquery.vmap.js');
		$javascript[] = HTML::script('js/jqvmap-stable/jqvmap/maps/jquery.vmap.usa.js');
		$javascript[] = HTML::script('js/landing/landing.js');
		if (Auth::check()) {
			// code...
		} else {
			$user_name = '';
		}

		$map_data = array(
			'legend_style' => 'legend-vertical',
			'legend_title' => 'Communities'
		);

		$master_data = array(
			'page_title' => 'Home',
			'page_stylesheets' => $stylesheets,
			'page_scriptlinks' => $javascript,
			'user_name' => $user_name,
			'news' => array(),
			'footer_contact' => ''
		);

		// $this->layout->with('landing.home', $master_data);
		return View::make('landing.home', $master_data, array('legend_data' => $map_data));
	}
}