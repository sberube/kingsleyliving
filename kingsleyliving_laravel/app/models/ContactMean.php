<?php

class ContactMean extends Eloquent {
	protected $table = 'contact_mean';

	public function contact()
	{
		return $this->hasMany('contact');
	}
}