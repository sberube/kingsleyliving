<?php

class ApplicationPricing extends Eloquent {
	protected $table = 'application_pricing';

	public function application()
	{
		return $this->belongsTo('Application');
	}
}