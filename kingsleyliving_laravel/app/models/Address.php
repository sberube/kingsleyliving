<?php

class Address extends Eloquent {
	protected $table = 'address';

	public function country()
	{
		return $this->belongsTo('AddressCountry');
	}

	public function google()
	{
		return $this->belongsTo('AddressGoogle');
	}
}