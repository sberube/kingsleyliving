<?php

class AmenityClass extends Eloquent {
	protected $table = 'amenities_classes';

	public function amenity()
	{
		return $this->hasMany('Amenity');
	}
}