<?php

class UserGender extends Eloquent {
	protected $table = 'user_gender';

	public function user_profile()
	{
		return $this->hasMany('UserProfile');
	}
}