<?php

class SpaceAppliance extends Eloquent {
	protected $table = 'space_appliance';

	public function space()
	{
		return $this->belongsTo('Space');
	}

	public function appliance_type()
	{
		return $this->belongsTo('SpaceApplianceType');
	}
}