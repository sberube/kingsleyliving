<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
// use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	// use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	// protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function getAuthIdentifier()
	{
	    return $this->getKey();
	}

	public function getAuthPassword()
	{
	    return $this->password;
	}

	public function userprofile()
	{
		return $this->hasOne('UserProfile');
	}

	public function employment()
	{
		return $this->belongsToMany('Employment', 'user_employment_history', 'user_id', 'employment_id');
	}

	public function space()
	{
		return $this->belongsToMany('Space', 'user_space', 'user_id', 'space_id');
	}

	public function community()
	{
		return $this->belongsToMany('Community', 'user_community', 'user_id', 'community_id');
	}

}
