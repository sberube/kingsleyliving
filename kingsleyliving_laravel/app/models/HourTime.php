<?php

class HourTime extends Eloquent {
	protected $table = 'hour_times';

	public function hour()
	{
		return $this->hasMany('Hour');
	}
}