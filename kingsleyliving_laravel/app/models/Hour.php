<?php

class Hour extends Eloquent {
	protected $table = 'hour';

	public function type()
	{
		return $this->belongsTo('HourType');
	}

	public function days()
	{
		return $this->belongsTo('HourDay');
	}

	public function time()
	{
		return $this->belongsTo('HourTime');
	}
}