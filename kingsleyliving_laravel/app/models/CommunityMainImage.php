<?php

class CommunityMainImage extends Eloquent {
	protected $table = 'community_main_image';

	public function community()
	{
		return $this->belongsTo('Community');
	}

	public function image()
	{
		return $this->belongsTo('Image');
	}
}