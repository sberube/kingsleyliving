<?php

class HourDay extends Eloquent {
	protected $table = 'hour_days';

	public function hour()
	{
		return $this->hasMany('Hour');
	}
}