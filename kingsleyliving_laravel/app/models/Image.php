<?php

class Image extends Eloquent {
	protected $table = 'image';

	public function communities()
	{
		return $this->belongsToMany('Community', 'community_images', 'image_id', 'community_id');
	}

	public function user_profile()
	{
		return $this->hasMany('UserProfile');
	}

	public function community_main_image()
	{
		return $this->hasMany('CommunityMainImage');
	}
}