<?php

class Application extends Eloquent {
	protected $table = 'application';

	public function community()
	{
		return $this->belongsTo('Community');
	}

	public function pricing()
	{
		return $this->hasMany('ApplicationPricing');
	}

	public function resident()
	{
		return $this->hasMany('ApplicationResident');
	}

	public function resident_income()
	{
		return $this->hasMany('ApplicationResidentIncome');
	}

	public function source()
	{
		return $this->hasMany('ApplicationSource');
	}

	public function employment()
	{
		return $this->belongsToMany('Employment', 'application_employment_history', 'application_id', 'employment_id')->withPivot('user_id');
	}

	public function space()
	{
		return $this->belongsToMany('Space', 'application_space', 'application_id', 'space_id');
	}
}