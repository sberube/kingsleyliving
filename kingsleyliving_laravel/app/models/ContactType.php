<?php

class ContactType extends Eloquent {
	protected $table = 'contact_type';

	public function contact()
	{
		return $this->hasMany('contact');
	}
}