<?php

class SpacePricing extends Eloquent {
	protected $table = 'space_pricing';

	public function space()
	{
		return $this->belongsTo('Space');
	}
}