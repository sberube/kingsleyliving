<?php

class ApplicationResident extends Eloquent {
	protected $table = 'application_resident';

	public function application()
	{
		return $this->belongsTo('Application');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function resident_type()
	{
		return $this->belongsTo('ApplicationResidentType');
	}
}