<?php

class UserProfile extends Eloquent {
	protected $table = 'user_profile';

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function group()
	{
		return $this->belongsTo('UserGroup');
	}

	public function marital_status()
	{
		return $this->belongsTo('UserMaritalStatus');
	}

	public function gender()
	{
		return $this->belongsTo('UserGender');
	}

	public function image()
	{
		return $this->belongsTo('Image');
	}
}