<?php

class Space extends Eloquent {
	protected $table = 'space';

	public function community()
	{
		return $this->belongsTo('Community');
	}

	public function space_status()
	{
		return $this->belongsTo('SpaceStatus');
	}

	public function amenities()
	{
		return $this->belongsToMany('Amenity', 'space_amenities', 'space_id', 'amenity_id');
	}

	public function appliance()
	{
		return $this->hasMany('SpaceAppliance');
	}

	public function details()
	{
		return $this->hasOne('SpaceDetail');
	}

	public function flyer()
	{
		return $this->hasMany('SpaceFlyer');
	}

	public function images()
	{
		return $this->belongsToMany('Image', 'space_images', 'space_id', 'image_id');
	}

	public function pricing()
	{
		return $this->hasOne('SpacePricing');
	}
}