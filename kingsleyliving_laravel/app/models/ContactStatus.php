<?php

class ContactStatus extends Eloquent {
	protected $table = 'contact_status';

	public function contact()
	{
		return $this->hasMany('contact');
	}
}