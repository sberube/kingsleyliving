<?php

class Amenity extends Eloquent {
	protected $table = 'amenities';

	public function amen_class()
	{
		return $this->belongsTo('AmenityClass');
	}

	public function amen_icon()
	{
		return $this->hasMany('AmenityIcon');
	}

	public function community()
	{
		return $this->belongsToMany('Community', 'community_amenities', 'amenity_id', 'community_id');
	}

	public function space()
	{
		return $this->belongsToMany('Space', 'space_amenities', 'amenity_id', 'space_id');
	}
}