<?php

class UserGroup extends Eloquent {
	protected $table = 'user_groups';

	public function user_profile()
	{
		return $this->hasMany('UserProfile');
	}
}