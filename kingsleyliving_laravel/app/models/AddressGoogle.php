<?php

class AddressGoogle extends Eloquent {
	protected $table = 'address_google';

	public function address()
	{
		return $this->hasMany('Address');
	}
}