<?php

class Community extends Eloquent {
	protected $table = 'community';

	public function property_manager()
	{
		return $this->belongsTo('User');
	}

	public function assistant_property_manager()
	{
		return $this->belongsTo('User');
	}

	public function account_manager()
	{
		return $this->belongsTo('User');
	}

	public function amenities()
	{
		return $this->belongsToMany('Amenity', 'community_amenities', 'community_id', 'amenity_id');
	}

	public function hours()
	{
		return $this->belongsToMany('Hour', 'community_hours', 'community_id', 'hour_id');
	}

	public function images()
	{
		return $this->belongsToMany('Image', 'community_images', 'community_id', 'image_id');
	}

	public function main_image()
	{
		return $this->hasOne('CommunityMainImage');
	}

	public function space()
	{
		return $this->hasMany('Space');
	}

	public function read_community_by_id($comm_id)
	{
		// code...
	}
}