<?php

class ContactInterest extends Eloquent {
	protected $table = 'contact_interest';

	public function contact()
	{
		return $this->hasMany('contact');
	}
}