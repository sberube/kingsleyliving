<?php

class SpaceDetail extends Eloquent {
	protected $table = 'space_details';

	public function space()
	{
		return $this->belongsTo('Space');
	}
}