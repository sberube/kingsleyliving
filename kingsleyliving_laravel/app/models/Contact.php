<?php

class Contact extends Eloquent {
	protected $table = 'contact';

	public function contact_interest()
	{
		return $this->belongsTo('ContactInterest');
	}

	public function contact_means()
	{
		return $this->belongsTo('ContactMeans');
	}

	public function contact_status()
	{
		return $this->belongsTo('ContactStatus');
	}

	public function contact_type()
	{
		return $this->belongsTo('ContactType');
	}

	public function captcha()
	{
		return $this->hasMany('ContactCaptcha');
	}

	public function hours()
	{
		return $this->belongsToMany('Hour', 'contact_hours', 'contact_id', 'hour_id');
	}
}