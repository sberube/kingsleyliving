<?php

class ApplicationSource extends Eloquent {
	protected $table = 'application_source';

	public function application()
	{
		return $this->belongsTo('Application');
	}
}