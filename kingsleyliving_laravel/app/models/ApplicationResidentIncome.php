<?php

class ApplicationResidentIncome extends Eloquent {
	protected $table = 'application_resident_income';

	public function application()
	{
		return $this->belongsTo('Application');
	}
}