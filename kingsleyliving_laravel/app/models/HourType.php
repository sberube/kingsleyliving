<?php

class HourType extends Eloquent {
	protected $table = 'hour_type';

	public function hour()
	{
		return $this->hasMany('Hour');
	}
}