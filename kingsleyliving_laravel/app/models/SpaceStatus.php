<?php

class SpaceStatus extends Eloquent {
	protected $table = 'space_status';

	public function space()
	{
		return $this->belongsTo('Space');
	}
}