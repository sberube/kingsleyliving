<?php

class SpaceFlyer extends Eloquent {
	protected $table = 'space_flyer';

	public function space()
	{
		return $this->belongsTo('Space');
	}
}