<?php

class SpaceApplianceType extends Eloquent {
	protected $table = 'space_appliance_type';

	public function appliance()
	{
		return $this->hasMany('SpaceAppliance');
	}
}