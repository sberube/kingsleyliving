<?php

class AmenityIcon extends Eloquent {
	protected $table = 'amenities_icons';

	public function amenity()
	{
		return $this->belongsTo('Amenity');
	}
}