<?php

class Employment extends Eloquent {
	protected $table = 'employment';

	public function address()
	{
		return $this->hasOne('EmploymentAddress');
	}

	public function time()
	{
		return $this->hasOne('EmploymentTime');
	}
}