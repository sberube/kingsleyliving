<?php

class EmploymentTime extends Eloquent {
	protected $table = 'employment_time';

	public function employment()
	{
		return $this->belongsTo('Employment');
	}
}