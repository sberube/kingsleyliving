<?php

class AddressCountry extends Eloquent {
	protected $table = 'address_countries';

	public function address()
	{
		return $this->hasMany('Address');
	}
}