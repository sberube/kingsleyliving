<?php

class UserMaritalStatus extends Eloquent {
	protected $table = 'user_marital_status';

	public function user_profile()
	{
		return $this->hasMany('UserProfile');
	}
}