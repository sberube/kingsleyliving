<?php

class ApplicationResidentType extends Eloquent {
	protected $table = 'application_resident_type';

	public function resident()
	{
		return $this->belongsTo('ApplicationResident');
	}
}