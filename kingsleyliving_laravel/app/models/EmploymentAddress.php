<?php

class EmploymentAddress extends Eloquent {
	protected $table = 'employment_address';

	public function employment()
	{
		return $this->belongsTo('Employment');
	}
}